﻿<%@ WebHandler Language="C#" Class="fileremover" %>

using System;
using System.Web;

public class fileremover : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        context.Response.ClearContent();
        context.Response.Clear();
        context.Response.Write("{\"Result\":\"File removed successfully.\"}");
        
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
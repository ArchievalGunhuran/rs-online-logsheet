﻿<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

</script>

<html>
<head>
    <title></title>

    <link rel="icon" type="image/ico" href="../../resources/images/favicon.ico" />
    <link rel="shortcut icon" href="../../resources/images/favicon.ico" />

    <!-- Core CSS -->
    <link href="../../js/external/bootstrap 3.5/css/bootstrap.css" rel="stylesheet" />
    <link href="../../js/external/Material-Font/css/materialdesignicons.min.css" rel="stylesheet" />
    <link href="../../js/external/animation/css/animations.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common-material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap 3.5/css/sticky-footer.css" rel="stylesheet" />

    <!-- Editable CSS -->
    <%--<link type="text/css" rel="stylesheet" href="css/bootstraphack.css" />--%>
    <link type="text/css" rel="stylesheet" href="../../css/StyleSheet.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>

        <div id="example">
            <div class="box">
                <h4>Information</h4>
                <p>
                    The Upload is able to upload files out-of-band using the
                    HTML5 File API with fallback for legacy browsers.
                </p>
                <p>
                    You need to configure save action that will receive
                    the uploaded files.
                    An optional remove action is also available.
                </p>
            </div>

            <div>
                <div class="demo-section k-content">
                    <input name="files" id="files" type="file" />
                </div>
            </div>

            <script>
                $(document).ready(function () {
                    $("#files").kendoUpload({
                        async: {
                            saveUrl: "save",
                            removeUrl: "remove",
                            autoUpload: true
                        }
                    });
                });
            </script>
        </div>


            <script type="text/javascript" language="javascript" src="../../js/jquery-2.1.4.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/json2.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/bootstrap 3.5/js/bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/bootbox.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/bootstrap-datepicker/js/moment.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/standardstyler.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/default.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/kendo.all.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/AbigImage/abigimage.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/LeadProfile/leadprofile.js"></script>

</body>
</html>

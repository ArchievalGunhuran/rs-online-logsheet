﻿<%@ WebService Language="C#" Class="ActivityMaintenance" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ActivityMaintenance : System.Web.Services.WebService
{

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEmployeeList()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEmployeeList();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CheckCSAssignmentPermission(int EmployeeID)
    {
        Transactions.Accounts a = new Transactions.Accounts();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = a.hasCSAssignmentPermission(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityTypeList(string usergroup)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.getActivityTypeList(usergroup);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityMaintenance(DataModels.ActivityList Activity)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.getActivityMaintenance(Activity);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityMaintenanceTypes(DataModels.ActivityTypes Activity)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.getActivityMaintenanceTypes(Activity);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddActivityMaintenance(DataModels.ActivityList Activity)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.addActivityMaintenance(Activity);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddActivityMaintenanceTypes(DataModels.ActivityTypes Activity)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.addActivityMaintenanceTypes(Activity);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateActivityMaintenance(DataModels.ActivityList Activity)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.updateActivityMaintenance(Activity);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateActivityMaintenanceTypes(DataModels.ActivityTypes Activity)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.updateActivityMaintenanceTypes(Activity);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEmployeeAssignment(DataModels.ActivityLevelAssignment ActivityLevelAssignment)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.getEmployeeAssignment(ActivityLevelAssignment);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddEmployeeAssignment(DataModels.ActivityLevelAssignment ActivityLevelAssignment)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.addEmployeeAssignment(ActivityLevelAssignment);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateEmployeeAssignment(DataModels.ActivityLevelAssignment ActivityLevelAssignment)
    {
        Transactions.ActivityList transaction = new Transactions.ActivityList();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.updateEmployeeAssignment(ActivityLevelAssignment);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }


    #region ActivityLogsView

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityLogView(string ActivityDateFrom, string ActivityDateTo)
    {
        Transactions.ActivityLogs2 al = new Transactions.ActivityLogs2();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getActivityLogView(ActivityDateFrom, ActivityDateTo);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    #endregion

}
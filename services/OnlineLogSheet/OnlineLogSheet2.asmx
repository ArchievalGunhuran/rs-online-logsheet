﻿<%@ WebService Language="C#" Class="OnlineLogSheet2" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class OnlineLogSheet2 : System.Web.Services.WebService
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject IsUserValid(string UserName, string Password)
    {
        Transactions.Accounts a = new Transactions.Accounts();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = a.isUserValid(UserName, Password);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CheckOnlineLogSheetPermission(int EmployeeID)
    {
        Transactions.Accounts a = new Transactions.Accounts();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = a.hasOnlineLogSheetPermission(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityTypeList(string UserGroup, int EmployeeID, bool IsManual)
    {
        Transactions.ActivityLogs2 al = new Transactions.ActivityLogs2();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getActivityTypeList(UserGroup, EmployeeID, IsManual);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityList(int ActivityTypeID, int EmployeeID, bool IsManual)
    {
        Transactions.ActivityLogs2 al = new Transactions.ActivityLogs2();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getActivityList(ActivityTypeID, EmployeeID, IsManual);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEmployeeList()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEmployeeList();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityLog(int EmployeeID, string ActivityDate, bool IsManual)
    {
        Transactions.ActivityLogs2 al = new Transactions.ActivityLogs2();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getActivityLog(EmployeeID, ActivityDate, IsManual);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateActivityLog(int EmployeeID, string ActivityDate, Object ActivityLog, bool IsManual)
    {
        Transactions.ActivityLogs2 al = new Transactions.ActivityLogs2();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var logDetails = serializer.Serialize(ActivityLog);
            DataTable tblAuditDetails = (DataTable)JsonConvert.DeserializeObject(logDetails, (typeof(DataTable)));

            result.ResponseItem = al.updateActivityLog(EmployeeID, ActivityDate, tblAuditDetails, IsManual);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

        
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateActivityLog(int EmployeeID, string ActivityDate, Object ActivityLog, bool IsManual, Object ActivityLogAutoPause)
    {
        Transactions.ActivityLogs2 al = new Transactions.ActivityLogs2();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var logDetails = serializer.Serialize(ActivityLog);
            DataTable tblAuditDetails = (DataTable)JsonConvert.DeserializeObject(logDetails, (typeof(DataTable)));

            var logDetailsAutoPause = serializer.Serialize(ActivityLogAutoPause);
            DataTable tblAuditDetailsAutoPause = (DataTable)JsonConvert.DeserializeObject(logDetailsAutoPause, (typeof(DataTable)));

            result.ResponseItem = al.updateActivityLog(EmployeeID, ActivityDate, tblAuditDetails, IsManual);

            bool willAutoPause = al.autoOnPauseActivity(EmployeeID, ActivityDate, tblAuditDetails, tblAuditDetailsAutoPause);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateActivityLogManual(int EmployeeID, string ActivityDate, Object ActivityLog, bool IsManual, string UserGroup, string EmployeeName)
    {
        Transactions.ActivityLogs2 al = new Transactions.ActivityLogs2();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var logDetails = serializer.Serialize(ActivityLog);
            DataTable tblAuditDetails = (DataTable)JsonConvert.DeserializeObject(logDetails, (typeof(DataTable)));

            result.ResponseItem = al.updateActivityLogManual(EmployeeID, ActivityDate, tblAuditDetails, IsManual, UserGroup, EmployeeName);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteActivityLog(Object ActivityLog, int ActivityLogID)
    {
        Transactions.ActivityLogs2 al = new Transactions.ActivityLogs2();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var logDetails = serializer.Serialize(ActivityLog);
            DataTable tblAuditDetails = (DataTable)JsonConvert.DeserializeObject(logDetails, (typeof(DataTable)));
            result.ResponseItem = al.deleteActivityLog(tblAuditDetails, ActivityLogID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

}
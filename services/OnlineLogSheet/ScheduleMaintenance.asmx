﻿<%@ WebService Language="C#" Class="ScheduleMaintenance" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class ScheduleMaintenance : System.Web.Services.WebService
{

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEmployeeList()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEmployeeList();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CheckCSAssignmentPermission(int EmployeeID)
    {
        Transactions.Accounts a = new Transactions.Accounts();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = a.hasCSAssignmentPermission(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetScheduleMaintenance(DataModels.ScheduleMaintenance Schedule)
    {
        Transactions.ScheduleMaintenance transaction = new Transactions.ScheduleMaintenance();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.getScheduleMaintenance(Schedule);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddScheduleMaintenance(DataModels.ScheduleMaintenance Schedule)
    {
        Transactions.ScheduleMaintenance transaction = new Transactions.ScheduleMaintenance();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.addScheduleMaintenance(Schedule);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateScheduleMaintenance(DataModels.ScheduleMaintenance Schedule)
    {
        Transactions.ScheduleMaintenance transaction = new Transactions.ScheduleMaintenance();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.updateScheduleMaintenance(Schedule);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteScheduleMaintenance(int ScheduleID)
    {
        Transactions.ScheduleMaintenance transaction = new Transactions.ScheduleMaintenance();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = transaction.deleteScheduleMaintenance(ScheduleID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

}
﻿<%@ WebService Language="C#" Class="OnlineLogSheet" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class OnlineLogSheet : System.Web.Services.WebService
{
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject IsUserValid(string UserName, string Password)
    {
        Transactions.Accounts a = new Transactions.Accounts();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = a.isUserValid(UserName, Password);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CheckOnlineLogSheetPermission(int EmployeeID)
    {
        Transactions.Accounts a = new Transactions.Accounts();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = a.hasOnlineLogSheetPermission(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CheckEFSTechPermission(int EmployeeID)
    {
        Transactions.Accounts a = new Transactions.Accounts();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = a.hasEFSTechPermission(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CheckCSAssignmentPermission(int EmployeeID)
    {
        Transactions.Accounts a = new Transactions.Accounts();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = a.hasCSAssignmentPermission(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityList(int ActivityTypeID, int EmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getActivityList(ActivityTypeID, EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetItemList(int ActivityTypeID, int ListTypeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getItemList(ActivityTypeID, ListTypeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEmployeeList()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEmployeeList();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEmployeeListAdhoc()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEmployeeListAdhoc();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEmployeeListTS()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEmployeeListTS();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEmployeeListProject()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEmployeeListAdhoc();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetActivityLog(int ActivityTypeID, int EmployeeID, string ActivityDate)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            switch (ActivityTypeID)
            {
                case 1: result.ResponseItem = al.getAdminActivityLog(EmployeeID, ActivityDate); break;
                case 2: result.ResponseItem = al.getAlarmActivityLog(EmployeeID, ActivityDate); break;
                case 3: result.ResponseItem = al.getCSActivityLog(EmployeeID, ActivityDate); break;
                case 4: result.ResponseItem = al.getProjectActivityLog(EmployeeID, ActivityDate); break;
                case 5: result.ResponseItem = al.getALRActivityLog(EmployeeID, ActivityDate); break;
                case 6: result.ResponseItem = al.getSPMActivityLog(EmployeeID, ActivityDate); break;
                case 7: result.ResponseItem = al.getFQRActivityLog(EmployeeID, ActivityDate); break;
                case 8: result.ResponseItem = al.getEMTActivityLog(EmployeeID, ActivityDate); break;
                case 9: result.ResponseItem = al.getEFSActivityLog(EmployeeID, ActivityDate); break;
                case 10: result.ResponseItem = al.getESSActivityLog(EmployeeID, ActivityDate); break;
                case 11: result.ResponseItem = al.getAAActivityLog(EmployeeID, ActivityDate); break;
                case 12: result.ResponseItem = al.getTSActivityLog(EmployeeID, ActivityDate); break;
                case 13: result.ResponseItem = al.getAdminProductiveActivityLog(EmployeeID, ActivityDate); break;
                case 14: result.ResponseItem = al.getRGActivityLog(EmployeeID, ActivityDate); break;
                default: throw new Exception(ActivityTypeID + " is not a valid Activity Type ID");
            }
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetPendingActivity(int ActivityTypeID, int EmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            switch (ActivityTypeID)
            {
                case 7: result.ResponseItem = al.getFQRAPending(EmployeeID); break;
                //case 8: result.ResponseItem = al.getEMTPending(EmployeeID); break;
                case 9: result.ResponseItem = al.getEFSFollowUp(); break;
                default: throw new Exception("No pending items for Activity ID: " + ActivityTypeID);
            }
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetAssignedCS(int EmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getCSAssignedPerTech(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetAssignedAA(int EmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getAAAssignedPerTech(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetAssignedTS(int EmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getTSAssignedPerTech(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }


    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetAssignedEMT(int EmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEMTAssignedPerTech(EmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddAdminActivityLog(DataModels.LogDetailAdmin ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addAdminActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddAdminProductiveActivityLog(DataModels.LogDetailAdminProductive ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addAdminProductiveActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddProjectActivityLog(DataModels.LogDetailProject ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addProjectActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddCSActivityLog(DataModels.LogDetailCS ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addCSActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddAlarmActivityLog(DataModels.LogDetailAlarm ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addAlarmActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddALRActivityLog(DataModels.LogDetailALR ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addALRActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddSPMActivityLog(DataModels.LogDetailSPM ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addSPMActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddFQRActivityLog(DataModels.LogDetailFQR ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addFQRActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddEMTActivityLog(DataModels.LogDetailEMT ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addEMTActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddEFSActivityLog(DataModels.LogDetailEFS ActivityLog, DataModels.Accounts Account)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addEFSActivityLog(ActivityLog, Account);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddESSActivityLog(DataModels.LogDetailESS ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addESSActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddAAActivityLog(DataModels.LogDetailAdhocAnalytics ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addAAActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CompleteStatusAAActivityLog(DataModels.LogDetailAdhocAnalytics ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.completeStatusAAActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CompleteStatusTSActivityLog(DataModels.LogDetailToolSupport ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.completeStatusTSActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CompleteStatusProjectAssignment(DataModels.LogDetailProject ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.completeProjectAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddQAForAdhocAnalyticsAssignment(DataModels.LogDetailAdhocAnalytics ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addQAForAdhocAnalyticsAssignments(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddQAForTSAssignment(DataModels.LogDetailToolSupport ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addQAForTSAssignments(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddQAForProjectAssignment(DataModels.LogDetailProject ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addQAForProjectAssignments(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddQAForProjectAssignmentProd(DataModels.LogDetailProject ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addQAForProjectAssignmentsProd(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddTSActivityLog(DataModels.LogDetailToolSupport ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addTSActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddRGActivityLog(DataModels.LogDetailRG ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addRGActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    //UPDATE
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateAdminActivityLog(DataModels.LogDetailAdmin ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateAdminActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateAdminProductiveActivityLog(DataModels.LogDetailAdminProductive ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateAdminProductiveActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateProjectActivityLog(DataModels.LogDetailProject ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateProjectActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateCSActivityLog(DataModels.LogDetailCS ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateCSActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateAlarmActivityLog(DataModels.LogDetailAlarm ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateAlarmActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateALRActivityLog(DataModels.LogDetailALR ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateALRActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateSPMActivityLog(DataModels.LogDetailSPM ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateSPMActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateFQRActivityLog(DataModels.LogDetailFQR ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateFQRActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateFQRPending(DataModels.LogDetailFQR ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateFQRPending(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateEMTActivityLog(DataModels.LogDetailEMT ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateEMTActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateEMTPending(DataModels.LogDetailEMT ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateEMTPending(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateEFSActivityLog(DataModels.LogDetailEFS ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateEFSActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateEFSFollowUp(DataModels.LogDetailEFS ActivityLog, DataModels.Accounts Account)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateEFSFollowUp(ActivityLog, Account);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateESSActivityLog(DataModels.LogDetailESS ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateESSActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateAAActivityLog(DataModels.LogDetailAdhocAnalytics ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateAAActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateTSActivityLog(DataModels.LogDetailToolSupport ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateTSActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateRGActivityLog(DataModels.LogDetailRG ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateRGActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    //DELETE
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteActivityLog(int ActivityTypeID, int ActivityLogID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.deleteActivityLog(ActivityTypeID, ActivityLogID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    ///////////////////////////////// CS ASSIGNMENT ///////////////////////////////////////////

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetCSAssignment()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getCSAssignment();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddCSAssignment(DataModels.LogDetailCS ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addCSAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateCSAssignment(DataModels.LogDetailCS ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateCSAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteCSAssignment(int CSAssignmentID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.deleteCSAssignment(CSAssignmentID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }


    ///////////////////////////////// EMT ASSIGNMENT ///////////////////////////////////////////

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetEMTAssignment()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getEMTAssignment();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddEMTAssignment(DataModels.LogDetailEMT ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addEMTAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateEMTAssignment(DataModels.LogDetailEMT ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateEMTAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteEMTAssignment(int EMTAssignmentID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.deleteEMTAssignment(EMTAssignmentID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }


    ///////////////////////////////// ADHOC ANALYTICS ASSIGNMENT ///////////////////////////////////////////

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetAAAssignment()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getAAAssignment();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetAAAssignmentForQA(int qaByEmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getAAAssignmentForQA(qaByEmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetTSAssignmentForQA(int qaByEmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getTSAssignmentForQA(qaByEmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetProjectAssignmentForQA(int qaByEmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getProjectAssignmentForQA(qaByEmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetProjectAssignmentForQAProd(int qaByEmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getProjectAssignmentForQAProd(qaByEmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddAAAssignment(DataModels.LogDetailAdhocAnalytics ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addAAAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateAAAssignment(DataModels.LogDetailAdhocAnalytics ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateAAAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteAAAssignment(int AAAssignmentID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.deleteAAAssignment(AAAssignmentID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    ///////////////////////////////// TOOL SUPPORT ASSIGNMENT ///////////////////////////////////////////

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetTSAssignment()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getTSAssignment();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddTSAssignment(DataModels.LogDetailToolSupport ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addTSAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateTSAssignment(DataModels.LogDetailToolSupport ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateTSAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteTSAssignment(int TSAssignmentID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.deleteTSAssignment(TSAssignmentID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    ///////////////////////////////// Project Assignment ///////////////////////////////////////////

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetProjectAssignment()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getProjectAssignment();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddProjectAssignment(DataModels.LogDetailProject ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addProjectAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateProjectAssignment(DataModels.LogDetailProject ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateProjectAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteProjectAssignment(int LogDetailID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.deleteProjectAssignment(LogDetailID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }


    #region Daily Upload
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetDUAssignment()
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getDUAssignment();
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddDUAssignment(DataModels.LogDetailDailyUploads ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addDUAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject CompleteStatusDUActivityLog(DataModels.LogDetailDailyUploads ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.completeStatusDUActivityLog(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject UpdateDUAssignment(DataModels.LogDetailDailyUploads ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.updateDUAssignment(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject DeleteDUAssignment(int DUAssignmentID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.deleteDUAssignment(DUAssignmentID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GetDUAssignmentForQA(int qaByEmployeeID)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.getDUAssignmentForQA(qaByEmployeeID);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
        [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject AddQAForDUAssignment(DataModels.LogDetailDailyUploads ActivityLog)
    {
        Transactions.ActivityLogs al = new Transactions.ActivityLogs();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = al.addQAForDUAssignments(ActivityLog);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
    #endregion
}
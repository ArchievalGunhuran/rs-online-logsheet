﻿<%@ WebService Language="C#" Class="LogSheetReport" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class LogSheetReport  : System.Web.Services.WebService {

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GenerateReportPerStandardTime(int ActivityTypeID, int SelectedFY)
    {
        Transactions.Reporting report = new Transactions.Reporting();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = report.reportPerStandardTime(ActivityTypeID, SelectedFY);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GenerateReportPerVolume(int ActivityTypeID, int SelectedFY)
    {
        Transactions.Reporting report = new Transactions.Reporting();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = report.reportPerVolume(ActivityTypeID, SelectedFY);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GenerateReportPieDonutChart(int ActivityTypeID, int SelectedFY)
    {
        Transactions.Reporting report = new Transactions.Reporting();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = report.reportPieDonutChart(ActivityTypeID, SelectedFY);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GenerateReportUtilization(int SelectedFY)
    {
        Transactions.Reporting report = new Transactions.Reporting();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = report.reportUtilization(SelectedFY);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataModels.ResponseObject GenerateReportUtilizationPerEmployee(int SelectedCY, int SelectedMonth)
    {
        Transactions.Reporting report = new Transactions.Reporting();
        DataModels.ResponseObject result = new DataModels.ResponseObject();
        try
        {
            result.ResponseItem = report.reportUtilizationPerEmployee(SelectedCY, SelectedMonth);
        }
        catch (Exception error)
        {
            result.ErrorMessage = error.Message;
        }
        return result;
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="webtemplate.aspx.cs" Inherits="webtemplate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Web Template</title>
    <link rel="icon" type="image/ico" href="resources/images/favicon.ico" />
    <link rel="shortcut icon" href="resources/images/favicon.ico" />


    <link href="js/external/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="js/external/bootstrap/css/sticky-footer-navbar.css" rel="stylesheet" />

    <link href="css/default.css" rel="stylesheet" />
    <link href="css/bootstraphack.css" rel="stylesheet" />
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
   
    <form id="frmMain" runat="server">
        <asp:HiddenField ID="hfBaseURL" runat="server" />
        <asp:HiddenField ID="hfURLChunks" runat="server" />
    </form>
    
    <div id="wrap" style="font-family:Arial">
        <div id="CommonHeader"></div>

        <div class="navbar" role="navigation">
                <div class="container">
                    <div class="navbar-collapse collapse">
                        <div class="navbar-form navbar-right" role="search">
                            <div class="form-group">
                                <input type="text" id='txtSearch' class="form-control" placeholder="Search" />
                            </div>
                            <button type="button" class="btn btn-default" onclick="javascript:getProjects();" ><span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </div>
                </div>
            </div>


        <div class="container">
            <div class="row ">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12">
                            1st Row
                        </div>
                    </div> 
                    <div class="row">                           
                        <div class="col-lg-9" >
                            <input type="text" class="form-control" id="textboxId" />
                        </div>
                        <div class="col-lg-3">
                            <input type="button" class="btn btn-block btn-primary" id="buttonId" value="Save"  />
                        </div>
                    </div> 
                    <div class="row">&nbsp;</div>
                    <div class="row">
                        <div class="col-lg-12">
                            2nd Row
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="alert alert-info">
                                1st Column
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="alert alert-info">
                                2nd Column
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>

                <div class="row">
                    <div class="col-lg-12">
                        3rd Row
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="alert alert-info">
                            1st Column
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="alert alert-info">
                            2nd Column
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="alert alert-info">
                            3rd Column
                        </div>
                    </div>
                </div>

                    <div class="row">&nbsp;</div>

                <div class="row">
                    <div class="col-lg-12">
                    4th Row
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="alert alert-info">
                            1st Column
                        </div>
                    </div>
                    <div class="col-lg-3 ">
                        <div class="alert alert-info">
                            2nd Column
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="alert alert-info">
                            3rd Column
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="alert alert-info">
                            4th Column
                        </div>
                    </div>
                    </div>
                              
                </div>
                <div class="col-lg-4">
                    <div class="row-fluid">
                        1st Row
                    </div>
                    <div class="row-fluid">
                        2nd Row
                    </div>
                    <div class="row-fluid">
                        3rd Row
                    </div>
                    <div class="row-fluid">
                        4th Row
                    </div>
               </div>
            </div>   
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
      
        <div style="background-color:#ededec">
            <div class="container">
            <div class="row ">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="row-fluid text-center">
                        <img src="resources/images/flat_clock.png" class="img-responsive  center-block"  style="width:80%"/>
                        <h4 style="font-weight: bold;">Employee Daily Hours</h4>
                        <p style="text-align: justify;" >Summary of total hours per day of Employee from the first day up to the last day of the selected Month.</p>
                        <a href="#" class="btn btn-primary">View Details</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="row-fluid text-center">
                        <img src="resources/images/flat_graph.png" class="img-responsive  center-block"  style="width:80%"/>
                        <h4 style="font-weight: bold;">Project Breakdown</h4>
                        <p style="text-align: justify;">Breakdown of all the hours spent per project. Contains summary per activity and per employee</p>
                        <a href="#" class="btn btn-primary">View Details</a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="row-fluid text-center">
                        <img src="resources/images/flat_calendar.png" class="img-responsive  center-block"   style="width:80%"/>
                        <h4 class="text-center" style="font-weight: bold;">Estimate vs. Actual</h4>
                        <p style="text-align: justify;">Total actual hours spend for each project versus the estimated hours needed to complete the project.</p>
                        <a href="#" class="btn btn-primary">View Details</a>
                    </div>
                </div>
            </div>
        </div> 
            <br/>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
   
    </div>

   

    <div id="footer"></div>
    <div id="footerMobile"></div>

    <!-- Javascript Libraries -->
    <%--<script type="text/javascript" src="js/external/utilities/jquery.2.1.1.min.js"></script>--%>
    <script type="text/javascript" src="js/external/utilities/jquery.1.10.2.js"></script>
    <script type="text/javascript" src="js/external/json2.js"></script>
    <script type="text/javascript" src="js/external/bootstrap/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="js/ajaxfunctions/ajaxfunctions.js"></script>

    <script type="text/javascript" src="js/standardstyler.js"></script>
    <script type="text/javascript" src="js/default.js"></script>

    <script type="text/javascript">
        jQuery.noConflict();
    </script>

</body>
</html>

﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="undermaintenance.aspx.cs" Inherits="undermaintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Web Template</title>
    <link rel="icon" type="image/ico" href="resources/images/favicon.ico" />
    <link rel="shortcut icon" href="resources/images/favicon.ico" />


    <link href="js/external/bootstrap/css/bootstrap.css" rel="stylesheet" />
    <link href="js/external/bootstrap/css/sticky-footer-navbar.css" rel="stylesheet" />

    <link href="css/default.css" rel="stylesheet" />
    <link href="css/bootstraphack.css" rel="stylesheet" />
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
   
    <form id="frmMain" runat="server">
        <asp:HiddenField ID="hfBaseURL" runat="server" />
        <asp:HiddenField ID="hfURLChunks" runat="server" />
    </form>
    
    <div id="wrap" style="font-family:Arial">
        <div id="CommonHeader"></div>
        <br/>
        <br/>
        <br/>
        <div class="container">
            <div class="jumbotron">
              <h1>Under Maintenance</h1>
              <p>The site you're looking for is temporarily unavailable at this moment.</p>
            </div>
        </div>  
    </div>
    <div id="footer"></div>
    <div id="footerMobile"></div>

    <!-- Javascript Libraries -->
    <%--<script type="text/javascript" src="js/external/utilities/jquery.2.1.1.min.js"></script>--%>
    <script type="text/javascript" src="js/external/utilities/jquery.1.10.2.js"></script>
    <script type="text/javascript" src="js/external/json2.js"></script>
    <script type="text/javascript" src="js/external/bootstrap/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="js/ajaxfunctions/ajaxfunctions.js"></script>

    <script type="text/javascript" src="js/standardstyler.js"></script>
    <script type="text/javascript" src="js/default.js"></script>

    <script type="text/javascript">
        jQuery.noConflict();
    </script>

</body>
</html>
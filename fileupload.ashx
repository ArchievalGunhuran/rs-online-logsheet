﻿<%@ WebHandler Language="C#" Class="fileupload" %>

using System;
using System.Web;

public class fileupload : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/html";
        context.Response.ClearContent();
        context.Response.Clear();
        try
        {

            if (context.Request.Files.Count == 0)
            {
                context.Response.Write("{\"Error\":\"There is a problem with file you are uploading.\"}");
            }
            else
            {
                HttpPostedFile uploadedfile = context.Request.Files[0];
                //DateTime dt = DateTime.Now;
                //string sessionKey = "FILE" + dt.ToFileTime().ToString();


                string EmployeeID = context.Request["empid"];

                string Filename = uploadedfile.FileName;
                string Filetype = uploadedfile.ContentType;
                int Filesize = uploadedfile.ContentLength;


                if (Filetype != "image/png" && 
                    Filetype != "image/x-png" &&
                    Filetype != "image/gif" &&
                    Filetype != "image/jpg" &&
                    Filetype != "image/jpeg" &&
                    Filetype != "image/pjpeg")
                {
                    context.Response.ContentType = "text/html";
                    context.Response.Write("{\"Error\":\"The file must be in these formats: jpeg, gif, png.\"}");
                    return;
                }
                
                if (Filesize <= 5000000)
                {
                    string[] tmp1 = Filename.Split(".".ToCharArray());
                    string fileExtension = tmp1[tmp1.Length - 1];

                    string[] tmp = Filename.Split(@"\".ToCharArray());
                    Filename = tmp[tmp.Length - 1];
                    string finalSaveFilename = context.Server.MapPath("./") + "\\resources\\profilepics\\" + ((Filename.Substring(0, 5) != "ATTCH") ? EmployeeID + "." + fileExtension : Filename);

                    uploadedfile.SaveAs(finalSaveFilename);

                    string ret = "{\"Filename\":\"" + ((Filename.Substring(0, 5) != "ATTCH") ? EmployeeID + "." + fileExtension : Filename) + "\",\"Error\":\"\"}";
                    context.Response.Write(ret);
                }
                else
                {
                    context.Response.ContentType = "text/html";
                    context.Response.Write("{\"Error\":\"The file exceeded the 5MB limit.\"}");
                }
            }

        }
        catch (Exception ex)
        {
            context.Response.ContentType = "text/html";
            context.Response.Write("{\"Error\":\"A problem occured while uploading your image. Please remember that it must be on accepted format (jpeg, gif, png).\"}");

        }
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
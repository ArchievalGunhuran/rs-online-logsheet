﻿
var activityLogFieldsList = new Array();

var arrFQRActivityLogs;
var arrFQRPending;
var arrFQRActivityList;
var arrFQRStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var tempStartDateTime = new Date();

jQuery(document).ready(function () {

    jQuery("#txtFQRActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridFQR').data('kendoGrid').dataSource.read(); }
    });


    //GET TASK LIST FOR FQR
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_FQR, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrFQRActivityList = ro;
                if (arrFQRActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from FQR - Task field");
            } else {
                bootbox.alert("Web service error when calling FQR - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM FQR
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_FQR, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrFQRStatus = ro;
                    loadGridFQR();
                } else {
                    bootbox.alert("Web service error when calling FQR - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridFQR() {

        var dataSourceFQR = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                        , { ActivityTypeID: ActivityType_FQR, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtFQRActivityDate").val() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrFQRActivityLogs = ro;
                                // on success
                                e.success(arrFQRActivityLogs);
                            } else {
                                bootbox.alert("Web service error when calling GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_FQR;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.State = e.data.State;
                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.RequestedBy = e.data.RequestedBy;
                obj.RequestMade = e.data.RequestMade;
                obj.NotesComment = e.data.NotesComment;
                obj.StatusID = e.data.StatusID;
                obj.DateTimeSaved = new Date();
                //obj.TimeSpent = e.data.TimeSpent;
                obj.Numberoftransaction = e.data.Numberoftransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddFQRActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrFQRActivityLogs = ro;
                            // on success
                            arrFQRActivityLogs.push(e.data);
                            e.success(arrFQRActivityLogs);
                            jQuery('#gridFQR').data('kendoGrid').dataSource.read();
                            jQuery('#gridFQRPending').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling AddFQRActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_FQR;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.State = e.data.State;
                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.RequestedBy = e.data.RequestedBy;
                obj.RequestMade = e.data.RequestMade;
                obj.NotesComment = e.data.NotesComment;
                obj.StatusID = e.data.StatusID;
                //obj.DateTimeSaved = e.data.DateTimeSaved;
                obj.TimeSpent = e.data.TimeSpent;
                obj.Numberoftransaction = e.data.Numberoftransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateFQRActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrFQRActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrFQRActivityLogs);
                            jQuery('#gridFQR').data('kendoGrid').dataSource.read();
                            jQuery('#gridFQRPending').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling UpdateFQRActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_FQR;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridFQR').data('kendoGrid').dataSource.read();
                                    jQuery('#gridFQRPending').data('kendoGrid').dataSource.read();
                                } else {
                                    bootbox.alert("Web service error when calling FQR - DeleteActivityLog. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrFQRActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        State: { validation: { required: true, maxLength: 10} },
                        StoreName: { validation: { required: true, maxLength: 250} },
                        StoreNumber: { type: "number" },
                        DateTimeRequested: { type: "date", defaultValue: function (e) { return new Date(); } },
                        RequestedBy: { validation: { maxLength: 250} },
                        RequestMade: { validation: { maxLength: 250} },
                        NotesComment: { validation: { maxLength: 250} },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        DateTimeSaved: { type: "date", defaultValue: "", editable: false },
                        StatusID: { defaultValue: arrFQRStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", editable: false },
                        TimeSpentStr: { editable: false },
                        Numberoftransaction: { type: "number", validation: { min: 0} },
                        ClosedWithin24Hrs: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridFQR").kendoGrid({
            dataSource: dataSourceFQR,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "State", title: "State", width: "120px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "StoreNumber", title: "Store Number", format: "{0:000000}", width: "120px" },
                    { field: "ActivityID", title: "Type of Request", editor: fqrTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "RequestedBy", title: "Requested By", width: "250px" },
                    { field: "RequestMade", title: "Request Made", width: "250px" },
                    { field: "NotesComment", title: "Notes / Comment", width: "250px" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "StatusID", title: "Status", editor: fqrStatusDDEditor, width: "200px", template: "#=StatusDescription#" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "ClosedWithin24Hrs", title: "Closed within 24hrs", width: "120px" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "Numberoftransaction", title: "Number of Transaction", width: "120px" },
                    //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                    {command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridFQR').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridFQR').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridFQR').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline",
            edit: function (e) {
                if (!e.model.isNew()) {
                    var select = e.container.find('input[data-bind="value:StatusID"]').data('kendoDropDownList');
                    select.enable(false);
                }
            }

        });

        ///////////////////////////////////////////////// PENDING ITEMS /////////////////////////////////////////////////////

        var dataSourceFQRPending = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetPendingActivity"
                        , { ActivityTypeID: ActivityType_FQR, EmployeeID: Employee_ID }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrFQRPending = ro;
                                // on success
                                e.success(arrFQRPending);
                            } else {
                                bootbox.alert("Web service error when calling FQR - GetPendingActivity. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
                , update: function (e) {
                    var param = new Object();
                    var obj = new Object();
                    var status = new Object();

                    obj.ActivityTypeID = ActivityType_FQR;
                    obj.LogDetailID = e.data.LogDetailID;
                    obj.ActivityLogID = e.data.ActivityLogID;
                    obj.EmployeeID = Employee_ID;
                    obj.ActivityDate = tempStartDateTime;
                    obj.ActivityID = e.data.ActivityID;

                    obj.State = e.data.State;
                    obj.StoreName = e.data.StoreName;
                    obj.StoreNumber = e.data.StoreNumber;
                    obj.DateTimeRequested = e.data.DateTimeRequested;
                    obj.RequestedBy = e.data.RequestedBy;
                    obj.RequestMade = e.data.RequestMade;
                    obj.NotesComment = e.data.NotesComment;
                    obj.DateTimeSaved = new Date();
                    //obj.TimeSpent = e.data.TimeSpent;
                    obj.StatusID = e.data.StatusID;
                    obj.Numberoftransaction = e.data.Numberoftransaction;

                    param.ActivityLog = obj;

                    callAjaxRequestJSON("UpdateFQRPending"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrFQRPending = ro;
                                // on success
                                //arrAdminActivityLogs.push(e.data);
                                e.success(arrFQRPending);
                                jQuery('#gridFQR').data('kendoGrid').dataSource.read();
                                jQuery('#gridFQRPending').data('kendoGrid').dataSource.read();
                            } else {
                                bootbox.alert("Web service error when calling UpdateFQRPending. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
            } //transport
            ,
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },

            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrFQRActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        State: { validation: { required: true, maxLength: 10} },
                        StoreName: { validation: { required: true, maxLength: 250} },
                        StoreNumber: { type: "number" },
                        DateTimeRequested: { type: "date", defaultValue: function (e) { return new Date(); } },
                        RequestedBy: { validation: { maxLength: 250} },
                        RequestMade: { validation: { maxLength: 250} },
                        NotesComment: { validation: { maxLength: 250} },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        DateTimeSaved: { type: "date", defaultValue: "", editable: false },
                        StatusID: { defaultValue: arrFQRStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", editable: false },
                        TimeSpentStr: { editable: false },
                        Numberoftransaction: { type: "number", validation: { min: 0} },
                        ClosedWithin24Hrs: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridFQRPending").kendoGrid({
            dataSource: dataSourceFQRPending,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "State", title: "State", width: "120px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "StoreNumber", title: "Store Number", format: "{0:000000}", width: "120px" },
                    { field: "ActivityID", title: "Type of Request", editor: fqrTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "RequestedBy", title: "Requested By", width: "250px" },
                    { field: "RequestMade", title: "Request Made", width: "250px" },
                    { field: "NotesComment", title: "Notes / Comment", width: "250px" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "StatusID", title: "Status", editor: fqrStatusDDEditor, width: "200px", template: "#=StatusDescription#" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "ClosedWithin24Hrs", title: "Closed within 24hrs", width: "120px" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "Numberoftransaction", title: "Number of Transaction", width: "120px" },
                    { command: ["edit"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline",
            edit: function (e) {
                tempStartDateTime = new Date();
            },
            save: function (e) {
                e.model.dirty = true;
            }
        });

        jQuery("#gridFQRPending").on("click", ".k-grid-PendingItems", function () {
            jQuery('#gridFQRPending').data('kendoGrid').dataSource.read();
        });

        jQuery(".k-grid-toolbar", "#gridFQRPending").prepend("<h4>PENDING ITEMS</h4>");

    }

    function fqrTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrFQRActivityList
            });
    }

    function fqrStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrFQRStatus
            });
    }


});
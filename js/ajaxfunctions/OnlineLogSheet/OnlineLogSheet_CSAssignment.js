﻿
var activityLogFieldsList = new Array();

var arrCSAssignment;
var arrCSAActivityList;
var arrCSAStatusList;
var arrCSAEmployeeList;
var listCSAEmployeeID = new Array();

jQuery(document).ready(function () {


    callAjaxRequestJSON("GetEmployeeList"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrCSAEmployeeList = ro;
                if (arrCSAEmployeeList.length > 0) {
                    for (i = 0; i < arrCSAEmployeeList.length; i++) {
                        //alert(arrCSAEmployeeList[i].ListDescription);
                        listCSAEmployeeID.push(arrCSAEmployeeList[i].ListID);
                    }
                    getActivityList();
                }
                else
                    alert("No items available for CS Assignment Employee List");
            } else {
                alert("Web service error when calling CS Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );


    function getActivityList() {

        callAjaxRequestJSON("GetActivityList"
            , { ActivityTypeID: ActivityType_CS, EmployeeID: arrCSAEmployeeList[0].ListID }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrCSAActivityList = ro;
                    if (arrCSAActivityList.length > 0)
                        getStatusList();
                    else
                        alert("No items available for CS Assignment Activity List");

                } else {
                    alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function getStatusList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_CS, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrCSAStatusList = ro;
                    if (arrCSAStatusList.length > 0)
                        loadCSAssignment();
                    else
                        alert("No items available for CS Assignment Status List");

                } else {
                    alert("Web service error when calling CS Assignment getStatusList_GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadCSAssignment() {

        var dataSourceCSAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetCSAssignment"
                    , { ActivityTypeID: ActivityType_CS }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrCSAssignment = ro;
                            // on success
                            e.success(arrCSAssignment);
                        } else {
                            alert("Web service error when calling loadCSAssignment_GetCSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var employeeList = new Object();
                
                obj.EmployeeID = Employee_ID;
                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                //obj.ActivityID = e.data.ActivityID;
                obj.ActivityID = e.data.ActivityID;
                obj.TicketNumber = e.data.TicketNumber;
                obj.DueDate = e.data.DueDate;

                obj.AssignEmployeeID = e.data.AssignEmployeeID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddCSAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrCSAssignment = ro;
                            e.success(arrCSAssignment);
                            jQuery('#gridCSAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            alert("Web service error calling loadCSAssignment_AddCSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var employeeList = new Object();
                var statusList = new Object();

                obj.CSAssignmentID = e.data.CSAssignmentID;
                obj.EmployeeID = Employee_ID;
                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.ActivityID = e.data.ActivityID;
                obj.TicketNumber = e.data.TicketNumber;
                obj.DueDate = e.data.DueDate;

                obj.AssignEmployeeID = e.data.AssignEmployeeID;
                obj.StatusID = e.data.StatusID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateCSAssignment"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    arrCSAssignment = ro;
                                    e.success(arrCSAssignment);
                                    jQuery('#gridCSAssignment').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error calling loadCSAssignment_UpdateCSAssignment. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {

                var param = new Object();
                param.CSAssignmentID = e.data.CSAssignmentID;

                callAjaxRequestJSON("DeleteCSAssignment"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridCSAssignment').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error calling loadCSAssignment_DeleteCSAssignment. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "CSAssignmentID",
                    fields: {
                        CSAssignmentID: { editable: false, nullable: false },
                        AssignEmployeeID: { validation: { required: true }, defaultValue: arrCSAEmployeeList[0].ListID },
                        AssignEmployeeName: { editable: false },
                        StoreName: { validation: { maxLength: 250, required: true} },
                        StoreNumber: { validation: { maxLength: 6, required: true } },
                        ActivityID: { validation: { required: true }, defaultValue: arrCSAActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        TicketNumber: { validation: { maxLength: 6, required: true } },
                        DueDate: { type: "date" },
                        StatusID: { validation: { required: true }, defaultValue: arrCSAStatusList[0].ListID, editable: false },
                        StatusDescription: { editable: false },
                        DateCompleted: { type: "date", defaultValue: "", editable: false },
                        SLA: { editable: false }
                    }
                }
            }
        });

        var gridCSAssignment = jQuery("#gridCSAssignment").kendoGrid({
            dataSource: dataSourceCSAssignment,
            pageable: true,
            toolbar: ["create", "cancel"],
            filterable: true,
            columns: [
                    {
                        field: "AssignEmployeeID", title: "Processor", editor: employeeListTaskDDEditor, template: "#=AssignEmployeeName#", width: "250px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrCSAEmployeeList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            ,operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "StoreName", title: "Store Name", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "StoreNumber", title: "Store Number", format: "{0:000000}", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ActivityID", title: "Type Of Program", editor: programTypeTaskDDEditor, template: "#=ActivityDescription#", width: "250px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrCSAActivityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TicketNumber", title: "Ticket Number", format: "{0:000000}", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "DueDate", title: "Due Date", width: "150px", format: "{0:MM/dd/yyyy}", filterable: false },
                    {
                        field: "StatusID", title: "Status", editor: statusListTaskDDEditor, template: "#=StatusDescription#", width: "200px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrCSAStatusList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline"
        });
        
    }

    function programTypeTaskDDEditor(container, options) {
        programContainer = container;
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrCSAActivityList
            });

    }

    function employeeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrCSAEmployeeList
                , change: function (e) {

                    callAjaxRequestJSON("GetActivityList"
                        , { ActivityTypeID: ActivityType_CS, EmployeeID: this.value() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrCSAActivityList = ro;
                                if (arrCSAActivityList.length > 0) {
                                    programContainer.empty();
                                    var dd = jQuery("<input data-bind='value:ActivityID'/>")
                                        .appendTo(programContainer)
                                        .kendoDropDownList({
                                            dataTextField: "ListDescription"
                                            , dataValueField: "ListID"
                                            , change: function (e) {
                                                options.model.ActivityID = this.value();
                                            }
                                        }).data("kendoDropDownList");

                                    dd.setDataSource(arrCSAActivityList);
                                    options.model.ActivityID = dd.value();
                                }
                                else
                                    alert("No items available for CS Assignment Activity List");

                            } else {
                                alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                }
            });
    }

    function statusListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrCSAStatusList
            });
    }

});
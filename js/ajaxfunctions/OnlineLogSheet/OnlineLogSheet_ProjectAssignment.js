﻿
var arrProjectAssignment;
var arrProjectActivityList;
var arrProjectStatusList;
var arrProjectEmployeeList;
var arrProjectEmployeeListTS;

jQuery(document).ready(function () {
    var detailsTemplate;

    var wnd = jQuery("#wnd").kendoWindow({
        title: "Complete Project",
        height: 450,
        width: 650,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    detailsTemplate = kendo.template(jQuery("#template").html());

    function show(id, value) {
        document.getElementById(id).style.display = value ? 'block' : 'none';
    }

    callAjaxRequestJSON("GetEmployeeListProject"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrProjectEmployeeListTS = ro;
            } else {
                alert("Web service error when calling Project GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    callAjaxRequestJSON("GetEmployeeListAdhoc"
    , {}
    , function (response) {
        var data = response['d']
        var ro = data.ResponseItem;
        if (data.ErrorMessage == null) {
            arrProjectEmployeeList = ro;
            if (arrProjectEmployeeList.length > 0) {
                getActivityList();
            }
            else {
                alert("No items available for Project Employee List");
            }
        } else {
            alert("Web service error when calling Project GetEmployeeList. " + data.ErrorMessage);
        }
    }
    , ajaxError
);


    function getActivityList() {

        callAjaxRequestJSON("GetActivityList"
            , { ActivityTypeID: ActivityType_PROJECT, EmployeeID: arrProjectEmployeeList[0].ListID }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrProjectActivityList = ro;
                    getStatusList();
                } else {
                    alert("Web service error when calling Project getActivityList_GetActivityList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function getStatusList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_PROJECT, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrProjectStatusList = ro;
                    if (arrProjectStatusList.length > 0)
                        loadProjectAssignment();
                    else
                        alert("No items available for Project Priority List");

                } else {
                    alert("Web service error when calling Project - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadProjectAssignment() {
        var dataSourceTSAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetProjectAssignment"
                    , { ActivityTypeID: ActivityType_PROJECT }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrProjectAssignment = ro;
                            e.success(arrProjectAssignment);
                        } else {
                            alert("Web service error when calling loadProjectAssignment_GetProjectAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.EmployeeID = Employee_ID;
                obj.ActivityID = e.data.ActivityID;
                obj.ActivityTypeID = 4;
                obj.Remarks = e.data.Remarks;
                obj.Sponsor = e.data.Sponsor;
                obj.StartDate = e.data.StartDate;

                obj.TargetTestingDate = e.data.TargetTestingDate;
                obj.TestingDate = e.data.TestingDate;
                obj.JointTestingDate = e.data.JointTestingDate;
                obj.AcceptanceDate = e.data.AcceptanceDate;
                obj.TargetDate = e.data.TargetDate;

                param.ActivityLog = obj;
                show('loading', true);

                callAjaxRequestJSON("AddProjectAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            show('loading', false);
                            arrProjectAssignment = ro;
                            arrProjectAssignment.push(e.data);
                            e.success(arrProjectAssignment);
                            jQuery('#gridProjectAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            show('loading', false);
                            bootbox.alert("Web service error when calling Tool Support AddTSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityID = e.data.ActivityID;
                obj.Remarks = e.data.Remarks;
                obj.Sponsor = e.data.Sponsor;
                obj.StartDate = e.data.StartDate;

                obj.TargetTestingDate = e.data.TargetTestingDate;
                obj.TestingDate = e.data.TestingDate;
                obj.JointTestingDate = e.data.JointTestingDate;
                obj.AcceptanceDate = e.data.AcceptanceDate;
                obj.TargetDate = e.data.TargetDate;

                param.ActivityLog = obj;
                show('loading', true);

                callAjaxRequestJSON("UpdateProjectAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            show('loading', false);
                            arrProjectAssignment = ro;
                            arrProjectAssignment.push(e.data);
                            e.success(arrProjectAssignment);
                            jQuery('#gridProjectAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            show('loading', false);
                            bootbox.alert("Web service error when calling Tool Support AddTSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
            }
            , destroy: function (e) {

                var param = new Object();
                param.LogDetailID = e.data.LogDetailID;

                callAjaxRequestJSON("DeleteProjectAssignment"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridProjectAssignment').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error calling loadProjectAssignment_DeleteTSAssignment. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ProjectNumber: { editable: false },
                        ActivityID: { defaultValue: arrProjectActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        Remarks: { validation: { required: true, maxLength: 250 } },
                        Sponsor: { validation: { required: true, maxLength: 250 } },
                        StartDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        ActivityDate: {
                            type: "date", defaultValue: function (e) { return new Date(); }
                            , validation: {
                                validateDate: function (input) {
                                    if (input.attr("data-bind") == "value:ActivityDate") {
                                        input.attr("data-validateDate-msg", "ProcessDate can not be later than 3 days or greater than today");
                                        return checkdate(input.val());
                                    }
                                    return true;
                                }
                            }
                        },
                        StatusID: { editable: false },
                        StatusDescription: { editable: false },
                        TimeSpent: { editable: false },

                        TargetTestingDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        TestingDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        JointTestingDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        TargetDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        CompletionDate: { type: "date", editable: false },
                        AcceptanceDate: { type: "date", defaultValue: function (e) { return new Date(); } },

                        NoOfDaysDelayedST: { editable: false },
                        FirstPassTimelinessST: { editable: false },
                        ActionItemST: { editable: false },
                        WorkingST: { editable: false },
                        FirstPassAccuracyST: { editable: false },

                        NoOfDaysDelayedPR: { editable: false },
                        ProjectClosureTimelinessPR: { editable: false },
                        ActionItemPR: { editable: false },
                        WorkingPR: { editable: false },
                        ProjectClosureAccuracyPR: { editable: false },

                        FirstPass: { editable: false },
                        ProjectClosure: { editable: false },
                        TotalQAScore: { editable: false },

                        QAByEmployeeName: { editable: false },
                        TimeSpentOfQA: { editable: false },
                        TimeSpentOfQAProd: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridProjectAssignment").kendoGrid({
            dataSource: dataSourceTSAssignment,
            pageable: true,
            toolbar: ["create", "cancel"],
            filterable: true,
            columns: [
                    {
                        field: "ProjectNumber", title: "Project Number", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ActivityID", title: "Project Name", editor: aaTaskDDEditor, width: "250px", template: "#=ActivityDescription#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrProjectActivityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "Remarks", title: "Remarks", width: "120px", filterable: false },
                    { field: "Sponsor", title: "Sponsor", width: "120px", filterable: false },
                    { field: "StartDate", title: "Start Date", format: "{0:MM/dd/yyyy}", width: "250px", filterable: false },
                    //{ field: "ActivityDate", title: "Start Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "StatusID", title: "Status", editor: tsStatusDDEditor, template: "#=StatusDescription#", width: "200px" },
                    { field: "TimeSpent", title: "Time Spent (Hours)", format: "{0:0.00}", width: "250px" },
                    { field: "TargetTestingDate", title: "Target Testing Date", format: "{0:MM/dd/yyyy}", width: "250px", filterable: false },
                    { field: "TestingDate", title: "Testing Date", format: "{0:MM/dd/yyyy}", width: "250px", filterable: false },
                    { field: "JointTestingDate", title: "Joint Testing Date", format: "{0:MM/dd/yyyy}", width: "250px", filterable: false },
                    { field: "TargetDate", title: "Target Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "CompletionDate", title: "Completion Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "AcceptanceDate", title: "Acceptance Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },

                    { field: "NoOfDaysDelayedST", title: "No. of Days Delayed", width: "250px", filterable: false },
                    { field: "FirstPassTimelinessST", title: "First Pass Timeliness", width: "250px", filterable: false },
                    { field: "ActionItemST", title: "Action Item", width: "120px", filterable: false },
                    { field: "WorkingST", title: "Working", width: "120px", filterable: false },
                    { field: "FirstPassAccuracyST", title: "First Pass Accuracy", width: "250px", filterable: false },
                    { field: "TimeSpentOfQA", title: "Time Spent of QA (Staging)", width: "250px", filterable: false },

                    { field: "NoOfDaysDelayedPR", title: "No. of Days Delayed", width: "250px", filterable: false },
                    { field: "ProjectClosureTimelinessPR", title: "Project Closure Timeliness", width: "250px", filterable: false },
                    { field: "ActionItemPR", title: "Action Item", width: "120px", filterable: false },
                    { field: "WorkingPR", title: "Working", width: "120px", filterable: false },
                    { field: "ProjectClosureAccuracyPR", title: "Project Closure Accuracy", width: "250px", filterable: false },
                    { field: "TimeSpentOfQAProd", title: "Time Spent of QA (Production)", width: "250px", filterable: false },

                    { field: "FirstPass", title: "First Pass", width: "120px", filterable: false },
                    { field: "ProjectClosure", title: "Project Closure", width: "250px", filterable: false },
                    { field: "TotalQAScore", title: "Total QA Score", width: "120px", filterable: false },

                    { field: "QAByEmployeeName", title: "QA By", width: "250px", filterable: false },

                    { command: { text: "Complete", click: completeStatus }, title: "", width: "100px" },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "250px", filterable: false }

            ],
            editable: "inline"
        });

    }
    function completeStatus(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

        if (dataItem.StatusDescription == 'Completed') {
            jQuery.confirm({
                title: 'Project',
                content: 'This Record is already Completed.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else if (dataItem.StatusDescription == '') {
            jQuery.confirm({
                title: 'Project',
                content: 'This Record is not yet Saved in the Database!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else if (dataItem.StatusDescription == 'On-Going') {
            wnd.content(detailsTemplate(dataItem));
            jQuery("#dropDownQAList").kendoDropDownList({
                dataSource: arrProjectEmployeeListTS,
                dataTextField: "ListDescription",
                dataValueField: "ListID"
            });
            document.getElementById('dateCompletionDate').valueAsDate = new Date();
            wnd.center().open();
        }
        else {
            jQuery.confirm({
                title: 'Project',
                content: 'Invalid Status',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }

    }

    $(document).on('click', '#btnComplete', function (e) {
        e.preventDefault();
        var timeSpent = $('#txtHourSpent').val();
        var logdetailID = $('#logdetailID').val();
        var assignedPerson = $('#dropDownQAList').val();
        var dateCompletion = $('#dateCompletionDate').val();

        if (timeSpent != '') {
            wnd.close();
            tempStartDateTime = new Date();
            var param = new Object();
            var obj = new Object();
            var statusList = new Object();

            obj.LogDetailID = logdetailID;
            obj.TimeSpent = timeSpent * 60;
            obj.CompletionDate = dateCompletion;
            obj.QAByEmployeeID = assignedPerson;
            obj.StatusID = 6;


            param.ActivityLog = obj;

            //console.log(param.ActivityLog);
            show('loading', true);

            callAjaxRequestJSON("CompleteStatusProjectAssignment"
                , param
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        show('loading', false);
                        jQuery.confirm({
                            title: 'Project',
                            content: 'Database Successfully Modified.',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                close: function () {
                                    window.location.reload();
                                }
                            }
                        });
                    } else {
                        show('loading', false);
                        bootbox.alert("Web service error AddTSActivityLog. " + data.ErrorMessage);
                    }
                }
                , ajaxError
            );
        }
        else {
            jQuery.confirm({
                title: 'Project',
                content: 'Time Spent is Required!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
    });

    function aaTaskDDEditor(container, options) {
        programContainer = container;
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrProjectActivityList
            });

    }

    function employeeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrProjectEmployeeList
                , change: function (e) {

                    callAjaxRequestJSON("GetActivityList"
                        , { ActivityTypeID: ActivityType_PROJECT, EmployeeID: this.value() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrProjectActivityList = ro;
                                if (arrProjectActivityList.length > 0) {
                                    programContainer.empty();
                                    var dd = jQuery("<input data-bind='value:ActivityID'/>")
                                        .appendTo(programContainer)
                                        .kendoDropDownList({
                                            dataTextField: "ListDescription"
                                            , dataValueField: "ListID"
                                            , change: function (e) {
                                                options.model.ActivityID = this.value();
                                            }
                                        }).data("kendoDropDownList");

                                    dd.setDataSource(arrProjectActivityList);
                                    options.model.ActivityID = dd.value();
                                }
                                else
                                    alert("No items available for CS Assignment Activity List");

                            } else {
                                alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                }
            });
    }

    function tsStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrProjectStatusList
            });
    }

});
﻿
var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[2];
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {

    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/LogSheetReport.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function ajaxError(response, error) {
    alert("Request Error" + response.responseText);
}

function ajaxSuccess(response) {
    alert("success " + JSON.stringify(response));
}

function generateReport(activityTypeID, selectedFY) {
    SelectedActivityTypeID = activityTypeID;
    selectedFY = parseInt(selectedFY);


    switch (activityTypeID) {
        case "4":
        case "11":
            callAjaxRequestJSON("GenerateReportPieDonutChart", { ActivityTypeID: activityTypeID, SelectedFY: selectedFY }, loadReportPieDonutChart, ajaxError);
            break;
        case "13":
            callAjaxRequestJSON("GenerateReportUtilization", { SelectedFY: selectedFY }, loadStackedChart, ajaxError);
            //loadStackedChart();
            break;
        default:
            callAjaxRequestJSON("GenerateReportPerStandardTime", { ActivityTypeID: activityTypeID, SelectedFY: selectedFY }, loadReportPerStandardTime, ajaxError);
            callAjaxRequestJSON("GenerateReportPerVolume", { ActivityTypeID: activityTypeID, SelectedFY: selectedFY }, loadReportPerVolume, ajaxError);
            break;
    }
}

function generateReportPerEmployee(activityTypeID, selectedCY, selectedCMonth) {
    SelectedActivityTypeID = activityTypeID;
    selectedFY = parseInt(selectedCY);
    selectedCMonth = parseInt(selectedCMonth);

    switch (activityTypeID) {
        case "14":
            callAjaxRequestJSON("GenerateReportUtilizationPerEmployee", { SelectedCY: selectedCY, SelectedMonth: selectedCMonth }, loadStackedChartPerEmployee, ajaxError);
            break;
        default:
            callAjaxRequestJSON("GenerateReportPerStandardTime", { ActivityTypeID: activityTypeID, SelectedFY: selectedFY }, loadReportPerStandardTime, ajaxError);
            callAjaxRequestJSON("GenerateReportPerVolume", { ActivityTypeID: activityTypeID, SelectedFY: selectedFY }, loadReportPerVolume, ajaxError);
            break;
    }
}

function loadReportPerStandardTime(response) {
    var root = JSON.parse(JSON.stringify(response));
    var data = root["d"];
    if (data == null)
        data = root;
    var ro = data.ResponseItem;
    var dt = new Object();
    if (data.ErrorMessage == null) {
        var dhtml = "";
        if (ro.length < 1)
            return bootbox.alert("No record retrieved.");

        //alert(JSON.stringify(ro));

        var xAxisCategory = new Array();
        var dataholder = new Array();
        var barcolor = new Array();

        for (var i = 0; i < ro.length; i++) {
            xAxisCategory.push(ro[i].LabelDate);
            dataholder.push(parseInt(ro[i].ReportHours));
            if (ro[i].LabelDate.indexOf("FY") > -1)
                barcolor.push("#01A4D2");
            else if (ro[i].LabelDate.indexOf("Q") > -1)
                barcolor.push("#62BB46");
            else
                barcolor.push("#004B8D");
        }

        jQuery("#divChart1").highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: ActivityTypeList[SelectedActivityTypeID - 1] + ' TOTAL TIME (HOURS)'
            },
            xAxis: {
                categories: xAxisCategory
            },
            yAxis: {
                title: { text: "Time (Hours)" }
            },
            colors: barcolor,
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                }
            },
            series: [{
                name: 'ReportHours',
                data: dataholder,
                showInLegend: false
            }]
        });

    } else {
        bootbox.alert("Request Error on loadReportPerStandardTime : " + data.ErrorMessage);
    }
}


function loadReportPerVolume(response) {
    var root = JSON.parse(JSON.stringify(response));
    var data = root["d"];
    if (data == null)
        data = root;
    var ro = data.ResponseItem;
    var dt = new Object();
    if (data.ErrorMessage == null) {
        var dhtml = "";
        if (ro.length < 1)
            return bootbox.alert("No record retrieved.");

        var xAxisCategory = new Array();
        var dataholder = new Array();
        var barcolor = new Array();

        for (var i = 0; i < ro.length; i++) {
            xAxisCategory.push(ro[i].LabelDate);
            dataholder.push(parseInt(ro[i].ReportVolume));
            if (ro[i].LabelDate.indexOf("FY") > -1)
                barcolor.push("#01A4D2");
            else if (ro[i].LabelDate.indexOf("Q") > -1)
                barcolor.push("#62BB46");
            else
                barcolor.push("#004B8D");
        }

        jQuery("#divChart2").highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: ActivityTypeList[SelectedActivityTypeID - 1] + ' TOTAL VOLUME'
            },
            xAxis: {
                categories: xAxisCategory
            },
            yAxis: {
                title: { text: "Volume" }
            },
            colors: barcolor,
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                }
            },
            series: [{
                name: 'ReportVolume',
                data: dataholder,
                showInLegend: false
            }]
        });

    } else {
        bootbox.alert("Request Error on loadReportPerStandardTime : " + data.ErrorMessage);
    }
}

function loadStackedChart(response) {
    var root = JSON.parse(JSON.stringify(response));
    var data = root["d"];
    if (data == null)
        data = root;
    var ro = data.ResponseItem;

    if (data.ErrorMessage == null) {
        var dhtml = "";
        if (ro.length < 1)
            return bootbox.alert("No record retrieved.");

        var xAxisCategory = new Array();
        var dataholderProductivity = new Array();
        var productivity = 0;
        var arrayUtilizationHours = new Array();
        var arrayUtilizationPercent = new Array();
        var objSeries = new Object();
        var totalActualTime = 0;
        var daysPresent = 0;
        var hoursPresent = 0;
        var divisor = 0;

        var arrALRActualTime = new Array();
        var arrFQRActualTime = new Array();
        var arrSPMActualTime = new Array();
        var arrEMTActualTime = new Array();
        var arrESSActualTime = new Array();
        var arrAlarmActualTime = new Array();
        var arrProjectActualTime = new Array();
        var arrAdhocAnalyticsActualTime = new Array();
        var arrToolSupportActualTime = new Array();
        var arrTrainingActualTime = new Array();
        var arrNPTActualTime = new Array();
        var arrEFSStandardTime = new Array();
        var arrCSSStandardTime = new Array();
        var arrEFSActualTime = new Array();
        var arrCSSActualTime = new Array();

        var arrALRPercentage = new Array();
        var arrFQRPercentage = new Array();
        var arrSPMPercentage = new Array();
        var arrEMTPercentage = new Array();
        var arrESSPercentage = new Array();
        var arrAlarmPercentage = new Array();
        var arrProjectPercentage = new Array();
        var arrAdhocAnalyticsPercentage = new Array();
        var arrToolSupportPercentage = new Array();
        var arrTrainingPercentage = new Array();
        var arrNPTPercentage = new Array();
        var arrEFSPercentage = new Array();
        var arrCSSPercentage = new Array();

        var stackedcolor = ["#008B8B", "#DAA520", "#8A4B05", "#D31245", "#959797", "#00AA7E", "#87E5FF"
            , "#6E298D", "#F79428", "#FFCF22", "#00A4D2", "#62BB46", "#004B8D"];


        for (var i = 0; i < ro.length; i++) {
            daysPresent = parseInt(ro[i].ExpectedWorkingDays) - parseInt(ro[i].NumberOfLeaves);
            hoursPresent = daysPresent * 7.5;

            totalActualTime = parseFloat(ro[i].EFSActualTime)
                + parseFloat(ro[i].CSSActualTime)
                + parseFloat(ro[i].ALRActualTime)
                + parseFloat(ro[i].FQRActualTime)
                + parseFloat(ro[i].SPMActualTime)
                + parseFloat(ro[i].EMTActualTime)
                + parseFloat(ro[i].ESSActualTime)
                + parseFloat(ro[i].AlarmActualTime)
                + parseFloat(ro[i].ProjectActualTime)
                + parseFloat(ro[i].AdhocAnalyticsActualTime)
                + parseFloat(ro[i].ToolSupportActualTime)
                + parseFloat(ro[i].TrainingActualTime)
                + parseFloat(ro[i].NPTActualTime);

            divisor = hoursPresent;
            if (totalActualTime > hoursPresent)
                divisor = totalActualTime;

            arrALRActualTime.push(ro[i].ALRActualTime);
            arrFQRActualTime.push(ro[i].FQRActualTime);
            arrSPMActualTime.push(ro[i].SPMActualTime);
            arrEMTActualTime.push(ro[i].EMTActualTime);
            arrESSActualTime.push(ro[i].ESSActualTime);
            arrAlarmActualTime.push(ro[i].AlarmActualTime);
            arrProjectActualTime.push(ro[i].ProjectActualTime);
            arrAdhocAnalyticsActualTime.push(ro[i].AdhocAnalyticsActualTime);
            arrToolSupportActualTime.push(ro[i].ToolSupportActualTime);
            arrTrainingActualTime.push(ro[i].TrainingActualTime);
            arrNPTActualTime.push(ro[i].NPTActualTime);
            arrEFSActualTime.push(ro[i].EFSActualTime);
            arrCSSActualTime.push(ro[i].CSSActualTime);

            arrEFSStandardTime.push(ro[i].EFSStandardTime);
            arrCSSStandardTime.push(ro[i].CSSStandardTime);


            arrALRPercentage.push(parseFloat(((arrALRActualTime[i] / divisor) * 100).toFixed(2)));
            arrFQRPercentage.push(parseFloat(((arrFQRActualTime[i] / divisor) * 100).toFixed(2)));
            arrSPMPercentage.push(parseFloat(((arrSPMActualTime[i] / divisor) * 100).toFixed(2)));
            arrEMTPercentage.push(parseFloat(((arrEMTActualTime[i] / divisor) * 100).toFixed(2)));
            arrESSPercentage.push(parseFloat(((arrESSActualTime[i] / divisor) * 100).toFixed(2)));
            arrAlarmPercentage.push(parseFloat(((arrAlarmActualTime[i] / divisor) * 100).toFixed(2)));
            arrProjectPercentage.push(parseFloat(((arrProjectActualTime[i] / divisor) * 100).toFixed(2)));
            arrAdhocAnalyticsPercentage.push(parseFloat(((arrAdhocAnalyticsActualTime[i] / divisor) * 100).toFixed(2)));
            arrToolSupportPercentage.push(parseFloat(((arrToolSupportActualTime[i] / divisor) * 100).toFixed(2)));
            arrTrainingPercentage.push(parseFloat(((arrTrainingActualTime[i] / divisor) * 100).toFixed(2)));
            arrNPTPercentage.push(parseFloat(((arrNPTActualTime[i] / divisor) * 100).toFixed(2)));
            arrEFSPercentage.push(parseFloat(((arrEFSStandardTime[i] / divisor) * 100).toFixed(2)));
            arrCSSPercentage.push(parseFloat(((arrCSSStandardTime[i] / divisor) * 100).toFixed(2)));

            xAxisCategory.push(ro[i].CalMonthName);

            productivity = parseFloat(ro[i].EFSStandardTime)
                + parseFloat(ro[i].CSSStandardTime)
                + parseFloat(ro[i].ALRActualTime)
                + parseFloat(ro[i].FQRActualTime)
                + parseFloat(ro[i].SPMActualTime)
                + parseFloat(ro[i].EMTActualTime)
                + parseFloat(ro[i].ESSActualTime)
                + parseFloat(ro[i].AlarmActualTime)
                + parseFloat(ro[i].ProjectActualTime)
                + parseFloat(ro[i].AdhocAnalyticsActualTime)
                + parseFloat(ro[i].ToolSupportActualTime)
                + parseFloat(ro[i].TrainingActualTime);

            productivity = (productivity / divisor) * 100;

            dataholderProductivity.push(parseFloat(productivity.toFixed(2)));
        }

        arrayUtilizationHours = [
                {
                    name: 'EFS',
                    data: arrEFSActualTime
                }
                ,
                {
                    name: 'CSS',
                    data: arrCSSActualTime
                }
                ,
                {
                    name: 'ALR',
                    data: arrALRActualTime
                }
                ,
                {
                    name: 'FQR',
                    data: arrFQRActualTime
                }
                ,
                {
                    name: 'SPM',
                    data: arrSPMActualTime
                }
                ,
                {
                    name: 'EMT',
                    data: arrEMTActualTime
                }
                ,
                {
                    name: 'ESS',
                    data: arrESSActualTime
                }
                ,
                {
                    name: 'ALM',
                    data: arrAlarmActualTime
                }
                ,
                {
                    name: 'PRJ',
                    data: arrProjectActualTime
                }
                ,
                {
                    name: 'ADC',
                    data: arrAdhocAnalyticsActualTime
                }
                ,
                {
                    name: 'TSP',
                    data: arrToolSupportActualTime
                }
                ,
                {
                    name: 'TRN',
                    data: arrTrainingActualTime
                }
                ,
                {
                    name: ' NTP',
                    data: arrNPTActualTime
                }
        ];


        arrayUtilizationPercent = [
                {
                    name: 'EFS',
                    data: arrEFSPercentage
                }
                ,
                {
                    name: 'CSS',
                    data: arrCSSPercentage
                }
                ,
                {
                    name: 'ALR',
                    data: arrALRPercentage
                }
                ,
                {
                    name: 'FQR',
                    data: arrFQRPercentage
                }
                ,
                {
                    name: 'SPM',
                    data: arrSPMPercentage
                }
                ,
                {
                    name: 'EMT',
                    data: arrEMTPercentage
                }
                ,
                {
                    name: 'ESS',
                    data: arrESSPercentage
                }
                ,
                {
                    name: 'ALM',
                    data: arrAlarmPercentage
                }
                ,
                {
                    name: 'PRJ',
                    data: arrProjectPercentage
                }
                ,
                {
                    name: 'ADC',
                    data: arrAdhocAnalyticsPercentage
                }
                ,
                {
                    name: 'TSP',
                    data: arrToolSupportPercentage
                }
                ,
                {
                    name: 'TRN',
                    data: arrTrainingPercentage
                }
                ,
                {
                    name: ' NTP',
                    data: arrNPTPercentage
                }
                ,
                {
                    name: 'Target',
                    type: 'scatter',
                    marker: {
                        enabled: false
                    },
                    data: [100]
                }
        ];

        jQuery("#divChart1").highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'TOTAL HOURS SPENT',
                align: 'center',
                y: 10
            },
            xAxis: {
                categories: xAxisCategory //['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Hours Spent'
                }
                ,
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                    , formatter: function () {
                        return (this.total) + ' HRS';
                        //return ((this.total/divisor)*100).toFixed(2) + " %";
                    }
                }
            },
            legend: {
                align: 'center',
                x: 15,
                verticalAlign: 'bottom',
                y: 20,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y} HRS'
            },
            colors: stackedcolor
            ,plotOptions: {
                column: {
                    stacking: 'normal'
                    //,
                    //dataLabels: {
                    //    enabled: true,
                    //    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    //}
                }
                ,
                series: {
                    minPointLength: function () {
                        this.y > 0 ? 5 : 0;
                    }
                }
            },
            series: arrayUtilizationHours

        });


        jQuery("#divChart2").highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'UTILIZATION',
                align: 'center',
                y: 10
            },
            xAxis: {
                categories: xAxisCategory //['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Percentage'
                }
                ,
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                    , formatter: function () {
                        return (this.total) + " %";
                        //return ((this.total/divisor)*100).toFixed(2) + " %";
                    }
                }
                , plotLines: [{
                    value: 100,
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '100% Target'
                    }
                }]
            },
            legend: {
                align: 'center',
                x: 15,
                verticalAlign: 'bottom',
                y: 20,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y} %'
            },
            colors: stackedcolor
            ,plotOptions: {
                column: {
                    stacking: 'normal'
                    //,
                    //dataLabels: {
                    //    enabled: true,
                    //    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    //}
                }
                ,
                series: {
                    minPointLength: function () {
                        this.y > 0 ? 5 : 0;
                    }
                }
            },
            series: arrayUtilizationPercent

        });

        jQuery("#divChart3").highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'PRODUCTIVITY',
                align: 'center',
                y: 10
            },
            xAxis: {
                categories: xAxisCategory
            },
            yAxis: {
                title: { text: "Percentage" }

                , plotLines: [{
                    value: 85,
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '85% Target'
                    }
                }]
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        crop: false,
                        overflow: 'none'
                        , formatter: function () {
                            return (this.y).toFixed(2) + " %";
                        }
                    }
                }
                ,
                series: {
                    minPointLength: function () {
                        this.y > 0 ? 5 : 0;
                    }
                }
            },
            series: [{
                name: 'Productivity',
                data: dataholderProductivity,
                showInLegend: false
            },
            {
                name: 'Goal',
                type: 'scatter',
                marker: {
                    enabled: false
                },
                data: [85],
                showInLegend: false
            }]
        });


    } else {
        bootbox.alert("Request Error on loadReportPerStandardTime : " + data.ErrorMessage);
    }
}

//////////////////// STACKED BAR PER EMPLOYEE ///////////////////////////////////////////

function loadStackedChartPerEmployee(response) {
    var root = JSON.parse(JSON.stringify(response));
    var data = root["d"];
    if (data == null)
        data = root;
    var ro = data.ResponseItem;

    if (data.ErrorMessage == null) {
        var dhtml = "";
        if (ro.length < 1)
            return bootbox.alert("No record retrieved.");

        var xAxisCategory = new Array();
        var dataholderProductivity = new Array();
        var productivity = 0;
        var arrayUtilizationHours = new Array();
        var arrayUtilizationPercent = new Array();
        var objSeries = new Object();
        var totalActualTime = 0;
        var daysPresent = 0;
        var hoursPresent = 0;
        var divisor = 0;

        var arrALRActualTime = new Array();
        var arrFQRActualTime = new Array();
        var arrSPMActualTime = new Array();
        var arrEMTActualTime = new Array();
        var arrESSActualTime = new Array();
        var arrAlarmActualTime = new Array();
        var arrProjectActualTime = new Array();
        var arrAdhocAnalyticsActualTime = new Array();
        var arrToolSupportActualTime = new Array();
        var arrTrainingActualTime = new Array();
        var arrNPTActualTime = new Array();
        var arrEFSActualTime = new Array();
        var arrCSSActualTime = new Array();
        var arrEFSStandardTime = new Array();
        var arrCSSStandardTime = new Array();
        var arrAdminProdActualTime = new Array();

        var arrALRPercentage = new Array();
        var arrFQRPercentage = new Array();
        var arrSPMPercentage = new Array();
        var arrEMTPercentage = new Array();
        var arrESSPercentage = new Array();
        var arrAlarmPercentage = new Array();
        var arrProjectPercentage = new Array();
        var arrAdhocAnalyticsPercentage = new Array();
        var arrToolSupportPercentage = new Array();
        var arrTrainingPercentage = new Array();
        var arrNPTPercentage = new Array();
        var arrEFSPercentage = new Array();
        var arrCSSPercentage = new Array();
        var arrAdminProdPercentage = new Array();

        var stackedcolor = ["#008B8B", "#DAA520", "#8A4B05", "#D31245", "#959797", "#00AA7E", "#87E5FF"
            , "#6E298D", "#F79428", "#FFCF22", "#00A4D2", "#008D5D", "#62BB46", "#004B8D"];


        for (var i = 0; i < ro.length; i++) {
            daysPresent = parseInt(ro[i].ExpectedWorkingDays) - parseInt(ro[i].NumberOfLeaves);
            hoursPresent = daysPresent * 7.5;

            totalActualTime = parseFloat(ro[i].EFSActualTime)
                + parseFloat(ro[i].CSSActualTime)
                + parseFloat(ro[i].ALRActualTime)
                + parseFloat(ro[i].FQRActualTime)
                + parseFloat(ro[i].SPMActualTime)
                + parseFloat(ro[i].EMTActualTime)
                + parseFloat(ro[i].ESSActualTime)
                + parseFloat(ro[i].AlarmActualTime)
                + parseFloat(ro[i].ProjectActualTime)
                + parseFloat(ro[i].AdhocAnalyticsActualTime)
                + parseFloat(ro[i].ToolSupportActualTime)
                + parseFloat(ro[i].TrainingActualTime)
                + parseFloat(ro[i].NPTActualTime)
                + parseFloat(ro[i].AdminProdActualTime);

            divisor = hoursPresent;
            if (totalActualTime > hoursPresent)
                divisor = totalActualTime;
            
            arrALRActualTime.push(ro[i].ALRActualTime);
            arrFQRActualTime.push(ro[i].FQRActualTime);
            arrSPMActualTime.push(ro[i].SPMActualTime);
            arrEMTActualTime.push(ro[i].EMTActualTime);
            arrESSActualTime.push(ro[i].ESSActualTime);
            arrAlarmActualTime.push(ro[i].AlarmActualTime);
            arrProjectActualTime.push(ro[i].ProjectActualTime);
            arrAdhocAnalyticsActualTime.push(ro[i].AdhocAnalyticsActualTime);
            arrToolSupportActualTime.push(ro[i].ToolSupportActualTime);
            arrTrainingActualTime.push(ro[i].TrainingActualTime);
            arrNPTActualTime.push(ro[i].NPTActualTime);
            arrEFSStandardTime.push(ro[i].EFSStandardTime);
            arrCSSStandardTime.push(ro[i].CSSStandardTime);

            arrEFSActualTime.push(ro[i].EFSActualTime);
            arrCSSActualTime.push(ro[i].CSSActualTime);

            arrAdminProdActualTime.push(ro[i].AdminProdActualTime);

            arrALRPercentage.push(parseFloat(((arrALRActualTime[i] / divisor) * 100).toFixed(2)));
            arrFQRPercentage.push(parseFloat(((arrFQRActualTime[i] / divisor) * 100).toFixed(2)));
            arrSPMPercentage.push(parseFloat(((arrSPMActualTime[i] / divisor) * 100).toFixed(2)));
            arrEMTPercentage.push(parseFloat(((arrEMTActualTime[i] / divisor) * 100).toFixed(2)));
            arrESSPercentage.push(parseFloat(((arrESSActualTime[i] / divisor) * 100).toFixed(2)));
            arrAlarmPercentage.push(parseFloat(((arrAlarmActualTime[i] / divisor) * 100).toFixed(2)));
            arrProjectPercentage.push(parseFloat(((arrProjectActualTime[i] / divisor) * 100).toFixed(2)));
            arrAdhocAnalyticsPercentage.push(parseFloat(((arrAdhocAnalyticsActualTime[i] / divisor) * 100).toFixed(2)));
            arrToolSupportPercentage.push(parseFloat(((arrToolSupportActualTime[i] / divisor) * 100).toFixed(2)));
            arrTrainingPercentage.push(parseFloat(((arrTrainingActualTime[i] / divisor) * 100).toFixed(2)));
            arrNPTPercentage.push(parseFloat(((arrNPTActualTime[i] / divisor) * 100).toFixed(2)));
            arrEFSPercentage.push(parseFloat(((arrEFSStandardTime[i] / divisor) * 100).toFixed(2)));
            arrCSSPercentage.push(parseFloat(((arrCSSStandardTime[i] / divisor) * 100).toFixed(2)));
            arrAdminProdPercentage.push(parseFloat(((arrAdminProdActualTime[i] / divisor) * 100).toFixed(2)));

            xAxisCategory.push(ro[i].EmployeeNames);

            //alert(ro[i].EmployeeNames + ": " + arrCSSStandardTime[i]);

            productivity = parseFloat(ro[i].EFSStandardTime)
                + parseFloat(ro[i].CSSStandardTime)
                + parseFloat(ro[i].ALRActualTime)
                + parseFloat(ro[i].FQRActualTime)
                + parseFloat(ro[i].SPMActualTime)
                + parseFloat(ro[i].EMTActualTime)
                + parseFloat(ro[i].ESSActualTime)
                + parseFloat(ro[i].AlarmActualTime)
                + parseFloat(ro[i].ProjectActualTime)
                + parseFloat(ro[i].AdhocAnalyticsActualTime)
                + parseFloat(ro[i].ToolSupportActualTime)
                + parseFloat(ro[i].TrainingActualTime)
                + parseFloat(ro[i].AdminProdActualTime);

            productivity = (productivity / divisor) * 100;

            dataholderProductivity.push(parseFloat(productivity.toFixed(2)));
        }

        arrayUtilizationHours = [
                {
                    name: 'EFS',
                    data: arrEFSActualTime
                }
                ,
                {
                    name: 'CSS',
                    data: arrCSSActualTime
                }
                ,
                {
                    name: 'ALR',
                    data: arrALRActualTime
                }
                ,
                {
                    name: 'FQR',
                    data: arrFQRActualTime
                }
                ,
                {
                    name: 'SPM',
                    data: arrSPMActualTime
                }
                ,
                {
                    name: 'EMT',
                    data: arrEMTActualTime
                }
                ,
                {
                    name: 'ESS',
                    data: arrESSActualTime
                }
                ,
                {
                    name: 'ALM',
                    data: arrAlarmActualTime
                }
                ,
                {
                    name: 'PRJ',
                    data: arrProjectActualTime
                }
                ,
                {
                    name: 'ADC',
                    data: arrAdhocAnalyticsActualTime
                }
                ,
                {
                    name: 'TSP',
                    data: arrToolSupportActualTime
                }
                ,
                {
                    name: 'AdProd',
                    data: arrAdminProdActualTime
                }
                ,
                {
                    name: 'TRN',
                    data: arrTrainingActualTime
                }
                ,
                {
                    name: ' NTP',
                    data: arrNPTActualTime
                }
        ];


        arrayUtilizationPercent = [
                {
                    name: 'EFS',
                    data: arrEFSPercentage
                }
                ,
                {
                    name: 'CSS',
                    data: arrCSSPercentage
                }
                ,
                {
                    name: 'ALR',
                    data: arrALRPercentage
                }
                ,
                {
                    name: 'FQR',
                    data: arrFQRPercentage
                }
                ,
                {
                    name: 'SPM',
                    data: arrSPMPercentage
                }
                ,
                {
                    name: 'EMT',
                    data: arrEMTPercentage
                }
                ,
                {
                    name: 'ESS',
                    data: arrESSPercentage
                }
                ,
                {
                    name: 'ALM',
                    data: arrAlarmPercentage
                }
                ,
                {
                    name: 'PRJ',
                    data: arrProjectPercentage
                }
                ,
                {
                    name: 'ADC',
                    data: arrAdhocAnalyticsPercentage
                }
                ,
                {
                    name: 'TSP',
                    data: arrToolSupportPercentage
                }
                ,
                {
                    name: 'AdProd',
                    data: arrAdminProdPercentage
                }
                ,
                {
                    name: 'TRN',
                    data: arrTrainingPercentage
                }
                ,
                {
                    name: ' NTP',
                    data: arrNPTPercentage
                }
                ,
                {
                    name: 'Target',
                    type: 'scatter',
                    marker: {
                        enabled: false
                    },
                    data: [100]
                }
        ];

        jQuery("#divChart1").highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'TOTAL HOURS SPENT',
                align: 'center',
                y: 10
            },
            xAxis: {
                categories: xAxisCategory //['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Hours Spent'
                }
                ,
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                    , formatter: function () {
                        return (this.total) + ' HRS';
                        //return ((this.total/divisor)*100).toFixed(2) + " %";
                    }
                }
            },
            legend: {
                align: 'center',
                x: 15,
                verticalAlign: 'bottom',
                y: 20,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y} HRS'
            },
            colors: stackedcolor
            , plotOptions: {
                column: {
                    stacking: 'normal'
                    //,
                    //dataLabels: {
                    //    enabled: true,
                    //    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    //}
                }
                ,
                series: {
                    minPointLength: function () {
                        this.y > 0.00 ? 100 : 0;
                    }
                }
            },
            series: arrayUtilizationHours

        });


        jQuery("#divChart2").highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'UTILIZATION',
                align: 'center',
                y: 10
            },
            xAxis: {
                categories: xAxisCategory //['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Percentage'
                }
                ,
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                    , formatter: function () {
                        return (this.total) + " %";
                        //return ((this.total/divisor)*100).toFixed(2) + " %";
                    }
                }
                , plotLines: [{
                    value: 100,
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '100% Target'
                    }
                }]
            },
            legend: {
                align: 'center',
                x: 15,
                verticalAlign: 'bottom',
                y: 20,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y} %'
            },
            colors: stackedcolor
            , plotOptions: {
                column: {
                    stacking: 'normal'
                    //,
                    //dataLabels: {
                    //    enabled: true,
                    //    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    //}
                }
                ,
                series: {
                    minPointLength: function () {
                        this.y > 0.00 ? 100 : 0;
                    }
                }
            },
            series: arrayUtilizationPercent

        });

        jQuery("#divChart3").highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'PRODUCTIVITY',
                align: 'center',
                y: 10
            },
            xAxis: {
                categories: xAxisCategory
            },
            yAxis: {
                title: { text: "Percentage" }

                , plotLines: [{
                    value: 85,
                    color: 'green',
                    dashStyle: 'shortdash',
                    width: 2,
                    label: {
                        text: '85% Target'
                    }
                }]
            },
            plotOptions: {
                column: {
                    dataLabels: {
                        enabled: true,
                        crop: false,
                        overflow: 'none'
                        , formatter: function () {
                            return (this.y).toFixed(2) + " %";
                        }
                    }
                }
            },
            series: [{
                name: 'Productivity',
                data: dataholderProductivity,
                showInLegend: false
            },
            {
                name: 'Goal',
                type: 'scatter',
                marker: {
                    enabled: false
                },
                data: [85],
                showInLegend: false
            }]
        });


    } else {
        bootbox.alert("Request Error on loadReportPerStandardTime : " + data.ErrorMessage);
    }
}

/////////////////////////////////////////////////////////
function loadReportPieDonutChart(response) {
    var root = JSON.parse(JSON.stringify(response));
    var data = root["d"];
    if (data == null)
        data = root;
    var ro = data.ResponseItem;
    var dt = new Object();
    if (data.ErrorMessage == null) {
        var dhtml = "";
        if (ro.length < 1)
            return bootbox.alert("No record retrieved.");

        var dataholder = new Array();
        for (var i = 0; i < ro.length; i++) {
            var tempData = new Array();
            tempData.push(ro[i].ActivityDescription);
            tempData.push(ro[i].Percentage);
            dataholder.push(tempData);
        }

        if (SelectedActivityTypeID == "4")
            textTitle = "PROJECT";
        if (SelectedActivityTypeID == "11")
            textTitle = "ADHOC ANALYTICS";

        jQuery("#divChart1").highcharts({
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45
                }
            },
            title: {
                text: textTitle
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45,
                    dataLabels: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    }
                }
            },
            series: [{
                name: 'Percentage',
                data: dataholder
            }]
        });

    } else {
        bootbox.alert("Request Error on loadReportPerStandardTime : " + data.ErrorMessage);
    }
}


/////////////////////////////////////////////////////////
function loadReportHitMissPieChart(response) {
    var root = JSON.parse(JSON.stringify(response));
    var data = root["d"];
    if (data == null)
        data = root;
    var ro = data.ResponseItem;
    var dt = new Object();
    if (data.ErrorMessage == null) {
        var dhtml = "";
        if (ro.length < 1)
            return bootbox.alert("No record retrieved.");

        var dataholder = new Array();
        for (var i = 0; i < ro.length; i++) {
            var tempData = new Array();
            tempData.push(ro[i].ActivityDescription);
            tempData.push(ro[i].Percentage);
            dataholder.push(tempData);
        }

        if (SelectedActivityTypeID == "4")
            textTitle = "PROJECT";
        if (SelectedActivityTypeID == "11")
            textTitle = "ADHOC ANALYTICS";

        jQuery("#divChart1").highcharts({
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45
                }
            },
            title: {
                text: textTitle
            },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45,
                    dataLabels: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b>: ' + this.y;
                        }
                    }
                }
            },
            series: [{
                name: 'Percentage',
                data: dataholder
            }]
        });

    } else {
        bootbox.alert("Request Error on loadReportPerStandardTime : " + data.ErrorMessage);
    }
}
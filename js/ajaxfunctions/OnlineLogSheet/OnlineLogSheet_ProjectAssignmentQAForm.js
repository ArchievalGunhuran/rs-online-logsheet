﻿
var arrProjectAssignment;
var arrProjectActivityList;
var arrProjectPriorityList;
var arrProjectEmployeeList;

jQuery(document).ready(function () {
    var detailsTemplate;

    var wnd = jQuery("#wnd").kendoWindow({
        title: "QA for Project",
        height: 750,
        width: 700,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    detailsTemplate = kendo.template(jQuery("#template").html());

    function show(id, value) {
        document.getElementById(id).style.display = value ? 'block' : 'none';
    }

    callAjaxRequestJSON("GetEmployeeListAdhoc"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrProjectEmployeeList = ro;
                if (arrProjectEmployeeList.length > 0) {
                    getActivityList();
                }
                else {
                    alert("No items available for Adhoc Analytics Assignment Employee List");
                }
            } else {
                alert("Web service error when calling Adhoc Analytics Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );


    function getActivityList() {

        callAjaxRequestJSON("GetActivityList"
            , { ActivityTypeID: ActivityType_AA, EmployeeID: arrProjectEmployeeList[0].ListID }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrProjectActivityList = ro;
                    if (arrProjectActivityList.length > 0)
                        getPriorityList();
                    else
                        alert("No items available for Adhoc Analytics Assignment Activity List");

                } else {
                    alert("Web service error when calling Adhoc Analytics Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function getPriorityList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_AA, ListTypeID: 2 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrProjectPriorityList = ro;
                    if (arrProjectPriorityList.length > 0)
                        loadTSAssignment();
                    else
                        alert("No items available for Adhoc Analytics Assignment Priority List");

                } else {
                    alert("Web service error when calling Adhoc Analytics - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadTSAssignment() {

        var dataSourceAAAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetProjectAssignmentForQA"
                    , { qaByEmployeeID: Employee_ID }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrProjectAssignment = ro;
                            e.success(arrProjectAssignment);
                        } else {
                            alert("Web service error when calling loadAAAssignment_GetCSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                }
            }
            ,
            error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ProjectNumber: { editable: false },
                        ActivityID: { defaultValue: arrProjectActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        Remarks: { validation: { required: true, maxLength: 250 } },
                        Sponsor: { validation: { required: true, maxLength: 250 } },
                        StartDate: { type: "date", editable: false },
                        ActivityDate: {
                            type: "date", defaultValue: function (e) { return new Date(); }
                            , validation: {
                                validateDate: function (input) {
                                    if (input.attr("data-bind") == "value:ActivityDate") {
                                        input.attr("data-validateDate-msg", "ProcessDate can not be later than 3 days or greater than today");
                                        return checkdate(input.val());
                                    }
                                    return true;
                                }
                            }
                        },
                        StatusID: { editable: false },
                        StatusDescription: { editable: false },
                        TimeSpent: { editable: false },

                        TargetTestingDate: { type: "date", editable: false },
                        TestingDate: { type: "date", editable: false },
                        JointTestingDate: { type: "date", editable: false },
                        TargetDate: { type: "date", editable: false },
                        CompletionDate: { type: "date", editable: false },
                        AcceptanceDate: { type: "date", editable: false },

                        NoOfDaysDelayedST: { editable: false },
                        FirstPassTimelinessST: { editable: false },
                        ActionItemST: { editable: false },
                        WorkingST: { editable: false },
                        FirstPassAccuracyST: { editable: false },

                        NoOfDaysDelayedPR: { editable: false },
                        ProjectClosureTimelinessPR: { editable: false },
                        ActionItemPR: { editable: false },
                        WorkingPR: { editable: false },
                        ProjectClosureAccuracyPR: { editable: false },

                        FirstPass: { editable: false },
                        ProjectClosure: { editable: false },
                        TotalQAScore: { editable: false },

                        QAByEmployeeName: { editable: false },
                        TimeSpentOfQA: { editable: false },
                        TimeSpentOfQAProd: { editable: false },
                        QAStatus: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridProjectAssignment").kendoGrid({
            dataSource: dataSourceAAAssignment,
            pageable: true,
            //toolbar: ["create", "cancel"],
            filterable: true,
            columns: [
                    {
                        field: "ProjectNumber", title: "Project Number", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ActivityID", title: "Project Name", editor: aaTaskDDEditor, width: "250px", template: "#=ActivityDescription#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrProjectActivityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "Remarks", title: "Remarks", width: "120px", filterable: false },
                    { field: "Sponsor", title: "Sponsor", width: "120px", filterable: false },
                    { field: "StartDate", title: "Start Date", format: "{0:MM/dd/yyyy}", width: "250px", filterable: false },
                    //{ field: "ActivityDate", title: "Start Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "StatusDescription", title: "Status", width: "120px", filterable: false },
                    { field: "TimeSpent", title: "Time Spent (Hours)", format: "{0:0.00}", width: "250px" },
                    { field: "TargetTestingDate", title: "Target Testing Date", format: "{0:MM/dd/yyyy}", width: "250px", filterable: false },
                    { field: "TestingDate", title: "Testing Date", format: "{0:MM/dd/yyyy}", width: "250px", filterable: false },
                    { field: "JointTestingDate", title: "Joint Testing Date", format: "{0:MM/dd/yyyy}", width: "250px", filterable: false },
                    { field: "TargetDate", title: "Target Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "CompletionDate", title: "Completion Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "AcceptanceDate", title: "Acceptance Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },

                    { field: "NoOfDaysDelayedST", title: "No. of Days Delayed", width: "250px", filterable: false },
                    { field: "FirstPassTimelinessST", title: "First Pass Timeliness", width: "250px", filterable: false },
                    { field: "ActionItemST", title: "Action Item", width: "120px", filterable: false },
                    { field: "WorkingST", title: "Working", width: "120px", filterable: false },
                    { field: "FirstPassAccuracyST", title: "First Pass Accuracy", width: "250px", filterable: false },
                    { field: "TimeSpentOfQA", title: "Time Spent of QA (Staging)", width: "250px", filterable: false },

                    { field: "NoOfDaysDelayedPR", title: "No. of Days Delayed", width: "250px", filterable: false },
                    { field: "ProjectClosureTimelinessPR", title: "Project Closure Timeliness", width: "250px", filterable: false },
                    { field: "ActionItemPR", title: "Action Item", width: "120px", filterable: false },
                    { field: "WorkingPR", title: "Working", width: "120px", filterable: false },
                    { field: "ProjectClosureAccuracyPR", title: "Project Closure Accuracy", width: "250px", filterable: false },
                    { field: "TimeSpentOfQAProd", title: "Time Spent of QA (Production)", width: "250px", filterable: false },

                    { field: "FirstPass", title: "First Pass", width: "120px", filterable: false },
                    { field: "ProjectClosure", title: "Project Closure", width: "250px", filterable: false },
                    { field: "TotalQAScore", title: "Total QA Score", width: "120px", filterable: false },

                    { field: "QAByEmployeeName", title: "QA By", width: "250px", filterable: false },
                    { field: "QAStatus", title: "QA Status", width: "120px", filterable: false },

                    { command: { text: "Add Score", click: completeStatus }, title: "", width: "200px" },

            ],
            editable: "inline"
        });

    }
    function completeStatus(e) {
        e.preventDefault();

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

        if (dataItem.QAStatus == 'COMPLETED') {
            jQuery.confirm({
                title: 'Project',
                content: 'This Record is already Completed.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else {
            var targetTestingDate = new Date(dataItem.TargetTestingDate);
            var testingDate = new Date(dataItem.TestingDate);
            var daysDelayed = 0;

            if (testingDate.getTime() <= targetTestingDate.getTime()) {
                daysDelayed = 0;
            }
            else {

                var diffTime = Math.abs(targetTestingDate - testingDate);
                var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                daysDelayed = diffDays;
            }
            var timeliness = (100 - (daysDelayed * 15)) * 0.4;
            dataItem.vDaysDelayed = daysDelayed;
            dataItem.vTimeliness = timeliness.toFixed(2);


            wnd.content(detailsTemplate(dataItem));
            wnd.center().open();

        }

    }

    $(document).on('click', '#btnComplete', function (e) {
        e.preventDefault();
        var projectassignmentID = $('#projectassignmentID').val();
        //alert(DaysDelayed);

        var daysdelayed = $('#txtDaysDelayed').val();
        var timeliness = $('#txtTimeliness').val();
        var actionItem = $('#txtActionItem').val();
        var working = $('#txtWorking').val();
        var accuracy = $('#txtAccuracy').val();
        var totalFirstPass = $('#txtFirstPass').val();
        var timeSpentQA = $('#txtTimeSpent').val();

        

        if (daysdelayed == '' || timeliness == '' || actionItem == '' || working == '' || accuracy == '' || totalFirstPass == '' || timeSpentQA == '') {
            jQuery.confirm({
                title: 'QA For Project',
                content: 'All Fields are Required',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else {
            wnd.close();
            tempStartDateTime = new Date();
            var param = new Object();
            var obj = new Object();

            obj.LogDetailID = projectassignmentID;

            obj.NoOfDaysDelayedST = daysdelayed;
            obj.FirstPassTimelinessST = timeliness;
            obj.ActionItemST = actionItem;
            obj.WorkingST = working;
            obj.FirstPassAccuracyST = accuracy;
            obj.FirstPass = totalFirstPass;

            obj.TimeSpentOfQA = timeSpentQA;


            param.ActivityLog = obj;

            show('loading', true);

            callAjaxRequestJSON("AddQAForProjectAssignment"
                , param
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        show('loading', false);
                        jQuery.confirm({
                            title: 'QA For Project',
                            content: 'Database Successfully Modified.',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                close: function () {
                                    window.location.reload();
                                }
                            }
                        });
                        loadAAAssignment();
                    } else {
                        show('loading', false);
                        bootbox.alert("Web service error Add QA for Project. " + data.ErrorMessage);
                    }
                }
                , ajaxError
            );
        }
    });

    function aaTaskDDEditor(container, options) {
        programContainer = container;
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrProjectActivityList
            });

    }

    function employeeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrProjectEmployeeList
                , change: function (e) {

                    callAjaxRequestJSON("GetActivityList"
                        , { ActivityTypeID: ActivityType_AA, EmployeeID: this.value() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrProjectActivityList = ro;
                                if (arrProjectActivityList.length > 0) {
                                    programContainer.empty();
                                    var dd = jQuery("<input data-bind='value:ActivityID'/>")
                                        .appendTo(programContainer)
                                        .kendoDropDownList({
                                            dataTextField: "ListDescription"
                                            , dataValueField: "ListID"
                                            , change: function (e) {
                                                options.model.ActivityID = this.value();
                                            }
                                        }).data("kendoDropDownList");

                                    dd.setDataSource(arrProjectActivityList);
                                    options.model.ActivityID = dd.value();
                                }
                                else
                                    alert("No items available for CS Assignment Activity List");

                            } else {
                                alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                }
            });
    }

    function aaPriorityDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrProjectPriorityList
            });
    }

});
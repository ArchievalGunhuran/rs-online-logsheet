﻿
var activityLogFieldsList = new Array();

var arrTSActivityLogs;
var arrTSActivityList;
var arrTSStatuslist;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var curDateTime = new Date();

jQuery(document).ready(function () {

    jQuery("#txtTSActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridTS').data('kendoGrid').dataSource.read(); }
    });


    //GET TASK LIST FOR Tool Support
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_TS, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrTSActivityList = ro;
                if (arrTSActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from Tool Support - Task field");
            } else {
                bootbox.alert("Web service error when calling Tool Support - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM Tool Support
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_TS, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrTSStatuslist = ro;
                    loadGridTS();
                } else {
                    bootbox.alert("Web service error when calling Tool Support - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }


    function loadGridTS() {

        var dataSourceTS = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                    , { ActivityTypeID: ActivityType_TS, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtTSActivityDate").val() }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrTSActivityLogs = ro;
                            e.success(arrTSActivityLogs);
                        } else {
                            bootbox.alert("Web service error getting GetActivityLog on loadgridTS. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var statusList = new Object();

                obj.ActivityTypeID = ActivityType_TS;
                obj.EmployeeID = Employee_ID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.TimeSpent = e.data.TimeSpent;
                obj.NumberOfTransaction = e.data.NumberOfTransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateTSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrTSAssigned = ro;
                            e.success(arrTSAssigned);
                            jQuery('#gridTSAssigned').data('kendoGrid').dataSource.read();
                            jQuery('#gridTS').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error UpdateAAActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_TS;
                param.ActivityLogID = e.data.ActivityLogID;

                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridTS').data('kendoGrid').dataSource.read();
                                    jQuery('#gridTSAssigned').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error when calling Tool Support DeleteActivityLog. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", editable: false, defaultValue: function (e) { return new Date(); } },

                        TicketNumber: { editable: false },
                        ActivityID: { editable: false },
                        ActivityDescription: { editable: false },
                        ToolName: { editable: false },
                        TaskDescription: { editable: false },
                        RequestedBy: { editable: false },
                        DateReceived: { type: "date", editable: false },
                        DueDate: { type: "date", editable: false },
                        DateCompleted: { type: "date", editable: false },
                        SLA: { editable: false },
                        NumberOfTransaction: { type: "number" },
                        StatusID: { editable: false },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", defaultValue: 0, validation: { min: 0.01, required: true } },
                        TimeSpentOfQA: { type: "number", defaultValue: 0.00, validation: { min: 0.01, required: true } }
                    }
                }
            }
        });

        jQuery("#gridTS").kendoGrid({
            dataSource: dataSourceTS,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "TicketNumber", title: "Ticket Number", width: "150px" },
                    { field: "ActivityID", title: "Type", editor: tsTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "ToolName", title: "Tool Name", width: "250px" },
                    { field: "TaskDescription", title: "Description", width: "250px" },
                    { field: "RequestedBy", title: "Requested By", width: "250px" },
                    { field: "DateReceived", title: "Date Received", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DueDate", title: "Due Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    { field: "NumberOfTransaction", title: "Number Of Transaction", format: "{0:0}", width: "150px" },
                    { field: "StatusID", title: "Status", editor: tsStatusListDDEditor, template: "#=StatusDescription#", width: "200px" },
                    { field: "TimeSpent", title: "Time Spent (Hours)", format: "{0:0.00}", width: "250px" },
                    { field: "TimeSpentOfQA", title: "Time Spent (QA)", format: "{0:0.00}", width: "250px" },
                    //{ command: ["destroy"], title: "&nbsp;", width: "250px" }
                    {
                        command: ["edit",
                                       {
                                           name: "Delete"
                                       , click: function (e) {
                                           e.preventDefault();
                                           var tr = $(e.target).closest("tr");
                                           var data = this.dataItem(tr);

                                           bootbox.confirm("Are you sure to delete this record?", function (result) {
                                               if (result) {
                                                   jQuery('#gridTS').data('kendoGrid').dataSource.remove(data);
                                                   jQuery('#gridTS').data('kendoGrid').dataSource.sync();
                                                   jQuery('#gridTS').data('kendoGrid').dataSource.read();
                                               }
                                           });
                                       }
                                       }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
            ],
            editable: "inline"

        });
        /////////////////////////////// PENDING //////////////////////////////////////

        var dataSourceTSAssigned = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetAssignedTS"
                    , { EmployeeID: Employee_ID }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrTSAssigned = ro;
                            // on success
                            //alert(JSON.stringify(arrTSAssigned));
                            e.success(arrTSAssigned);
                        } else {
                            bootbox.alert("Web service error getting GetAssignedAA on loadgridTS. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var statusList = new Object();

                obj.ActivityTypeID = ActivityType_TS;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = tempStartDateTime;
                obj.TSAssignmentID = e.data.TSAssignmentID;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;
                obj.NumberOfTransaction = e.data.NumberOfTransaction;
                obj.TSAssignmentID = e.data.TSAssignmentID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddTSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrTSAssigned = ro;
                            e.success(arrTSAssigned);
                            jQuery('#gridTSAssigned').data('kendoGrid').dataSource.read();
                            jQuery('#gridTS').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error AddTSActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "TSAssignmentID",
                    fields: {
                        TSAssignmentID: { editable: false, nullable: false },
                        TicketNumber: { editable: false },
                        ActivityID: { editable: false },
                        ActivityDescription: { editable: false },
                        ToolName: { editable: false },
                        TaskDescription: { editable: false },
                        RequestedBy: { editable: false },
                        DateReceived: { type: "date", editable: false },
                        DueDate: { type: "date", editable: false },
                        DateCompleted: { type: "date", editable: false },
                        SLA: { editable: false },
                        NumberOfTransaction: { type: "number" },
                        StatusID: {
                            defaultValue: arrCSStatus[0].ListID
                            , validation: {
                                validateStatus: function (input) {
                                    if (input.attr("data-bind") == "value:StatusID") {
                                        selectedStatus = input.val();
                                        input.attr("data-validateStatus-msg", "Status is required");
                                        if (selectedStatus < 1) {
                                            bootbox.alert("status is required");
                                            return false;
                                        }
                                        else {
                                            return true;
                                        }
                                    }
                                    return true;
                                }
                            }
                        },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", defaultValue: 0.00, validation: { min: 0.01, required: true } },
                    }
                }
            }
        });

        jQuery("#gridTSAssigned").kendoGrid({
            dataSource: dataSourceTSAssigned,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "TicketNumber", title: "Ticket Number", width: "150px" },
                    { field: "ActivityID", title: "Type", editor: tsTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "TaskDescription", title: "Description", width: "250px" },
                    { field: "ToolName", title: "Tool Name", width: "250px" },
                    { field: "RequestedBy", title: "Requested By", width: "250px" },
                    { field: "DateReceived", title: "Date Received", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DueDate", title: "Due Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    { field: "StatusID", title: "Status", editor: tsStatusListDDEditor, template: "#=StatusDescription#", width: "200px" },
                    { field: "NumberOfTransaction", title: "Number Of Transaction", format: "{0:0}", width: "150px" },
                    { field: "TimeSpent", title: "Time Spent (Hours)", format: "{0:0.00}", width: "150px" },
                    { command: ["edit"], title: "&nbsp;", width: "250px" }
            ],
            editable: "inline",
            edit: function (e) {
                tempStartDateTime = new Date();
                //alert(tempStartDateTime);
            }
        });

        jQuery(".k-grid-toolbar", "#gridTSAssigned").prepend("<h4>ASSIGNED TOOL SUPPORT</h4>");
        jQuery(".k-grid-toolbar", "#gridTS").prepend("<h4>ACTIVITY LOG</h4>");

    }

    function tsTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrTSActivityList
            });
    }

    function tsStatusListDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrTSStatuslist
            });
    }

});
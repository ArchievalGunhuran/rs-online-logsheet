﻿
var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2];
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {

    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/ActivityMaintenance.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

var arrActivityTypeList;
var arrActivity;
var arrEmployeeAssignment;
var arrEmployeeList;
var selectedActivityID = 0;
var selectedActivitySTID = 0;
var curDateTime = new Date();

jQuery(document).ready(function () {

    callAjaxRequestJSON("GetEmployeeList"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrEmployeeList = ro;
                
                if (arrEmployeeList.length > 0) {
                    getActivityList();
                }
                else
                    alert("No items from CS Assignment - Employee List");
            } else {
                alert("Web service error when calling CS Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    function getActivityList() {
        var usergroup = $('#hfUserGroup').val();
        //alert(usergroup);
        callAjaxRequestJSON("GetActivityTypeList"
            , { usergroup: usergroup}
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrActivityTypeList = ro;
                    //alert(JSON.stringify(arrActivityTypeList));
                    if (arrActivityTypeList.length > 0) {
                        loadGridActivity();
                        loadGridEmployeeAssignment();
                    }
                    else
                        alert("No items from Activity Type field");
                } else {
                    alert("Web service error when calling Activity Maintenance getActivityList_GetActivityTypeList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridActivity() {

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    param.Activity = obj;

                    callAjaxRequestJSON("GetActivityMaintenance"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivity = ro;
                                //alert(JSON.stringify(arrActivity));
                                e.success(arrActivity);
                            } else {
                                alert("Web service error when calling Activity Maintenance loadGridActivity_GetActivityMaintenance. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
                , create: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    obj.ActivityTypeID = e.data.ActivityTypeID;
                    obj.ActivityID = 0;

                    obj.ActivityDescription = e.data.ActivityDescription;
                    obj.IsActive = e.data.IsActive;
                    obj.HaveST = e.data.HaveST;
                    obj.StandardTime = e.data.StandardTime;
                    obj.EmployeeID = Employee_ID;

                    param.Activity = obj;

                    callAjaxRequestJSON("AddActivityMaintenance"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivity = ro;
                                // on success
                                arrActivity.push(e.data);
                                e.success(arrActivity);
                                jQuery('#gridActivityMaintenance').data('kendoGrid').dataSource.read();
                            } else {
                                alert(data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
                , update: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    obj.ActivityTypeID = e.data.ActivityTypeID;
                    obj.ActivityID = e.data.ActivityID;

                    obj.ActivityDescription = e.data.ActivityDescription;
                    obj.IsActive = e.data.IsActive;
                    obj.HaveST = e.data.HaveST;
                    obj.StandardTime = e.data.StandardTime;
                    obj.EmployeeID = Employee_ID;
                    obj.ActivitySTID = e.data.ActivitySTID;

                    param.Activity = obj;

                    callAjaxRequestJSON("UpdateActivityMaintenance"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivity = ro;
                                // on success
                                arrActivity.push(e.data);
                                e.success(arrActivity);
                                jQuery('#gridActivityMaintenance').data('kendoGrid').dataSource.read();
                            } else {
                                alert(data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
            }
            , error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 20,
            batch: false,
            schema: {
                model: {
                    id: "ActivityID",
                    fields: {
                        ActivityID: { editable: false, nullable: false },
                        ActivitySTID: { editable: false },
                        ActivityTypeID: { validation: { required: true }, defaultValue: arrActivityTypeList[0].ListID },
                        ActivityTypeDescription: { editable: false },
                        ActivityDescription: { validation: { required: true, maxLength: 250} },
                        IsActive: { type: "boolean", defaultValue: true },
                        HaveST: { type: "boolean", defaultValue: false },
                        StandardTime: { type: "number", validation: { min: 0, required: false} }
                    }
                }
            }
        });

        jQuery("#gridActivityMaintenance").kendoGrid({
            dataSource: dataSource,
            pageable: true,
            selectable: "row",
            change: function (arg) {
                var selectedRowData;
                var gridData = jQuery("#gridActivityMaintenance").data("kendoGrid");
                selectedRowData = gridData.dataItem(jQuery("#gridActivityMaintenance").find("tr.k-state-selected"));

                selectedActivityID = selectedRowData.ActivityID;
                selectedActivitySTID = selectedRowData.ActivitySTID;
                selectedHaveST = selectedRowData.HaveST;
                
                if (selectedHaveST) {
                    jQuery('#divEmpAssignHolder').show();
                    jQuery('#gridEmployeeAssignment').data('kendoGrid').dataSource.read();
                }
                else
                    jQuery('#divEmpAssignHolder').hide();

            },
            filterable: true,
            toolbar: ["create"],
            columns: [
                    {
                        field: "ActivityTypeID", title: "Activity Type", editor: activityTypeDDEditor, width: "250px", template: "#=ActivityTypeDescription#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrActivityTypeList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ActivityDescription", title: "Activity Description", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "IsActive", title: "Is Active", width: "100px", template: "#= IsActive ? 'Active' : 'Deactivated' #", filterable: false },
                    {
                        field: "HaveST", title: "Have ST", width: "100px", template: "#= HaveST ? 'Yes' : 'No' #"
                        , filterable: {
                            extra: false
                            }
                    },
                    { field: "StandardTime", title: "Standard Time (Min)", width: "150px", filterable: false },
                    { command: ["edit"], title: "&nbsp;", width: "250px", filterable: false }
                ],
            editable: "inline"
        });

    }

    function activityTypeDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrActivityTypeList
            });
    }

    ///////// Employee Assignment //////

    function loadGridEmployeeAssignment() {

        jQuery('#divEmpAssignHolder').hide();

        var dataSourceEmployeeAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    var obj = new Object();
                    var param = new Object();

                    obj.ActivitySTID = selectedActivitySTID;

                    param.ActivityLevelAssignment = obj;

                    callAjaxRequestJSON("GetEmployeeAssignment"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            //alert("function " + selectedActivityID);
                            if (data.ErrorMessage == null) {
                                arrEmployeeAssignment = ro;
                                //alert(JSON.stringify(arrEmployeeAssignment));
                                e.success(arrEmployeeAssignment);
                            } else {
                                alert("Web service error when calling Activity Maintenance loadGridEmployeeAssignment_GetEmployeeAssignment. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
                , create: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    obj.EmployeeID = e.data.EmployeeID;
                    obj.ActivitySTID = selectedActivitySTID;
                    obj.EffectiveDate = e.data.EffectiveDate;
                    obj.IsActive = e.data.IsActive;
                    obj.UserEmployeeID = Employee_ID;

                    param.ActivityLevelAssignment = obj;

                    callAjaxRequestJSON("AddEmployeeAssignment"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEmployeeAssignment = ro;
                                // on success
                                arrActivity.push(e.data);
                                e.success(arrEmployeeAssignment);
                                jQuery('#gridEmployeeAssignment').data('kendoGrid').dataSource.read();
                            } else {
                                alert(data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
                , update: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    obj.EmployeeID = e.data.EmployeeID;
                    obj.ActivitySTID = selectedActivitySTID;
                    obj.EffectiveDate = e.data.EffectiveDate;
                    obj.IsActive = e.data.IsActive;
                    obj.UserEmployeeID = Employee_ID;
                    obj.ALAssignID = e.data.ALAssignID;

                    param.ActivityLevelAssignment = obj;

                    callAjaxRequestJSON("UpdateEmployeeAssignment"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEmployeeAssignment = ro;
                                // on success
                                arrActivity.push(e.data);
                                e.success(arrEmployeeAssignment);
                                jQuery('#gridEmployeeAssignment').data('kendoGrid').dataSource.read();
                            } else {
                                alert(data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
            }
            , error: function (e) {
                // handle data operation error
                alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "ALAssignID",
                    fields: {
                        ALAssignID: { editable: false, nullable: false },
                        ActivitySTID: { editable: false, nullable: false },
                        EmployeeID: { validation: { required: true }, defaultValue: arrEmployeeList[0].ListID },
                        EmployeeName: { editable: false },
                        EffectiveDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        IsActive: { type: "boolean", defaultValue: true }
                    }
                }
            }

        });

        jQuery("#gridEmployeeAssignment").kendoGrid({
            dataSource: dataSourceEmployeeAssignment,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "EmployeeID", title: "Employee Name", editor: emplpoyeeDDEditor, width: "250px", template: "#=EmployeeName#" },
                    { field: "EffectiveDate", title: "Effective Date", format: "{0:MM/dd/yyyy}", width: "250px" },
                    { field: "IsActive", title: "Is Active", width: "150px", template: "#= IsActive ? 'Active' : 'Deactivated' #" },
                    { command: ["edit"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline"
        });
    }

    function emplpoyeeDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEmployeeList
            });
    }

});

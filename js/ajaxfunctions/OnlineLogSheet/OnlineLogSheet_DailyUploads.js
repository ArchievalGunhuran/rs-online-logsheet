﻿
var arrDUAssignment;
var arrAAActivityList;
var arrAAPriorityList;
var arrAAEmployeeList;

jQuery(document).ready(function () {
    var detailsTemplate;

    var wnd = jQuery("#wnd").kendoWindow({
        title: "Complete Daily Uploads",
        height: 500,
        width: 550,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    detailsTemplate = kendo.template(jQuery("#template").html());

    function show(id, value) {
        document.getElementById(id).style.display = value ? 'block' : 'none';
    }

    callAjaxRequestJSON("GetEmployeeListAdhoc"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrAAEmployeeList = ro;
                if (arrAAEmployeeList.length > 0) {
                    getPriorityList();
                }
                else {
                    alert("No items available for Adhoc Analytics Assignment Employee List");
                }
            } else {
                alert("Web service error when calling Adhoc Analytics Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    //function aaPriorityDDEditor(container, options) {
    //    jQuery("<input data-bind='value:" + options.field + "'/>")
    //        .appendTo(container)
    //        .kendoDropDownList({
    //            dataTextField: "ListDescription"
    //            , dataValueField: "ListID"
    //            , dataSource: arrAAPriorityList
    //        });
    //}

    function getActivityList() {

        callAjaxRequestJSON("GetActivityList"
            , { ActivityTypeID: ActivityType_DU, EmployeeID: arrAAEmployeeList[0].ListID }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrAAActivityList = ro;
                    if (arrAAActivityList.length > 0)
                        getPriorityList();
                    else
                        alert("No items available for Adhoc Analytics Assignment Activity List");

                } else {
                    alert("Web service error when calling Adhoc Analytics Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function getPriorityList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_DU, ListTypeID: 2 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    loadDUAssignment();
                } else {
                    alert("Web service error when calling Adhoc Analytics - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadDUAssignment() {

        var dataSourceDUAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetDUAssignment"
                    , { ActivityTypeID: ActivityType_DU }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrDUAssignment = ro;
                            e.success(arrDUAssignment);
                        } else {
                            alert("Web service error when calling loadAAAssignment_GetCSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.AssignEmployeeID = e.data.AssignEmployeeID;
                obj.TaskDescription = e.data.TaskDescription;
                obj.DateReceived = e.data.DateReceived;
                obj.DueDate = e.data.DueDate;
                obj.StatusID = 0;
                obj.EmployeeID = Employee_ID;

                param.ActivityLog = obj;
                show('loading', true);

                callAjaxRequestJSON("AddDUAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            show('loading', false);
                            arrDUAssignment = ro;
                            arrDUAssignment.push(e.data);
                            e.success(arrDUAssignment);
                            jQuery('#gridAAAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            show('loading', false);
                            bootbox.alert("Web service error when calling Adhoc Analytics AddAAAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.DUAssignmentID = e.data.DUAssignmentID;
                obj.AssignEmployeeID = e.data.AssignEmployeeID;
                obj.EmployeeID = Employee_ID;
                obj.TaskDescription = e.data.TaskDescription;
                obj.DateReceived = e.data.DateReceived;
                obj.DueDate = e.data.DueDate;

                param.ActivityLog = obj;

                show('loading', true);
                callAjaxRequestJSON("UpdateDUAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            show('loading', false);
                            arrDUAssignment = ro;
                            arrDUAssignment.push(e.data);
                            e.success(arrDUAssignment);
                            jQuery('#gridAAAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            show('loading', false);
                            alert("Web service error when calling Adhoc Analytics UpdateAAAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
            }
            , destroy: function (e) {

                var param = new Object();
                param.DUAssignmentID = e.data.DUAssignmentID;

                callAjaxRequestJSON("DeleteDUAssignment"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridAAAssignment').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error calling loadAAAssignment_DeleteAAAssignment. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "DUAssignmentID",
                    fields: {
                        DUAssignmentID: { editable: false, nullable: false },
                        AssignEmployeeID: { validation: { required: true }, defaultValue: arrAAEmployeeList[0].ListID },
                        AssignEmployeeName: { editable: false },
                        TicketNumber: { editable: false },
                        TaskDescription: { validation: { required: true, maxLength: 250 } },
                        DateReceived: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DueDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DateCompleted: { type: "date", defaultValue: "", editable: false },
                        SLA: { editable: false },
                        StatusDescription: { defaultValue: "Pending", editable: false },
                        TimeSpentofProcessor: { editable: false },

                        QAByEmployeeName: { editable: false },
                        NoOfDaysDelayed: { editable: false },
                        Timeliness: { editable: false },
                        CorrectData: { editable: false },
                        Accuracy: { editable: false },
                        TotalQAScore: { editable: false },
                        TimeSpentOfQA: { editable: false }

                    }
                }
            }
        });

        jQuery("#gridAAAssignment").kendoGrid({
            dataSource: dataSourceDUAssignment,
            pageable: true,
            toolbar: ["create", "cancel"],
            filterable: true,
            columns: [
                    {
                        field: "AssignEmployeeID", title: "Processor", editor: employeeListTaskDDEditor, template: "#=AssignEmployeeName#", width: "250px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrAAEmployeeList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            ,operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TicketNumber", title: "Ticket Number", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TaskDescription", title: "Description", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "DateReceived", title: "Date Received", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DueDate", title: "Due Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    {
                        field: "StatusDescription"
                        , title: "Status"
                        , width: "120px"
                        , filterable: false
                        , template: "# if(StatusDescription == 'Pending') {"
                                + "# <span style='background-color:red;color:white;'>Pending</span> #"
                                + "} else { "
                                + "# <span style='background-color:green;color:white;'>Completed</span> #"
                                + "} #"
                    },
                    {
                        field: "TimeSpentofProcessor", title: "Time Spent (Processor)", width: "200px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "QAByEmployeeName", title: "QA By", width: "250px", filterable: false },
                    { field: "NoOfDaysDelayed", title: "No. of Days Delayed", width: "150px", filterable: false },
                    { field: "Timeliness", title: "Timeliness", width: "120px", filterable: false },
                    { field: "CorrectData", title: "Correct Data", width: "120px", filterable: false },
                    { field: "Accuracy", title: "Accuracy", width: "120px", filterable: false },
                    { field: "TotalQAScore", title: "Total QA Score", width: "120px", filterable: false },
                    { field: "TimeSpentOfQA", title: "Time Spent of QA", width: "120px", filterable: false },

                    { command: { text: "Complete", click: completeStatus }, title: "", width: "100px" },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "250px", filterable: false }

            ],
            editable: "inline"
        });
        
    }
    function completeStatus(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        //alert(dataItem.DUAssignmentID);
        if (dataItem.StatusDescription == 'Completed') {
            jQuery.confirm({
                title: 'Daily Uploads Assignment',
                content: 'This Record is already Completed.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else if (dataItem.DUAssignmentID == '') {
            jQuery.confirm({
                title: 'Daily Uploads Assignment',
                content: 'This Record is not yet Saved in the Database!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else if (dataItem.StatusDescription == 'Pending') {
            //console.log(arrAAEmployeeList);
            wnd.content(detailsTemplate(dataItem));
            jQuery("#dropDownQAList").kendoDropDownList({
                dataSource: arrAAEmployeeList,
                dataTextField: "ListDescription",
                dataValueField: "ListID",
                change: onChange
            });
            document.getElementById('dateCompletionDate').valueAsDate = new Date();
            wnd.center().open();
        }
        else {
            jQuery.confirm({
                title: 'Adhoc Analytics Assignment',
                content: 'Invalid Status',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }

    }
    function onChange(e) {
        //  You can do with this also
        //   $("#skuCode").val($("#productName").data("kendoDropDownList").text());
        $("#qaByEmployeeName").val(e.sender.text());
        var x = $("#qaByEmployeeName").val();
    };

    $(document).on('click', '#btnComplete', function (e) {
        e.preventDefault();

        var assignmentID = $('#assignmentID').val();
        var timeSpent = $('#txtHourSpent').val();
        var qaBy = $('#dropDownQAList').val();
        var dateCompletion = $('#dateCompletionDate').val();
        var remarks = $('#txtremarks').val();
 
        if (timeSpent != '') {
            wnd.close();

            var param = new Object();
            var obj = new Object();
            var statusList = new Object();

            obj.DUAssignmentID = assignmentID;
            obj.EmployeeID = Employee_ID;
            obj.TimeSpent = timeSpent;
            obj.QAByEmployeeID = qaBy;
            obj.DateCompleted = dateCompletion;
            obj.TaskDescription = remarks;
            obj.ActivityDate = new Date();
            param.ActivityLog = obj;

            //console.log(param.ActivityLog);
            show('loading', true);

            callAjaxRequestJSON("CompleteStatusDUActivityLog"
                , param
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        show('loading', false);
                        jQuery.confirm({
                            title: 'Daily Uploads',
                            content: 'Database Successfully Modified.',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                close: function () {
                                    //window.location.reload();
                                    jQuery('#gridAAAssignment').data('kendoGrid').dataSource.read();
                                }
                            }
                        });
                    } else {
                        show('loading', false);
                        jQuery.confirm({
                            title: 'Daily Uploads',
                            content: 'Error in completing Daily Uploads - ' + data.ErrorMessage,
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                close: function () {
                                    //window.location.reload();
                                }
                            }
                        });
                    }
                }
                , ajaxError
            );
        }
        else {
            jQuery.confirm({
                title: 'Daily Uploads',
                content: 'Time Spent is Required!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
    });

    function aaTaskDDEditor(container, options) {
        programContainer = container;
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAActivityList
            });

    }

    function employeeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAEmployeeList
                , change: function (e) {

                    callAjaxRequestJSON("GetActivityList"
                        , { ActivityTypeID: ActivityType_AA, EmployeeID: this.value() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrAAActivityList = ro;
                                if (arrAAActivityList.length > 0) {
                                    programContainer.empty();
                                    var dd = jQuery("<input data-bind='value:ActivityID'/>")
                                        .appendTo(programContainer)
                                        .kendoDropDownList({
                                            dataTextField: "ListDescription"
                                            , dataValueField: "ListID"
                                            , change: function (e) {
                                                options.model.ActivityID = this.value();
                                            }
                                        }).data("kendoDropDownList");

                                    dd.setDataSource(arrAAActivityList);
                                    options.model.ActivityID = dd.value();
                                }
                                else
                                    alert("No items available for CS Assignment Activity List");

                            } else {
                                alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                }
            });
    }

    function aaPriorityDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAPriorityList
            });
    }

});
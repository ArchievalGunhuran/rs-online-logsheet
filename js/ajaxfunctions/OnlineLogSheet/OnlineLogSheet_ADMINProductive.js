﻿var activityLogFieldsList = new Array();

var arrAdminProductiveActivityLogs;
var arrAdminProductiveActivityList;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;

jQuery(document).ready(function () {

    jQuery("#txtAdminProductiveActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridAdminProductive').data('kendoGrid').dataSource.read(); }
    });

    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_ADMINPRODUCTIVE, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrAdminProductiveActivityList = ro;
                if (arrAdminProductiveActivityList.length > 0)
                    loadgridAdminProductive();
                else
                    bootbox.alert("No items from ADMIN - Task field");

            } else {
                bootbox.alert("Web service error when calling Admin - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    function loadgridAdminProductive() {

        var dataSourceAdmin = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                        , { ActivityTypeID: ActivityType_ADMINPRODUCTIVE, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtAdminProductiveActivityDate").val() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrAdminProductiveActivityLogs = ro;
                                // on success
                                e.success(arrAdminProductiveActivityLogs);
                            } else {
                                bootbox.alert("Web service error calling Admin GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                } //read function

            , create: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.ActivityTypeID = ActivityType_ADMINPRODUCTIVE;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.Remarks = e.data.Remarks;
                obj.TimeSpent = e.data.TimeSpent;
                obj.Numberoftransaction = e.data.Numberoftransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddAdminProductiveActivityLog"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrAdminProductiveActivityLogs = ro;
                                e.success(arrAdminProductiveActivityLogs);
                                jQuery('#gridAdminProductive').data('kendoGrid').dataSource.read();
                            } else {
                                bootbox.alert("Web service error when calling AddAdminProductiveActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

            } //create

            , update: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.ActivityTypeID = ActivityType_ADMINPRODUCTIVE;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.Remarks = e.data.Remarks;
                obj.TimeSpent = e.data.TimeSpent;
                obj.Numberoftransaction = e.data.Numberoftransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateAdminProductiveActivityLog"
                                , param
                                , function (response) {
                                    var data = response['d']
                                    var ro = data.ResponseItem;
                                    if (data.ErrorMessage == null) {
                                        arrAdminProductiveActivityLogs = ro;
                                        e.success(arrAdminProductiveActivityLogs);
                                        jQuery('#gridAdminProductive').data('kendoGrid').dataSource.read();
                                    } else {
                                        bootbox.alert("Web service error when calling UpdateAdminProductiveActivityLog. " + data.ErrorMessage);
                                    }
                                }
                                , ajaxError
                            );

            } //updates

            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_ADMINPRODUCTIVE;
                param.ActivityLogID = e.data.ActivityLogID;

                callAjaxRequestJSON("DeleteActivityLog"
                                , param
                                , function (response) {
                                    var data = response['d']
                                    var ro = data.ResponseItem;
                                    if (data.ErrorMessage == null) {
                                        e.success();
                                        jQuery('#gridAdminProductive').data('kendoGrid').dataSource.read();
                                    } else {
                                        bootbox.alert("Web service error when calling DeleteActivityLog. " + data.ErrorMessage);
                                    }
                                }
                                , ajaxError
                            );

                jQuery('#gridAdminProductive').data('kendoGrid').dataSource.read();
            } //delete

            }, //transaction
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDateDisplay: { type: "date" },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }
                            //, validation: {
                            //    validateDate: function (input) {
                            //        if (input.attr("data-bind") == "value:ActivityDate") {
                            //            input.attr("data-validateDate-msg", "ProcessDate can not be later than 4 days or greater than today");
                            //            return checkdate(input.val());
                            //        }
                            //        return true;
                            //    }
                            //}
                        },
                        ActivityID: { validation: { required: true }, defaultValue: arrAdminProductiveActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        //Numberoftransaction: { type: "number", validation: { min: 0 } },
                        Remarks: { validation: { maxLength: 250 } },
                        TimeSpent: { type: "number", validation: { min: 0.01, required: true} }
                    }
                }
            }
        });


        var grid = jQuery("#gridAdminProductive").kendoGrid({
            dataSource: dataSourceAdmin,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "ActivityID", title: "Task", editor: adminTaskDDEditor, template: "#=ActivityDescription#", width: "250px" },
                    { field: "Numberoftransaction", title: "Number of Transaction", format: "{0:0}", width: "150px" },
                    { field: "Remarks", width: "250px" },
                    { field: "TimeSpent", title: "Time Spent (HR)", format: "{0:0.00}", width: "120px" },
                    { command: ["edit",
                                { name: "Delete"
                                , click: function (e) {
                                    e.preventDefault();
                                    var tr = $(e.target).closest("tr");
                                    var data = this.dataItem(tr);
                                    
                                    bootbox.confirm("Are you sure to delete this record?", function (result) {
                                        if (result) {
                                            jQuery('#gridAdminProductive').data('kendoGrid').dataSource.remove(data);
                                            jQuery('#gridAdminProductive').data('kendoGrid').dataSource.sync();
                                            jQuery('#gridAdminProductive').data('kendoGrid').dataSource.read();
                                        }
                                    });
                                }
                                }
                    ], title: "&nbsp;"
                    }
                ],
            editable: "inline",
            save: function () {
                jQuery('#gridAdminProductive').data('kendoGrid').dataSource.read();
            }
        });

    }

    function adminTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAdminProductiveActivityList
            });
    }

});
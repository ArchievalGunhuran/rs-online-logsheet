﻿
var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2];
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {

    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/ActivityMaintenance.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

var arrActivityTypeList;
var arrActivity;
var arrEmployeeAssignment;
var arrEmployeeList;
var selectedActivityID = 0;
var selectedActivitySTID = 0;
var curDateTime = new Date();

jQuery(document).ready(function () {

    callAjaxRequestJSON("GetEmployeeList"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrEmployeeList = ro;
                
                if (arrEmployeeList.length > 0) {
                    getActivityList();
                }
                else
                    alert("No items from CS Assignment - Employee List");
            } else {
                alert("Web service error when calling CS Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    function getActivityList() {
        var usergroup = $('#hfUserGroup').val();
        //alert(usergroup);
        callAjaxRequestJSON("GetActivityTypeList"
            , { usergroup: usergroup }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrActivityTypeList = ro;
                    //alert(JSON.stringify(arrActivityTypeList));
                    if (arrActivityTypeList.length > 0) {
                        loadGridActivity();
                        loadGridEmployeeAssignment();
                    }
                    else
                        alert("No items from Activity Type field");
                } else {
                    alert("Web service error when calling Activity Maintenance getActivityList_GetActivityTypeList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridActivity() {

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    param.Activity = obj;

                    callAjaxRequestJSON("GetActivityMaintenanceTypes"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivity = ro;
                                //alert(JSON.stringify(arrActivity));
                                e.success(arrActivity);
                            } else {
                                alert("Web service error when calling Activity Maintenance loadGridActivity_GetActivityMaintenance. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
                , create: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    obj.ActivityTypeID = 0;
                    obj.ActivityTypeDescription = e.data.ActivityTypeDescription;
                    obj.UserGroup = e.data.UserGroup;

                    param.Activity = obj;

                    callAjaxRequestJSON("AddActivityMaintenanceTypes"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivity = ro;
                                // on success
                                arrActivity.push(e.data);
                                e.success(arrActivity);
                                jQuery('#gridActivityMaintenance').data('kendoGrid').dataSource.read();
                            } else {
                                alert(data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
                , update: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    obj.ActivityTypeID = e.data.ActivityTypeID;
                    obj.ActivityTypeDescription = e.data.ActivityTypeDescription;
                    obj.UserGroup = e.data.UserGroup;

                    param.Activity = obj;

                    callAjaxRequestJSON("UpdateActivityMaintenanceTypes"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivity = ro;
                                // on success
                                arrActivity.push(e.data);
                                e.success(arrActivity);
                                jQuery('#gridActivityMaintenance').data('kendoGrid').dataSource.read();
                            } else {
                                alert(data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
            }
            , error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 20,
            batch: false,
            schema: {
                model: {
                    id: "ActivityTypeID",
                    fields: {
                        ActivityTypeID: { editable: false, nullable: false },
                        ActivityTypeDescription: { validation: { required: true, maxLength: 50 } },
                        UserGroup: { validation: { required: true, maxLength: 5 } },
                      
                    }
                }
            }
        });

        jQuery("#gridActivityMaintenance").kendoGrid({
            dataSource: dataSource,
            pageable: true,
            selectable: "row",
            filterable: true,
            toolbar: ["create"],
            columns: [
                   {
                    field: "ActivityTypeDescription", title: "Activity Description", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                     },
                    {
                        field: "UserGroup", title: "User Group", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { command: ["edit"], title: "&nbsp;", width: "250px", filterable: false }
                ],
            editable: "inline"
        });

    }
});

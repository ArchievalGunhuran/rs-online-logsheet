﻿var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[1];
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

var activityLogFieldsList = new Array();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function ajaxError(response, error) {
    //hideloaders();
    alert("Request Error" + response.responseText);
}

function ajaxSuccess(response) {
    alert("success");
    alert(JSON.stringify(response));
    var obj = JSON.parse(response.responseText);
    alert(obj);
    alert("Request Successful");
}

//function initializeAdmin() {
//    callAjaxRequestJSON()
//}


jQuery(document).ready(function () {
    try {

        callAjaxRequestJSON("GetAdminActivityLogFields", {}
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    activityLogFieldsList = ro;
                    alert(activityLogFieldsList.length);
                } else {
                    bootbox.alert("Web service error getting testimonials. " + data.ErrorMessage);
                }
            }

            , ajaxError);


        jQuery("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
                    , select: function (e) {
                        alert(jQuery(e.item).index());
                    }
        });

        var col = [];

        col.push("ProductID");
        col.push("ProductName");
        col.push("UnitPrice");
        col.push("Discontinued");
        col.push("UnitsInStock");

        //                                for (i = 0; i < col.length; i++) {
        //                                    alert(col[i]);
        //                                }

        var arrModel = {};

        for (i = 0; i < col.length; i++) {
            if (col[i] == "ProductID")
                arrModel[col[i]] = { editable: false, nullable: true };
            if (col[i] == "ProductName") {
                arrModel[col[i]] = { validation: { required: true
                                                            , maxLength: 10
                                                            , validateEmailFormat: function (input) {
                                                                if (input.attr("data-bind") == "value:ProductName") {
                                                                    input.attr("data-validateEmailFormat-msg", "Invalid format. HH:MM AM/PM");
                                                                    return validateEmail(input.val());
                                                                }
                                                                return true;
                                                            }

                }
                };
            }
            if (col[i] == "UnitPrice")
                arrModel[col[i]] = { type: "number", validation: { required: true, min: 1} };
            if (col[i] == "Discontinued")
                arrModel[col[i]] = { type: "boolean" };
            if (col[i] == "UnitsInStock")
                arrModel[col[i]] = { type: "number", validation: { min: 0, required: true} };
        }

        function validateEmail(email) {
            var re = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):?[0-5][0-9]$/;
            var res = re.test(email);
            return res;
        }

        // var arrColumn = [{ field: "ProductID", title: "Product ID", hidden: true },
        // { field: "ProductName", title: "Product Name" },
        // { field: "UnitPrice", title: "Unit Price", format: "{0:c}" }, //, width: "120px" },
        // {field: "UnitsInStock", title: "Units In Stock" }, //width: "120px" },
        // {field: "Discontinued" }, //width: "120px" },
        // {command: ["edit", "destroy"], title: "&nbsp;"}];

        var arrColumn = [];
        arrColumn.push({ field: "ProductID", title: "Product ID", hidden: true });
        arrColumn.push({ field: "ProductName", title: "Product Name" });
        arrColumn.push({ field: "UnitPrice", title: "Unit Price", format: "{0:c}" });
        arrColumn.push({ field: "UnitsInStock", title: "Units In Stock" });
        arrColumn.push({ field: "Discontinued" });
        arrColumn.push({ command: ["edit", "destroy"], title: "&nbsp;" });

        var crudServiceBaseUrl = "//demos.telerik.com/kendo-ui/service",
                        dataSource = new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: crudServiceBaseUrl + "/Products",
                                    dataType: "jsonp"
                                },
                                update: {
                                    url: crudServiceBaseUrl + "/Products/Update",
                                    dataType: "jsonp"
                                },
                                destroy: {
                                    url: crudServiceBaseUrl + "/Products/Destroy",
                                    dataType: "jsonp"
                                },
                                create: {
                                    url: crudServiceBaseUrl + "/Products/Create",
                                    dataType: "jsonp"
                                },
                                parameterMap: function (options, operation) {
                                    if (operation !== "read" && options.models) {
                                        return { models: kendo.stringify(options.models) };
                                    }
                                }
                            },
                            batch: true,
                            //pageSize: 20,
                            schema: {
                                model: {
                                    id: "ProductID",
                                    fields: //{
                                        arrModel
                                    //  ProductID: { editable: false, nullable: true },
                                    //  ProductName: { validation: { required: true, maxLength: 10
                                    //              , validateEmailFormat: function (input) {
                                    //                  if (input.attr("data-bind") == "value:ProductName") {
                                    //                      input.attr("data-validateEmailFormat-msg", "Invalid format. HH:MM AM/PM");
                                    //                      return validateEmail(input.val());
                                    //                  }
                                    //                  return true;
                                    //              } //validateEmailFormat
                                    //      }
                                    //  },
                                    //  UnitPrice: { type: "number", validation: { required: true, min: 1} },
                                    //  Discontinued: { type: "boolean" },
                                    //  UnitsInStock: { type: "number", validation: { min: 0, required: true} }
                                    //}
                                }
                            }
                        });

        jQuery("#gridAdmin").kendoGrid({
            dataSource: dataSource,
            pageable: false,
            //height: 550,
            toolbar: ["create"],
            columns: arrColumn,
            //                [
            //                            { field: "ProductID", title: "Product ID", hidden: true },
            //                            { field: "ProductName", title: "Product Name" },
            //                            { field: "UnitPrice", title: "Unit Price", format: "{0:c}" }, //, width: "120px" },
            //                            {field: "UnitsInStock", title: "Units In Stock" }, //width: "120px" },
            //                            {field: "Discontinued" }, //width: "120px" },
            //                            {command: ["edit", "destroy"], title: "&nbsp;"}], //, width: "250px"}],
            editable: "inline"

        });
    }
    catch (err) {
        alert(err.Description + "/" + err.Message + "/" + err.Source);
    }
});
﻿var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[1];
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

var activityLogFieldsList = new Array();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function ajaxError(response, error) {
    //hideloaders();al
    alert("Request Error" + response.responseText);
}

function ajaxSuccess(response) {
    alert("success");
    alert(JSON.stringify(response));
    var obj = JSON.parse(response.responseText);
    alert(obj);
    alert("Request Successful");
}

//function initializeAdmin() {
//    callAjaxRequestJSON()
//}


jQuery(document).ready(function () {
    try {

        jQuery("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
                    , select: function (e) {
                        alert(jQuery(e.item).index());
                    }
        });

        dataSource = new kendo.data.DataSource({
            transport: {
                read: function (options) {
                    jQuery.ajax({
                        type: "POST",
                        url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet.asmx/GetAdminActivityLog",
                        contentType: "application/json; charset=utf-8",
                        dataType: 'json',
                        data: JSON.stringify({}),
                        success: function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                options.success(ro);
                            } else {
                                return bootbox.alert("Web service error on GetAdminActivityLog. " + data.ErrorMessage);
                            }
                        }
                    });
                }

            },
            batch: true,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false }
                        , ActivityLogID: { editable: false }
                        , EmployeeID: { editable: false }
                        , ActivityDate: { validation: { required: true} }
                        , ActivityDescription: { defaultValue: { ListID: 0, ListDescription: ""} }
                        , Remarks: { validation: { maxLength: 255} }
                        , TimeSpent: { type: "number", validation: { required: true, min: 1} }
                    }
                }
            }
        });

        jQuery("#gridAdmin").kendoGrid({
            dataSource: dataSource
            , pageable: false
            , toolbar: ["create"]
            , columns: [
                "ActivityDate"
                //, "ActivityDescription"
                , { field: "ActivityDescription", editor: categoryDropDownEditor, template: "#=ActivityDescription.ListDescription#" }
                , "Remarks"
                , "TimeSpent"
                , { command: ["destroy"], title: "&nbsp;", width: "150px" }
            ]
            , editable: true

        });

        function categoryDropDownEditor(container, options) {
            var Category =
                         [
                                    { 'ListDescription': 'Beverages', 'ListID': 1 },
                                    { 'ListDescription': 'Condiments', 'ListID': 2 },
                                    { 'ListDescription': 'Confections', 'ListID': 3 },
                                    { 'ListDescription': 'Dairy Products', 'ListID': 4 },
                                    { 'ListDescription': 'Grains/Cereals', 'ListID': 5 },
                                    { 'ListDescription': 'Meat/Poultry', 'ListID': 6 },
                                    { 'ListDescription': 'Produce', 'ListID': 7 },
                                    { 'ListDescription': 'Seafood', 'ListID': 8 }
                        ];

            $('<input required data-text-field="ListDescription" data-value-field="ListID" data-bind="value:' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataSource: Category
                        });
        }

    }
    catch (err) {
        alert(err.Description + "/" + err.Message + "/" + err.Source);
    }
});
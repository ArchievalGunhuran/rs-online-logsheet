﻿
var arrAAAssignment;
var arrAAActivityList;
var arrAAPriorityList;
var arrAAEmployeeList;

jQuery(document).ready(function () {
    var detailsTemplate;

    var wnd = jQuery("#wnd").kendoWindow({
        title: "QA for Adhoc Analytics Assignment",
        height: 750,
        width: 700,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    detailsTemplate = kendo.template(jQuery("#template").html());

    function show(id, value) {
        document.getElementById(id).style.display = value ? 'block' : 'none';
    }

    callAjaxRequestJSON("GetEmployeeListAdhoc"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrAAEmployeeList = ro;
                if (arrAAEmployeeList.length > 0) {
                    getActivityList();
                }
                else {
                    alert("No items available for Adhoc Analytics Assignment Employee List");
                }
            } else {
                alert("Web service error when calling Adhoc Analytics Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );


    function getActivityList() {

        callAjaxRequestJSON("GetActivityList"
            , { ActivityTypeID: ActivityType_AA, EmployeeID: arrAAEmployeeList[0].ListID }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrAAActivityList = ro;
                    if (arrAAActivityList.length > 0)
                        getPriorityList();
                    else
                        alert("No items available for Adhoc Analytics Assignment Activity List");

                } else {
                    alert("Web service error when calling Adhoc Analytics Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function getPriorityList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_AA, ListTypeID: 2 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrAAPriorityList = ro;
                    if (arrAAPriorityList.length > 0)
                        loadAAAssignment();
                    else
                        alert("No items available for Adhoc Analytics Assignment Priority List");

                } else {
                    alert("Web service error when calling Adhoc Analytics - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadAAAssignment() {

        var dataSourceAAAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetAAAssignmentForQA"
                    , { qaByEmployeeID: Employee_ID }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAAAssignment = ro;
                            e.success(arrAAAssignment);
                        } else {
                            alert("Web service error when calling loadAAAssignment_GetCSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                }
            }
            , 
            error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "AAAssignmentID",
                    fields: {
                        AAAssignmentID: { editable: false, nullable: false },
                        AssignEmployeeID: { validation: { required: true }, defaultValue: arrAAEmployeeList[0].ListID },
                        AssignEmployeeName: { editable: false },
                        TicketNumber: { editable: false },
                        ActivityID: { defaultValue: arrAAActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        PriorityListID: { defaultValue: arrAAPriorityList[0].ListID },
                        PriorityListDescription: { editable: false },
                        TaskDescription: { validation: { required: true, maxLength: 250 } },
                        RequestedBy: { validation: { required: true, maxLength: 50 } },
                        EmailAddressofRequestor: { validation: { required: true } },
                        DateReceived: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DueDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DateCompleted: { type: "date", defaultValue: "", editable: false },
                        SLA: { editable: false },
                        StatusDescription: { editable: false },
                        TimeSpentofProcessor: { editable: false },

                        QAByEmployeeName: { editable: false },
                        NoOfDaysDelayed: { editable: false },
                        Timeliness: { editable: false },
                        CorrectData: { editable: false },
                        MeetTheCriteria: { editable: false },
                        Accuracy: { editable: false },
                        TotalQAScore: { editable: false },
                        TimeSpentOfQA: { editable: false },
                        QAStatus: { editable: false }

                    }
                }
            }
        });

        jQuery("#gridAAAssignment").kendoGrid({
            dataSource: dataSourceAAAssignment,
            pageable: true,
            //toolbar: ["create", "cancel"],
            filterable: true,
            columns: [
                    {
                        field: "AssignEmployeeName", title: "Processor", width: "200px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TicketNumber", title: "Ticket Number", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "PriorityListID", title: "Priority", editor: aaPriorityDDEditor, width: "250px", template: "#=PriorityListDescription#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrAAPriorityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ActivityID", title: "Type", editor: aaTaskDDEditor, width: "250px", template: "#=ActivityDescription#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrAAActivityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TaskDescription", title: "Description", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "RequestedBy", title: "Requested By", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "DateReceived", title: "Date Received", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DueDate", title: "Due Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    { field: "StatusDescription", title: "Status", width: "120px", filterable: false },
                    {
                        field: "TimeSpentofProcessor", title: "Time Spent (Processor)", width: "200px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "QAByEmployeeName", title: "QA By", width: "150px", filterable: false },
                    { field: "NoOfDaysDelayed", title: "No. of Days Delayed", width: "150px", filterable: false },
                    { field: "Timeliness", title: "Timeliness", width: "120px", filterable: false },
                    { field: "CorrectData", title: "Correct Data", width: "120px", filterable: false },
                    { field: "MeetTheCriteria", title: "Meet the Criteria", width: "120px", filterable: false },
                    { field: "Accuracy", title: "Accuracy", width: "120px", filterable: false },
                    { field: "TotalQAScore", title: "Total QA Score", width: "120px", filterable: false },
                    { field: "TimeSpentOfQA", title: "Time Spent of QA", width: "120px", filterable: false },
                    { field: "QAStatus", title: "QA Status", width: "120px", filterable: false },

                    { command: { text: "Add Score", click: completeStatus }, title: "", width: "200px" },

            ],
            editable: "inline"
        });

    }
    function completeStatus(e) {
        e.preventDefault();

        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

        if (dataItem.QAStatus == 'COMPLETED') {
            jQuery.confirm({
                title: 'Adhoc Analytics Assignment',
                content: 'This Record is already Completed.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else {
            var daysDelayed = dataItem.NoOfDaysDelayed;
            var timeliness = (100 - (daysDelayed * 15)) * 0.5;
            dataItem.vDaysDelayed = daysDelayed;
            dataItem.vTimeliness = timeliness.toFixed(2);


            wnd.content(detailsTemplate(dataItem));
            wnd.center().open();

        }

    }

    $(document).on('click', '#btnComplete', function (e) {
        e.preventDefault();
        var aaassignmentID = $('#assignmentID').val();
        //alert(DaysDelayed);

        var daysdelayed = $('#txtDaysDelayed').val();
        var timeliness = $('#txtTimeliness').val();
        var correctData = $('#txtCorrectData').val();
        var meetTheCriteria = $('#txtMeetTheCriteria').val();
        var accuracy = $('#txtAccuracy').val();
        var totalQA = $('#txtTotalQA').val();
        var timeSpent = $('#txtTimeSpent').val();

        var timeSpentOfProcessor = $('#timeSpentOfProcessor').val();
        var ticketNumber = $('#ticketNumber').val();

        
        
        if (daysdelayed == '' || timeliness == '' || correctData == '' || meetTheCriteria == '' || accuracy == '' || totalQA == '' || timeSpent == '') {
            jQuery.confirm({
                title: 'Adhoc Analytics Assignment',
                content: 'All Fields are Required',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else {
            wnd.close();
            tempStartDateTime = new Date();
            var param = new Object();
            var obj = new Object();

            obj.AAAssignmentID = aaassignmentID;

            obj.NoOfDaysDelayed = daysdelayed;
            obj.Timeliness = timeliness;
            obj.CorrectData = correctData;
            obj.MeetTheCriteria = meetTheCriteria;
            obj.Accuracy = accuracy;
            obj.TotalQAScore = totalQA;
            obj.TimeSpentOfQA = timeSpent;

            obj.TotalQAScore = totalQA;

            obj.TimeSpentOfQA = timeSpent;
            obj.TicketNumber = ticketNumber;
            obj.TimeSpentofProcessor = timeSpentOfProcessor;

            param.ActivityLog = obj;

            show('loading', true);

            callAjaxRequestJSON("AddQAForAdhocAnalyticsAssignment"
                , param
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        show('loading', false);
                        jQuery.confirm({
                            title: 'Adhoc Analytics Assignment',
                            content: 'Database Successfully Modified.',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                close: function () {
                                    window.location.reload();
                                }
                            }
                        });
                        loadAAAssignment();
                    } else {
                        show('loading', false);
                        bootbox.alert("Web service error Add QA for AdhocAnalytics Assignment. " + data.ErrorMessage);
                    }
                }
                , ajaxError
            );
        }
    });

    function aaTaskDDEditor(container, options) {
        programContainer = container;
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAActivityList
            });

    }

    function employeeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAEmployeeList
                , change: function (e) {

                    callAjaxRequestJSON("GetActivityList"
                        , { ActivityTypeID: ActivityType_AA, EmployeeID: this.value() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrAAActivityList = ro;
                                if (arrAAActivityList.length > 0) {
                                    programContainer.empty();
                                    var dd = jQuery("<input data-bind='value:ActivityID'/>")
                                        .appendTo(programContainer)
                                        .kendoDropDownList({
                                            dataTextField: "ListDescription"
                                            , dataValueField: "ListID"
                                            , change: function (e) {
                                                options.model.ActivityID = this.value();
                                            }
                                        }).data("kendoDropDownList");

                                    dd.setDataSource(arrAAActivityList);
                                    options.model.ActivityID = dd.value();
                                }
                                else
                                    alert("No items available for CS Assignment Activity List");

                            } else {
                                alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                }
            });
    }

    function aaPriorityDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAPriorityList
            });
    }

});
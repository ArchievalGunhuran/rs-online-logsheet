﻿
var activityLogFieldsList = new Array();

var arrEFSActivityLogs;
var arrEFSActivityList;
var arrEFSStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var curDateTime = new Date();

jQuery(document).ready(function () {

    jQuery("#txtEFSActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridEFS').data('kendoGrid').dataSource.read(); }
    });


    //GET TASK LIST FOR EFS
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_EFS, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrEFSActivityList = ro;
                if (arrEFSActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from EFS - Task field");
            } else {
                bootbox.alert("Web service error when calling EFS - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM PROJECT
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_EFS, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrEFSStatus = ro;
                    loadGridEFS();
                } else {
                    bootbox.alert("Web service error when calling EFS - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridEFS() {

        var dataSourceEFS = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                        , { ActivityTypeID: ActivityType_EFS, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtEFSActivityDate").val() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEFSActivityLogs = ro;
                                // on success
                                e.success(arrEFSActivityLogs);
                            } else {
                                bootbox.alert("Web service error when calling EFS - GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var objAcct = new Object();
                var status = new Object();

                objAcct.Username = jQuery("#hfUsername").val();
                objAcct.Password = sessionStorage.getItem("UserPassword"); //jQuery("#hfPassword").val();
                objAcct.EmailAddress = jQuery("#hfEmailAddress").val();

                obj.ActivityTypeID = ActivityType_EFS;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.StoreName = e.data.StoreName;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;
                obj.WorkOrderNumber = e.data.WorkOrderNumber;
                obj.RequestMade = e.data.RequestMade;
                obj.DateTimeCompleted = new Date();
                obj.Remarks = e.data.Remarks;
                obj.StatusID = e.data.StatusID;
                obj.StatusDescription = e.data.StatusDescription;
                obj.TimeSpent = e.data.TimeSpent;
                obj.DateTimeToFollowUp = e.data.DateTimeToFollowUp;
                //obj.TargetDateTime = e.data.TargetDateTime;

                param.ActivityLog = obj;
                param.Account = objAcct;

                var gridToLoad = jQuery('#gridEFS');
                kendo.ui.progress(gridToLoad, true);

                callAjaxRequestJSON("AddEFSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrEFSActivityLogs = ro;
                            // on success
                            arrEFSActivityLogs.push(e.data);
                            e.success(arrEFSActivityLogs);
                            jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                            jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling EFS AddEFSActivityLog. " + data.ErrorMessage);
                        }
                        kendo.ui.progress(gridToLoad, false);
                    }
                    , function (response) {
                        ajaxError();
                        kendo.ui.progress(gridToLoad, false);
                    }
                );


                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_EFS;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.StoreName = e.data.StoreName;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;
                obj.WorkOrderNumber = e.data.WorkOrderNumber;
                obj.RequestMade = e.data.RequestMade;
                obj.DateTimeCompleted = new Date();
                obj.Remarks = e.data.Remarks;
                obj.StatusID = e.data.StatusID;
                obj.StatusDescription = e.data.StatusDescription;
                obj.TimeSpent = e.data.TimeSpent;
                obj.DateTimeToFollowUp = e.data.DateTimeToFollowUp;
                //obj.TargetDateTime = e.data.TargetDateTime;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateEFSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrEFSActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrEFSActivityLogs);
                            jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                            jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling EFS UpdateEFSActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_EFS;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                                    jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();
                                } else {
                                    bootbox.alert("Web service error when calling EFS - DeleteActivityLog. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrEFSActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        StoreName: { validation: { required: true, maxLength: 250} },
                        DateTimeRequested: { type: "date", defaultValue: function (e) { return new Date(); } },
                        Requestor: { validation: { maxLength: 250} },
                        WorkOrderNumber: { type: "number", validation: { min: 0, maxLength: 7} },
                        RequestMade: { validation: { maxLength: 250} },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        Remarks: { validation: { maxLength: 250} },
                        StatusID: { defaultValue: arrEFSStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number" },
                        TimeSpentStr: { editable: false },
                        FollowUpBy: { editable: false },
                        FollowUpDateTime: { editable: false },
                        FollowUpStatusList: { editable: false },
                        DateTimeToFollowUp: { type: "date", defaultValue: function (e) { return new Date(); } },
                        //TargetDateTime: { type: "date", defaultValue: function (e) { return new Date(); } },
                        SLA: { editable: false },
                    }
                }
            }
        });

        jQuery("#gridEFS").kendoGrid({
            dataSource: dataSourceEFS,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "ActivityID", title: "Type of Controller", editor: efsTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Requestor", title: "Requestor", width: "250px" },
                    { field: "WorkOrderNumber", title: "Work Order Number", format: "{0:0000000}", width: "120px" },
                    { field: "RequestMade", title: "Request Made", width: "250px" },
                    //{ field: "TargetDateTime", title: "Target Date Time", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "SLA", title: "SLA", width: "100px" },
                    { field: "Remarks", title: "Remarks", width: "250px" },
                    { field: "StatusID", title: "1st Touch Status", editor: efsStatusDDEditor, width: "250px", template: "#=StatusDescription#" },
                    { field: "DateTimeToFollowUp", title: "Date Time to Follow Up", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "FollowUpBy", title: "Follow Up By", width: "200px" },
                    { field: "FollowUpDateTime", title: "Follow Up Date & Time", width: "150px" },
                    { field: "FollowUpStatusList", title: "Follow Up Status", width: "150px" },
                    //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                    {command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridEFS').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridEFS').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline",
            edit: function (e) {
                if (!e.model.isNew()) {
                    var select = e.container.find('input[data-bind="value:StatusID"]').data('kendoDropDownList');
                    select.enable(false);
                }
            }

        });

        ///////////////////////////////////////////////// PENDING ITEMS /////////////////////////////////////////////////////

        var dataSourceEFSPending = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetPendingActivity"
                        , { ActivityTypeID: ActivityType_EFS, EmployeeID: Employee_ID }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEFSPending = ro;
                                // on success
                                e.success(arrEFSPending);
                            } else {
                                bootbox.alert("Web service error when calling EFS - GetPendingActivity. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
                , update: function (e) {

                    var param = new Object();
                    var obj = new Object();
                    var objAcct = new Object();
                    var status = new Object();

                    objAcct.Username = jQuery("#hfUsername").val();
                    objAcct.Password = sessionStorage.getItem("UserPassword"); //jQuery("#hfPassword").val();
                    objAcct.EmailAddress = jQuery("#hfEmailAddress").val();

                    obj.ActivityTypeID = ActivityType_EFS;
                    obj.LogDetailID = e.data.LogDetailID;
                    obj.ActivityLogID = e.data.ActivityLogID;
                    obj.EmployeeID = Employee_ID;
                    obj.ActivityDate = tempStartDateTime;
                    obj.ActivityID = e.data.ActivityID;

                    obj.StoreName = e.data.StoreName;
                    obj.DateTimeRequested = e.data.DateTimeRequested;
                    obj.Requestor = e.data.Requestor;
                    obj.WorkOrderNumber = e.data.WorkOrderNumber;
                    obj.RequestMade = e.data.RequestMade;
                    obj.DateTimeCompleted = new Date();
                    obj.Remarks = e.data.Remarks;
                    obj.StatusID = e.data.StatusID;
                    obj.StatusDescription = e.data.StatusDescription;
                    obj.DateTimeToFollowUp = e.data.DateTimeToFollowUp;

                    param.ActivityLog = obj;
                    param.Account = objAcct;

                    callAjaxRequestJSON("UpdateEFSFollowUp"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEFSPending = ro;

                                e.success(arrEFSPending);
                                jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                                jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();
                            } else {
                                bootbox.alert("Web service error when calliung EFS - UpdateEFSFollowUp. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
            } //transport
            ,
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },

            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrEFSActivityList[0].ListID, editable: false },
                        ActivityDescription: { editable: false },
                        StoreName: { validation: { required: true, maxLength: 250 }, editable: false },
                        DateTimeRequested: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        Requestor: { validation: { maxLength: 250 }, editable: false },
                        WorkOrderNumber: { type: "number", validation: { min: 0 }, editable: false },
                        RequestMade: { validation: { maxLength: 250 }, editable: false },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        Remarks: { validation: { maxLength: 250 }, editable: false },
                        StatusID: { defaultValue: arrEFSStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", editable: false },
                        TimeSpentStr: { editable: false },
                        FollowUpBy: { editable: false },
                        DateTimeToFollowUp: { type: "date", defaultValue: function (e) { return new Date(); }
                            , validation: {
                                validateDateTimeToFollow: function (input) {
                                    if (input.attr("data-bind") == "value:StatusID") {
                                        selectedStatus = input.val();
                                    }
                                    if (input.attr("data-bind") == "value:DateTimeToFollowUp") {
                                        selectedDateTimeToFollowUp = input.val()
                                        input.attr("data-validateDateTimeToFollow-msg", "Date Time To Follow is required.");
                                        //return checkdate(input.val());
                                        if (selectedStatus == 22 && selectedDateTimeToFollowUp == "")
                                            return false;
                                        else
                                            return true;
                                    }
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        });


        jQuery("#gridEFSPending").kendoGrid({
            dataSource: dataSourceEFSPending,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "ActivityID", title: "Type of Controller", editor: efsTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Requestor", title: "Requestor", width: "250px" },
                    { field: "WorkOrderNumber", title: "Work Order Number", format: "{0:0000000}", width: "120px" },
                    { field: "RequestMade", title: "Request Made", width: "250px" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Remarks", title: "Remarks", width: "250px" },
                    { field: "StatusID", title: "1st Touch Status", editor: efsStatusDDEditor, width: "250px", template: "#=StatusDescription#" },
                    { field: "DateTimeToFollowUp", title: "Date Time to Follow Up", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "FollowUpBy", title: "Follow Up By", width: "200px" },
                    { command: ["edit"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline",
            edit: function (e) {
                tempStartDateTime = new Date();
                e.model.DateTimeToFollowUp = new Date();
            },
            save: function (e) {
                e.model.dirty = true;
            }
        });

        jQuery(".k-grid-toolbar", "#gridEFSPending").prepend("<h4>FOR FOLLOW UP</h4>");

    }

    function efsTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEFSActivityList
            });
    }

    function efsStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEFSStatus
            });
    }

});
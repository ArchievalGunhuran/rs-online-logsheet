﻿
var activityLogFieldsList = new Array();

var arrCSActivityLogs;
var arrCSAssigned;
var arrCSActivityList;
var arrCSStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var tempStartDateTime = new Date();


jQuery(document).ready(function () {

    jQuery("#txtCSActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridCSAssigned').data('kendoGrid').dataSource.read(); jQuery('#gridCS').data('kendoGrid').dataSource.read(); }
    });


    //GET LIST - TYPE OF PROGRAM
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_CS, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrCSActivityList = ro;
                if (arrCSActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from CS - Type of Program field");
            } else {
                bootbox.alert("Web service error when calling CS - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_CS, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrCSStatus = ro;
                    loadGridCS();
                } else {
                    bootbox.alert("Web service error when calling CS - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridCS() {

        var dataSourceCS = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                    , { ActivityTypeID: ActivityType_CS, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtCSActivityDate").val() }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrCSActivityLogs = ro;
                            e.success(arrCSActivityLogs);
                        } else {
                            bootbox.alert("Web service error getting GetActivityLog on loadGridCS. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var statusList = new Object();

                obj.ActivityTypeID = ActivityType_CS;
                obj.EmployeeID = Employee_ID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.TimeSpent = e.data.TimeSpent;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateCSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrCSAssigned = ro;
                            e.success(arrCSAssigned);
                            jQuery('#gridCSAssigned').data('kendoGrid').dataSource.read();
                            jQuery('#gridCS').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error AddCSActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_CS;
                param.ActivityLogID = e.data.ActivityLogID;

                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridCS').data('kendoGrid').dataSource.read();
                                    jQuery('#gridCSAssigned').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error getting testimonials. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", editable: false, defaultValue: function (e) { return new Date(); } },

                        StoreName: { editable: false },
                        StoreNumber: { editable: false },
                        ActivityID: { editable: false },
                        ActivityDescription: { editable: false },
                        TicketNumber: { editable: false },
                        DueDate: { type: "date", editable: false },
                        StatusID: { editable: false },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", defaultValue: 0, validation: { min: 0.01, required: true} },
                        TotalTimeSpent: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridCS").kendoGrid({
            dataSource: dataSourceCS,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "StoreNumber", title: "Store Number", width: "150px" },
                    { field: "ActivityID", title: "Type Of Program", editor: csTaskDDEditor, template: "#=ActivityDescription#", width: "250px" },
                    { field: "TicketNumber", title: "Ticket Number", width: "150px" },
                    { field: "DueDate", title: "Due Date", width: "150px", format: "{0:MM/dd/yyyy}" },
                    { field: "StatusID", title: "Status", editor: csStatusDDEditor, template: "#=StatusDescription#", width: "200px" },
                    { field: "TotalTimeSpent", title: "Total Time Spent", format: "{0:00:00}", width: "150px" },
                    { field: "TimeSpent", title: "Time Spent (Hours)", format: "{0:0.00}", width: "150px" },
                    //{ command: ["destroy"], title: "&nbsp;", width: "250px" }
                    {command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridCS').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridCS').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridCS').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline"

        });
        /////////////////////////////// PENDING //////////////////////////////////////

        var dataSourceCSAssigned = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetAssignedCS"
                    , { EmployeeID: Employee_ID }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrCSAssigned = ro;
                            // on success
                            //alert(JSON.stringify(arrCSAssigned));
                            e.success(arrCSAssigned);
                        } else {
                            bootbox.alert("Web service error getting GetPendingActivity on loadGridCS. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var statusList = new Object();

                obj.ActivityTypeID = ActivityType_CS;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = tempStartDateTime;
                obj.CSAssignmentID = e.data.CSAssignmentID;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddCSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrCSAssigned = ro;
                            e.success(arrCSAssigned);
                            jQuery('#gridCSAssigned').data('kendoGrid').dataSource.read();
                            jQuery('#gridCS').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error AddCSActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "CSAssignmentID",
                    fields: {
                        CSAssignmentID: { editable: false, nullable: false },
                        StoreName: { editable: false },
                        StoreNumber: { editable: false },
                        ActivityID: { editable: false },
                        ActivityDescription: { editable: false },
                        TicketNumber: { editable: false },
                        DueDate: { type:"date", editable: false },
                        StatusID: {
                            defaultValue: arrCSStatus[0].ListID
                            , validation: {
                                validateStatus: function (input) {
                                    if (input.attr("data-bind") == "value:StatusID") {
                                        selectedStatus = input.val();
                                        input.attr("data-validateStatus-msg", "Status is required");
                                        if (selectedStatus < 1) {
                                            bootbox.alert("status is required");
                                            return false;
                                        }
                                        else {
                                            return true;
                                        }
                                    }
                                    return true;
                                }
                            }
                        },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", defaultValue: 0.00, validation: { min: 0.01, required: true} },
                        TotalTimeSpent: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridCSAssigned").kendoGrid({
            dataSource: dataSourceCSAssigned,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "StoreNumber", title: "Store Number",  width: "150px" },
                    { field: "ActivityID", title: "Type Of Program", editor: csTaskDDEditor, template: "#=ActivityDescription#", width: "250px" },
                    { field: "TicketNumber", title: "Ticket Number", width: "150px" },
                    { field: "DueDate", title: "Due Date", width: "150px", format: "{0:MM/dd/yyyy}" },
                    { field: "StatusID", title: "Status", editor: csStatusDDEditor, template: "#=StatusDescription#", width: "200px" },
                    { field: "TotalTimeSpent", title: "Total Time Spent", format: "{0:00:00}", width: "150px" },
                    { field: "TimeSpent", title: "Time Spent (Hours)", format: "{0:0.00}", width: "150px" },
                    { command: ["edit"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline",
            edit: function (e) {
                tempStartDateTime = new Date();
                //alert(tempStartDateTime);
            }
        });

        jQuery(".k-grid-toolbar", "#gridCSAssigned").prepend("<h4>ASSIGNED CS</h4>");
        jQuery(".k-grid-toolbar", "#gridCS").prepend("<h4>ACTIVITY LOG</h4>");

    }

    function csTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrCSActivityList
            });
    }

    function csStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrCSStatus
            });
    }

});
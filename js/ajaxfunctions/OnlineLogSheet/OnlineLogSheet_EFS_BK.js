﻿
var activityLogFieldsList = new Array();

var arrEFSActivityLogs;
var arrEFSActivityList;
var arrEFSStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var curDateTime = new Date();

jQuery(document).ready(function () {

    jQuery("#txtEFSActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridEFS').data('kendoGrid').dataSource.read(); }
    });


    //GET TASK LIST FOR EFS
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_EFS, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrEFSActivityList = ro;
                if (arrEFSActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from EFS - Task field");
            } else {
                bootbox.alert("Web service error when calling EFS - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM PROJECT
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_EFS, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrEFSStatus = ro;
                    loadGridEFS();
                    var dhtml = "";
                    jQuery("#select1stTouchStatus").empty();
                    for (var i = 0; i < arrEFSStatus.length; i++) {
                        dhtml = dhtml + "<option value='" + arrEFSStatus[i].ListID + "'>" + arrEFSStatus[i].ListDescription + "</option>";
                    }
                    jQuery("#select1stTouchStatus").append(dhtml);
                } else {
                    bootbox.alert("Web service error when calling EFS - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridEFS() {

        var dataSourceEFS = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                        , { ActivityTypeID: ActivityType_EFS, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtEFSActivityDate").val() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEFSActivityLogs = ro;
                                // on success
                                e.success(arrEFSActivityLogs);
                            } else {
                                bootbox.alert("Web service error when calling EFS - GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                    var param = new Object();
                    var obj = new Object();
                    var objAcct = new Object();
                    var status = new Object();

                    objAcct.Username = jQuery("#hfUsername").val();
                    objAcct.Password = sessionStorage.getItem("UserPassword"); //jQuery("#hfPassword").val();
                    objAcct.EmailAddress = jQuery("#hfEmailAddress").val();

                    obj.ActivityTypeID = ActivityType_EFS;
                    obj.LogDetailID = 0;
                    obj.ActivityLogID = 0;
                    obj.EmployeeID = Employee_ID;
                    obj.ActivityDate = e.data.ActivityDate;
                    obj.ActivityID = e.data.ActivityID;

                    obj.StoreName = e.data.StoreName;
                    obj.DateTimeRequested = e.data.DateTimeRequested;
                    obj.Requestor = e.data.Requestor;
                    obj.WorkOrderNumber = e.data.WorkOrderNumber;
                    obj.RequestMade = e.data.RequestMade;
                    obj.DateTimeCompleted = new Date();
                    obj.Remarks = e.data.Remarks;
                    obj.StatusID = e.data.StatusID;
                    obj.StatusDescription = e.data.StatusDescription;
                    obj.TimeSpent = e.data.TimeSpent;
                    obj.DateTimeToFollowUp = e.data.DateTimeToFollowUp;

                    param.ActivityLog = obj;
                    param.Account = objAcct;

                    var gridToLoad = jQuery('#gridEFS');
                    kendo.ui.progress(gridToLoad, true);

                    callAjaxRequestJSON("AddEFSActivityLog"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEFSActivityLogs = ro;
                                // on success
                                arrEFSActivityLogs.push(e.data);
                                e.success(arrEFSActivityLogs);
                                jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                                jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();
                            } else {
                                bootbox.alert("Web service error when calling EFS AddEFSActivityLog. " + data.ErrorMessage);
                            }
                            kendo.ui.progress(gridToLoad, false);
                        }
                        , function (response) {
                            ajaxError();
                            kendo.ui.progress(gridToLoad, false);
                        }
                    );


                    //e.error("XHR response", "status code", "error message");
                }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_EFS;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.StoreName = e.data.StoreName;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;
                obj.WorkOrderNumber = e.data.WorkOrderNumber;
                obj.RequestMade = e.data.RequestMade;
                obj.DateTimeCompleted = new Date();
                obj.Remarks = e.data.Remarks;
                obj.StatusID = e.data.StatusID;
                obj.StatusDescription = e.data.StatusDescription;
                obj.TimeSpent = e.data.TimeSpent;
                obj.DateTimeToFollowUp = e.data.DateTimeToFollowUp;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateEFSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrEFSActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrEFSActivityLogs);
                            jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                            jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling EFS UpdateEFSActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_EFS;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                                    jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();
                                } else {
                                    bootbox.alert("Web service error when calling EFS - DeleteActivityLog. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrEFSActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        StoreName: { validation: { required: true, maxLength: 250} },
                        DateTimeRequested: { type: "date", defaultValue: function (e) { return new Date(); } },
                        Requestor: { validation: { maxLength: 250} },
                        WorkOrderNumber: { type: "number", validation: { min: 0, maxLength: 7} },
                        RequestMade: { validation: { maxLength: 250} },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        Remarks: { validation: { maxLength: 250} },
                        StatusID: { defaultValue: arrEFSStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number" },
                        TimeSpentStr: { editable: false },
                        FollowUpBy: { editable: false },
                        FollowUpDateTime: { editable: false },
                        FollowUpStatusList: { editable: false },
                        DateTimeToFollowUp: { type: "date", defaultValue: function (e) { return new Date(); } }
                    }
                }
            }
        });

        jQuery("#gridEFS").kendoGrid({
            dataSource: dataSourceEFS,
            pageable: true,
            //toolbar: ["create"],
            toolbar:   [      //split button
                 {
                     type: "button",
                     text: "Add New Record",
                     id: "AddNewRecord"
                 }, ],
              columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "ActivityID", title: "Type of Controller", editor: efsTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Requestor", title: "Requestor", width: "250px" },
                    { field: "WorkOrderNumber", title: "Work Order Number", format: "{0:0000000}", width: "120px" },
                    { field: "RequestMade", title: "Request Made", width: "250px" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "Remarks", title: "Remarks", width: "250px" },
                    { field: "StatusID", title: "1st Touch Status", editor: efsStatusDDEditor, width: "250px", template: "#=StatusDescription#" },
                    { field: "DateTimeToFollowUp", title: "Date Time to Follow Up", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "FollowUpBy", title: "Follow Up By", width: "200px" },
                    { field: "FollowUpDateTime", title: "Follow Up Date & Time", width: "150px" },
                    { field: "FollowUpStatusList", title: "Follow Up Status", width: "150px" },
                    //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                    {command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridEFS').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridEFS').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline",
            edit: function (e) {
                if (!e.model.isNew()) {
                    var select = e.container.find('input[data-bind="value:StatusID"]').data('kendoDropDownList');
                    select.enable(false);
                }
            }

        });
        jQuery("#gridEFS .k-grid-AddNewRecord").on("click", function () {
            jQuery("#AddNewRecord").modal("show");
            let current_datetime = new Date();
            var format_date = current_datetime.toLocaleString('en-US', { year: "numeric", month: "2-digit", day: "numeric", hour: 'numeric', minute: 'numeric', hour12: true });
            let formatted_date = current_datetime.getFullYear() + "/" + (current_datetime.getMonth() + 1) + "/" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes("MM") + ":" + current_datetime.getSeconds()
            console.log(formatted_date)
            jQuery('#txtProcessDate').val(format_date);
            jQuery('#txtDateTimeRequested').val(format_date);
			jQuery('#EFSDateTimeNow').val(format_date);
        });
        ///////////////////////////////////////////////// PENDING ITEMS /////////////////////////////////////////////////////

        var dataSourceEFSPending = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetPendingActivity"
                        , { ActivityTypeID: ActivityType_EFS, EmployeeID: Employee_ID }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEFSPending = ro;
                                // on success
                                e.success(arrEFSPending);
                            } else {
                                bootbox.alert("Web service error when calling EFS - GetPendingActivity. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
                , update: function (e) {

                    var param = new Object();
                    var obj = new Object();
                    var objAcct = new Object();
                    var status = new Object();

                    objAcct.Username = jQuery("#hfUsername").val();
                    objAcct.Password = sessionStorage.getItem("UserPassword"); //jQuery("#hfPassword").val();
                    objAcct.EmailAddress = jQuery("#hfEmailAddress").val();

                    obj.ActivityTypeID = ActivityType_EFS;
                    obj.LogDetailID = e.data.LogDetailID;
                    obj.ActivityLogID = e.data.ActivityLogID;
                    obj.EmployeeID = Employee_ID;
                    obj.ActivityDate = tempStartDateTime;
                    obj.ActivityID = e.data.ActivityID;

                    obj.StoreName = e.data.StoreName;
                    obj.DateTimeRequested = e.data.DateTimeRequested;
                    obj.Requestor = e.data.Requestor;
                    obj.WorkOrderNumber = e.data.WorkOrderNumber;
                    obj.RequestMade = e.data.RequestMade;
                    obj.DateTimeCompleted = new Date();
                    obj.Remarks = e.data.Remarks;
                    obj.StatusID = e.data.StatusID;
                    obj.StatusDescription = e.data.StatusDescription;
                    obj.DateTimeToFollowUp = e.data.DateTimeToFollowUp;

                    param.ActivityLog = obj;
                    param.Account = objAcct;

                    callAjaxRequestJSON("UpdateEFSFollowUp"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEFSPending = ro;

                                e.success(arrEFSPending);
                                jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                                jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();
                            } else {
                                bootbox.alert("Web service error when calliung EFS - UpdateEFSFollowUp. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
            } //transport
            ,
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },

            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrEFSActivityList[0].ListID, editable: false },
                        ActivityDescription: { editable: false },
                        StoreName: { validation: { required: true, maxLength: 250 }, editable: false },
                        DateTimeRequested: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        Requestor: { validation: { maxLength: 250 }, editable: false },
                        WorkOrderNumber: { type: "number", validation: { min: 0 }, editable: false },
                        RequestMade: { validation: { maxLength: 250 }, editable: false },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        Remarks: { validation: { maxLength: 250 }, editable: false },
                        StatusID: { defaultValue: arrEFSStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", editable: false },
                        TimeSpentStr: { editable: false },
                        FollowUpBy: { editable: false },
                        DateTimeToFollowUp: { type: "date", defaultValue: function (e) { return new Date(); }
                            , validation: {
                                validateDateTimeToFollow: function (input) {
                                    if (input.attr("data-bind") == "value:StatusID") {
                                        selectedStatus = input.val();
                                    }
                                    if (input.attr("data-bind") == "value:DateTimeToFollowUp") {
                                        selectedDateTimeToFollowUp = input.val()
                                        input.attr("data-validateDateTimeToFollow-msg", "Date Time To Follow is required.");
                                        //return checkdate(input.val());
                                        if (selectedStatus == 22 && selectedDateTimeToFollowUp == "")
                                            return false;
                                        else
                                            return true;
                                    }
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        });


        jQuery("#gridEFSPending").kendoGrid({
            dataSource: dataSourceEFSPending,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "ActivityID", title: "Type of Controller", editor: efsTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Requestor", title: "Requestor", width: "250px" },
                    { field: "WorkOrderNumber", title: "Work Order Number", format: "{0:0000000}", width: "120px" },
                    { field: "RequestMade", title: "Request Made", width: "250px" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Remarks", title: "Remarks", width: "250px" },
                    { field: "StatusID", title: "1st Touch Status", editor: efsStatusDDEditor, width: "250px", template: "#=StatusDescription#" },
                    { field: "DateTimeToFollowUp", title: "Date Time to Follow Up", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "FollowUpBy", title: "Follow Up By", width: "200px" },
                    { command: ["edit"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline",
            edit: function (e) {
                tempStartDateTime = new Date();
                e.model.DateTimeToFollowUp = new Date();
            },
            save: function (e) {
                e.model.dirty = true;
            }
        });

        //jQuery(".k-grid-toolbar", "#gridEFSPending").prepend("<h4>FOR FOLLOW UP</h4>");

    }

    function efsTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEFSActivityList
            });
    }

    function efsStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEFSStatus
            });
    }

    // VALIDATION EFS //
    jQuery.noConflict();
    jQuery(document).ready(function () {
        jQuery('#select1stTouchStatus').on('change', function () {
            if (jQuery(this).val() == 22) {
                jQuery('#DateTimeFollowUpList').show();
            } else {
                jQuery('#DateTimeFollowUpList').hide();
            }
        });
        var validator = 0;
        jQuery("#addNewRecord").on('click', function () {
            if (jQuery('#txtDateTimeRequested').val() == "") { jQuery('#txtDateTimeRequested').css("border-color", "red"); } else { jQuery('#txtDateTimeRequested').css("border-color", "#f0f0f0"); }
            if (jQuery('#txtRequestMade').val() == "") { jQuery('#txtRequestMade').css("border-color", "red"); } else { jQuery('#txtRequestMade').css("border-color", "#f0f0f0"); }
            if (jQuery('#txtStoreName').val() == "") { jQuery('#txtStoreName').css("border-color", "red"); } else { jQuery('#txtStoreName').css("border-color", "#f0f0f0"); }
            if (jQuery('#txtRemarks').val() == "") { jQuery('#txtRemarks').css("border-color", "red"); } else { jQuery('#txtRemarks').css("border-color", "#f0f0f0"); }
            if (jQuery('#txtRequestor').val() == "") { jQuery('#txtRequestor').css("border-color", "red"); } else { jQuery('#txtRequestor').css("border-color", "#f0f0f0"); }
            if ((jQuery('#txtStoreName').val() != "") && (jQuery('#txtDateTimeRequested').val() != "") && (jQuery('#txtRequestMade').val() != "") && (jQuery('#txtRemarks').val() != "") && (jQuery('#txtRequestor').val() != "")) { addEFS(); } else { alert('Please input required field'); }
        });
        jQuery(".datePicker").kendoDateTimePicker({
            value: new Date(),
            dateInput: true
        });

        callAjaxRequestJSON("CheckEFSTechPermission"
                , { EmployeeID: Employee_ID }
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        if (!ro) {
                            bootbox.alert("You have no permission on this page.");
                            window.history.back();
                        }
                        else {
                            if (!sessionStorage.getItem("UserPassword")) {
                                jQuery("#divEnterPassorword").show().kendoWindow({
                                    width: "420px",
                                    height: "150px",
                                    modal: true,
                                    resizable: false,
                                    draggable: false,
                                    title: ""
                                }).data("kendoWindow").center();
                                jQuery("#divEnterPassorword").parent().find(".k-window-action").css("visibility", "hidden");
                                jQuery("#txtPassword").focus();
                            }
                        }
                    } else {
                        bootbox.alert("Web service error when loading activitylog page. " + data.ErrorMessage);
                    }
                }
                , function () {
                    window.history.back();
                }
        );

    });


    function generateIndividualSummaryReport() {

        if (jQuery("#hfRoleName").val() == "Supervisor")
            window.open("http://phectsmpc-db01.emrsn.org/ReportServer/Pages/ReportViewer.aspx?%2fERS+Reports%2fOnline+Log+Sheet%2fActivityLogReport&rs:Command=Render", "_blank");
        else
            window.open("http://phectsmpc-db01.emrsn.org/ReportServer/Pages/ReportViewer.aspx?%2fERS+Reports%2fOnline+Log+Sheet%2fActivityLogReportIndividual&rs:Command=Render&EmployeeID=" + jQuery("#hfEmployeeID").val(), "_blank");
    }

    HeaderHolder();
    LogSheetMenu();
    FooterMobile();
    Footer();
    //

    // CREATE FUNCTION
    function addEFS() {
        var param = new Object();
        var obj = new Object();
        var objAcct = new Object();
        var status = new Object();
        var StatusDesc = "";

        if (document.getElementById("select1stTouchStatus").value == 19) { StatusDesc = "Cancelled"; } else if (document.getElementById("select1stTouchStatus").value == 20) { StatusDesc = "Communication Problem"; } else if (document.getElementById("select1stTouchStatus").value == 21) { StatusDesc = "Completed"; } else if (document.getElementById("select1stTouchStatus").value == 22) { StatusDesc = "Completed - Further Action"; } else if (document.getElementById("select1stTouchStatus").value == 23) { StatusDesc = "Completed - Out of Scope"; }
        objAcct.Username = document.getElementById("hfUsername").value; //jQuery("#hfUsername").val();
        objAcct.Password = sessionStorage.getItem("UserPassword");
        objAcct.EmailAddress = document.getElementById("hfEmailAddress").value; //jQuery("#hfEmailAddress").val();
        obj.FullName = document.getElementById("hfEmployeeName").value;

        obj.ActivityTypeID = 9;
        obj.LogDetailID = 0;
        obj.ActivityLogID = 0;
        obj.EmployeeID = document.getElementById("hfEmployeeID").value; //jQuery("#hfEmployeeID").val();
        obj.ActivityDate = document.getElementById("EFSDateTimeNow").value; //jQuery("#txtEFSActivityDate").val();
        obj.ActivityID = 116;

        obj.StoreName = document.getElementById("txtStoreName").value; //jQuery("txtStoreName").val();
        obj.DateTimeRequested = document.getElementById("txtDateTimeRequested").value; //jQuery("txtDateTimeRequested").val();
        obj.Requestor = document.getElementById("txtRequestor").value; //jQuery("txtRequestor").val();
        obj.WorkOrderNumber = document.getElementById("txtWorkOrderNumber").value; //jQuery("txtWorkOrderNumber").val();
        obj.RequestMade = document.getElementById("txtRequestMade").value; //jQuery("txtRequestMade").val();
        obj.DateTimeCompleted = new Date();
        obj.Remarks = document.getElementById("txtRemarks").value; //jQuery("txtRemarks").val();
        obj.StatusID = document.getElementById("select1stTouchStatus").value; //jQuery("select1stTouchStatus").val();
        obj.StatusDescription = StatusDesc;
        //obj.TimeSpent = e.data.TimeSpent;
        obj.DateTimeToFollowUp = document.getElementById("txtDateTimeFollowUp").value; //txtDateTimeFollowUp
        param.ActivityLog = obj;
        param.Account = objAcct;

        callAjaxRequestJSON("AddEFSActivityLog"
            , param
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrEFSActivityLogs = ro;
                    // on success
                    arrEFSActivityLogs.push(e.data);
                    e.success(arrEFSActivityLogs);
                    jQuery('#gridEFS').data('kendoGrid').dataSource.read();
                    jQuery('#gridEFSPending').data('kendoGrid').dataSource.read();                    
                } else {
                    //bootbox.alert("Web service error when calling EFS AddEFSActivityLog. " + data.ErrorMessage);
                }
                kendo.ui.progress(gridToLoad, false);
            }
            , function (response) {
                ajaxError();
                kendo.ui.progress(gridToLoad, false);
            }
        );
        loadGridEFS();
        jQuery("#AddNewRecord").modal("hide");

        //e.error("XHR response", "status code", "error message");
    }
    //

});
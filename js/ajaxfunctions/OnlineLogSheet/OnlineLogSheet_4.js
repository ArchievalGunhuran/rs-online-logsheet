﻿var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[1];
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

var activityLogFieldsList = new Array();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function ajaxError(response, error) {
    //hideloaders();al
    alert("Request Error" + response.responseText);
}

function ajaxSuccess(response) {
    alert("success");
    alert(JSON.stringify(response));
    var obj = JSON.parse(response.responseText);
    alert(obj);
    alert("Request Successful");
}


var sampleData = [
            { ProductID: 1, ProductID2: 4, ProductName: "Apple iPhone 5s", Introduced: new Date(2013, 8, 10), UnitPrice: 525, Discontinued: false, UnitsInStock: 10, StateCol: { StateID: 1, StateName: "Virginia"} },
            { ProductID: 2, ProductID2: 5, ProductName: "HTC One M8", Introduced: new Date(2014, 2, 25), UnitPrice: 425, Discontinued: false, UnitsInStock: 3, StateCol: { StateID: 2, StateName: "Massachusetts"} },
            { ProductID: 3, ProductID2: 6, ProductName: "Nokia 5880", Introduced: new Date(2008, 10, 2), UnitPrice: 275, Discontinued: true, UnitsInStock: 0, StateCol: { StateID: 3, StateName: "Ohio"} }
        ];

var states = [
            { StateID: 1, StateName: "Virginia" },
            { StateID: 2, StateName: "Massachusetts" },
            { StateID: 3, StateName: "Ohio" }
        ];

// custom logic start

var sampleDataNextID = sampleData.length + 1;
var empid = 0;

function getIndexById(id) {
    var idx,
                l = sampleData.length;

    for (var j; j < l; j++) {
        if (sampleData[j].ProductID == id) {
            return j;
        }
    }
    return null;
}

jQuery(document).ready(function () {
    jQuery("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
                    , select: function (e) {
                        alert(jQuery(e.item).index());
                    }
    });

    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (e) {
                // on success
                e.success(sampleData);
                // on failure
                //e.error("XHR response", "status code", "error message");
            },
            create: function (e) {
                // assign an ID to the new item
                test();
                e.data.ProductID = empid;
                alert(e.data.ProductID);
                //e.data.ProductID = sampleDataNextID++;
                // save data item to the original datasource
                sampleData.push(e.data);
                // on success
                e.success(e.data);
                // on failure
                //e.error("XHR response", "status code", "error message");
            },
            update: function (e) {
                // locate item in original datasource and update it
                sampleData[getIndexById(e.data.ProductID)] = e.data;

                alert(e.data.ProductID);
                // on success
                e.success();
                // on failure
                //e.error("XHR response", "status code", "error message");
            },
            destroy: function (e) {
                // locate item in original datasource and remove it
                sampleData.splice(getIndexById(e.data.ProductID), 1);
                // on success
                e.success();
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
        },
        error: function (e) {
            // handle data operation error
            alert("Status: " + e.status + "; Error message: " + e.errorThrown);
        },
        pageSize: 10,
        batch: false,
        schema: {
            model: {
                id: "ProductID",
                fields: {
                    ProductID: { editable: false, nullable: true },
                    ProductID2: { editable: false, nullable: true },
                    ProductName: { validation: { required: true} },
                    Introduced: { type: "date" },
                    UnitPrice: { type: "number", validation: { required: true, min: 1} },
                    Discontinued: { type: "boolean" },
                    UnitsInStock: { type: "number", validation: { min: 0, required: true} },
                    StateCol: { defaultValue: { StateID: 1, StateName: "Virginia"} }
                }
            }
        }
    });

    function test() {
        empid = 4;
    }

    jQuery("#gridAdmin").kendoGrid({
        dataSource: dataSource,
        pageable: true,
        toolbar: ["create"],
        columns: [
                    { field: "ProductName", title: "Mobile Phone" },
                    { field: "Introduced", title: "Introduced", format: "{0:yyyy/MM/dd}", width: "200px" },
                    { field: "UnitPrice", title: "Price", format: "{0:c}", width: "120px" },
                    { field: "UnitsInStock", title: "Units In Stock", width: "120px" },
                    { field: "Discontinued", width: "120px" },
                    { field: "StateCol", title: "State", editor: stateDDEditor, template: "#=StateCol.StateName#" },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
                ],
        editable: "inline"
    });


    function getState(_stateID) {
        alert(_stateID);
        for (var i = 0, len = states.length; i < len; i++) {
            if (states[i].StateID == _stateID)
                return states[i].StateName;
        }
    }

    function stateDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "StateName"
                , dataValueField: "StateID"
                , dataSource: states
            });
    }

});
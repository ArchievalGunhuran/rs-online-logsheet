﻿
var activityLogFieldsList = new Array();

var arrSPMActivityLogs;
var arrSPMActivityList;
var arrSPMStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;

jQuery(document).ready(function () {

    jQuery("#txtSPMActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridSPM').data('kendoGrid').dataSource.read(); }
    });

    //GET TASK LIST FOR SOM
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_SPM, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrSPMActivityList = ro;
                if (arrSPMActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from SPM - Task field");
            } else {
                bootbox.alert("Web service error when calling SPM - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM PROJECT
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_SPM, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrSPMStatus = ro;
                    loadGridSPM();
                } else {
                    bootbox.alert("Web service error when calling SPM - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridSPM() {

        var dataSourceSPM = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                        , { ActivityTypeID: ActivityType_SPM, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtSPMActivityDate").val() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrSPMActivityLogs = ro;
                                // on success
                                //alert(JSON.stringify(arrSPMActivityLogs));
                                e.success(arrSPMActivityLogs);
                            } else {
                                bootbox.alert("Web service error when calling SPM - GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_SPM;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;
                obj.DateTimeCompleted = new Date(); ;
                obj.Remarks = e.data.Remarks;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;
                obj.Numberoftransaction = e.data.Numberoftransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddSPMActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrSPMActivityLogs = ro;
                            // on success
                            arrSPMActivityLogs.push(e.data);
                            e.success(arrSPMActivityLogs);
                            jQuery('#gridSPM').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling AddSPMActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_SPM;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;
                obj.DateTimeCompleted = e.data.DateTimeCompleted;
                obj.Remarks = e.data.Remarks;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;
                obj.Numberoftransaction = e.data.Numberoftransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateSPMActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrSPMActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrSPMActivityLogs);
                            jQuery('#gridSPM').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling UpdateSPMActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_SPM;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridSPM').data('kendoGrid').dataSource.read();
                                } else {
                                    bootbox.alert("Web service error when calling SPM - DeleteActivityLog. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrSPMActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        StoreName: { validation: { required: true, maxLength: 250} },
                        StoreNumber: { type: "number" },
                        DateTimeRequested: { type: "date", defaultValue: function (e) { return new Date(); } },
                        Requestor: { validation: { maxLength: 250} },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        Remarks: { validation: { maxLength: 250} },
                        StatusID: { defaultValue: arrSPMStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpentStr: { editable: false },
                        Numberoftransaction: { type: "number", validation: { min: 0} }
                    }
                }
            }
        });

        jQuery("#gridSPM").kendoGrid({
            dataSource: dataSourceSPM,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "StoreNumber", title: "Store Number", format: "{0:000000}", width: "120px" },
                    { field: "ActivityID", title: "Type of Request", editor: spmTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Requestor", title: "Requestor", width: "250px" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Remarks", width: "250px" },
                    { field: "StatusID", title: "Status", editor: spmStatusDDEditor, width: "150px", template: "#=StatusDescription#" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "Numberoftransaction", title: "Number of Transaction", format: "{0:0}", width: "120px" },
                    //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                    {command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridSPM').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridSPM').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridSPM').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline"
        });
    }

    function spmTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrSPMActivityList
            });
    }

    function spmStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrSPMStatus
            });
    }

});
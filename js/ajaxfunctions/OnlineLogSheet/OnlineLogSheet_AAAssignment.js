﻿
var arrAAAssignment;
var arrAAActivityList;
var arrAAPriorityList;
var arrAAEmployeeList;

jQuery(document).ready(function () {
    var detailsTemplate;

    var wnd = jQuery("#wnd").kendoWindow({
        title: "Complete Adhoc Analytics Assignment",
        height: 450,
        width: 650,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    detailsTemplate = kendo.template(jQuery("#template").html());

    function show(id, value) {
        document.getElementById(id).style.display = value ? 'block' : 'none';
    }

    callAjaxRequestJSON("GetEmployeeListAdhoc"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrAAEmployeeList = ro;
                if (arrAAEmployeeList.length > 0) {
                    getActivityList();
                }
                else {
                    alert("No items available for Adhoc Analytics Assignment Employee List");
                }
            } else {
                alert("Web service error when calling Adhoc Analytics Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    //function aaPriorityDDEditor(container, options) {
    //    jQuery("<input data-bind='value:" + options.field + "'/>")
    //        .appendTo(container)
    //        .kendoDropDownList({
    //            dataTextField: "ListDescription"
    //            , dataValueField: "ListID"
    //            , dataSource: arrAAPriorityList
    //        });
    //}

    function getActivityList() {

        callAjaxRequestJSON("GetActivityList"
            , { ActivityTypeID: ActivityType_AA, EmployeeID: arrAAEmployeeList[0].ListID }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrAAActivityList = ro;
                    if (arrAAActivityList.length > 0)
                        getPriorityList();
                    else
                        alert("No items available for Adhoc Analytics Assignment Activity List");

                } else {
                    alert("Web service error when calling Adhoc Analytics Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function getPriorityList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_AA, ListTypeID: 2 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrAAPriorityList = ro;
                    if (arrAAPriorityList.length > 0)
                        loadAAAssignment();
                    else
                        alert("No items available for Adhoc Analytics Assignment Priority List");

                } else {
                    alert("Web service error when calling Adhoc Analytics - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadAAAssignment() {

        var dataSourceAAAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetAAAssignment"
                    , { ActivityTypeID: ActivityType_AA }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAAAssignment = ro;
                            e.success(arrAAAssignment);
                        } else {
                            alert("Web service error when calling loadAAAssignment_GetCSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.EmployeeID = Employee_ID;
                obj.ActivityID = e.data.ActivityID;

                obj.AssignEmployeeID = e.data.AssignEmployeeID;

                obj.TicketNumber = e.data.TicketNumber;
                obj.TaskDescription = e.data.TaskDescription;
                obj.RequestedBy = e.data.RequestedBy;
                obj.DateReceived = e.data.DateReceived;
                obj.DueDate = e.data.DueDate;
                obj.NumberOfTransaction = e.data.NumberOfTransaction;
                obj.PriorityListID = e.data.PriorityListID;;

                obj.EmailAddressofRequestor = e.data.EmailAddressofRequestor;
                //obj.TimeSpentofProcessor = e.data.TimeSpentofProcessor;

                param.ActivityLog = obj;
                show('loading', true);

                callAjaxRequestJSON("AddAAAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            show('loading', false);
                            arrAAAssignment = ro;
                            arrAAAssignment.push(e.data);
                            e.success(arrAAAssignment);
                            jQuery('#gridAAAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            show('loading', false);
                            bootbox.alert("Web service error when calling Adhoc Analytics AddAAAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.AAAssignmentID = e.data.AAAssignmentID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityID = e.data.ActivityID;

                obj.AssignEmployeeID = e.data.AssignEmployeeID;

                obj.TicketNumber = e.data.TicketNumber;
                obj.TaskDescription = e.data.TaskDescription;
                obj.RequestedBy = e.data.RequestedBy;
                obj.DateReceived = e.data.DateReceived;
                obj.DueDate = e.data.DueDate;
                obj.NumberOfTransaction = e.data.NumberOfTransaction;
                obj.PriorityListID = e.data.PriorityListID;;

                obj.EmailAddressofRequestor = e.data.EmailAddressofRequestor;
                //obj.TimeSpentofProcessor = e.data.TimeSpentofProcessor;

                param.ActivityLog = obj;

                show('loading', true);
                callAjaxRequestJSON("UpdateAAAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            show('loading', false);
                            arrAAAssignment = ro;
                            arrAAAssignment.push(e.data);
                            e.success(arrAAAssignment);
                            jQuery('#gridAAAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            show('loading', false);
                            alert("Web service error when calling Adhoc Analytics UpdateAAAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
            }
            , destroy: function (e) {

                var param = new Object();
                param.AAAssignmentID = e.data.AAAssignmentID;

                callAjaxRequestJSON("DeleteAAAssignment"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridAAAssignment').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error calling loadAAAssignment_DeleteAAAssignment. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "AAAssignmentID",
                    fields: {
                        AAAssignmentID: { editable: false, nullable: false },
                        AssignEmployeeID: { validation: { required: true }, defaultValue: arrAAEmployeeList[0].ListID },
                        AssignEmployeeName: { editable: false },
                        TicketNumber: { editable: false },
                        ActivityID: { defaultValue: arrAAActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        PriorityListID: { defaultValue: arrAAPriorityList[0].ListID },
                        PriorityListDescription: { editable: false },
                        TaskDescription: { validation: { required: true, maxLength: 250 } },
                        RequestedBy: { validation: { required: true, maxLength: 50 } },
                        EmailAddressofRequestor: { type: "email", validation: { required: true } },
                        DateReceived: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DueDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DateCompleted: { type: "date", defaultValue: "", editable: false },
                        SLA: { editable: false },
                        StatusDescription: { editable: false },
                        TimeSpentofProcessor: { editable: false },

                        QAByEmployeeName: { editable: false },
                        NoOfDaysDelayed: { editable: false },
                        Timeliness: { editable: false },
                        CorrectData: { editable: false },
                        MeetTheCriteria: { editable: false },
                        Accuracy: { editable: false },
                        TotalQAScore: { editable: false },
                        TimeSpentOfQA: { editable: false }

                    }
                }
            }
        });

        jQuery("#gridAAAssignment").kendoGrid({
            dataSource: dataSourceAAAssignment,
            pageable: true,
            toolbar: ["create", "cancel"],
            filterable: true,
            columns: [
                    {
                        field: "AssignEmployeeID", title: "Processor", editor: employeeListTaskDDEditor, template: "#=AssignEmployeeName#", width: "250px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrAAEmployeeList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            ,operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TicketNumber", title: "Ticket Number", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "PriorityListID", title: "Priority", editor: aaPriorityDDEditor, width: "250px", template: "#=PriorityListDescription#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrAAPriorityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ActivityID", title: "Type", editor: aaTaskDDEditor, width: "250px", template: "#=ActivityDescription#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrAAActivityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TaskDescription", title: "Description", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "RequestedBy", title: "Requested By", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "EmailAddressofRequestor", title: "Email Address (Requestor)", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "DateReceived", title: "Date Received", format: "{0:MM/dd/yyyy}", width: "150px", filterable:false },
                    { field: "DueDate", title: "Due Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    { field: "StatusDescription", title: "Status", width: "120px", filterable: false },
                    {
                        field: "TimeSpentofProcessor", title: "Time Spent (Processor)", width: "200px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "QAByEmployeeName", title: "QA By", width: "250px", filterable: false },
                    { field: "NoOfDaysDelayed", title: "No. of Days Delayed", width: "150px", filterable: false },
                    { field: "Timeliness", title: "Timeliness", width: "120px", filterable: false },
                    { field: "CorrectData", title: "Correct Data", width: "120px", filterable: false },
                    { field: "MeetTheCriteria", title: "Meet the Criteria", width: "120px", filterable: false },
                    { field: "Accuracy", title: "Accuracy", width: "120px", filterable: false },
                    { field: "TotalQAScore", title: "Total QA Score", width: "120px", filterable: false },
                    { field: "TimeSpentOfQA", title: "Time Spent of QA", width: "120px", filterable: false },

                    { command: { text: "Complete", click: completeStatus }, title: "", width: "100px" },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "250px", filterable: false }

            ],
            editable: "inline"
        });
        
    }
    function completeStatus(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

        if (dataItem.StatusDescription == 'Completed') {
            jQuery.confirm({
                title: 'Adhoc Analytics Assignment',
                content: 'This Record is already Completed.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else if (dataItem.StatusDescription == '') {
            jQuery.confirm({
                title: 'Adhoc Analytics Assignment',
                content: 'This Record is not yet Saved in the Database!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else if (dataItem.StatusDescription == 'On-going') {
            console.log(arrAAEmployeeList);
            wnd.content(detailsTemplate(dataItem));
            jQuery("#dropDownQAList").kendoDropDownList({
                dataSource: arrAAEmployeeList,
                dataTextField: "ListDescription",
                dataValueField: "ListID",
                change: onChange
            });
            document.getElementById('dateCompletionDate').valueAsDate = new Date();
            wnd.center().open();
        }
        else {
            jQuery.confirm({
                title: 'Adhoc Analytics Assignment',
                content: 'Invalid Status',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }

    }
    function onChange(e) {
        //  You can do with this also
        //   $("#skuCode").val($("#productName").data("kendoDropDownList").text());
        $("#qaByEmployeeName").val(e.sender.text());
        var x = $("#qaByEmployeeName").val();
    };

    $(document).on('click', '#btnComplete', function (e) {
        e.preventDefault();

        var timeSpent = $('#txtHourSpent').val();
        var assignmentID = $('#assignmentID').val();
        var qaBy = $('#dropDownQAList').val();
        var emailAddressOfRequestor = $('#emailAddressOfRequestor').val();

        var assignEmployeeName = $('#assignEmployeeName').val();
        var qaByEmployeeName = $('#qaByEmployeeName').val();
        var ticketNumber = $('#ticketNumber').val();
        var priorityListDescription = $('#priorityListDescription').val();
        var taskDescription = $('#taskDescription').val();
        var dateCompletion = $('#dateCompletionDate').val();
        var dueDate = $('#dueDate').val();

        
        if (timeSpent != '') {
            var xdueDate = new Date(dueDate);
            var xdateCompleted = new Date(dateCompletion);
            var daysDelayed = 0;

            if (xdateCompleted.getTime() <= xdueDate.getTime()) {
                daysDelayed = 0;
            }
            else {

                var diffTime = Math.abs(xdueDate - xdateCompleted);
                var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                daysDelayed = diffDays - 1;
            }

            //alert(daysDelayed);

            wnd.close();
            tempStartDateTime = new Date();
            var param = new Object();
            var obj = new Object();
            var statusList = new Object();

            obj.ActivityTypeID = ActivityType_AA;
            obj.EmployeeID = Employee_ID;
            obj.ActivityDate = tempStartDateTime;
            obj.AAAssignmentID = assignmentID;
            obj.TimeSpent = timeSpent;
            obj.StatusID = 35;
            obj.NumberOfTransaction = 1;
            obj.QAByEmployeeID = qaBy;
            obj.EmailAddressofRequestor = emailAddressOfRequestor;

            obj.AssignEmployeeName = assignEmployeeName;
            obj.QAByEmployeeName = qaByEmployeeName;
            obj.TicketNumber = ticketNumber;
            obj.PriorityListDescription = priorityListDescription;
            obj.TaskDescription = taskDescription;

            obj.DateCompleted = dateCompletion;
            obj.NoOfDaysDelayed = daysDelayed;



            param.ActivityLog = obj;

            //console.log(param.ActivityLog);
            show('loading', true);

            callAjaxRequestJSON("CompleteStatusAAActivityLog"
                , param
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        show('loading', false);
                        jQuery.confirm({
                            title: 'Adhoc Analytics Assignment',
                            content: 'Database Successfully Modified.',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                close: function () {
                                    window.location.reload();
                                }
                            }
                        });
                    } else {
                        show('loading', false);
                        bootbox.alert("Web service error AddCSActivityLog. " + data.ErrorMessage);
                    }
                }
                , ajaxError
            );
        }
        else {
            jQuery.confirm({
                title: 'Adhoc Analytics Assignment',
                content: 'Time Spent is Required!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
    });

    function aaTaskDDEditor(container, options) {
        programContainer = container;
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAActivityList
            });

    }

    function employeeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAEmployeeList
                , change: function (e) {

                    callAjaxRequestJSON("GetActivityList"
                        , { ActivityTypeID: ActivityType_AA, EmployeeID: this.value() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrAAActivityList = ro;
                                if (arrAAActivityList.length > 0) {
                                    programContainer.empty();
                                    var dd = jQuery("<input data-bind='value:ActivityID'/>")
                                        .appendTo(programContainer)
                                        .kendoDropDownList({
                                            dataTextField: "ListDescription"
                                            , dataValueField: "ListID"
                                            , change: function (e) {
                                                options.model.ActivityID = this.value();
                                            }
                                        }).data("kendoDropDownList");

                                    dd.setDataSource(arrAAActivityList);
                                    options.model.ActivityID = dd.value();
                                }
                                else
                                    alert("No items available for CS Assignment Activity List");

                            } else {
                                alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                }
            });
    }

    function aaPriorityDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAPriorityList
            });
    }

});
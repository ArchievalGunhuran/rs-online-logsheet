﻿var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2];
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

var activityLogFieldsList = new Array();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function ajaxError(response, error) {
    alert("Request Error. " + response.responseText);
}

function ajaxSuccess(response) {
    alert("success");
    alert(JSON.stringify(response));
    var obj = JSON.parse(response.responseText);
    alert("Request Successful");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jQuery(document).ready(function () {
    TriggerTimer();
    jQuery("#logSheetMenu").kendoMenu();
    jQuery("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
        , select: function (e) {
            //alert(jQuery(e.item).index());
            if (jQuery(e.item).index() == 2) {
                if (jQuery('#gridAlarm').hasClass('pending')) {
                    jQuery('#gridAlarm').removeClass('pending');
                    if (!jQuery('#gridAlarm').hasClass('started')) {
                        alert("There is an ongoing transaction before you navigated to another tab, this will now be resumed."
                            , function () { jQuery('#gridAlarm').addClass('started'); }
                        );
                        jQuery('#gridAlarm').addClass('started');
                    }
                }
            }
            else {
                if (jQuery('#gridAlarm').hasClass('started')) {
                    if (confirm("There is an ongoing transaction, navigating to another tab will pause this. Would you like to leave this tab?")) {
                        jQuery('#gridAlarm').removeClass('started');
                        jQuery('#gridAlarm').addClass('pending');
                    }
                    else {
                        e.preventDefault();
                    }
                }
            }

        }
    });
});

function TriggerTimer() {
    setInterval(TickTimer, 1000);
    //setTimeout(TickTimer, 1000);
}

function TickTimer() {

    //alert("ticktimer");
    jQuery('#gridAlarm table tbody tr:first td:nth-last-child(2)').stop(true, true);
    jQuery('#gridActivityLog table tbody tr td:nth-last-child(1)').stop(true, true);

    if (jQuery('#gridAlarm').hasClass('started')) {
        var seconds = 0, minutes = 0, hours = 0;
        var dTime = jQuery('#gridAlarm table tbody tr:first td:nth-last-child(2)').text().split(':');

        hours = parseInt(dTime[0]);
        minutes = parseInt(dTime[1]);
        seconds = parseInt(dTime[2]);

        seconds++;
        if (seconds >= 60) {
            seconds = 0;
            minutes++;
            if (minutes >= 60) {
                minutes = 0;
                hours++;
            }
        }

        jQuery('#gridAlarm table tbody tr:first td:nth-last-child(2)').text((hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds));

    }


    jQuery('#gridActivityLog table tbody tr').each(function () {
        if (jQuery(this).children('td:nth-last-child(2)').text() == "On-going") {
            //var seconds = 0, minutes = 0, hours = 0;
            //var dTime = jQuery(this).children('td:nth-last-child(1)').text().split(':');

            //hours = parseInt(dTime[0]);
            //minutes = parseInt(dTime[1]);
            //seconds = parseInt(dTime[2]);

            //seconds++;
            //if (seconds >= 60) {
            //    seconds = 0;
            //    minutes++;
            //    if (minutes >= 60) {
            //        minutes = 0;
            //        hours++;
            //    }
            //}

            //jQuery(this).children('td:nth-last-child(1)').text((hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds));


        }

        if (jQuery(this).children('td:nth-last-child(2)').text() == "Completed") {


        }
    })

    //TriggerTimer();
}

function checkdate(processDate) {
    if (Date.parse(processDate) < new Date(new Date().setDate(new Date().getDate() - 5))) {
        return false;
    }
    if (Date.parse(processDate) > new Date()) {
        return false;
    }
    return true;
}

jQuery("#btnEnterPassword").click(function ()
{
    var inputPassword = jQuery("#txtPassword").val();
    var currentUserName = jQuery("#hfUsername").val();

    if (inputPassword == "")
    {
        return alert("Please enter your password.");
    }

    showLoading();

    callAjaxRequestJSON("IsUserValid"
                , { UserName: currentUserName, Password: inputPassword }
                , function(response) {

        var data = response['d']
                    var ro = data.ResponseItem;

        if (data.ErrorMessage == null)
        {
            if (ro.Active)
            {
                //alert("valid");
                sessionStorage.setItem("UserPassword", ro.Password);
                //jQuery("#hfPassword").val(ro.Password);
                jQuery("#divEnterPassorword").data("kendoWindow").close();
                jQuery("#divLoading").data("kendoWindow").close();
            }
            else
            {
                alert("Incorrect password! Please try again.");
                jQuery("#divLoading").data("kendoWindow").close();
            }
        }
        else
        {
            //alert("ui error");
            alert("Incorrect password! Please try again. " + data.ErrorMessage);
            jQuery("#divLoading").data("kendoWindow").close();
        }

    }
                , function() {
        window.history.back();
    }
        );

});

function showLoading() {    
    jQuery("#divLoading").show().kendoWindow({
            width: "420px",
            height: "150px",
            modal: true,
            resizable: false,
            draggable: false,
            title: ""
        }).data("kendoWindow").center();
        jQuery("#divLoading").parent().find(".k-window-action").css("visibility", "hidden");

}

jQuery('#divEnterPassorword').keypress(function (e) {
    if (e.which == 13) {
        jQuery("#btnEnterPassword").click();
    }
});

//////////////// PAGE STYLER ///////////////////////////////

function HeaderHolder() {

    var holder = document.getElementById("headerholder");
    var dhtml = "";

    dhtml = dhtml + " <header>";
    dhtml = dhtml + "        <div class=\"container\">";
    dhtml = dhtml + "            <div class=\"row\">";
    dhtml = dhtml + "                <div class=\"col-sm-6 col-xs-5\">";
    dhtml = dhtml + "                    <img src=\"../../images/logo-white.png\" class=\"emr\"/>";
    dhtml = dhtml + "                </div>";
    dhtml = dhtml + "                <div class=\"col-sm-6 col-xs-7\">";
    dhtml = dhtml + "                   <div class=\"top-text-holder pull-right\" id=\"userheader\">";
    dhtml = dhtml + "                       <a href=\"http://proact.emerson.com/rssite/Home.aspx\"><span class='glyphicon glyphicon-home' style='color:white;' title='Go to Home Page' ></span> <span id=\"usernameheader\" style='color:white;' title='Go to Home Page'>" + jQuery("#hfUsername").val() + "</span></a>";
    dhtml = dhtml + "                       | <a href=\"http://proact.emerson.com/rssite/Login.aspx\" style='color:white;'><span id=\"logout\" title=\"Logout\">LOGOUT</span></a>";
    dhtml = dhtml + "                    </div>";
    dhtml = dhtml + "                </div>";
    dhtml = dhtml + "           </div>";
    dhtml = dhtml + "        </div>";
    dhtml = dhtml + " </header>";

    holder.innerHTML = dhtml;
}

function LogSheetMenu() {
    var holder = document.getElementById("navigationmenu");
    var dhtml = "";

    dhtml = dhtml + "<div class='container'>";
    dhtml = dhtml + "   <ul id='logSheetMenu'>";    
	if (jQuery("#hfUserGroup").val() == "AES") {
        dhtml = dhtml + "       <li><a href='" + __baseURL + "/webscreens/OnlineLogSheet/activitylog.aspx'><span>Activity Logs</span></a></li>";
    }
    else {
        dhtml = dhtml + "       <li><a href='" + __baseURL + "/webscreens/OnlineLogSheet/activitylog2.aspx'><span>Activity Logs</span></a></li>";
    }
    dhtml = dhtml + "       <li>Activity Assignment";
    dhtml = dhtml + "           <ul>";
    dhtml = dhtml + "               <li><a href='" + __baseURL + "/webscreens/OnlineLogSheet/csassignment.aspx'><span>CS</span></a></li>";
    dhtml = dhtml + "               <li><a href='aaassignment.aspx'><span>Adhoc Analytics</span></a></li>";
    dhtml = dhtml + "               <li><a href='tsassignment.aspx'><span>Tool Support</span></a></li>";
    dhtml = dhtml + "               <li><a href='project.aspx'><span>Project</span></a></li>";
    dhtml = dhtml + "               <li><a href='" + __baseURL + "/webscreens/OnlineLogSheet/emtassignment.aspx'><span>EM&T</span></a></li>";
    dhtml = dhtml + "               <li><a href='dailyupload.aspx'><span>Daily Uploads</span></a></li>";
    dhtml = dhtml + "           </ul>";
    dhtml = dhtml + "       </li>";
    if (jQuery("#hfRoleName").val() == "Supervisor") {
        dhtml = dhtml + "       <li>Maintenance";
        dhtml = dhtml + "           <ul>";
        dhtml = dhtml + "               <li><a href='activitymaintenanceTypes.aspx'><span>Activity Types</span></a></li>";
        dhtml = dhtml + "               <li><a href='activitymaintenance.aspx'><span>Activity Task</span></a></li>";
        dhtml = dhtml + "               <li><a href='activitylogsView.aspx'><span>Activity Logs View</span></a></li>";
        dhtml = dhtml + "           </ul>";
        dhtml = dhtml + "       </li>";
    }
	
	if (jQuery("#hfUserGroup").val() == "AES") {
		dhtml = dhtml + "       <li><a href='http://proact.emerson.com/rsbook/landingpage.aspx?empid=" + jQuery("#hfEmployeeID").val() + "&pageID=7'><span>Schedule Maintenance</span></a></li>";
	}
	else{
		dhtml = dhtml + "       <li><a href='" + __baseURL + "/webscreens/OnlineLogSheet/schedulemaintenance.aspx'><span>Schedule Maintenance</span></a></li>";
	}
	
	
	if (jQuery("#hfUserGroup").val() == "AES") {
		if (jQuery("#hfRoleName").val() == "User") {
			dhtml = dhtml + "       <li><a href='http://proact.emerson.com/rsbook/landingpage.aspx?empid=" + jQuery("#hfEmployeeID").val() + "&pageID=11'><span>Logsheet Schedule Builder</span></a></li>";
		}
		else {
			dhtml = dhtml + "       <li><a href='http://proact.emerson.com/rsbook/landingpage.aspx?empid=" + jQuery("#hfEmployeeID").val() + "&pageID=10'><span>Logsheet Schedule Builder</span></a></li>";
		}
	}
	
    dhtml = dhtml + "       <li><a href='" + __baseURL + "/webscreens/OnlineLogSheet/reportmaintenance.aspx'><span>Report</span></a></li>";
	dhtml = dhtml + "       <li>QA";
    dhtml = dhtml + "           <ul>";
    dhtml = dhtml + "               <li><a href='aaassignmentQAForm.aspx'><span>QA For Adhoc Analytics Assignment</span></a></li>";
    dhtml = dhtml + "               <li><a href='tsassignmentQAForm.aspx'><span>QA For Tool Support Assignment</span></a></li>";
    dhtml = dhtml + "               <li><a href='projectQAForm.aspx'><span>QA For Project (Staging)</span></a></li>";
    dhtml = dhtml + "               <li><a href='projectQAFormProd.aspx'><span>QA For Project (Production)</span></a></li>";
    dhtml = dhtml + "               <li><a href='dailyuploadQAForm.aspx'><span>QA For Daily Uploads</span></a></li>";
    dhtml = dhtml + "           </ul>";
    dhtml = dhtml + "       </li>";
    dhtml = dhtml + "   </ul>";
    dhtml = dhtml + "</div>";

    holder.innerHTML = dhtml;

}


function Footer() {
    var holder = document.getElementById("footer");
    var dhtml = "";

    dhtml = dhtml + " <div class=\"footer\">";
    dhtml = dhtml + " <div class=\"container\" >";
    dhtml = dhtml + "    <div style=\"color:#808080; margin-top:5px; margin-bottom:5px;\" class=\"visible-lg visible-md\">";
    dhtml = dhtml + "       <div class=\"row\">";
    dhtml = dhtml + "           <div style=\"float:left;\">";
    dhtml = dhtml + "               <img src=\"../../images/logo.gif\" />";
    dhtml = dhtml + "           </div>";
    dhtml = dhtml + "           <div style=\"display:inline-table; float:right; padding-right:10px; text-align:right\">";
    dhtml = dhtml + "               <ul style=\"list-style-type:none; \">";
    dhtml = dhtml + "                   <li><small><a target='_blank' href=\"http://www.emerson.com/en-us/Pages/privacy-policy.aspx\">Privacy Policy</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target='_blank' href=\"http://www.emerson.com/en-us/Pages/terms-of-use.aspx\">Terms of Use</a></small></li>";
    dhtml = dhtml + "                   <li><small></small></li>";
    dhtml = dhtml + "                   <li><small>©2016 Emerson Electric Co. All rights reserved.</small></li>";
    dhtml = dhtml + "                   <li><small>CONSIDER IT SOLVED.</small></li>";
    dhtml = dhtml + "               </ul>";
    dhtml = dhtml + "           </div>";
    dhtml = dhtml + "       </div>";
    dhtml = dhtml + "    </div>";
    dhtml = dhtml + " </div>";
    dhtml = dhtml + " </div>";

    holder.innerHTML = dhtml;
}

function FooterMobile() {
    var holder = document.getElementById("footerMobile");
    var dhtml = "";

    dhtml = dhtml + " <div class=\"container visible-xs visible-sm\" style=\"background-color: #f5f5f5;\">";
    dhtml = dhtml + " <div style=\"color:#808080; margin-top:5px; margin-bottom:5px; font-size:9px\" class=\"visible-xs visible-sm\">";
    dhtml = dhtml + "   <div class=\"row\">";
    //dhtml = dhtml + "       <div class=\"col-sm-12\">";
    //dhtml = dhtml + "           <img class=\"img-responsive\" src=\"images/logo.png\" />";
    //dhtml = dhtml + "       </div>";
    //dhtml = dhtml + "       <div>&nbsp;</div>";
    dhtml = dhtml + "       <div class=\"col-sm-12\">";
    dhtml = dhtml + "           <div><a href=\"#\">Privacy Policy</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href=\"#\">Terms of Use</a></div>";
    //dhtml = dhtml + "           <div>&nbsp;</div>";
    dhtml = dhtml + "           <div>©2015 Emerson Electric Co. All rights reserved.</div>";
    dhtml = dhtml + "           <div>CONSIDER IT SOLVED.</div>";
    dhtml = dhtml + "       </div>";
    dhtml = dhtml + "   </div>";
    dhtml = dhtml + " </div>";
    dhtml = dhtml + " </div>";

    holder.innerHTML = dhtml;
}


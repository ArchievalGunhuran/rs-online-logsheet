﻿var activityLogFieldsList = new Array();

var arrAlarmActivityLogs;
var arrAlarmActivityList;
var arrAlarmStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;

jQuery(document).ready(function () {

    jQuery("#txtAlarmActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridAlarm').data('kendoGrid').dataSource.read(); }
    });

    //GET TASK LIST FOR Alarm
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_ALARM, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrAlarmActivityList = ro;
                if (arrAlarmActivityList.length > 0)
                    getItemList();
                else
                    bootbox.alert("No items from ALRM - Task field");
            } else {
                bootbox.alert("Web service error getting testimonials. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM Alarm
    function getItemList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_ALARM, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrAlarmStatus = ro;
                    loadGridAlarm();
                } else {
                    bootbox.alert("Web service error getting testimonials. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridAlarm() {

        var dataSourceAlarm = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                    , { ActivityTypeID: ActivityType_ALARM, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtAlarmActivityDate").val() }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAlarmActivityLogs = ro;
                            // on success
                            e.success(arrAlarmActivityLogs);
                        } else {
                            alert("Web service error getting testimonials. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_ALARM;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.StartTime = e.data.StartTime;
                obj.EndTime = new Date();
                obj.NumberOfTransaction = e.data.NumberOfTransaction;
                //obj.StatusID = e.data.StatusID;
                obj.Remarks = e.data.Remarks;
                obj.TimeSpentStr = jQuery('#gridAlarm table tbody tr:first td:nth-last-child(2)').text();

                var seconds = 0, minutes = 0, hours = 0;
                var dTime = jQuery('#gridAlarm table tbody tr:first td:nth-last-child(2)').text().split(':');

                hours = parseInt(dTime[0]);
                minutes = parseInt(dTime[1]);
                seconds = parseInt(dTime[2]);

                obj.TimeSpent = (hours * 60) + minutes + (seconds / 60);

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddAlarmActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAlarmActivityLogs = ro;
                            // on success
                            //arrAlarmActivityLogs.push(e.data);
                            e.success(arrAlarmActivityLogs);
                            jQuery('#gridAlarm').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error getting testimonials. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_ALARM;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.StartTime = e.data.StartTime;
                obj.EndTime = new Date();
                obj.NumberOfTransaction = e.data.NumberOfTransaction;
                //obj.StatusID = e.data.StatusID;
                obj.Remarks = e.data.Remarks;
                obj.TimeSpentStr = jQuery('#gridAlarm table tbody tr:first td:nth-last-child(2)').text();

                var seconds = 0, minutes = 0, hours = 0;
                var dTime = jQuery('#gridAlarm table tbody tr:first td:nth-last-child(2)').text().split(':');

                hours = parseInt(dTime[0]);
                minutes = parseInt(dTime[1]);
                seconds = parseInt(dTime[2]);

                obj.TimeSpent = (hours * 60) + minutes + (seconds / 60);

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateAlarmActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAlarmActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrAlarmActivityLogs);
                            jQuery('#gridAlarm').data('kendoGrid').dataSource.read();
                        } else {
                            alert("Web service error getting testimonials. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_ALARM;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridAlarm').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error getting testimonials. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrAlarmActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        StartTime: { type: "date", editable: false, defaultValue: function (e) { return new Date(); } },
                        //StatusID: { defaultValue: arrAlarmStatus[0].ListID },
                        //StatusDescription: { editable: false },
                        Remarks: { validation: { maxLength: 250 } },
                        EndTime: { type: "date", editable: false, defaultValue: "" },
                        NumberOfTransaction: { type: "number" },
                        TimeSpentStr: { editable: false, defaultValue: "00:00:00" }
                    }
                }
            }
        });

        jQuery("#gridAlarm").kendoGrid({
            dataSource: dataSourceAlarm,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "ActivityID", title: "Task", editor: alarmTaskDDEditor, width: "150px", template: "#=ActivityDescription#" },
                    { field: "StartTime", title: "Start Time", format: "{0:MM/dd/yyyy hh:mm:ss}", width: "150px" },
                    //{ field: "StatusID", title: "Remarks", editor: alarmStatusDDEditor, width: "150px", template: "#=StatusDescription#" },
                    { field: "Remarks", width: "250px" },
                    { field: "EndTime", title: "End Time", format: "{0:MM/dd/yyyy hh:mm:ss}", width: "150px" },
                    { field: "NumberOfTransaction", title: "Number Of Transaction", format: "{0:0}", width: "150px" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "150px" },
                    //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                            {command: ["edit",
                                        { name: "Delete"
                                        , click: function (e) {
                                            e.preventDefault();
                                            var tr = $(e.target).closest("tr");
                                            var data = this.dataItem(tr);

                                            bootbox.confirm("Are you sure to delete this record?", function (result) {
                                                if (result) {
                                                    jQuery('#gridAlarm').data('kendoGrid').dataSource.remove(data);
                                                    jQuery('#gridAlarm').data('kendoGrid').dataSource.sync();
                                                    jQuery('#gridAlarm').data('kendoGrid').dataSource.read();
                                                }
                                            });
                                        }
                                        }
                            ], title: "&nbsp;"
                            , width: "250px"
                        }
                ],
            editable: "inline",
            cancel: function (e) {
                jQuery('#gridAlarm').removeClass('started');
            },
            save: function (e) {
                jQuery('#gridAlarm').removeClass('started');
            }

        });

    } //function loadGrid

    function alarmTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAlarmActivityList
            });
    }

    function alarmStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAlarmStatus
            });
    }

    jQuery('#gridAlarm').off('click').on('click', '.k-grid-add', function (e) {
        if (!jQuery('#gridAlarm').hasClass('started')) {
            jQuery('#gridAlarm').addClass('started');
        }
    });

});
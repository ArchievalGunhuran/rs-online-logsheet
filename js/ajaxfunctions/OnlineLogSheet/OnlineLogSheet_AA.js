﻿
var arrAAActivityLogs;
var arrAAActivityList;
var arrAAPriorityList;
var arrAAStatusList;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var curDateTime = new Date();

jQuery(document).ready(function () {

    jQuery("#txtAAActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridAA').data('kendoGrid').dataSource.read(); }
    });


    //GET TASK LIST FOR AA
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_AA, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrAAActivityList = ro;
                if (arrAAActivityList.length > 0)
                    getPriorityList();
                else
                    bootbox.alert("No items from AA - Task field");
            } else {
                bootbox.alert("Web service error when calling Adhoc Analytics - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    function getPriorityList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_AA, ListTypeID: 2 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrAAPriorityList = ro;
                    getStatusList();
                } else {
                    bootbox.alert("Web service error when calling Adhoc Analytics - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    //GET STATUS LIST FORM AA
    function getStatusList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_AA, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrAAStatusList = ro;
                    loadGridAA();
                } else {
                    bootbox.alert("Web service error when calling Adhoc Analytics - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridAA() {

        var dataSourceAA = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                    , { ActivityTypeID: ActivityType_AA, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtAAActivityDate").val() }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAAActivityLogs = ro;
                            e.success(arrAAActivityLogs);
                        } else {
                            bootbox.alert("Web service error getting GetActivityLog on loadgridAA. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var statusList = new Object();

                obj.ActivityTypeID = ActivityType_AA;
                obj.EmployeeID = Employee_ID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.TimeSpent = e.data.TimeSpent;
                obj.NumberOfTransaction = e.data.NumberOfTransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateAAActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAAAssigned = ro;
                            e.success(arrAAAssigned);
                            jQuery('#gridAAAssigned').data('kendoGrid').dataSource.read();
                            jQuery('#gridAA').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error UpdateAAActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_AA;
                param.ActivityLogID = e.data.ActivityLogID;

                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridAA').data('kendoGrid').dataSource.read();
                                    jQuery('#gridAAAssigned').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error getting testimonials. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", editable: false, defaultValue: function (e) { return new Date(); } },
                        
                        TicketNumber: { editable: false },
                        ActivityID: { editable: false },
                        ActivityDescription: { editable: false },
                        PriorityListID: { editable: false },
                        PriorityListDescription: { editable: false },
                        TaskDescription: { editable: false },
                        RequestedBy: { editable: false },
                        DateReceived: { type: "date", editable: false },
                        DueDate: { type: "date", editable: false },
                        DateCompleted: { type: "date", editable: false },
                        SLA: { editable: false },
                        NumberOfTransaction: { type: "number" },
                        StatusID: { editable: false },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", defaultValue: 0, validation: { min: 0.01, required: true } },
                    }
                }
            }
        });

        jQuery("#gridAA").kendoGrid({
            dataSource: dataSourceAA,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "TicketNumber", title: "Ticket Number", width: "150px" },
                    { field: "ActivityID", title: "Type", editor: aaTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "PriorityListID", title: "Priority", editor: aaPriorityDDEditor, width: "250px", template: "#=PriorityListDescription#" },
                    { field: "TaskDescription", title: "Description", width: "250px" },
                    { field: "RequestedBy", title: "Requested By", width: "250px" },
                    { field: "DateReceived", title: "Date Received", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DueDate", title: "Due Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    { field: "NumberOfTransaction", title: "Number Of Transaction", format: "{0:0}", width: "150px" },
                    { field: "StatusID", title: "Status", editor: aaStatusDDEditor, template: "#=StatusDescription#", width: "200px" },
                    { field: "TimeSpent", title: "Time Spent (Hours)", format: "{0:0.00}", width: "250px" },
                    { field: "TimeSpentOfQA", title: "Time Spent (QA)", format: "{0:0.00}", width: "250px" },
                    //{ command: ["destroy"], title: "&nbsp;", width: "250px" }
                    {
                        command: ["edit",
                                       {
                                           name: "Delete"
                                       , click: function (e) {
                                           e.preventDefault();
                                           var tr = $(e.target).closest("tr");
                                           var data = this.dataItem(tr);

                                           bootbox.confirm("Are you sure to delete this record?", function (result) {
                                               if (result) {
                                                   jQuery('#gridAA').data('kendoGrid').dataSource.remove(data);
                                                   jQuery('#gridAA').data('kendoGrid').dataSource.sync();
                                                   jQuery('#gridAA').data('kendoGrid').dataSource.read();
                                               }
                                           });
                                       }
                                       }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
            ],
            editable: "inline"

        });
        /////////////////////////////// PENDING //////////////////////////////////////

        var dataSourceAAAssigned = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetAssignedAA"
                    , { EmployeeID: Employee_ID }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAAAssigned = ro;
                            // on success
                            //alert(JSON.stringify(arrAAAssigned));
                            e.success(arrAAAssigned);
                        } else {
                            bootbox.alert("Web service error getting GetAssignedAA on loadgridAA. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var statusList = new Object();

                obj.ActivityTypeID = ActivityType_AA;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = tempStartDateTime;
                obj.AAAssignmentID = e.data.AAAssignmentID;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;
                obj.NumberOfTransaction = e.data.NumberOfTransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddAAActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrAAAssigned = ro;
                            e.success(arrAAAssigned);
                            jQuery('#gridAAAssigned').data('kendoGrid').dataSource.read();
                            jQuery('#gridAA').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error AddCSActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "AAAssignmentID",
                    fields: {
                        AAAssignmentID: { editable: false, nullable: false },
                        TicketNumber: { editable: false },
                        ActivityID: { editable: false },
                        ActivityDescription: { editable: false },
                        PriorityListID: { editable: false },
                        PriorityListDescription: { editable: false },
                        TaskDescription: { editable: false },
                        RequestedBy: { editable: false },
                        DateReceived: { type: "date", editable: false },
                        DueDate: { type: "date", editable: false },
                        DateCompleted: { type: "date", editable: false },
                        SLA: { editable: false },
                        NumberOfTransaction: { type: "number" },
                        StatusID: {
                            defaultValue: arrCSStatus[0].ListID
                            , validation: {
                                validateStatus: function (input) {
                                    if (input.attr("data-bind") == "value:StatusID") {
                                        selectedStatus = input.val();
                                        input.attr("data-validateStatus-msg", "Status is required");
                                        if (selectedStatus < 1) {
                                            bootbox.alert("status is required");
                                            return false;
                                        }
                                        else {
                                            return true;
                                        }
                                    }
                                    return true;
                                }
                            }
                        },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", defaultValue: 0.00, validation: { min: 0.01, required: true } },
                        TimeSpentOfQA: { type: "number", defaultValue: 0.00, validation: { min: 0.01, required: true } } 
                    }
                }
            }
        });

        jQuery("#gridAAAssigned").kendoGrid({
            dataSource: dataSourceAAAssigned,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "TicketNumber", title: "Ticket Number", width: "150px" },
                    { field: "ActivityID", title: "Type", editor: aaTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "PriorityListID", title: "Priority", editor: aaPriorityDDEditor, width: "250px", template: "#=PriorityListDescription#" },
                    { field: "TaskDescription", title: "Description", width: "250px" },
                    { field: "RequestedBy", title: "Requested By", width: "250px" },
                    { field: "DateReceived", title: "Date Received", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DueDate", title: "Due Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    { field: "StatusID", title: "Status", editor: aaStatusDDEditor, template: "#=StatusDescription#", width: "200px" },
                    { field: "NumberOfTransaction", title: "Number Of Transaction", format: "{0:0}", width: "150px" },
                    { field: "TimeSpent", title: "Time Spent (Hours)", format: "{0:0.00}", width: "150px" },
                    { command: ["edit"], title: "&nbsp;", width: "250px" }
            ],
            editable: "inline",
            edit: function (e) {
                tempStartDateTime = new Date();
                //alert(tempStartDateTime);
            }
        });

        jQuery(".k-grid-toolbar", "#gridAAAssigned").prepend("<h4>ASSIGNED ADHOC ANALYTICS</h4>");
        jQuery(".k-grid-toolbar", "#gridAA").prepend("<h4>ACTIVITY LOG</h4>");
    }

    function aaTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAActivityList
            });
    }

    function aaPriorityDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAPriorityList
            });
    }

    function aaStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrAAStatusList
            });
    }

});
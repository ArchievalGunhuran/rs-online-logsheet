﻿
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callAjaxRequestJSON2(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet2.asmx/" + _serviceVerb
        , async: false
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function callAsyncAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet2.asmx/" + _serviceVerb
        , async: false
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function ajaxError(response, error) {
    alert("Request Error. " + response.responseText);
}

function ajaxSuccess(response) {
    alert("success");
    alert(JSON.stringify(response));
    var obj = JSON.parse(response.responseText);
    alert("Request Successful");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var arrActivityLogs;
var arrActivityTypeList;
var arrActivityList;
var arrTimeStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var programContainer;
var gridActivityLog;
var currentTR;
var previousActivityTypeID = 0;

var arrTimeStatus = [
      { ID: "New", Value: "New" },
      { ID: "On-going", Value: "On-going" },
      { ID: "On-pause", Value: "On-pause" },
      { ID: "Completed", Value: "Completed" }
];
var arrTimeStatusEditor = [
      { ID: "On-going", Value: "On-going" },
      { ID: "On-pause", Value: "On-pause" },
      { ID: "Completed", Value: "Completed" }
];



jQuery(document).ready(function () {
    
    jQuery("#txtActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridActivityLog').data('kendoGrid').dataSource.read(); }
    });

    callAjaxRequestJSON2("GetActivityTypeList"
        , { UserGroup: USER_GROUP, EmployeeID: Employee_ID, IsManual: false }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrActivityTypeList = ro;
                if (arrActivityTypeList.length > 0)
                    getActivityList();
                else
                    bootbox.alert("No Activity Type assigned.");
            } else {
                bootbox.alert("Web service error when calling Activity Type List. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    function getActivityList() {
        callAjaxRequestJSON2("GetActivityList"
            , { ActivityTypeID: arrActivityTypeList[0].ListID, EmployeeID: Employee_ID, IsManual: false }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrActivityList = ro;
                    if (arrActivityList.length > 0)

                        loadActivityGrid();


                    else
                        bootbox.alert("No Activity assigned.");
                } else {
                    bootbox.alert("Web service error when calling Activity List. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadActivityGrid() {

        var dataSourceActivityLog = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                        callAjaxRequestJSON2("GetActivityLog"
                        , { EmployeeID: Employee_ID, ActivityDate: jQuery("#txtActivityDate").val(), IsManual: false }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivityLogs = ro;
                                e.success(arrActivityLogs);
                                jQuery('#gridActivityLog').data('kendoGrid').refresh();
                            } else {
                                bootbox.alert("Web service error when calling GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                } //read
                , create: function (e) {
                    //alert("for create");

                    var activitData = e.data;

                    //var datasourceAll = dataSourceActivityLog.data();
                    var param = new Object();
                    var logdetail = new Array();
                    var autoPauseLogDetails = new Array();

                    //for (var i = 0; i < datasourceAll.length; i++) {

                    var obj = new Object();
                    var strtime = "00:00:00";

                    jQuery('#gridActivityLog table tbody tr').each(function () {
                        if (jQuery(this).children('td:nth-child(2)').text() == activitData.LogDetailID) {
                            strtime = jQuery(this).children('td:last-child').text();
                        }
                    });

                    var seconds = 0, minutes = 0, hours = 0, actualTimeSpent = 0;
                    var dTime = strtime.split(':');

                    hours = parseInt(dTime[0]);
                    minutes = parseInt(dTime[1]);
                    seconds = parseInt(dTime[2]);

                    actualTimeSpent = (hours * 60) + minutes + (seconds / 60);

                    obj.ActivityLogID = activitData.ActivityLogID == "" ? 0 : activitData.ActivityLogID;
                    obj.LogDetailID = activitData.LogDetailID == "" ? 0 : activitData.LogDetailID;
                    obj.ActivityTypeID = activitData.ActivityTypeID;
                    obj.EmployeeID = Employee_ID;
                    obj.ActivityDate = jQuery("#txtActivityDate").val();//datasourceAll[i].ActivityDate;
                    obj.ActivityID = activitData.ActivityID;
                    obj.TimeSpent = actualTimeSpent; //0; //datasourceAll[i].TimeSpent;
                    obj.NumberOfTransaction = activitData.NumberOfTransaction;
                    obj.Remarks = activitData.Remarks;
                    obj.TimeStatus = activitData.TimeStatus;
                    obj.TimeSpentStr = strtime;

                    logdetail.push(obj);
                    //}

                    //var datasourceAll = dataSourceActivityLog.data();

                    //for (var i = 0; i < datasourceAll.length; i++) {
                    //    var objAuto = new Object();

                    //    if (datasourceAll[i].TimeStatus == "On-going") {
                    //        objAuto.ActivityLogID = datasourceAll[i].ActivityLogID == "" ? 0 : datasourceAll[i].ActivityLogID;
                    //        objAuto.LogDetailID = datasourceAll[i].LogDetailID == "" ? 0 : datasourceAll[i].LogDetailID;
                    //        objAuto.ActivityTypeID = datasourceAll[i].ActivityTypeID;
                    //        objAuto.EmployeeID = Employee_ID;
                    //        objAuto.ActivityDate = jQuery("#txtActivityDate").val();//datasourceAll[i].ActivityDate;
                    //        objAuto.ActivityID = datasourceAll[i].ActivityID;
                    //        objAuto.TimeSpent = actualTimeSpent; //0; //datasourceAll[i].TimeSpent;
                    //        objAuto.NumberOfTransaction = datasourceAll[i].NumberOfTransaction;
                    //        objAuto.Remarks = datasourceAll[i].Remarks;
                    //        objAuto.TimeStatus = "On-pause";
                    //        objAuto.TimeSpentStr = strtime;

                    //        autoPauseLogDetails.push(objAuto);
                    //    }
                    //}

                    param.EmployeeID = Employee_ID;
                    param.ActivityDate = jQuery("#txtActivityDate").val();
                    param.ActivityLog = logdetail;
                    param.IsManual = false;
                    param.ActivityLogAutoPause = autoPauseLogDetails;

                    callAjaxRequestJSON2("UpdateActivityLog"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivityLogs = ro;
                                arrActivityLogs.push(e.data);
                                e.success(arrActivityLogs);
                                jQuery('#gridActivityLog').data('kendoGrid').dataSource.read();
                            } else {
                                bootbox.alert("Web service error when calling AddActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }//create
                , update: function (e) {
                    //alert("for update");
                    var activitData = e.data;

                    //console.log(activitData);

                    var param = new Object();
                    var logdetail = new Array();
                    var autoPauseLogDetails = new Array();
                    
                    //for (var i = 0; i < datasourceAll.length; i++) {
                    var obj = new Object();
                    var strtime = "00:00:00";

                    jQuery('#gridActivityLog table tbody tr').each(function () {
                        if (jQuery(this).children('td:nth-child(2)').text() == activitData.LogDetailID) {
                            strtime = jQuery(this).children('td:last-child').text();
                        }
                    });

                    var seconds = 0, minutes = 0, hours = 0, actualTimeSpent = 0;
                    var dTime = strtime.split(':');

                    hours = parseInt(dTime[0]);
                    minutes = parseInt(dTime[1]);
                    seconds = parseInt(dTime[2]);

                    actualTimeSpent = (hours * 60) + minutes + (seconds / 60);

                    obj.ActivityLogID = activitData.ActivityLogID;
                    obj.LogDetailID = activitData.LogDetailID;
                    obj.ActivityTypeID = activitData.ActivityTypeID;
                    obj.EmployeeID = Employee_ID;
                    obj.ActivityDate = jQuery("#txtActivityDate").val();//datasourceAll[i].ActivityDate;
                    obj.ActivityID = activitData.ActivityID;
                    obj.TimeSpent = actualTimeSpent; // 0; //datasourceAll[i].TimeSpent;
                    obj.NumberOfTransaction = activitData.NumberOfTransaction;
                    obj.Remarks = activitData.Remarks;
                    obj.TimeStatus = activitData.TimeStatus;
                    obj.TimeSpentStr = strtime;
                    //alert(actualTimeSpent)
                    //alert(strtime)
                    logdetail.push(obj);

                    //}

                    var datasourceAll = dataSourceActivityLog.data();

                    for (var i = 0; i < datasourceAll.length; i++) {
                        var objAuto = new Object();

                        if (datasourceAll[i].TimeStatus == "On-going" && activitData.TimeStatus == "On-going") {
                            objAuto.ActivityLogID = datasourceAll[i].ActivityLogID == "" ? 0 : datasourceAll[i].ActivityLogID;
                            objAuto.LogDetailID = datasourceAll[i].LogDetailID == "" ? 0 : datasourceAll[i].LogDetailID;
                            objAuto.ActivityTypeID = datasourceAll[i].ActivityTypeID;
                            objAuto.EmployeeID = Employee_ID;
                            objAuto.ActivityDate = jQuery("#txtActivityDate").val();//datasourceAll[i].ActivityDate;
                            objAuto.ActivityID = datasourceAll[i].ActivityID;
                            objAuto.TimeSpent = actualTimeSpent; //0; //datasourceAll[i].TimeSpent;
                            objAuto.NumberOfTransaction = datasourceAll[i].NumberOfTransaction;
                            objAuto.Remarks = datasourceAll[i].Remarks;
                            objAuto.TimeStatus = activitData.TimeStatus == "Completed" ? "On-going" : "On-pause";
                            objAuto.TimeSpentStr = strtime;

                            autoPauseLogDetails.push(objAuto);
                        }
                    }

                    param.EmployeeID = Employee_ID;
                    param.ActivityDate = jQuery("#txtActivityDate").val();
                    param.ActivityLog = logdetail;
                    param.IsManual = false;
                    param.ActivityLogAutoPause = autoPauseLogDetails;

                    callAjaxRequestJSON2("UpdateActivityLog"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            //console.log(ro);
                            if (data.ErrorMessage == null) {
                                arrActivityLogs = ro;
                                arrActivityLogs.push(e.data);
                                e.success(arrActivityLogs);
                                jQuery('#gridActivityLog').data('kendoGrid').dataSource.read();
                                jQuery('#gridActivityLog').data('kendoGrid').refresh();
                            } else {
                                bootbox.alert("Web service error when calling AddActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }//update
                , destroy: function (e) {
                    var activitData = e.data;

                    var datasourceAll = dataSourceActivityLog.data();
                    var param = new Object();
                    var logdetail = new Array();

                    //for (var i = 0; i < datasourceAll.length; i++) {
                    var obj = new Object();
                    var strtime = "00:00:00";

                    jQuery('#gridActivityLog table tbody tr').each(function () {
                        if (jQuery(this).children('td:nth-child(2)').text() == activitData.LogDetailID) {
                            strtime = jQuery(this).children('td:last-child').text();
                        }
                    });

                    var seconds = 0, minutes = 0, hours = 0, actualTimeSpent = 0;
                    var dTime = strtime.split(':');

                    hours = parseInt(dTime[0]);
                    minutes = parseInt(dTime[1]);
                    seconds = parseInt(dTime[2]);

                    actualTimeSpent = (hours * 60) + minutes + (seconds / 60);

                    obj.ActivityLogID = activitData.ActivityLogID;
                    obj.LogDetailID = activitData.LogDetailID;
                    obj.ActivityTypeID = activitData.ActivityTypeID;
                    obj.EmployeeID = Employee_ID;
                    obj.ActivityDate = jQuery("#txtActivityDate").val();//datasourceAll[i].ActivityDate;
                    obj.ActivityID = activitData.ActivityID;
                    obj.TimeSpent = actualTimeSpent; // 0; //datasourceAll[i].TimeSpent;
                    obj.NumberOfTransaction = activitData.NumberOfTransaction;
                    obj.Remarks = activitData.Remarks;
                    obj.TimeStatus = activitData.TimeStatus;
                    obj.TimeSpentStr = strtime;

                    logdetail.push(obj);
                    //}

                    param.ActivityLog = logdetail;
                    param.ActivityLogID = e.data.ActivityLogID;
                    callAjaxRequestJSON2("DeleteActivityLog"
                                , param
                                , function (response) {
                                    var data = response['d']
                                    var ro = data.ResponseItem;
                                    if (data.ErrorMessage == null) {
                                        e.success();
                                        jQuery('#gridActivityLog').data('kendoGrid').dataSource.read();
                                    } else {
                                        bootbox.alert("Web service error when calling DeleteActivityLog. " + data.ErrorMessage);
                                    }
                                }
                                , ajaxError
                            );
                }//delete destroy

            }//transport
            , schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        //ActivityDate: { type: "date", editable: false, defaultValue: function (e) { return new Date(); } },
                        ActivityDate: { editable: false, defaultValue: function (e) { return new Date(); } },
                        ActivityTypeID: { defaultValue: arrActivityTypeList[0].ListID },
                        ActivityTypeDescription: { editable: false },
                        ActivityID: { defaultValue: arrActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        NumberOfTransaction: { type: "number", validation: { min: 0 }, defaultValue: 1 },
                        Remarks: { validation: { maxLength: 250 } },
                        TimeStatus: { defaultValue: arrTimeStatus[0].ID },
                        TimeSpentStr: { editable: false, defaultValue: "00:00:00" }
                    }
                }
            } //schema
            , autoSync: true
            //, batch: true
        }); //var dataSourceActivityLog = new kendo.data.DataSource


        gridActivityLog = jQuery("#gridActivityLog").kendoGrid({
            dataSource: dataSourceActivityLog,
            toolbar: ["create"], //,"save","cancel"],
            columns: [
                    {
                        command: ["destroy"]
                        , title: "&nbsp;"
                        , width: "200px"
                    },
                    { field: "LogDetailID", title: "Log ID", width: "80px" },
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "ActivityTypeID", title: "Activity Type", editor: activityTypeListTaskDDEditor, width: "250px", template: "#=ActivityTypeDescription#" },
                    { field: "ActivityID", title: "Task", editor: activityListTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "NumberOfTransaction", title: "Number of Transaction", width: "120px" },
                    { field: "Remarks", width: "250px" },
                    { field: "TimeStatus", title: "Status", editor: statusListTaskDDEditor, width: "150px", template: "#=TimeStatus#" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" }
            ],
            editable: true //"inline"
            , edit: function (e) {
                //alert(e.model.ActivityTypeID);
                previousActivityTypeID = e.model.ActivityTypeID;
                if (e.model.TimeStatus == "Completed")
                    jQuery("#gridActivityLog").data("kendoGrid").closeCell();
            }
        });

    }//load activity grid




    function statusListTaskDDEditor(container, options) {

        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "Value"
                , dataValueField: "ID"
                , dataSource: arrTimeStatusEditor
            });
    }

    function activityTypeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrActivityTypeList
            });
    }

    function activityListTaskDDEditor(container, options) {
        //alert(JSON.stringify(options.model.ActivityTypeID));
        
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: updateActivtyList(options.model.ActivityTypeID)
            });
    }

    function updateActivtyList(activityTypeID) {

        callAsyncAjaxRequestJSON("GetActivityList"
                , { ActivityTypeID: activityTypeID, EmployeeID: Employee_ID, IsManual: false }
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        if (ro.length > 0) {
                            arrActivityList = ro;
                            //var dataSource = new kendo.data.DataSource({
                            //    data: arrActivityList
                            //});
                            //var ddd = dd.data("kendoDropDownList");
                            //ddd.setDataSource(dataSource);
                        }
                        else
                            bootbox.alert("No Activity assigned.");
                    } else {
                        bootbox.alert("Web service error when calling Activity List. " + data.ErrorMessage);
                    }
                }
                , ajaxError
            );

        return arrActivityList;

    }

}); //jQuery(document).ready
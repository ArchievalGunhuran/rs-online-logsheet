﻿
var activityLogFieldsList = new Array();

var arrProjectActivityLogs;
var arrProjectActivityList;
var arrProjectStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;

jQuery(document).ready(function () {

    jQuery("#txtProjectActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridProject').data('kendoGrid').dataSource.read(); }
    });

    //GET TASK LIST FOR PROJECT
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_PROJECT, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrProjectActivityList = ro;
                if (arrProjectActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from PROJECT - Task field");
            } else {
                bootbox.alert("Web service error when calling Project - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM PROJECT
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_PROJECT, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrProjectStatus = ro;
                    loadGridProject();
                } else {
                    bootbox.alert("Web service error calling Project - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridProject() {
        var dataSourceProject = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                    , { ActivityTypeID: ActivityType_PROJECT, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtProjectActivityDate").val() }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrProjectActivityLogs = ro;
                            // on success
                            e.success(arrProjectActivityLogs);
                        } else {
                            bootbox.alert("Web service error when calling Project - GetActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var projectStatus = new Object();

                obj.ActivityTypeID = ActivityType_PROJECT;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.Remarks = e.data.Remarks;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddProjectActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrProjectActivityLogs = ro;
                            // on success
                            arrProjectActivityLogs.push(e.data);
                            e.success(arrProjectActivityLogs);
                            jQuery('#gridProject').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error getting testimonials. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var projectStatus = new Object();

                obj.ActivityTypeID = ActivityType_PROJECT;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.Remarks = e.data.Remarks;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateProjectActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrProjectActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrProjectActivityLogs);
                            jQuery('#gridProject').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error getting testimonials. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_PROJECT;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridProject').data('kendoGrid').dataSource.read();
                                } else {
                                    bootbox.alert("Web service error getting testimonials. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }
                            , validation: {
                                validateDate: function (input) {
                                    if (input.attr("data-bind") == "value:ActivityDate") {
                                        input.attr("data-validateDate-msg", "ProcessDate can not be later than 3 days or greater than today");
                                        return checkdate(input.val());
                                    }
                                    return true;
                                }
                            }
                        },
                        ActivityID: { defaultValue: arrProjectActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        Remarks: { validation: { maxLength: 250} },
                        StatusID: { defaultValue: arrProjectStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", validation: { min: 0.01, required: true } },
                        //QAByEmployeeName: { editable: false },
                    }
                }
            }
        });

        jQuery("#gridProject").kendoGrid({
            dataSource: dataSourceProject,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "ActivityID", title: "Task", editor: projectTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "Remarks", width: "250px" },
                    { field: "StatusID", title: "Status", editor: projectStatusDDEditor, width: "150px", template: "#=StatusDescription#" },
                    { field: "TimeSpent", title: "Time Spent (Processor)", format: "{0:0.00}", width: "250px" },
                    { field: "TimeSpentOfQA", title: "Time Spent of QA (Staging)", format: "{0:0.00}", width: "250px" },
                    { field: "TimeSpentOfQAProd", title: "Time Spent of QA (Production)", format: "{0:0.00}", width: "250px" },
                    //{ field: "QAByEmployeeName", title: "QA By", width: "250px" },
                    //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                    {command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridProject').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridProject').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridProject').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline"
        });
    }

    function projectTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrProjectActivityList
            });
    }

    function projectStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrProjectStatus
            });
    }

});
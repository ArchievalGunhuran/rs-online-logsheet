﻿
var activityLogFieldsList = new Array();

var arrEMTAssignment;
var arrEMTActivityList;
var arrEMTStatusList;
var arrEMTEmployeeList;
var listEMTEmployeeID = new Array();

jQuery(document).ready(function () {


    callAjaxRequestJSON("GetEmployeeList"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrEMTEmployeeList = ro;
                if (arrEMTEmployeeList.length > 0) {
                    for (i = 0; i < arrEMTEmployeeList.length; i++) {
                        //alert(arrEMTEmployeeList[i].ListDescription);
                        listEMTEmployeeID.push(arrEMTEmployeeList[i].ListID);
                    }
                    getActivityList();
                }
                else
                    alert("No items available for EMT Assignment Employee List");
            } else {
                alert("Web service error when calling EMT Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );


    function getActivityList() {

        callAjaxRequestJSON("GetActivityList"
            , { ActivityTypeID: ActivityType_EMT, EmployeeID: arrEMTEmployeeList[0].ListID }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrEMTActivityList = ro;
                    if (arrEMTActivityList.length > 0)
                        getStatusList();
                    else
                        alert("No items available for EMT Assignment Activity List");

                } else {
                    alert("Web service error when calling EMT Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function getStatusList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_EMT, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrEMTStatusList = ro;
                    if (arrEMTStatusList.length > 0)
                        loadEMTAssignment();
                    else
                        alert("No items available for EMT Assignment Status List");

                } else {
                    alert("Web service error when calling EMT Assignment getStatusList_GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadEMTAssignment() {

        var dataSourceEMTAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetEMTAssignment"
                    , { ActivityTypeID: ActivityType_EMT }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrEMTAssignment = ro;
                            // on success
                            e.success(arrEMTAssignment);
                        } else {
                            alert("Web service error when calling loadEMTAssignment_GetEMTAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var employeeList = new Object();
                
                obj.EmployeeID = Employee_ID;
                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.ActivityID = e.data.ActivityID;
                obj.DueDate = e.data.DueDate;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;

                obj.AssignEmployeeID = e.data.AssignEmployeeID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddEMTAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrEMTAssignment = ro;
                            e.success(arrEMTAssignment);
                            jQuery('#gridEMTAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            alert("Web service error calling loadEMTAssignment_AddEMTAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var employeeList = new Object();
                var statusList = new Object();

                obj.EMTAssignmentID = e.data.EMTAssignmentID;
                obj.EmployeeID = Employee_ID;
                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.ActivityID = e.data.ActivityID;
                obj.DueDate = e.data.DueDate;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;

                obj.AssignEmployeeID = e.data.AssignEmployeeID;
                obj.StatusID = e.data.StatusID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateEMTAssignment"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    arrEMTAssignment = ro;
                                    e.success(arrEMTAssignment);
                                    jQuery('#gridEMTAssignment').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error calling loadEMTAssignment_UpdateEMTAssignment. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {

                var param = new Object();
                param.EMTAssignmentID = e.data.EMTAssignmentID;

                callAjaxRequestJSON("DeleteEMTAssignment"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridEMTAssignment').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error calling loadEMTAssignment_DeleteEMTAssignment. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "EMTAssignmentID",
                    fields: {
                        EMTAssignmentID: { editable: false, nullable: false },
                        AssignEmployeeID: { validation: { required: true }, defaultValue: arrEMTEmployeeList[0].ListID },
                        AssignEmployeeName: { editable: false },
                        StoreName: { validation: { maxLength: 250, required: true} },
                        StoreNumber: { type: "number", validation: { maxLength: 6, required: true } },
                        ActivityID: { validation: { required: true }, defaultValue: arrEMTActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        Requestor: { validation: { maxLength: 250 } },
                        TicketNumber: { editable: false },
                        DateTimeRequested: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DueDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        StatusID: { validation: { required: true }, defaultValue: arrEMTStatusList[0].ListID, editable: false },
                        StatusDescription: { editable: false },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        SLA: { editable: false }
                    }
                }
            }
        });

        var gridEMTAssignment = jQuery("#gridEMTAssignment").kendoGrid({
            dataSource: dataSourceEMTAssignment,
            pageable: true,
            toolbar: ["create", "cancel"],
            filterable: true,
            columns: [
                    {
                        field: "AssignEmployeeID", title: "Processor", editor: employeeListTaskDDEditor, template: "#=AssignEmployeeName#", width: "250px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrEMTEmployeeList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            ,operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "StoreName", title: "Store Name", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "StoreNumber", title: "Store Number", format: "{0:000000}", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ActivityID", title: "Type Of Request", editor: programTypeTaskDDEditor, template: "#=ActivityDescription#", width: "250px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrEMTActivityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TicketNumber", title: "Ticket Number", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "Requestor", title: "Requestor", width: "250px" },
                    { field: "DateTimeRequested", title: "Date Time Requested", width: "250px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: false },
                    { field: "DueDate", title: "Due Date", width: "250px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: false },
                    {
                        field: "StatusID", title: "Status", editor: statusListTaskDDEditor, template: "#=StatusDescription#", width: "200px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrEMTStatusList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "250px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline"
        });
        
    }

    function programTypeTaskDDEditor(container, options) {
        programContainer = container;
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEMTActivityList
            });

    }

    function employeeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEMTEmployeeList
                , change: function (e) {

                    callAjaxRequestJSON("GetActivityList"
                        , { ActivityTypeID: ActivityType_EMT, EmployeeID: this.value() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEMTActivityList = ro;
                                if (arrEMTActivityList.length > 0) {
                                    programContainer.empty();
                                    var dd = jQuery("<input data-bind='value:ActivityID'/>")
                                        .appendTo(programContainer)
                                        .kendoDropDownList({
                                            dataTextField: "ListDescription"
                                            , dataValueField: "ListID"
                                            , change: function (e) {
                                                options.model.ActivityID = this.value();
                                            }
                                        }).data("kendoDropDownList");

                                    dd.setDataSource(arrEMTActivityList);
                                    options.model.ActivityID = dd.value();
                                }
                                else
                                    alert("No items available for CS Assignment Activity List");

                            } else {
                                alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                }
            });
    }

    function statusListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEMTStatusList
            });
    }

});
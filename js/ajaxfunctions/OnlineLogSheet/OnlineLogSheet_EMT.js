﻿
var activityLogFieldsList = new Array();

var arrEMTActivityLogs;
var arrEMTPending;
var arrEMTActivityList;
var arrEMTStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var tempStartDateTime = new Date();

jQuery(document).ready(function () {

    jQuery("#txtEMTActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridEMT').data('kendoGrid').dataSource.read(); }
    });

    //GET TASK LIST FOR EMT
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_EMT, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrEMTActivityList = ro;
                if (arrEMTActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from EMT - Task field");
            } else {
                bootbox.alert("Web service error when calling EMT - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM PROJECT
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_EMT, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrEMTStatus = ro;
                    loadGridEMT();
                } else {
                    bootbox.alert("Web service error when calling EMT - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridEMT() {
        var dataSourceEMT = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                        , { ActivityTypeID: ActivityType_EMT, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtEMTActivityDate").val() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEMTActivityLogs = ro;
                                // on success
                                e.success(arrEMTActivityLogs);
                            } else {
                                bootbox.alert("Web service error when calling EMT - GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_EMT;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;
                obj.DateTimeSaved = new Date();
                obj.Remarks = e.data.Remarks;
                obj.StatusID = e.data.StatusID;
                obj.Numberoftransaction = e.data.Numberoftransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddEMTActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrEMTActivityLogs = ro;
                            // on success
                            arrEMTActivityLogs.push(e.data);
                            e.success(arrEMTActivityLogs);
                            jQuery('#gridEMT').data('kendoGrid').dataSource.read();
                            jQuery('#gridEMTPending').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling AddEMTActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_EMT;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.StoreName = e.data.StoreName;
                obj.StoreNumber = e.data.StoreNumber;
                obj.DateTimeRequested = e.data.DateTimeRequested;
                obj.Requestor = e.data.Requestor;
                obj.DateTimeCompleted = e.data.DateTimeCompleted;
                obj.Remarks = e.data.Remarks;
                obj.StatusID = e.data.StatusID;
                obj.TimeSpent = e.data.TimeSpent;
                obj.Numberoftransaction = e.data.Numberoftransaction;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateEMTActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrEMTActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrEMTActivityLogs);
                            jQuery('#gridEMT').data('kendoGrid').dataSource.read();
                            jQuery('#gridEMTPending').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling UpdateEMTActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_EMT;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridEMT').data('kendoGrid').dataSource.read();
                                    jQuery('#gridEMTPending').data('kendoGrid').dataSource.read();
                                } else {
                                    bootbox.alert("Web service error when calling EMT - DeleteActivityLog. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { editable: false, defaultValue: arrEMTActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        StoreName: { editable: false, validation: { required: true, maxLength: 250 } },
                        StoreNumber: { editable: false, type: "number" },
                        DateTimeRequested: { editable: false, type: "date", defaultValue: function (e) { return new Date(); } },
                        Requestor: { editable: false, validation: { maxLength: 250 } },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        Remarks: { validation: { maxLength: 250} },
                        StatusID: { defaultValue: arrEMTStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number" },
                        TimeSpentStr: { editable: false },
                        Numberoftransaction: { type: "number", validation: { min: 0} },
                        TAT: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridEMT").kendoGrid({
            dataSource: dataSourceEMT,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "StoreNumber", title: "Store Number", format: "{0:000000}", width: "120px" },
                    { field: "ActivityID", title: "Type of Request", editor: emtTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Requestor", title: "Requestor", width: "250px" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Remarks", title: "Remarks", width: "250px" },
                    { field: "StatusID", title: "Status", editor: emtStatusDDEditor, width: "150px", template: "#=StatusDescription#" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "Numberoftransaction", title: "Number of Transaction", width: "120px" },
                    { field: "TAT", title: "TAT", width: "120px" },
                    //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                    {command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridEMT').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridEMT').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridEMT').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline",
            edit: function (e) {
                if (!e.model.isNew()) {
                    var select = e.container.find('input[data-bind="value:StatusID"]').data('kendoDropDownList');
                    select.enable(false);
                }
            }
        });


        ///////////////////////////////////////////////// PENDING ITEMS /////////////////////////////////////////////////////

        var dataSourceEMTPending = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetAssignedEMT"
                        , { EmployeeID: Employee_ID }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEMTPending = ro;
                                // on success
                                e.success(arrEMTPending);
                            } else {
                                bootbox.alert("Web service error when calling EMT - GetPendingActivity. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
                , update: function (e) {
                        var param = new Object();
                        var obj = new Object();
                        var status = new Object();

                        obj.ActivityTypeID = ActivityType_EMT;
                        //obj.LogDetailID = e.data.LogDetailID;
                        //obj.ActivityLogID = e.data.ActivityLogID;
                        obj.EmployeeID = Employee_ID;
                        obj.ActivityDate = tempStartDateTime;
                        obj.ActivityID = e.data.ActivityID;

                        //obj.StoreName = e.data.StoreName;
                        //obj.StoreNumber = e.data.StoreNumber;
                        //obj.DateTimeRequested = e.data.DateTimeRequested;
                        //obj.Requestor = e.data.Requestor;
                        obj.DateTimeSaved = new Date();
                        obj.Remarks = e.data.Remarks;
                        obj.StatusID = e.data.StatusID;
                        obj.Numberoftransaction = e.data.Numberoftransaction;
                        obj.EMTAssignmentID = e.data.EMTAssignmentID;

                        param.ActivityLog = obj;

                        callAjaxRequestJSON("AddEMTActivityLog"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrEMTPending = ro;

                                e.success(arrEMTPending);
                                jQuery('#gridEMT').data('kendoGrid').dataSource.read();
                                jQuery('#gridEMTPending').data('kendoGrid').dataSource.read();
                            } else {
                                bootbox.alert("Web service error when calling AddEMTActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
            } //transport
            ,
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },

            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "EMTAssignmentID",
                    fields: {
                        EMTAssignmentID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { editable: false, defaultValue: arrEMTActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        StoreName: { editable: false, validation: { required: true, maxLength: 250 } },
                        StoreNumber: { editable: false, type: "number" },
                        DateTimeRequested: { editable: false, type: "date", defaultValue: function (e) { return new Date(); } },
                        Requestor: { editable: false, validation: { maxLength: 250 } },
                        DateTimeCompleted: { type: "date", defaultValue: "", editable: false },
                        Remarks: { validation: { maxLength: 250} },
                        StatusID: { defaultValue: arrEMTStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number" },
                        TimeSpentStr: { editable: false },
                        Numberoftransaction: { type: "number", validation: { min: 0} },
                        TAT: { editable: false }
                    }
                }
            }
        });


        jQuery("#gridEMTPending").kendoGrid({
            dataSource: dataSourceEMTPending,
            pageable: true,
            toolbar: [],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "StoreName", title: "Store Name", width: "250px" },
                    { field: "StoreNumber", title: "Store Number", format: "{0:000000}", width: "120px" },
                    { field: "ActivityID", title: "Type of Request", editor: emtTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "DateTimeRequested", title: "Date Time Requested", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Requestor", title: "Requestor", width: "250px" },
                    { field: "DateTimeCompleted", title: "Date Time Completed", format: "{0:MM/dd/yyyy hh:mm tt}", width: "200px" },
                    { field: "Remarks", title: "Remarks", width: "250px" },
                    { field: "StatusID", title: "Status", editor: emtStatusDDEditor, width: "150px", template: "#=StatusDescription#" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { field: "Numberoftransaction", title: "Number of Transaction", width: "120px" },
                    { field: "TAT", title: "TAT", width: "120px" },
                    { command: ["edit"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline",
            edit: function (e) {
                tempStartDateTime = new Date();
            },
            save: function (e) {
                e.model.dirty = true;
            }
        });

        jQuery(".k-grid-toolbar", "#gridEMTPending").prepend("<h4>ASSIGNED EM&T</h4>");
        jQuery(".k-grid-toolbar", "#gridEMT").prepend("<h4>ACTIVITY LOG</h4>");
    }

    function emtTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEMTActivityList
            });
    }

    function emtStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEMTStatus
            });
    }

});
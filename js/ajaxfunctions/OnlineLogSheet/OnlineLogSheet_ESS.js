﻿
var activityLogFieldsList = new Array();

var arrESSActivityLogs;
var arrESSActivityList;
var arrESSStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var curDateTime = new Date();

jQuery(document).ready(function () {

    jQuery("#txtESSActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridESS').data('kendoGrid').dataSource.read(); }
    });


    //GET TASK LIST FOR EFS
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_ESS, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrESSActivityList = ro;
                if (arrESSActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from ESS - Task field");
            } else {
                bootbox.alert("Web service error when calling ESS - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM PROJECT
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_ESS, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrESSStatus = ro;
                    loadGridESS();
                } else {
                    bootbox.alert("Web service error when calling ESS - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridESS() {

        var dataSourceESS = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                        , { ActivityTypeID: ActivityType_ESS, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtESSActivityDate").val() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrESSActivityLogs = ro;
                                // on success
                                e.success(arrESSActivityLogs);
                            } else {
                                bootbox.alert("Web service error when calling ESS - GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var objAcct = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_ESS;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.CustomerName = e.data.CustomerName;
                obj.CustomerCompany = e.data.CustomerCompany;
                obj.Issue = e.data.Issue;
                obj.TicketNumber = e.data.TicketNumber;
                obj.DateTimeSaved = new Date();
                obj.DateTimeReceived = e.data.DateTimeReceived;

                obj.StatusID = e.data.StatusID;
                obj.StatusDescription = e.data.StatusDescription;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddESSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrESSActivityLogs = ro;
                            // on success
                            arrESSActivityLogs.push(e.data);
                            e.success(arrESSActivityLogs);
                            jQuery('#gridESS').data('kendoGrid').dataSource.read();
                            
                        } else {
                            bootbox.alert("Web service error when calling ESS AddESSActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_ESS;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;

                obj.CustomerName = e.data.CustomerName;
                obj.CustomerCompany = e.data.CustomerCompany;
                obj.Issue = e.data.Issue;
                obj.TicketNumber = e.data.TicketNumber;
                obj.DateTimeSaved = new Date();
                obj.DateTimeReceived = e.data.DateTimeReceived;

                obj.StatusID = e.data.StatusID;
                obj.StatusDescription = e.data.StatusDescription;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateESSActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrEFSActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrEFSActivityLogs);
                            jQuery('#gridESS').data('kendoGrid').dataSource.read();
                            
                        } else {
                            bootbox.alert("Web service error when calling ESS UpdateESSActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );

            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_ESS;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridESS').data('kendoGrid').dataSource.read();
                                    
                                } else {
                                    bootbox.alert("Web service error when calling ESS - DeleteActivityLog. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                bootbox.alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }, editable: false },
                        ActivityID: { defaultValue: arrESSActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        DateTimeSaved: { type: "date", defaultValue: "", editable: false },
                        DateTimeReceived: { type: "date", defaultValue: function (e) { return new Date(); } },
                        CustomerName: { validation: { required: true, maxLength: 250} },
                        CustomerCompany: { validation: { required: true, maxLength: 250} },
                        Issue: { validation: { required: true, maxLength: 250} },
                        TicketNumber: { type: "number", validation: { min: 0, maxLength: 6} },
                        StatusID: { defaultValue: arrESSStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { editable: false, type: "number" },
                        TimeSpentStr: { editable: false },
                        SLA: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridESS").kendoGrid({
            dataSource: dataSourceESS,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "ActivityID", title: "Type", editor: essTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "CustomerName", title: "Customer Name", width: "250px" },
                    { field: "CustomerCompany", title: "Customer Company", width: "250px" },
                    { field: "Issue", title: "Issue", width: "250px" },
                    { field: "TicketNumber", title: "Ticket Number", format: "{0:000000}", width: "120px" },
                    { field: "DateTimeReceived", title: "Date Time Received", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "DateTimeSaved", title: "Date Time Saved", format: "{0:MM/dd/yyyy hh:mm tt}", width: "150px" },
                    { field: "SLA", title: "SLA", width: "100px" },
                    { field: "StatusID", title: "Status", editor: essStatusDDEditor, width: "250px", template: "#=StatusDescription#" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" },
                    { command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridESS').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridESS').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridESS').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline"

        });
    }

    function essTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrESSActivityList
            });
    }

    function essStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrESSStatus
            });
    }

});
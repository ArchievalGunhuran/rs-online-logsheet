﻿
var activityLogFieldsList = new Array();

var arrALRActivityLogs;
var arrALRActivityList;
var arrALRStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;

jQuery(document).ready(function () {

    jQuery("#txtALRActivityDate").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridALR').data('kendoGrid').dataSource.read(); }
    });


    //GET TASK LIST FOR ALR
    callAjaxRequestJSON("GetActivityList"
        , { ActivityTypeID: ActivityType_ALR, EmployeeID: Employee_ID }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrALRActivityList = ro;
                if (arrALRActivityList.length > 0)
                    getList();
                else
                    bootbox.alert("No items from ALR - Reports field");
            } else {
                bootbox.alert("Web service error when calling ALR - GetActivityList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );
    //GET STATUS LIST FORM
    function getList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_ALR, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrALRStatus = ro;
                    loadGridALR();
                } else {
                    bootbox.alert("Web service error when calling ALR - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadGridALR() {

        var dataSourceProject = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLog"
                    , { ActivityTypeID: ActivityType_ALR, EmployeeID: Employee_ID, ActivityDate: jQuery("#txtALRActivityDate").val() }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrALRActivityLogs = ro;
                            // on success
                            e.success(arrALRActivityLogs);
                        } else {
                            bootbox.alert("Web service error calling ALR - GetActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_ALR;
                obj.LogDetailID = 0;
                obj.ActivityLogID = 0;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("AddALRActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrALRActivityLogs = ro;
                            // on success
                            arrALRActivityLogs.push(e.data);
                            e.success(arrALRActivityLogs);
                            jQuery('#gridALR').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling AddALRActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();
                var status = new Object();

                obj.ActivityTypeID = ActivityType_ALR;
                obj.LogDetailID = e.data.LogDetailID;
                obj.ActivityLogID = e.data.ActivityLogID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityDate = e.data.ActivityDate;
                obj.ActivityID = e.data.ActivityID;
                obj.TimeSpent = e.data.TimeSpent;
                obj.StatusID = e.data.StatusID;

                param.ActivityLog = obj;

                callAjaxRequestJSON("UpdateALRActivityLog"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrALRActivityLogs = ro;
                            // on success
                            //arrAdminActivityLogs.push(e.data);
                            e.success(arrALRActivityLogs);
                            jQuery('#gridALR').data('kendoGrid').dataSource.read();
                        } else {
                            bootbox.alert("Web service error when calling UpdateALRActivityLog. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            , destroy: function (e) {
                var param = new Object();
                param.ActivityTypeID = ActivityType_ALR;
                param.ActivityLogID = e.data.ActivityLogID;
                callAjaxRequestJSON("DeleteActivityLog"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridALR').data('kendoGrid').dataSource.read();
                                } else {
                                    bootbox.alert("Web service error when calling DeleteActivityLog. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        ActivityDate: { type: "date", defaultValue: function (e) { return new Date(); }
                            , validation: {
                                validateDate: function (input) {
                                    if (input.attr("data-bind") == "value:ActivityDate") {
                                        input.attr("data-validateDate-msg", "ProcessDate can not be later than 3 days or greater than today");
                                        return checkdate(input.val());
                                    }
                                    return true;
                                }
                            }
                        },
                        ActivityID: { defaultValue: arrALRActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        StatusID: { defaultValue: arrALRStatus[0].ListID },
                        StatusDescription: { editable: false },
                        TimeSpent: { type: "number", validation: { min: 0.01, required: true} }
                    }
                }
            }
        });

        jQuery("#gridALR").kendoGrid({
            dataSource: dataSourceProject,
            pageable: true,
            toolbar: ["create"],
            columns: [
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "ActivityID", title: "Reports", editor: alrTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "StatusID", title: "Status", editor: alrStatusDDEditor, width: "150px", template: "#=StatusDescription#" },
                    { field: "TimeSpent", title: "Time Spent (hour)", format: "{0:0.00}", width: "120px" },
                    //{ command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                    {command: ["edit",
                                    { name: "Delete"
                                    , click: function (e) {
                                        e.preventDefault();
                                        var tr = $(e.target).closest("tr");
                                        var data = this.dataItem(tr);

                                        bootbox.confirm("Are you sure to delete this record?", function (result) {
                                            if (result) {
                                                jQuery('#gridALR').data('kendoGrid').dataSource.remove(data);
                                                jQuery('#gridALR').data('kendoGrid').dataSource.sync();
                                                jQuery('#gridALR').data('kendoGrid').dataSource.read();
                                            }
                                        });
                                    }
                                    }
                        ], title: "&nbsp;"
                        , width: "250px"
                    }
                ],
            editable: "inline"
        });
    }

    function alrTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrALRActivityList
            });
    }

    function alrStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrALRStatus
            });
    }

});
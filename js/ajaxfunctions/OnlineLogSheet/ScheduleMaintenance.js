﻿
var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[2];
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {

    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/ScheduleMaintenance.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

var arrEmployeeList;
var arrSchedule;
var arrMonthList;
var arrMonthName;
var arrMonthID;
var curDateTime = new Date();

jQuery(document).ready(function () {

    callAjaxRequestJSON("GetEmployeeList"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrEmployeeList = ro;
                arrMonthList = new Array();
                
                arrMonthList = [{ListID: 1, ListDescription: "January"}
                    , {ListID: 2, ListDescription: "February"}
                    , {ListID: 3, ListDescription: "March"}
                    , {ListID: 4, ListDescription: "April"}
                    , {ListID: 5, ListDescription: "May"}
                    , {ListID: 6, ListDescription: "June"}
                    , {ListID: 7, ListDescription: "July"}
                    , {ListID: 8, ListDescription: "August"}
                    , {ListID: 9, ListDescription: "September"}
                    , {ListID: 10, ListDescription: "October"}
                    , { ListID: 11, ListDescription: "November" }
                    , {ListID: 12, ListDescription: "December"}
                ];
                
                if (arrEmployeeList.length > 0) {
                    loadGridSchedule();
                }
                else
                    alert("No items from Schedule Maintenance - Employee List");
            } else {
                alert("Web service error getting GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    function loadGridSchedule() {

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    param.Schedule = obj;

                    callAjaxRequestJSON("GetScheduleMaintenance"
                        , param 
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrSchedule = ro;
                                //alert(JSON.stringify(arrSchedule));
                                e.success(arrSchedule);
                            } else {
                                alert("Web service error when calling loadGridSchedule - GetScheduleMaintenance. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
                , create: function (e) {
                    var param = new Object();
                    var obj = new Object();
                
                    obj.EmployeeID = e.data.EmployeeID;
                    obj.ScheduleMonthID = e.data.ScheduleMonthID;
                    obj.ScheduleYear = e.data.ScheduleYear;
                    obj.ExpectedWorkingDays = e.data.ExpectedWorkingDays;
                    obj.NumberOfLeaves = e.data.NumberOfLeaves;
                    obj.ScheduledLeaves = e.data.ScheduledLeaves;
                    obj.Tardiness = e.data.Tardiness;
                    obj.RegisteredBy = Employee_ID;

                    param.Schedule = obj;

                    callAjaxRequestJSON("AddScheduleMaintenance"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrSchedule = ro;
                                // on success
                                arrSchedule.push(e.data);
                                e.success(arrSchedule);
                                jQuery('#gridScheduleMaintenance').data('kendoGrid').dataSource.read();
                            } else {
                                alert(data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
                , update: function (e) {
                    var param = new Object();
                    var obj = new Object();

                    obj.ScheduleID = e.data.ScheduleID;
                    obj.EmployeeID = e.data.EmployeeID;
                    obj.ScheduleMonthID = e.data.ScheduleMonthID;
                    obj.ScheduleYear = e.data.ScheduleYear;
                    obj.ExpectedWorkingDays = e.data.ExpectedWorkingDays;
                    obj.NumberOfLeaves = e.data.NumberOfLeaves;
                    obj.ScheduledLeaves = e.data.ScheduledLeaves;
                    obj.Tardiness = e.data.Tardiness;
                    obj.RegisteredBy = Employee_ID;

                    param.Schedule = obj;

                    callAjaxRequestJSON("UpdateScheduleMaintenance"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrSchedule = ro;
                                // on success
                                arrSchedule.push(e.data);
                                e.success(arrSchedule);
                                jQuery('#gridScheduleMaintenance').data('kendoGrid').dataSource.read();
                            } else {
                                alert(data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }
                , destroy: function (e) {
                    var param = new Object();
                    param.ScheduleID = e.data.ScheduleID;

                    callAjaxRequestJSON("DeleteScheduleMaintenance"
                                    , param
                                    , function (response) {
                                        var data = response['d']
                                        var ro = data.ResponseItem;
                                        if (data.ErrorMessage == null) {
                                            e.success();
                                            jQuery('#gridScheduleMaintenance').data('kendoGrid').dataSource.read();
                                        } else {
                                            alert("Web service error when calling DeleteScheduleMaintenance. " + data.ErrorMessage);
                                        }
                                    }
                                    , ajaxError
                                );

                    jQuery('#gridScheduleMaintenance').data('kendoGrid').dataSource.read();
                } //delete
            }
            , error: function (e) {
                // handle data operation error
                alert("Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 15,
            batch: false,
            schema: {
                model: {
                    id: "ScheduleID",
                    fields: {
                        ScheduleID: { editable: false, nullable: false },
                        EmployeeID: { defaultValue: arrEmployeeList[0].ListID },
                        EmployeeName: { editable: false },
                        ScheduleMonthID: { defaultValue: arrMonthList[0].ListID },
                        ScheduleMonthName: { editable: false },
                        ScheduleYear: { type: "number", validation: { maxLength: 4, required: true, min: 2016, max: 2999 }, defaultValue: function (e) { var currentTime = new Date(); return year = currentTime.getFullYear();}  },
                        ExpectedWorkingDays: { type: "number", validation: { required: true, maxLength: 2, min:0, max: 31 } },
                        NumberOfLeaves: { type: "number", validation: { required: true, maxLength: 3, min: 0, max: 31 } },
                        ScheduledLeaves: { type: "number", validation: { required: true, maxLength: 3, min: 0, max: 31 } },
                        Tardiness: { type: "number", validation: { required: true, maxLength: 3, min: 0, max: 8 } }
                    }
                }
            }
        });

        jQuery("#gridScheduleMaintenance").kendoGrid({
            dataSource: dataSource,
            pageable: true,
            filterable: true,
            toolbar: ["create"],
            columns: [
                    {
                        field: "EmployeeID", title: "Employee Name", editor: emplpoyeeDDEditor, width: "250px", template: "#=EmployeeName#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrEmployeeList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduleMonthID", title: "Month", editor: monthDDEditor, width: "150px", template: "#=ScheduleMonthName#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrMonthList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ScheduleYear", title: "Year", width: "100px", format: "{0:0000}"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "ExpectedWorkingDays", title: "Expected Working Days", width: "150px", format: "{0:0}", filterable: false },
                    { field: "NumberOfLeaves", title: "Unscheduled Leaves", width: "150px", format: "{0:0.0}", filterable: false },
                    { field: "ScheduledLeaves", title: "Scheduled Leaves", width: "150px", format: "{0:0.0}", filterable: false },
                    { field: "Tardiness", title: "Tardiness (Minutes)", width: "150px", format: "{0:0}", filterable: false },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
                ],
            editable: "inline"
        });

    }

    function emplpoyeeDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrEmployeeList
            });
    }

    function monthDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrMonthList
            });
    }

});

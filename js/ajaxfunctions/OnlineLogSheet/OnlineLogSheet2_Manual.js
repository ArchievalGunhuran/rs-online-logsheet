﻿
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callAjaxRequestJSON2(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet2.asmx/" + _serviceVerb
        , async: false
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function callAsyncAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/OnlineLogSheet2.asmx/" + _serviceVerb
        , async: false
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function ajaxError(response, error) {
    alert("Request Error. " + response.responseText);
}

function ajaxSuccess(response) {
    alert("success");
    alert(JSON.stringify(response));
    var obj = JSON.parse(response.responseText);
    alert("Request Successful");
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var arrActivityLogs;
var arrActivityTypeListManual;
var arrActivityListManual;
var arrTimeStatus;
var curActivityLogID = 0;
var curActivityLogDetailID = 0;
var programContainer;
var gridActivityLog;
var currentTR;
var previousActivityTypeID = 0;

var arrTimeStatus = [
      { ID: "New", Value: "New" },
      { ID: "On-going", Value: "On-going" },
      { ID: "On-pause", Value: "On-pause" },
      { ID: "Completed", Value: "Completed" }
];


jQuery(document).ready(function () {

    jQuery("#txtActivityDateManual").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridActivityLogManual').data('kendoGrid').dataSource.read(); }
    });

    callAjaxRequestJSON2("GetActivityTypeList"
        , { UserGroup: USER_GROUP, EmployeeID: Employee_ID, IsManual: false }
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrActivityTypeListManual = ro;
                if (arrActivityTypeListManual.length > 0)
                    getActivityList();
                else
                    bootbox.alert("No Activity Type assigned.");
            } else {
                bootbox.alert("Web service error when calling Activity Type List. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    function getActivityList() {
        callAjaxRequestJSON2("GetActivityList"
            , { ActivityTypeID: arrActivityTypeListManual[0].ListID, EmployeeID: Employee_ID, IsManual: false }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrActivityListManual = ro;
                    if (arrActivityListManual.length > 0)
                        loadActivityGrid();
                    else
                        bootbox.alert("No Activity assigned.");
                } else {
                    bootbox.alert("Web service error when calling Activity List. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadActivityGrid() {

        var dataSourceActivityLog = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                        callAjaxRequestJSON2("GetActivityLog"
                        , { EmployeeID: Employee_ID, ActivityDate: jQuery("#txtActivityDateManual").val(), IsManual: false }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivityLogs = ro;
                                e.success(arrActivityLogs);
                                jQuery('#gridActivityLogManual').data('kendoGrid').refresh();
                                jQuery("#gridActivityLogManual td:contains('Completed')").css({ 'background-color': 'blue' });
                            } else {
                                bootbox.alert("Web service error when calling GetActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                } //read
                , create: function (e) {

                    var datasourceAll = dataSourceActivityLog.data();
                    var param = new Object();
                    var logdetail = new Array();

                    for (var i = 0; i < datasourceAll.length; i++) {
                        var obj = new Object();

                        var seconds = 0, minutes = 0, hours = 0, actualTimeSpent = 0;
                        var dTime = datasourceAll[i].TimeSpentStr.split(':');

                        hours = parseInt(dTime[0]);
                        minutes = parseInt(dTime[1]);
                        seconds = parseInt(dTime[2]);

                        actualTimeSpent = (hours * 60) + minutes + (seconds / 60);

                            obj.ActivityLogID = datasourceAll[i].ActivityLogID == "" ? 0 : datasourceAll[i].ActivityLogID;
                            obj.LogDetailID = datasourceAll[i].LogDetailID == "" ? 0 : datasourceAll[i].LogDetailID;
                            obj.ActivityTypeID = datasourceAll[i].ActivityTypeID;
                            obj.EmployeeID = Employee_ID;
                            obj.ActivityDate = jQuery("#txtActivityDateManual").val();//datasourceAll[i].ActivityDate;
                            obj.ActivityID = datasourceAll[i].ActivityID;
                            obj.TimeSpent = actualTimeSpent; //0; //datasourceAll[i].TimeSpent;
                            obj.NumberOfTransaction = datasourceAll[i].NumberOfTransaction;
                            obj.Remarks = datasourceAll[i].Remarks;
                            obj.TimeStatus = datasourceAll[i].TimeStatus;
                            obj.TimeSpentStr = datasourceAll[i].TimeSpentStr;

                            logdetail.push(obj);

                    }

                    param.EmployeeID = Employee_ID;
                    param.ActivityDate = jQuery("#txtActivityDateManual").val();
                    param.ActivityLog = logdetail;
                    param.IsManual = false;

                    callAjaxRequestJSON2("UpdateActivityLog"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivityLogs = ro;
                                arrActivityLogs.push(e.data);
                                e.success(arrActivityLogs);
                                jQuery('#gridActivityLogManual').data('kendoGrid').dataSource.read();
                            } else {
                                bootbox.alert("Web service error when calling AddActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }//create
                , update: function (e) {

                    var datasourceAll = dataSourceActivityLog.data();
                    var param = new Object();
                    var logdetail = new Array();

                    for (var i = 0; i < datasourceAll.length; i++) {
                        var obj = new Object();

                        var seconds = 0, minutes = 0, hours = 0, actualTimeSpent = 0;
                        var dTime = datasourceAll[i].TimeSpentStr.split(':');

                        hours = parseInt(dTime[0]);
                        minutes = parseInt(dTime[1]);
                        seconds = parseInt(dTime[2]);

                        actualTimeSpent = (hours * 60) + minutes + (seconds / 60);

                        obj.ActivityLogID = datasourceAll[i].ActivityLogID;
                        obj.LogDetailID = datasourceAll[i].LogDetailID;
                        obj.ActivityTypeID = datasourceAll[i].ActivityTypeID;
                        obj.EmployeeID = Employee_ID;
                        obj.ActivityDate = jQuery("#txtActivityDateManual").val();//datasourceAll[i].ActivityDate;
                        obj.ActivityID = datasourceAll[i].ActivityID;
                        obj.TimeSpent = actualTimeSpent; //0; //datasourceAll[i].TimeSpent;
                        obj.NumberOfTransaction = datasourceAll[i].NumberOfTransaction;
                        obj.Remarks = datasourceAll[i].Remarks;
                        obj.TimeStatus = datasourceAll[i].TimeStatus;
                        obj.TimeSpentStr = datasourceAll[i].TimeSpentStr;

                        logdetail.push(obj);

                    }
                    param.EmployeeID = Employee_ID;
                    param.ActivityDate = jQuery("#txtActivityDateManual").val();
                    param.ActivityLog = logdetail;
                    param.IsManual = false;
                    param.UserGroup = USER_GROUP;
                    param.EmployeeName = EmployeeName;

                    callAjaxRequestJSON2("updateActivityLogManual"
                        , param
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivityLogs = ro;
                                arrActivityLogs.push(e.data);
                                e.success(arrActivityLogs);
                                jQuery('#gridActivityLogManual').data('kendoGrid').dataSource.read();
                                jQuery('#gridActivityLogManual').data('kendoGrid').refresh();
                            } else {
                                bootbox.alert("Web service error when calling AddActivityLog. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                }//update
                , destroy: function (e) {
                    var datasourceAll = dataSourceActivityLog.data();
                    var param = new Object();
                    var logdetail = new Array();

                    for (var i = 0; i < datasourceAll.length; i++) {
                        var obj = new Object();

                        var seconds = 0, minutes = 0, hours = 0, actualTimeSpent = 0;
                        var dTime = datasourceAll[i].TimeSpentStr.split(':');

                        hours = parseInt(dTime[0]);
                        minutes = parseInt(dTime[1]);
                        seconds = parseInt(dTime[2]);

                        actualTimeSpent = (hours * 60) + minutes + (seconds / 60);

                        obj.ActivityLogID = datasourceAll[i].ActivityLogID;
                        obj.LogDetailID = datasourceAll[i].LogDetailID;
                        obj.ActivityTypeID = datasourceAll[i].ActivityTypeID;
                        obj.EmployeeID = Employee_ID;
                        obj.ActivityDate = jQuery("#txtActivityDateManual").val();//datasourceAll[i].ActivityDate;
                        obj.ActivityID = datasourceAll[i].ActivityID;
                        obj.TimeSpent = actualTimeSpent; 0; //datasourceAll[i].TimeSpent;
                        obj.NumberOfTransaction = datasourceAll[i].NumberOfTransaction;
                        obj.Remarks = datasourceAll[i].Remarks;
                        obj.TimeStatus = datasourceAll[i].TimeStatus;
                        obj.TimeSpentStr = datasourceAll[i].TimeSpentStr;

                        logdetail.push(obj);

                    }

                    param.ActivityLog = logdetail;
                    param.ActivityLogID = e.data.ActivityLogID;
                    callAjaxRequestJSON2("DeleteActivityLog"
                                , param
                                , function (response) {
                                    var data = response['d']
                                    var ro = data.ResponseItem;
                                    if (data.ErrorMessage == null) {
                                        e.success();
                                        jQuery('#gridActivityLogManual').data('kendoGrid').dataSource.read();
                                    } else {
                                        bootbox.alert("Web service error when calling DeleteActivityLog. " + data.ErrorMessage);
                                    }
                                }
                                , ajaxError
                            );
                }//delete destroy

            }//transport

            , change: function (e) {
                //jQuery('#gridActivityLogManual table tbody tr td').each(function () {
                //    if ($(this).text() == 'Completed') $(this).css('background-color', 'blue');
                //    alert($(this).text());
                //});
            }
            , schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        //ActivityDate: { type: "date", editable: false, defaultValue: function (e) { return new Date(); } },
                        ActivityDate: { editable: false, defaultValue: function (e) { return new Date(); } },
                        ActivityTypeID: { defaultValue: arrActivityTypeListManual[0].ListID },
                        ActivityTypeDescription: { editable: false },
                        ActivityID: { defaultValue: arrActivityListManual[0].ListID },
                        ActivityDescription: { editable: false },
                        NumberOfTransaction: { type: "number", validation: { min: 0 }, defaultValue: 1 },
                        Remarks: { validation: { maxLength: 250 } },
                        TimeStatus: { defaultValue: arrTimeStatus[0].ID },
                        TimeSpentStr: { validation: { required: true}, defaultValue: "00:00:00" }
                    }
                }
            } //schema
            , autoSync: true
            //, batch: true
        }); //var dataSourceActivityLog = new kendo.data.DataSource


        gridActivityLog = jQuery("#gridActivityLogManual").kendoGrid({
            dataSource: dataSourceActivityLog,
            toolbar: ["create"], //,"save","cancel"],
            columns: [
                    {
                        command: ["destroy"]
                        , title: "&nbsp;"
                        , width: "200px"
                    },
                    { field: "LogDetailID", title: "Log ID", width: "80px" },
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "ActivityTypeID", title: "Activity Type", editor: activityTypeListTaskDDEditor, width: "250px", template: "#=ActivityTypeDescription#" },
                    { field: "ActivityID", title: "Task", editor: activityListTaskDDEditor, width: "250px", template: "#=ActivityDescription#" },
                    { field: "NumberOfTransaction", title: "Number of Transaction", width: "120px" },
                    { field: "Remarks", width: "250px" },
                    { field: "TimeStatus", title: "Status", editor: statusListTaskDDEditor, width: "150px", template: "#=TimeStatus#" },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" }
            ],
            editable: true //"inline"
            , edit: function (e) {
                //alert(e.model.ActivityTypeID);
                previousActivityTypeID = e.model.ActivityTypeID;
                if (e.model.TimeStatus == "Completed")
                    jQuery("#gridActivityLogManual").data("kendoGrid").closeCell();
            }
        });

    }//load activity grid


    function statusListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "Value"
                , dataValueField: "ID"
                , dataSource: arrTimeStatus
            });
    }

    function activityTypeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrActivityTypeListManual
            });
    }

    function activityListTaskDDEditor(container, options) {
        //alert(JSON.stringify(options.model.ActivityTypeID));
        
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: updateActivtyList(options.model.ActivityTypeID)
            });
    }

    function updateActivtyList(activityTypeID) {

        callAsyncAjaxRequestJSON("GetActivityList"
                , { ActivityTypeID: activityTypeID, EmployeeID: Employee_ID, IsManual: false }
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        if (ro.length > 0) {
                            arrActivityListManual = ro;
                            //var dataSource = new kendo.data.DataSource({
                            //    data: arrActivityListManual
                            //});
                            //var ddd = dd.data("kendoDropDownList");
                            //ddd.setDataSource(dataSource);
                        }
                        else
                            bootbox.alert("No Activity assigned.");
                    } else {
                        bootbox.alert("Web service error when calling Activity List. " + data.ErrorMessage);
                    }
                }
                , ajaxError
            );

        return arrActivityListManual;

    }


}); //jQuery(document).ready
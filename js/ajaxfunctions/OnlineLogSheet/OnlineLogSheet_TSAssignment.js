﻿
var arrTSAssignment;
var arrTSActivityList;
var arrTSStatusList;
var arrTSEmployeeList;
var arrTSEmployeeListTS;

jQuery(document).ready(function () {
    var detailsTemplate;

    var wnd = jQuery("#wnd").kendoWindow({
        title: "Complete Tool Support Assignment",
        height: 450,
        width: 650,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    detailsTemplate = kendo.template(jQuery("#template").html());

    function show(id, value) {
        document.getElementById(id).style.display = value ? 'block' : 'none';
    }

    callAjaxRequestJSON("GetEmployeeListTS"
        , {}
        , function (response) {
            var data = response['d']
            var ro = data.ResponseItem;
            if (data.ErrorMessage == null) {
                arrTSEmployeeListTS = ro;
            } else {
                alert("Web service error when calling Tool Support Assignment GetEmployeeList. " + data.ErrorMessage);
            }
        }
        , ajaxError
    );

    callAjaxRequestJSON("GetEmployeeListAdhoc"
    , {}
    , function (response) {
        var data = response['d']
        var ro = data.ResponseItem;
        if (data.ErrorMessage == null) {
            arrTSEmployeeList = ro;
            if (arrTSEmployeeList.length > 0) {
                getActivityList();
            }
            else {
                alert("No items available for Tool Support Assignment Employee List");
            }
        } else {
            alert("Web service error when calling Tool Support Assignment GetEmployeeList. " + data.ErrorMessage);
        }
    }
    , ajaxError
);


    function getActivityList() {

        callAjaxRequestJSON("GetActivityList"
            , { ActivityTypeID: ActivityType_TS, EmployeeID: arrTSEmployeeList[0].ListID }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrTSActivityList = ro;
                    getStatusList();
                } else {
                    alert("Web service error when calling Tool Support Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function getStatusList() {
        callAjaxRequestJSON("GetItemList"
            , { ActivityTypeID: ActivityType_TS, ListTypeID: 1 }
            , function (response) {
                var data = response['d']
                var ro = data.ResponseItem;
                if (data.ErrorMessage == null) {
                    arrTSStatusList = ro;
                    if (arrTSStatusList.length > 0)
                        loadTSAssignment();
                    else
                        alert("No items available for Tool Support Assignment Priority List");

                } else {
                    alert("Web service error when calling Adhoc Analytics - GetItemList. " + data.ErrorMessage);
                }
            }
            , ajaxError
        );
    }

    function loadTSAssignment() {

        var dataSourceTSAssignment = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetTSAssignment"
                    , { ActivityTypeID: ActivityType_TS }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            arrTSAssignment = ro;
                            e.success(arrTSAssignment);
                        } else {
                            alert("Web service error when calling loadTSAssignment_GetTSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
            }
            , create: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.EmployeeID = Employee_ID;
                obj.ActivityID = e.data.ActivityID;

                obj.AssignEmployeeID = e.data.AssignEmployeeID;

                obj.ToolName = e.data.ToolName;
                obj.TaskDescription = e.data.TaskDescription;
                obj.RequestedBy = e.data.RequestedBy;
                obj.DateReceived = e.data.DateReceived;
                obj.DueDate = e.data.DueDate;

                param.ActivityLog = obj;
                show('loading', true);

                callAjaxRequestJSON("AddTSAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            show('loading', false);
                            arrTSAssignment = ro;
                            arrTSAssignment.push(e.data);
                            e.success(arrTSAssignment);
                            jQuery('#gridTSAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            show('loading', false);
                            bootbox.alert("Web service error when calling Tool Support AddTSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
                //e.error("XHR response", "status code", "error message");
            }
            , update: function (e) {
                var param = new Object();
                var obj = new Object();

                obj.TSAssignmentID = e.data.TSAssignmentID;
                obj.EmployeeID = Employee_ID;
                obj.ActivityID = e.data.ActivityID;

                obj.AssignEmployeeID = e.data.AssignEmployeeID;

                obj.ToolName = e.data.ToolName;
                obj.TaskDescription = e.data.TaskDescription;
                obj.RequestedBy = e.data.RequestedBy;
                obj.DateReceived = e.data.DateReceived;
                obj.DueDate = e.data.DueDate;

                param.ActivityLog = obj;
                show('loading', true);

                callAjaxRequestJSON("UpdateTSAssignment"
                    , param
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            show('loading', false);
                            arrTSAssignment = ro;
                            arrTSAssignment.push(e.data);
                            e.success(arrTSAssignment);
                            jQuery('#gridTSAssignment').data('kendoGrid').dataSource.read();
                        } else {
                            show('loading', false);
                            alert("Web service error when calling Tool Support UpdateTSAssignment. " + data.ErrorMessage);
                        }
                    }
                    , ajaxError
                );
            }
            , destroy: function (e) {

                var param = new Object();
                param.TSAssignmentID = e.data.TSAssignmentID;

                callAjaxRequestJSON("DeleteTSAssignment"
                            , param
                            , function (response) {
                                var data = response['d']
                                var ro = data.ResponseItem;
                                if (data.ErrorMessage == null) {
                                    e.success();
                                    jQuery('#gridTSAssignment').data('kendoGrid').dataSource.read();
                                } else {
                                    alert("Web service error calling loadTSAssignment_DeleteTSAssignment. " + data.ErrorMessage);
                                }
                            }
                            , ajaxError
                        );

                // on failure
                //e.error("XHR response", "status code", "error message");
            }
            },
            error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 10,
            batch: false,
            schema: {
                model: {
                    id: "TSAssignmentID",
                    fields: {
                        TSAssignmentID: { editable: false, nullable: false },
                        AssignEmployeeID: { validation: { required: true }, defaultValue: arrTSEmployeeList[0].ListID },
                        AssignEmployeeName: { editable: false },
                        TicketNumber: { editable: false },
                        ToolName: { validation: { required: true, maxLength: 250 } },
                        ActivityID: { defaultValue: arrTSActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        StatusID: { editable: false },
                        StatusDescription: { editable: false },
                        TaskDescription: { validation: { required: true, maxLength: 250 } },
                        RequestedBy: { validation: { required: true, maxLength: 50 } },
                        DateReceived: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DueDate: { type: "date", defaultValue: function (e) { return new Date(); } },
                        DateCompleted: { type: "date", defaultValue: "", editable: false },
                        SLA: { editable: false },

                        AssignedEmployeeName: { editable: false },
                        QAByEmployeeName: { editable: false },
                        TimeSpentofProcessor: { editable: false },
                        NoOfDaysDelayed: { editable: false },
                        Timeliness: { editable: false },
                        ActionItem: { editable: false },
                        Working: { editable: false },
                        Accuracy: { editable: false },
                        TotalQAScore: { editable: false },
                        TimeSpentOfQA: { editable: false }
                    }
                }
            }
        });

        jQuery("#gridTSAssignment").kendoGrid({
            dataSource: dataSourceTSAssignment,
            pageable: true,
            toolbar: ["create", "cancel"],
            filterable: true,
            columns: [
                    {
                        field: "AssignEmployeeID", title: "Processor", editor: employeeListTaskDDEditor, template: "#=AssignEmployeeName#", width: "250px"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrTSEmployeeList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            ,operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "TicketNumber", title: "Ticket Number", width: "150px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ToolName", title: "Tool Name", width: "200px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "ActivityID", title: "Type", editor: aaTaskDDEditor, width: "250px", template: "#=ActivityDescription#"
                        , filterable: {
                            extra: false,
                            ui: function (element) {
                                element.kendoDropDownList({
                                    dataTextField: "ListDescription"
                                    , dataValueField: "ListID"
                                    , dataSource: arrTSActivityList
                                    , optionLabel: "-- Select Value --"
                                });
                            }
                            , operators: {
                                string: {
                                    eq: "Is equal to"
                                }
                            }
                        }
                    },
                    
                    {
                        field: "TaskDescription", title: "Description", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    {
                        field: "RequestedBy", title: "Requested By", width: "250px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "DateReceived", title: "Date Received", format: "{0:MM/dd/yyyy}", width: "150px", filterable:false },
                    { field: "DueDate", title: "Due Date", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "StatusID", title: "Status", editor: tsStatusDDEditor, width: "250px", template: "#=StatusDescription#" , filterable: false },
                    { field: "DateCompleted", title: "Date Completed", format: "{0:MM/dd/yyyy}", width: "150px", filterable: false },
                    { field: "SLA", title: "SLA", width: "120px", filterable: false },

                    { field: "AssignedEmployeeName", title: "Assigned Person", width: "250px", filterable: false },
                    {
                        field: "TimeSpentofProcessor", title: "Time Spent (Processor)", width: "200px"
                        , filterable: {
                            extra: false
                            , operators: {
                                string: {
                                    contains: "Contains"
                                    , eq: "Is equal to"
                                }
                            }
                        }
                    },
                    { field: "NoOfDaysDelayed", title: "No. of Days Delayed", width: "150px", filterable: false },
                    { field: "Timeliness", title: "Timeliness", width: "120px", filterable: false },
                    { field: "ActionItem", title: "Action Item", width: "120px", filterable: false },
                    { field: "Working", title: "Working", width: "120px", filterable: false },
                    { field: "Accuracy", title: "Accuracy", width: "120px", filterable: false },
                    { field: "TotalQAScore", title: "Total QA Score", width: "120px", filterable: false },
                    { field: "QAByEmployeeName", title: "QA By", width: "250px", filterable: false },
                    { field: "TimeSpentOfQA", title: "Time Spent of QA", width: "120px", filterable: false },

                    { command: { text: "Complete", click: completeStatus }, title: "", width: "100px" },
                    { command: ["edit", "destroy"], title: "&nbsp;", width: "250px", filterable: false }
                ],
            editable: "inline"
        });
        
    }
    function completeStatus(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));

        if (dataItem.StatusDescription == 'Completed') {
            jQuery.confirm({
                title: 'Tool Support Assignment',
                content: 'This Record is already Completed.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else if (dataItem.StatusDescription == '') {
            jQuery.confirm({
                title: 'Tool Support Assignment',
                content: 'This Record is not yet Saved in the Database!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
        else if (dataItem.StatusDescription == 'In progress') {
            wnd.content(detailsTemplate(dataItem));
            jQuery("#dropDownQAList").kendoDropDownList({
                dataSource: arrTSEmployeeListTS,
                dataTextField: "ListDescription",
                dataValueField: "ListID"
            });
            document.getElementById('dateCompletionDate').valueAsDate = new Date();
            wnd.center().open();
        }
        else {
            jQuery.confirm({
                title: 'Tool Support Assignment',
                content: 'Invalid Status',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }

    }

    $(document).on('click', '#btnComplete', function (e) {
        e.preventDefault();
        var timeSpent = $('#txtHourSpent').val();
        var assignmentID = $('#tsassignmentID').val();
        var assignedPerson = $('#dropDownQAList').val();
        var dateCompletion = $('#dateCompletionDate').val();
        var dueDate = $('#dueDate').val();

        if (timeSpent != '') {
            var xdueDate = new Date(dueDate);
            var xdateCompleted = new Date(dateCompletion);
            var daysDelayed = 0;

            if (xdateCompleted.getTime() <= xdueDate.getTime()) {
                daysDelayed = 0;
            }
            else {

                var diffTime = Math.abs(xdueDate - xdateCompleted);
                var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
                daysDelayed = diffDays - 1;
            }


            wnd.close();
            tempStartDateTime = new Date();
            var param = new Object();
            var obj = new Object();
            var statusList = new Object();

            obj.ActivityTypeID = ActivityType_TS;
            obj.EmployeeID = Employee_ID;
            obj.ActivityDate = tempStartDateTime;
            obj.NumberOfTransaction = 1;
            obj.TimeSpent = timeSpent;
            obj.TSAssignmentID = assignmentID;
            obj.StatusID = 32;

            obj.QAByEmployeeID = 539;
            obj.AssignedByEmployeeID = assignedPerson;
            obj.DateCompleted = dateCompletion;

            obj.NoOfDaysDelayed = daysDelayed;

            param.ActivityLog = obj;

            //console.log(param.ActivityLog);
            show('loading', true);

            callAjaxRequestJSON("CompleteStatusTSActivityLog"
                , param
                , function (response) {
                    var data = response['d']
                    var ro = data.ResponseItem;
                    if (data.ErrorMessage == null) {
                        show('loading', false);
                        jQuery.confirm({
                            title: 'Tool Support Assignment',
                            content: 'Database Successfully Modified.',
                            type: 'green',
                            typeAnimated: true,
                            buttons: {
                                close: function () {
                                    window.location.reload();
                                }
                            }
                        });
                    } else {
                        show('loading', false);
                        bootbox.alert("Web service error AddTSActivityLog. " + data.ErrorMessage);
                    }
                }
                , ajaxError
            );
        }
        else {
            jQuery.confirm({
                title: 'Tool Support Assignment',
                content: 'Time Spent is Required!',
                type: 'red',
                typeAnimated: true,
                buttons: {
                    close: function () {
                    }
                }
            });
        }
    });

    function aaTaskDDEditor(container, options) {
        programContainer = container;
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrTSActivityList
            });

    }

    function employeeListTaskDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrTSEmployeeList
                , change: function (e) {

                    callAjaxRequestJSON("GetActivityList"
                        , { ActivityTypeID: ActivityType_TS, EmployeeID: this.value() }
                        , function (response) {
                            var data = response['d']
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrTSActivityList = ro;
                                if (arrTSActivityList.length > 0) {
                                    programContainer.empty();
                                    var dd = jQuery("<input data-bind='value:ActivityID'/>")
                                        .appendTo(programContainer)
                                        .kendoDropDownList({
                                            dataTextField: "ListDescription"
                                            , dataValueField: "ListID"
                                            , change: function (e) {
                                                options.model.ActivityID = this.value();
                                            }
                                        }).data("kendoDropDownList");

                                    dd.setDataSource(arrTSActivityList);
                                    options.model.ActivityID = dd.value();
                                }
                                else
                                    alert("No items available for CS Assignment Activity List");

                            } else {
                                alert("Web service error when calling CS Assignment getActivityList_GetActivityList. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );

                }
            });
    }

    function tsStatusDDEditor(container, options) {
        jQuery("<input data-bind='value:" + options.field + "'/>")
            .appendTo(container)
            .kendoDropDownList({
                dataTextField: "ListDescription"
                , dataValueField: "ListID"
                , dataSource: arrTSStatusList
            });
    }

});
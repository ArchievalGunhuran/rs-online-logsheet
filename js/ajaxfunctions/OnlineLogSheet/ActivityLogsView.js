﻿
var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2];
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {

    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/OnlineLogSheet/ActivityMaintenance.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    });
}

var arrActivityTypeList;
var arrActivity;
var arrEmployeeAssignment;
var arrEmployeeList;
var selectedActivityID = 0;
var selectedActivitySTID = 0;
var curDateTime = new Date();

jQuery(document).ready(function () {
    jQuery("#txtActivityDateTo").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridActivityMaintenance').data('kendoGrid').dataSource.read(); }
    });
    jQuery("#txtActivityDateFrom").kendoDatePicker({
        animation: {
            close: {
                effects: "fadeOut zoom:out",
                duration: 300
            },
            open: {
                effects: "fadeIn zoom:in",
                duration: 300
            }
        },
        value: new Date(),
        change: function () { jQuery('#gridActivityMaintenance').data('kendoGrid').dataSource.read(); }
    });

    loadGridActivity();

   
    function loadGridActivity() {

        var dataSource = new kendo.data.DataSource({
            transport: {
                read: function (e) {
                    callAjaxRequestJSON("GetActivityLogView"
                        , { ActivityDateFrom: jQuery("#txtActivityDateFrom").val(), ActivityDateTo: jQuery("#txtActivityDateTo").val() }
                        , function (response) {
                            var data = response['d'];
                            var ro = data.ResponseItem;
                            if (data.ErrorMessage == null) {
                                arrActivity = ro;
                                //alert(JSON.stringify(arrActivity));
                                e.success(arrActivity);
                            } else {
                                alert("Web service error when calling Activity Maintenance loadGridActivity_GetActivityMaintenance. " + data.ErrorMessage);
                            }
                        }
                        , ajaxError
                    );
                    // on failure
                    //e.error("XHR response", "status code", "error message");
                }
            }
            , error: function (e) {
                // handle data operation error
                alert("Data operation error. Status: " + e.status + "; Error message: " + e.errorThrown);
            },
            pageSize: 20,
            batch: false, schema: {
                model: {
                    id: "LogDetailID",
                    fields: {
                        LogDetailID: { editable: false, nullable: false },
                        ActivityLogID: { editable: false, nullable: false },
                        EmployeeID: { editable: false, nullable: false },
                        //ActivityDate: { type: "date", editable: false, defaultValue: function (e) { return new Date(); } },
                        ActivityDate: { editable: false, defaultValue: function (e) { return new Date(); } },
                        //ActivityTypeID: { defaultValue: arrActivityTypeList[0].ListID },
                        ActivityTypeDescription: { editable: false },
                        //ActivityID: { defaultValue: arrActivityList[0].ListID },
                        ActivityDescription: { editable: false },
                        NumberOfTransaction: { type: "number", validation: { min: 0 }, defaultValue: 1 },
                        Remarks: { validation: { maxLength: 250 } },
                        //TimeStatus: { defaultValue: arrTimeStatus[0].ID },
                        TimeSpentStr: { editable: false, defaultValue: "00:00:00" }
                    }
                }
            } //schema
        });

        jQuery("#gridActivityMaintenance").kendoGrid({
            dataSource: dataSource,
            pageable: true,
            selectable: "row",
            filterable: true,
            columns: [
                    { field: "LogDetailID", title: "Log ID", width: "80px" },
                    { field: "FullName", title: "Employee Name", width: "170px" },
                    { field: "ActivityDate", title: "Process Date", format: "{0:MM/dd/yyyy}", width: "150px" },
                    { field: "ActivityTypeDescription", title: "Activity Type", width: "150px" },
                    { field: "ActivityDescription", title: "Task", width: "250px" },
                    { field: "NumberOfTransaction", title: "Number of Transaction", width: "150px" },
                    { field: "Remarks", width: "250px" },
                    {
                        field: "TimeStatus", title: "Status", width: "150px", template: function (dataItem) {
                            if(dataItem.TimeStatus == "On-going"){
                                return "<strong style='background-color:orange;color:white;'>" + kendo.htmlEncode(dataItem.TimeStatus) + "</strong>";
                            }
                            else if (dataItem.TimeStatus == "On-pause") {
                                return "<strong style='background-color:red;color:white;'>" + kendo.htmlEncode(dataItem.TimeStatus) + "</strong>";
                            }
                            else {
                                return "<strong style='background-color:green;color:white;'>" + kendo.htmlEncode(dataItem.TimeStatus) + "</strong>";
                            }
                        }
                    },
                    { field: "TimeSpentStr", title: "Time Spent", width: "120px" }
            ],
            editable: false
        });

        function setColor(item) {

            if (item.TimeStatus == "Completed") {
                return "<span style='color:green;'>" + item.ContactName + "</span>";
            }
            else if (item.TimeStatus == "On-pause")
            {
                return "<span style='color:red;'>" + item.ContactName + "</span>";
            }
            else if (item.TimeStatus == "On-going") {
                return "<span style='color:orange;'>" + item.ContactName + "</span>";
            }

            return item.ContactName;
        }

    }
});

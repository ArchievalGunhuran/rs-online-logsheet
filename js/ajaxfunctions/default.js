﻿jQuery(window).load(function () {
    //Footer();
    //FooterMobile();
    //NavigationHolder();
    //HeaderHolder();


    function resizeLogic() {
         
        if (jQuery(window).width() < 992) {
            jQuery('#navbar').addClass('mobilemenu');
            jQuery('.nav').removeClass('pull-right');
            jQuery('div[name=listwrapper]').show();
            jQuery('div[name=gridlist]').hide();
            //jQuery('#alltickets-wrap').show();
            //jQuery('#alltickets-wrap1').show();
            //jQuery('#grid').hide();
        } else {
            jQuery('#navbar').removeClass('mobilemenu');
            jQuery('.nav').addClass('pull-right');
            jQuery('div[name=listwrapper]').hide();
            jQuery('div[name=gridlist]').show();
//            jQuery('#alltickets-wrap').hide();
//            jQuery('#alltickets-wrap1').hide();
//            jQuery('#grid').show();
        };
    }

    resizeLogic(); // on load
    jQuery(window).resize(resizeLogic);

    jQuery('#menutoggle').click(function () {
        jQuery('.mobilemenu').css('left', '0');
        console.log('click');

    });

    jQuery('#menuclose').click(function () {
        jQuery('.mobilemenu').css('left', '-250px');
    });

});
//    footer();

//    pimpdefaultbuttons();
//    pimpdefaultmobilemenu();
//    pimpdefaultradio();

//    //---buttons and links
//    jQuery(".lists").click(function () {
//        setPhase3();
//    });

//    jQuery("#MainSearch").click(function () {
//        setPhase1();
//    });

//    jQuery("#buttonFirstSearch").click(function () {
//        setPhase2();
//    });

//    jQuery("#buttonSecondSearch").click(function () {
//        setPhase4();
//    });

//    jQuery("#linkAddItem").click(function () {
//        setPhase5();
//    });

//    jQuery("#buttonDefaultSave").click(function () {
//        CustomMessageSuccess("Successfully saved.");
//        setPhase3();
//    });

//    jQuery("#linkDefaultEditItem").click(function () {
//        setPhase6();
//    });

//    jQuery("#buttonSpecialRulesSave").click(function () {
//        setPhase9();
//    });

//    jQuery("#linkSpecialEditItem").click(function () {
//        setPhase8();
//    });
//    //---end

//    jQuery(".datepicker").datepicker({
//        todayHighlight: true
//    });

//});

//function setPhase1() // search all
//{
//    jQuery("#divFirstSearch").css("display", "block");
//    jQuery("#divSecondSearch").css("display", "none");
//    jQuery("#divSearchDetails").css("display", "none");

//    jQuery("#DefaultPricing").css("display", "none");
//    jQuery("#SpecialRules").css("display", "none");
//}

//function setPhase2() // search all and Search Details
//{
//    jQuery("#divFirstSearch").css("display", "block");
//    jQuery("#divSecondSearch").css("display", "none");
//    jQuery("#divSearchDetails").css("display", "block");

//    jQuery("#DefaultPricing").css("display", "none");
//    jQuery("#SpecialRules").css("display", "none");
//}

//function setPhase3() // Display Details/Data
//{
//    jQuery("#divFirstSearch").css("display", "none");
//    jQuery("#divSecondSearch").css("display", "block");
//    jQuery("#divSearchDetails").css("display", "none");

//    jQuery("#DefaultPricing").css("display", "block");
//    jQuery("#divDefaultPricingDetail").css("display", "block");
//    jQuery("#divDefaultPricingEntryFields").css("display", "none");

//    jQuery("#SpecialRules").css("display", "block");
//    jQuery("#divSpecialRulesDetail").css("display", "block");
//    jQuery("#divSpecialRulesEntryFields").css("display", "none");
//}

//function setPhase4() //  Mini Search and Search Details
//{
//    jQuery("#divFirstSearch").css("display", "none");
//    jQuery("#divSecondSearch").css("display", "block");
//    jQuery("#divSearchDetails").css("display", "block");

//    jQuery("#DefaultPricing").css("display", "none");
//    jQuery("#SpecialRules").css("display", "none");
//}

//function setPhase5() // Display or Add New Default Pricing (Entry Fields)
//{

//    jQuery("#divFirstSearch").css("display", "none");
//    jQuery("#divSecondSearch").css("display", "block");
//    jQuery("#divSearchDetails").css("display", "none");

//    jQuery("#DefaultPricing").css("display", "block");
//    jQuery("#divDefaultPricingDetail").css("display", "none");
//    jQuery("#divDefaultPricingEntryFields").css("display", "block");

//    jQuery("#SpecialRules").css("display", "none");
//}

//function setPhase6() // Edit Default Pricing
//{
//    jQuery("#DefaultPricing").css("display", "block");
//    jQuery("#divDefaultPricingDetail").css("display", "none");
//    jQuery("#divDefaultPricingEntryFields").css("display", "block");
//}

//function setPhase8() // Edit Special Rules
//{
//    jQuery("#SpecialRules").css("display", "block");
//    jQuery("#divSpecialRulesDetail").css("display", "none");
//    jQuery("#divSpecialRulesEntryFields").css("display", "block");
//}

//function setPhase9() // Save Edited Special Rules
//{
//    jQuery("#SpecialRules").css("display", "block");
//    jQuery("#divSpecialRulesDetail").css("display", "block");
//    jQuery("#divSpecialRulesEntryFields").css("display", "none");
//}

//function CustomMessageSuccess(cusMsgSuccess) {
//    var msg = "";
//    msg = msg + "<div class=\"alert alert-success\">" + cusMsgSuccess + "</div>";

//    jQuery("#Message").html(msg).fadeIn(1000).delay(2000).fadeOut(1000);
//}

//function CustomMessageError(cusMsgError) {
//    var msg = "";
//    msg = msg + "<div class=\"alert alert-error\">" + cusMsgError + "</div>";

//    jQuery("#Message").html(msg).fadeIn(1000).delay(2000).fadeOut(1000);
//}
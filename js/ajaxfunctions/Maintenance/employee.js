﻿var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[1];
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

var listcount = 1;
var awardslist = new Array();
var supervisorlist = new Array();
var positionlist = new Array();
var courselist = new Array();
var currID = 0;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callAjaxRequestJSON(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/Maintenance/Employee.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function callAjaxRequestJSONUser(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/UserProfile/UserProfile.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function ajaxError(response, error) {
    //hideloaders();
    alert("Request Error" + response.responseText);
}

function ajaxSuccess(response) {
    alert("success");
    alert(JSON.stringify(response));
    var obj = JSON.parse(response.responseText);
    alert(obj);
    alert("Request Successful");
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function getDefaultList() {
    getFullEmployeeList();
    callAjaxRequestJSON("GetSupervisorsList", new Object(), setSupervisorList, ajaxError);
    callAjaxRequestJSON("GetPositionList", new Object(), setPositionList, ajaxError);
    //callAjaxRequestJSON("", new Object(), setCourseList, ajaxError);
}

function getFullEmployeeList() { 
    var submitObject = new Object();
    submitObject.EmployeeID = 0; //jQuery("#hfEmployeeID").val();GetEmployeeInformation
    callAjaxRequestJSON("GetEmployee", submitObject, setGrid, ajaxError);
}

function getEmployeeFiltered() {
    var submitObject = new Object();
    submitObject.Filter = jQuery("#txtSearch").val(); //jQuery("#hfEmployeeID").val();GetEmployeeInformation
    callAjaxRequestJSON("GetEmployeeFiltered", submitObject, setGrid, ajaxError);
}

function setSupervisorList(response) {
    var data = response['d']
    var ro = data.ResponseItem;
    supervisorlist = ro;
}

function setPositionList(response) {
    var data = response['d']
    var ro = data.ResponseItem;
    positionlist = ro;
}

function setCourseList(response) {
    var data = response['d']
    var ro = data.ResponseItem;
    courselist = ro;
}

function getEmployeeAwardsList(_employeeID) {
    currID = _employeeID;
    var submitObject = new Object();
    submitObject.EmployeeID = _employeeID;
    callAjaxRequestJSONUser("GetEmployeeAwards", submitObject, setAwardsList, ajaxError);
}

function popInformationUpdate(_employeeID) {
    currID = _employeeID;
    var submitObject = new Object();
    submitObject.EmployeeID = _employeeID;
    callAjaxRequestJSONUser("GetEmployeeInformation", submitObject, setPopForUpdate, ajaxError);
}

function setPopForUpdate(response) {
    var data = response['d']
    var item = data.ResponseItem;

    var list;
    if (item.length > 0 )
        list = item[0];

    bootbox.dialog({
        message:
            "   <div class='form-group'>" +
            "       <div class='title-input' data-field='position'>Position</div>" +
            "       <select id='position' name='position' class='form-control material-inputdropdown'></select><span class='bar'></span>" +
        //"       <input type='text' id='position' class='form-control material-input' value='" + list.Position + "' />" +

            "       <div class='title-input' data-field='supervisor'>Supervisor</div>" +
            "       <select id='supervisor' name='supervisor' class='form-control material-inputdropdown'></select><span class='bar'></span>" +
        //"       <input type='text' id='birthday' class='form-control material-input' value='" + list.SupervisorName + "' />" +

            "       <div class='title-input' data-field='datehired'>Date Hired</div>" +
            "       <div class='input-group date'>" +
            "           <input type='text' id='datehired' class='form-control material-input' value='" + list.DateHired + "' />" +
            "           <span class='input-group-addon material-dropdown'><span class='mdi mdi-calendar-today'></span></span class='mdi mdi-menu-down'></span></span>" +
            "       </div>" +

            "       <div class='title-input' data-field='birthday'>Birthday</div>" +
            "       <div class='input-group date'>" +
            "       <input type='text' id='birthday' class='form-control material-input' value='" + list.Birthday + "' />" +
            "           <span class='input-group-addon material-dropdown'><span class='mdi mdi-calendar-today'></span></span class='mdi mdi-menu-down'></span></span>" +
            "       </div>" +

            "       <div class='title-input' data-field='college'>College</div>" +
            "       <input type='text' id='college' class='form-control material-input' value='" + list.College + "' />" +

            "       <div class='title-input' data-field='course'>Course</div>" +
            "       <input type='text' id='course' class='form-control material-input' value='" + list.Course + "' />" +
            " </div>",
        title: "Update Employee Information",
        buttons: {
            Cancel: {
                className: "material-btn material-btn-primary",
                callback: function () {
                    return;
                }
            },
            Save: {
                className: "material-btn material-btn-primary",
                callback: function () {
                    var submitObject = new Object();
                    var Data = new Object();
                    Data.EmployeeID = currID;
                    Data.DateHired = jQuery("#datehired").val();
                    Data.Birthday = jQuery("#birthday").val();
                    Data.PositionID = jQuery("#position").val();
                    Data.SupervisorID = jQuery("#supervisor").val();
                    Data.College= jQuery("#college").val();
                    Data.Course = jQuery("#course").val();

                    submitObject.Data = Data;
                    //bootbox.alert(JSON.stringify(submitObject));
                    callAjaxRequestJSON("UpdateEmployeeBySupervisor", submitObject, successUpdateInfo, ajaxError);
                }
            }
        }
    });

    jQuery('.date').datetimepicker({ format: 'MM/DD/YYYY' });
    var dhtml = "";
    for (var i = 0; i < supervisorlist.length; i++) {
        dhtml = "<option value='" + supervisorlist[i].ListID + "'>" + supervisorlist[i].ListDescription + "</option>";
        jQuery("#supervisor").append(dhtml);
    }

    for (var i = 0; i < positionlist.length; i++) {
        dhtml = "<option value='" + positionlist[i].ListID + "'>" + positionlist[i].ListDescription + "</option>";
        jQuery("#position").append(dhtml);
    }

    jQuery("#position").val(list.PositionID);
    jQuery("#supervisor").val(list.SupervisorID);
}

function successUpdateInfo(response) {
    var data = response['d']
    var item = data.ResponseItem;

    if (data.ErrorMessage == null) {
        bootbox.alert("Employee information successfully updated.");
    } else {
        bootbox.alert("Please try again. Error updating employee information. " + data.ErrorMessage);
    }
}

function showHideElement(_idToHide, _idToShow) {
    jQuery("#" + _idToHide).hide();
    jQuery("#" + _idToShow).show();
}

function deleteAward(_awardID) {
    var submitObject = new Object();
    submitObject.AwardID = _awardID;
    callAjaxRequestJSON("DeleteAward", submitObject, successAwardDeletion, ajaxError);
}

function updateSingleAward(_awardID) {
    var submitObject = new Object();
    submitObject.AwardID = _awardID;
    submitObject.AwardDescription = jQuery("#txtAward" + _awardID).val();
    callAjaxRequestJSON("UpdateAward", submitObject, successAwardUpdate, ajaxError);
}

function successAwardUpdate(response) {
    var data = response['d']
    var item = data.ResponseItem;

    if (data.ErrorMessage == null) {
        jQuery("#award" + item).html(jQuery("#txtAward" + item).val());
        showHideElement("editable" + item, "awardList" + item);
    } else {
        bootbox.alert(data.ErrorMessage);
    }
}

function successAwardDeletion(response) {
    var data = response['d']
    var item = data.ResponseItem;

    if (data.ErrorMessage == null) {
        jQuery("ul #listHolder" + item).remove();
    }
}

function setAwardsList(response) {
    var data = response['d']
    var item = data.ResponseItem;

    listcount = 1;

    var dhtml = "";
    dhtml = "<ul class='list-unstyled' >";
    for (var i = 0; i < item.length; i++) {
        dhtml += "<li id='listHolder" + item[i].ListID + "' class='list-holder-li-small'>";
        dhtml += "    <div id='awardList" + item[i].ListID + "'>";
        dhtml += "      <img class='nameinitialsmall' src='../../images/award.png' alt='awards'/>";
        dhtml += "       <div class='nameuser' id='award" + item[i].ListID + "'>" + item[i].ListDescription + "</div>";
        dhtml += "       <span class='mdi mdi-delete edit-admin material-btn-primary btn pull-right' onclick='javascript:deleteAward(" + item[i].ListID + ");'  ></span>";
        dhtml += "       <span class='mdi mdi-pencil edit-admin material-btn-primary btn pull-right' onclick='javascript:showHideElement(\"awardList" + item[i].ListID + "\",\"editable" + item[i].ListID + "\");' ></span>";
        dhtml += "   </div>";
        dhtml += "    <div id='editable" + item[i].ListID + "' style='display:none'>";
        dhtml += "      <img class='nameinitialsmall' src='../../images/award.png' alt='awards'/>";
        dhtml += "      <input  class='nameuser' id='txtAward" + item[i].ListID + "' type='text' value='" + item[i].ListDescription + "' />";
        dhtml += "       <span class='mdi mdi-close edit-admin material-btn-primary btn pull-right' onclick='javascript:showHideElement(\"editable" + item[i].ListID + "\",\"awardList" + item[i].ListID + "\");' ></span>";
        dhtml += "       <span class='mdi mdi-check edit-admin material-btn-primary btn pull-right' onclick='javascript:updateSingleAward(" + item[i].ListID + ");'  ></span>";
        dhtml += "   </div>";
        dhtml += "</li>";
    }
    dhtml = dhtml + "</ul>";

    bootbox.dialog({
        message:
            "<div>" + dhtml +
            "<div>" +
            "<div class='form-group' style='padding: 2px 2px 2px 2px' ><br>" +
            "   <ul class='awardtoaddholder list-unstyled' >" +
            "       <li> " + listcount + ". <input type='text' class='awardToSave form-control material-input' placeholder='award name here' /></li>" +
            "   </ul>" +
            "</div>" +
            "<div style='padding-top:25px;'><input id='addRowAwards' type='button' onclick='javascript:addAwardsRow();' class='mdi mdi-plus btn material-btn-primary' value='+' /></div>",

        title: "Enter award names",
        buttons: {
            Cancel: {
                className: "material-btn material-btn-primary",
                callback: function () {
                    return;
                }
            },
            Save: {
                className: "material-btn material-btn-primary",
                callback: function () {
                    var submitObject = new Object();
                    submitObject.RegisteredBy = 12; //jQuery("#hfUserID").val();
                    submitObject.EmployeeID = currID;
                    var AwardDescription = new Array();

                    jQuery(".awardToSave").each(function () {
                        if (jQuery.trim(this.value) != "")
                            AwardDescription.push(this.value);
                    });

                    submitObject.AwardDescription = AwardDescription;
                    if (AwardDescription.length > 0)
                        callAjaxRequestJSON("AddAwards", submitObject, awardsAdded, ajaxError);
                    else
                        bootbox.alert("Nothing to save.");
                }
            }
        }
    });

}

function setGrid(response) {
    var data = response['d']
    var item = data.ResponseItem;

    var grid = jQuery("#grid").kendoGrid({
        dataSource: {
            data: item,
            pageSize: 20
        },
        toolbar: kendo.template(jQuery("#toolbartemplate").html()),
        //groupable: true,
        sortable: true,
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 10
        },
        columns: [{
            field: "Lastname",
            title: "Lastname"
        }, {
            field: "Firstname",
            title: "Firstname"
        }, {
            field: "",
            title: "Awards",
            template: "<img src='../../images/award-add.png' onclick='javascript:getEmployeeAwardsList(#=EmployeeID#)' class='mdi mdi-magnify btn material-btn-primary rowopenticket' />"
        }, {
            field: "",
            title: "Personal Info", 
            template: "<span class='mdi mdi-pencil edit-admin material-btn-primary btn' onclick='javascript:popInformationUpdate(#=EmployeeID#)' ></span>"
        }
        ]
    });
}

function addAwardsRow() {
    var dhtml = "";
    dhtml = dhtml + " <li>" + Math.floor(listcount += 1) + ".<input style='padding-top:3px;' type='text' class='awardToSave form-control material-input' placeholder='award name here' /></li>";
    jQuery(".awardtoaddholder").append(dhtml);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DB OPERATIONS

function awardsAdded(response) {
    var data = response['d']
    var item = data.ResponseItem;

    if (data.ErrorMessage == null) {
        bootbox.alert("Awards successfully added");
    } else {
        bootbox.alert("Error adding awards. " + data.ErrorMessage);
    }
}
﻿//var urlchunks = document.getElementById("hfURLChunks").value;
//var baseurl = document.getElementById("hfBaseURL").value;
//var tempsplit = urlchunks.split("|");
//var currentpath = "DEFAULT";
//if (tempsplit.length > 0) {
//    currentpath = tempsplit[0];
//}
/////////////////////////////////////////////////////////////////////////////

function HeaderHolder() {

    var holder = document.getElementById("headerholder");
    var dhtml = "";

    dhtml = dhtml + " <header>";
    dhtml = dhtml + "        <div class=\"container\">";
    dhtml = dhtml + "         <div class=\"row\">";
    dhtml = dhtml + "             <div class=\"col-sm-6 col-xs-5\">";
    dhtml = dhtml + "                <img src=\"../../images/logo-white.png\" class=\"emr\"/>";
    dhtml = dhtml + "            </div>";
    dhtml = dhtml + "            <div class=\"col-sm-6 col-xs-7\">";
    dhtml = dhtml + "               <div class=\"top-text-holder pull-right\" id=\"userheader\">";
    dhtml = dhtml + "                  <a href=\"http://proact.emerson.com/rssite/Home.aspx\"><span class='glyphicon glyphicon-home' style='color:white;'></span></a>  <span id=\"usernameheader\">" + jQuery("#hfUsername").val() + "</span>";
    dhtml = dhtml + "                   | <a href=\"http://proact.emerson.com/rssite/Login.aspx\" style='color:white;'><span id=\"logout\" title=\"Logout\">Logout</span></a>";
    dhtml = dhtml + "               </div>";
    //    dhtml = dhtml + "                <div id=\"namemarcomm\">marcomm creative ideas...</div>";
    dhtml = dhtml + "            </div>";
    dhtml = dhtml + "        </div>";
    dhtml = dhtml + "        </div>";
    dhtml = dhtml + " </header>";

    holder.innerHTML = dhtml;
}


function NavigationHolder() {
    var holder = document.getElementById("navigationmenu");
    var dhtml = "";

    dhtml = dhtml + " <div class=\"navbar navbar-default nav-custom-default\" id=\"\">";
    dhtml = dhtml + "   <div class=\"container\">";
    dhtml = dhtml + "     <div class=\"navbar-header\" style=\"display:inline-flex\">";
    //dhtml = dhtml + "       <button type=\"button\" id=\"menutoggle\" class=\"navbar-toggle collapsed pull-left\" data-toggle=\"collapse\" aria-expanded=\"false\" aria-controls=\"navbar\">";
    //dhtml = dhtml + "         <span class=\"mdi mdi-menu\"></span>";
    //dhtml = dhtml + "       </button>";
    if (jQuery("#hfRoleName").val() == "User")
        dhtml = dhtml + "       <a class=\"navbar-brand\" href=\"../../webscreens/UserProfile/userprofile.aspx\">My Profile</a>";
    else
        dhtml = dhtml + "       <a class=\"navbar-brand\" href=\"../../webscreens/UserProfile/userprofile.aspx\">My Profile</a>";
    dhtml = dhtml + "     </div>";
    dhtml = dhtml + "     <div class=\"navbar-header pull-right\" style=\"display:inline-flex;width:30%;\">";
    dhtml = dhtml + "       <input id='txtFriend' type='text' class='right pull-right' style='width:100%;'  />";
    dhtml = dhtml + "     </div>";

    dhtml = dhtml + "     <div  id=\"navbar\" class=\"navbar-collapse collapse\">";
    //dhtml = dhtml + "     <a class=\"boxclose hidden-lg hidden-md hidden-sm\" id=\"menuclose\"></a>";
    //dhtml = dhtml + "         <ul class=\"nav navbar-nav pull-right\">";

    //    //listed pages

    //    dhtml = dhtml + "         <li id=\"samplepopup\" class=\"\"><a href=\"samplepopup.aspx\">Sample Popup<div class=\"cart-counter\" id=\"cartcount\"></div></a></li>";


    //dhtml = dhtml + "         </ul>";
    dhtml = dhtml + "     </div>";
    dhtml = dhtml + "   </div>";
    dhtml = dhtml + " </div>";

    holder.innerHTML = dhtml;

    getAllUserList();
}

function Footer() {
    var holder = document.getElementById("footer");
    var dhtml = "";

    dhtml = dhtml + " <div class=\"footer\">";
    dhtml = dhtml + " <div class=\"container\" >";
    dhtml = dhtml + "    <div style=\"color:#808080; margin-top:5px; margin-bottom:5px;\" class=\"visible-lg visible-md\">";
    dhtml = dhtml + "       <div class=\"row\">";
    dhtml = dhtml + "           <div style=\"float:left;\">";
    dhtml = dhtml + "               <img src=\"../../images/logo.gif\" />";
    dhtml = dhtml + "           </div>";
    dhtml = dhtml + "           <div style=\"display:inline-table; float:right; padding-right:10px; text-align:right\">";
    dhtml = dhtml + "               <ul style=\"list-style-type:none; \">";
    dhtml = dhtml + "                   <li><small><a target='_blank' href=\"http://www.emerson.com/en-us/Pages/privacy-policy.aspx\">Privacy Policy</a> &nbsp;&nbsp;&nbsp;&nbsp; <a target='_blank' href=\"http://www.emerson.com/en-us/Pages/terms-of-use.aspx\">Terms of Use</a></small></li>";
    dhtml = dhtml + "                   <li><small></small></li>";
    dhtml = dhtml + "                   <li><small>©2015 Emerson Electric Co. All rights reserved.</small></li>";
    dhtml = dhtml + "                   <li><small>CONSIDER IT SOLVED.</small></li>";
    dhtml = dhtml + "               </ul>";
    dhtml = dhtml + "           </div>";
    dhtml = dhtml + "       </div>";
    dhtml = dhtml + "    </div>";
    dhtml = dhtml + " </div>";
    dhtml = dhtml + " </div>";

    holder.innerHTML = dhtml;
}

function FooterMobile() {
    var holder = document.getElementById("footerMobile");
    var dhtml = "";

    dhtml = dhtml + " <div class=\"container visible-xs visible-sm\" style=\"background-color: #f5f5f5;\">";
    dhtml = dhtml + " <div style=\"color:#808080; margin-top:5px; margin-bottom:5px; font-size:9px\" class=\"visible-xs visible-sm\">";
    dhtml = dhtml + "   <div class=\"row\">";
    //dhtml = dhtml + "       <div class=\"col-sm-12\">";
    //dhtml = dhtml + "           <img class=\"img-responsive\" src=\"images/logo.png\" />";
    //dhtml = dhtml + "       </div>";
    //dhtml = dhtml + "       <div>&nbsp;</div>";
    dhtml = dhtml + "       <div class=\"col-sm-12\">";
    dhtml = dhtml + "           <div><a href=\"#\">Privacy Policy</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href=\"#\">Terms of Use</a></div>";
    //dhtml = dhtml + "           <div>&nbsp;</div>";
    dhtml = dhtml + "           <div>©2015 Emerson Electric Co. All rights reserved.</div>";
    dhtml = dhtml + "           <div>CONSIDER IT SOLVED.</div>";
    dhtml = dhtml + "       </div>";
    dhtml = dhtml + "   </div>";
    dhtml = dhtml + " </div>";
    dhtml = dhtml + " </div>";

    holder.innerHTML = dhtml;
}

//jQuery(window).scroll(function (event) {
//    if (jQuery(window).scrollTop() > 90) {
//        jQuery('.nav-custom-default').addClass("navbar-fixed-top");

//        if (jQuery(window).width() < 767) {
//            jQuery('.nav-custom-default').removeClass("navbar-fixed-top");
//            jQuery('#navigationmenu').css('position', 'fixed');
//            jQuery('#navigationmenu').css('z-index', '40');
//            jQuery('#navigationmenu').css('width', '100%');
//            jQuery('#navigationmenu').css('top', '0');
//        }
//    }
//    else {
//        jQuery('.nav-custom-default').removeClass("navbar-fixed-top");
//        jQuery('#navigationmenu').css('position', 'inherit');
//        jQuery('#navigationmenu').css('z-index', '1');
//        jQuery('#navigationmenu').css('width', '100%');
//        jQuery('#navigationmenu').css('top', 'none');

//    }
//});



//jQuery(function () {
//    var lastScrollTop = 0, delta = 5;
//    jQuery(window).scroll(function (event) {
//        var st = jQuery(this).scrollTop();

//        if (Math.abs(lastScrollTop - st) <= delta)
//            return;

//        if (st > lastScrollTop) {
//            // downscroll code
//            console.log('scroll down');
//            jQuery('#navigationmenu').css('position', 'inherit');
//            jQuery('#navigationmenu').css('z-index', '1');
//            jQuery('#navigationmenu').css('width', '100%');
//            jQuery('#navigationmenu').css('top', 'none');
//        } else {
//            // upscroll code
//            if (jQuery(window).scrollTop() < 90) {
//                jQuery('#navigationmenu').css('position', 'inherit');
//                jQuery('#navigationmenu').css('z-index', '1');
//                jQuery('#navigationmenu').css('width', '100%');
//                jQuery('#navigationmenu').css('top', 'none');
//                jQuery('.navbar').removeClass('fixed-top');
//            }
//            else {
//                jQuery('#navigationmenu').css('position', 'fixed');
//                jQuery('#navigationmenu').css('z-index', '40');
//                jQuery('#navigationmenu').css('width', '100%');
//                jQuery('#navigationmenu').css('top', '0');
//            }
//            console.log('scroll up');
//        }
//        lastScrollTop = st;
//    });
//});




//======= SEARCH OTHER Employee ===========//
var __loc = window.location.toString();
var __tmpArr = __loc.split("/");
var __baseURL = __tmpArr[0] + "//" + __tmpArr[2];
//var __baseURL = __tmpArr[0] + "//" + __tmpArr[2] + "/" + __tmpArr[3];

var userCollection = new Array();
var selectedEmployee = '';
var isSelectedEmployee = false;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callAjaxRequestJSONDefault(_serviceVerb, _postBody, _function_success, _function_error) {
    jQuery.ajax({
        type: "post"
        , url: __baseURL + "/services/UserProfile/UserProfile.asmx/" + _serviceVerb
        , data: JSON.stringify(_postBody)
        , dataType: "json"
        , contentType: "application/json; charset=UTF-8"
        , error: _function_error
        , success: _function_success
    })
}

function getAllUserList() {
    var param = new Object();
    callAjaxRequestJSONDefault("GetFullEmployeeList", param, loadAllEmployee, ajaxError);
}

function loadAllEmployee(response) {
    try {
        var result = response;
        var data = result["d"];
        if (data == null)
            data = result;

        if (data.ErrorMessage == null) {
            var item = data.ResponseItem;
            if (item != null) {
                userCollection = item;

                var sysArray = [];
                for (i = 0; i <= item.length - 1; i++) {
                    sysArray.push(item[i].ListDescription);
                }

                var widget = jQuery('#txtFriend')
                    .kendoAutoComplete({
                        dataTextField: "ListDescription",
                        dataSource: item,
                        headerTemplate: '<div class="noDataMessage">No results found</div>',
                        filter: "contains",
                        placeholder: "Search name",
                        select: function (e) {
                            var dataItem = this.dataItem(e.item.index());
                            selectedEmployee = dataItem.ListID
                            isSelectedSystem = true

                            if (selectedEmployee != jQuery("#hfEmployeeID").val())
                                window.location.href = __baseURL + "/webscreens/ViewProfile/viewuserprofile.aspx?userID=" + selectedEmployee;
                            else
                                window.location.href = __baseURL + "/webscreens/UserProfile/userprofile.aspx";

                        },
                        dataBound: function () {
                            var noItems = this.list.find(".noDataMessage");

                            if (!this.dataSource.view()[0]) {
                                noItems.show();
                                this.popup.open();
                                selectedEmployee = undefined
                                isSelectedEmployee = false
                            } else {
                                noItems.hide();
                            }
                        },
                        close: function (e) {
                            var widget = e.sender;

                            if (!widget.shouldClose && !this.dataSource.view()[0]) {
                                isSelectedEmployee = false
                                selectedEmployee = undefined
                                e.preventDefault();
                            }
                        }
                    }).data("kendoAutoComplete");

                widget.element
                    .on("blur", function () {
                        if (!isSelectedSystem) {
                            jQuery('#txtFriend').val("")
                            selectedEmployee = undefined
                            isSelectedEmployee = false
                        }
                        widget.shouldClose = true;
                        widget.close();
                        widget.shouldClose = false;

                    })
                  .on("focus", function () {
                      if (typeof selectedEmployee === "undefined") {
                          jQuery('#txtFriend').val("")
                      }
                  })
            }
        }
        else {
            alert("Web Service Error at GetFullEmployeeList: " + data.ErrorMessage);
        }
    }
    catch (ex) {
        alert("Javascript Error at GetFullEmployeeList: " + ex.Message);
    }

}
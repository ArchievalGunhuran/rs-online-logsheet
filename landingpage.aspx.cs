﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class landingpage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //int employeeID = int.Parse(Request.QueryString[0]);
            //int pageIndex = int.Parse(Request.QueryString[1]);

            int employeeID = 806;
            int pageIndex = 1;

            if (employeeID == 0 || pageIndex == 0)
			{
				Response.Redirect("http://proact.emerson.com/rssite/Login.aspx");
			}
            else
            {
                Transactions.Accounts accounts = new Transactions.Accounts();
                Transactions.ActivityLogs2 activitylog = new Transactions.ActivityLogs2();
                DataModels.Accounts a = accounts.getUserDetailsForSession(employeeID);

                if (a.AccountID > 0)
                {
                    General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
                    String details = "";
                    details += a.AccountID.ToString() + "|";
                    details += a.EmpID.ToString() + "|";
                    details += a.Username + "|";
                    details += a.EmailAddress + "|";
                    details += a.EmployeeName + "|";
                    details += a.RoleName + "|";
                    details += a.AlarmCenter + "|";
                    details += a.PermissionName +"|";
                    details += a.LogSheetUserGroup;

                    HttpCookie UserDetails = new HttpCookie("CurrentUser", sm.Encrypt(details));
                    Response.Cookies.Add(UserDetails);

                    switch (pageIndex)
                    {
                        case 1:
                            if (activitylog.getEmployeeUserGroup(employeeID) == "")
                            {
								Response.Redirect("http://proact.emerson.com/rssite/Login.aspx");
                                break;
                            }
                            else if (activitylog.getEmployeeUserGroup(employeeID) == "AES")
                            {
                                Response.Redirect("webscreens\\OnlineLogSheet\\activitylog.aspx");
                                break;
                            }
                            else
                            {
                                Response.Redirect("webscreens\\OnlineLogSheet\\activitylog2.aspx");
                                break;
                            }
                        case 2: 
                            Response.Redirect("webscreens\\OnlineLogSheet\\csassignment.aspx");
                            break;
                        case 3: 
                            Response.Redirect("webscreens\\OnlineLogSheet\\activitymaintenance.aspx");
                            break;
                        case 4: 
                            Response.Redirect("webscreens\\OnlineLogSheet\\schedulemaintenance.aspx");
                            break;
                        case 5:
                            Response.Redirect("webscreens\\OnlineLogSheet\\emtassignment.aspx");
                            break;
                        case 6:
                            Response.Redirect("webscreens\\OnlineLogSheet\\tsassignment.aspx");
                            break;
                        case 7:
                            Response.Redirect("webscreens\\OnlineLogSheet\\aaassignment.aspx");
                            break;
                        case 8:
                            Response.Redirect("webscreens\\OnlineLogSheet\\reportmaintenance.aspx");
                            break;
						case 9:
                            Response.Redirect("webscreens\\OnlineLogSheet\\activitylog.aspx");
                            break;
                        default:
							Response.Redirect("http://proact.emerson.com/rssite/Login.aspx");
                            break;
                    }
                }
                else
                {
					Response.Redirect("http://proact.emerson.com/rssite/Login.aspx");
                }

            }
        }
    }

    protected void setUpCookie(string _empID, string _username, string _rolename)
    {
        //object alarmCenter = a.AlarmCenter;
        General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
        String details = "";
        details += _empID + "|";
        details += _username + "|" + _rolename;
        HttpCookie UserDetails = new HttpCookie("CurrentAccount", sm.Encrypt(details));
        Response.Cookies.Add(UserDetails);
    }
}
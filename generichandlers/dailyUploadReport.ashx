﻿<%@ WebHandler Language="C#" Class="dailyUploadReport" %>

using System;
using System.Web;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using SpreadsheetLight;
using Utility;
using DocumentFormat.OpenXml.Spreadsheet;

public class dailyUploadReport : IHttpHandler {

    ConnectionMaster cm = new ConnectionMaster();
    DataTransformerUtility dtu = new DataTransformerUtility();
    SpreadSheetStyler sStyler = new SpreadSheetStyler();

    public void ProcessRequest (HttpContext context) {

        //string startdate = "5/1/2018"; //context.Request.QueryString["startdate"];
        //string enddate = "5/31/2018"; //context.Request.QueryString["enddate"];

        string from = context.Request.QueryString["from"];
        string to = context.Request.QueryString["to"];

        DataTable dt = new DataTable();
        ArrayList paramList = new ArrayList();

        paramList.Add(new SqlParameter("@fromDate", from));
        paramList.Add(new SqlParameter("@toDate", to));

        dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_Report_DailyUpload", paramList);

        HttpResponse response = HttpContext.Current.Response;
        response.Clear();
        response.Charset = "";
        response.ContentEncoding = System.Text.Encoding.Default;
        
        
        //response.AddHeader("Content-Disposition", "attachment;filename=\" ALL_" + year + ".xls");
        //response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        context.Response.AddHeader("content-disposition", "attachment;filename=DailyUploadReport-" + from + "_" + to + ".xls");
        context.Response.ContentType = "application/vnd.ms-excel";

        string dhtml = "";

        foreach(DataRow row in dt.Rows)
        {
            if (dt.Rows.IndexOf(row) == 0)
            {
                dhtml = "<html><head><meta charset=\"utf-8\"/></head><body>";
                dhtml += "<table cellpadding='0' cellspacing='0' border='1'>";
                dhtml += "  <tr align='center'>";
                dhtml += "      <td  bgcolor='#92D050'>Processor</td>";
                dhtml += "      <td  bgcolor='#92D050'>Ticket Number</td>";
                dhtml += "      <td  bgcolor='#92D050'>Descriptions</td>";
                dhtml += "      <td  bgcolor='#92D050'>Date Received</td>";
                dhtml += "      <td  bgcolor='#92D050'>Time Spent of Processor</td>";
                dhtml += "      <td  bgcolor='#92D050'>QA By</td>";
                dhtml += "      <td  bgcolor='#92D050'>Total QA Score</td>";
                dhtml += "      <td  bgcolor='#92D050'>Time Spent of QA</td>";
                dhtml += "  </tr>";
            }
            dhtml += "<tr align='center'>";
            dhtml += "  <td>" + row["EmployeeName"] + "</td>";
            dhtml += "  <td>" + row["TicketNumber"] + "</td>";
            dhtml += "  <td>" + row["TaskDescription"] + "</td>";
            dhtml += "  <td>" + row["DateReceived"] + "</td>";
            dhtml += "  <td>" + row["TimeSpentofProcessor"] + "</td>";
            dhtml += "  <td>" + row["QAByEmployeeName"] + "</td>";
            dhtml += "  <td>" + row["TotalQAScore"] + "</td>";
            dhtml += "  <td>" + row["TimeSpentOfQA"] + "</td>";
            dhtml += "</tr>";
        }

        dhtml += "</table>";
        dhtml += "</body></html>";
        context.Response.Write(dhtml);

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}
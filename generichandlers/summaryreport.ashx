﻿<%@ WebHandler Language="C#" Class="summaryreport" %>

using System;
using System.Web;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using SpreadsheetLight;
using Utility;
using DocumentFormat.OpenXml.Spreadsheet;

public class summaryreport : IHttpHandler {

    ConnectionMaster cm = new ConnectionMaster();
    DataTransformerUtility dtu = new DataTransformerUtility();
    SpreadSheetStyler sStyler = new SpreadSheetStyler();
    
    public void ProcessRequest (HttpContext context) {

        string startdate = "5/1/2018"; //context.Request.QueryString["startdate"];
        string enddate = "5/31/2018"; //context.Request.QueryString["enddate"];

        DataSet dataset = new DataSet();
        ArrayList paramList = new ArrayList();

        paramList.Add(new SqlParameter("@StartDate", startdate));
        paramList.Add(new SqlParameter("@EndDate", enddate));

        dataset = cm.retrieveDataSetBySP("sp_LogSheet_Report_Summary", paramList);

        DataTable table1 = dataset.Tables[0];
        DataTable table2 = dataset.Tables[1];
        int colindex = 0;
        int rowindex = 0;

        SLDocument sldoc = new SLDocument();

        sldoc.RenameWorksheet(SLDocument.DefaultFirstSheetName, "SUMMARY");

        sldoc.SetCellValue("B1", "Shift Lead");
        sldoc.SetCellValue("C1", "QA/Training");
        sldoc.SetCellValue("D1", "Database");
        sldoc.SetCellValue("E1", "Advanced Analytics");
        sldoc.SetCellValue("F1", "Cross-Trained MT");

        sldoc.SetCellValue("A2", "Shift Lead");
        sldoc.SetCellValue("A3", "QA/Training");
        sldoc.SetCellValue("A4", "Database");
        sldoc.SetCellValue("A5", "Cross-Trained");
        sldoc.SetCellValue("A6", "Alarm Management");
        sldoc.SetCellValue("A7", "Administrative (NPT)");
        sldoc.SetCellValue("A8", "Productivity");
        sldoc.SetCellValue("A9", "Utilization");
        
        foreach (DataRow dr in table1.Rows)
        {
            colindex = 0;
            rowindex = 0;
            switch (dtu.extractStringValue(dr["UserGroup"], DataTransformerUtility.DataType.STRING))
            {
                case "CS": colindex = 2; break;
                case "QA": colindex = 3; break;
                case "DB": colindex = 4; break;
            }
            switch (dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING))
            {
                case "Admin": rowindex = 7; break;
                case "Alarm Management": rowindex = 6; break;
                case "Database": rowindex = 4; break;
                case "QA/Training": rowindex = 3; break;
                case "Shift Lead": rowindex = 2; break;
                case "Special Project": rowindex = 5; break;
            }
            sldoc.SetCellValueNumeric(rowindex, colindex, dtu.extractStringValue(dr["HrTimeSpent"], DataTransformerUtility.DataType.DECIMAL));

        }

        for (colindex = 2; colindex < 7; colindex++)
        {
            sldoc.SetCellValue(8, colindex, "=SUM(" + SLConvert.ToCellRange(2, colindex, 6, colindex) + ")");
        }
        for (colindex = 2; colindex < 7; colindex++)
        {
            sldoc.SetCellValue(9, colindex, "=SUM(" + SLConvert.ToCellRange(2, colindex, 7, colindex) + ")");
        }

        sldoc.SetCellStyle(1, 1, 9, 6, sStyler.styleBorder(BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin, BorderStyleValues.Thin));
        sldoc.SetCellStyle(2, 2, 9, 6, sStyler.styleText(true,VerticalAlignmentValues.Center,HorizontalAlignmentValues.Center,"00.00"));
        sldoc.SetCellStyle(8, 1, 9, 6, sStyler.styleFont(true,11,"Calibri",System.Drawing.Color.Black));
        sldoc.AutoFitColumn(1, 10);
        
        context.Response.Clear();
        context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        context.Response.AddHeader("Content-Disposition", "attachment; filename=Log Sheet Utilization Report.xlsx");
        sldoc.SaveAs(context.Response.OutputStream);
        context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webtemplate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string burl = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
        string origurl = Request.Url.AbsoluteUri;
        if (burl.EndsWith("/", StringComparison.CurrentCultureIgnoreCase) == false)
            burl += "/";
        string remainingchunks = ((origurl.Replace(burl, "")).Replace("/", "|")).ToUpper();
        this.hfBaseURL.Value = burl;
        this.hfURLChunks.Value = remainingchunks;
    }
}
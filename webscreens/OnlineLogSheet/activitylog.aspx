﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="activitylog.aspx.cs" Inherits="webscreens_OnlineLogSheet_activitylog" %>

<!DOCTYPE html5>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Online Log Sheet</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link rel="icon" type="image/ico" href="../../resources/images/favicon.ico" />
    <link rel="shortcut icon" href="../../resources/images/favicon.ico" />

    <!-- Core CSS -->
    <link href="../../js/external/bootstrap 3.5/css/bootstrap.css" rel="stylesheet" />
    <link href="../../js/external/bootstrap 3.5/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../js/external/Material-Font/css/materialdesignicons.min.css" rel="stylesheet" />
    <link href="../../js/external/animation/css/animations.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/jqueryui/jquery-ui.min.css" rel="stylesheet" />
    <link href="../../js/external/telerik-kendo/styles/kendo.common-material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap 3.5/css/sticky-footer.css" rel="stylesheet" />

    <!-- Editable CSS -->
    <%--<link type="text/css" rel="stylesheet" href="../../css/bootstraphack.css" />--%>
    <link type="text/css" rel="stylesheet" href="../../css/StyleSheet.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    
        <input type="hidden" runat="server" id="hfAccountID" name="hfAccountID" />
        <input type="hidden" runat="server" id="hfUsername" name="hfUsername" />
        <input type="hidden" runat="server" id="hfEmailAddress" name="hfEmailAddress" />
        <input type="hidden" runat="server" id="hfEmployeeName" name="hfEmployeeName" />
        <input type="hidden" runat="server" id="hfEmployeeID" name="hfEmployeeID" />
        <input type="hidden" runat="server" id="hfRoleName" name="hfRoleName" />
        <input type="hidden" runat="server" id="hfAlarmCenter" name="hfAlarmCenter" />
        <input type="hidden" runat="server" id="hfPermissionName" name="hfPermissionName" />
        <input type="hidden" runat="server" id="hfPassword" name="hfPassword" />
		<input type="hidden" runat="server" id="EFSDateTimeNow" name="EFSDateTimeNow" />

    </form>

    <div id="wrap">
        <div id="wrapper">
            <div id="headerholder">
            </div>
            <div id="navigationmenu">
            </div>

            <div class="main-container">
                <div class="container">
                    <div class="row">
                        <form role="form">      
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                            
                                <div class="form-holder">
                                    <div id="tabstrip">
                                        <ul>
                                            <li title="Admin" class="k-state-active">
                                                Admin
                                            </li>
                                            <li title="Admin Productive">
                                                Admin (Productive)
                                            </li>
                                            <li title="Alarm"> 
                                                Alarm
                                            </li>
                                            <li title="CS">
                                                CS
                                            </li>
                                            <li title="Project">
                                                Project
                                            </li>
                                            <li title="ALR">
                                                ALR
                                            </li>
                                            <li title="SPM">
                                                SPM
                                            </li>
                                            <li title="FQR">
                                                FQR
                                            </li>
                                            <li title="EM&T">
                                                EM&T
                                            </li>
                                            <li title="EFS">
                                                EFS
                                            </li>
                                            <li title="Enterprise Software Support">
                                                ESS
                                            </li>
                                            <li title="Adhoc Analytics">
                                                AA
                                            </li>
                                            <li title="Tool Support">
                                                TS
                                            </li>
                                            <li title="Report Generation">
                                                RG
                                            </li>
                                          <%--  <li>
                                                Reports
                                            </li>--%>
                                        </ul>
                                        <div>
                                            <div>
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - Admin</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtAdminActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridAdmin"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div>
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - Admin Productive</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtAdminProductiveActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridAdminProductive"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - Alarm</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtAlarmActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridAlarm"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - CS</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtCSActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridCSAssigned"></div>
                                                <h2><span></span></h2>
                                                <div id="gridCS"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - Project</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtProjectActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridProject"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - ALR</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtALRActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridALR"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - SPM</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtSPMActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridSPM"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - FQR</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtFQRActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridFQRPending"></div>
                                                <h2><span></span></h2>
                                                <div id="gridFQR"></div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - EM&T</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtEMTActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridEMTPending"></div>
                                                <h2><span></span></h2>
                                                <div id="gridEMT"></div>
                                            </div>
                                        </div>
                                        
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - EFS</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtEFSActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridEFSPending"></div>
                                                <h2><span></span></h2>
                                                <div id="gridEFS"></div>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - ESS</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtESSActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridESS"></div>
                                            </div>
                                        </div>
                                        
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - Adhoc Analytics</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtAAActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridAAAssigned"></div>
                                                <h2><span></span></h2>
                                                <div id="gridAA"></div>
                                            </div>
                                        </div>
                                        
                                        <div>
                                            <div class="weather">
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - Tool Support</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtTSActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridTSAssigned"></div>
                                                <h2><span></span></h2>
                                                <div id="gridTS"></div>
                                            </div>
                                        </div>

                                        <div>
                                            <div>
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Online Log Sheet - Report Generation</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtRGActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridRG"></div>
                                            </div>
                                        </div>

                                  <%--      <div>
                                            <div class="weather">
                                                <h2><span>Online Log Sheet - Report</span></h2>
                                                <div id="Div1">
                                                    <ul> 
                                                        <li><a href="javascript:generateIndividualSummaryReport();">Individual Summary Report</a></li>
                                                        <li><a href="reportlist.aspx?report=9" target="_blank">EFS Report</a></li>
                                                        <li><a href="reportlist.aspx?report=6" target="_blank">SPM Report</a></li>
                                                        <li><a href="reportlist.aspx?report=5" target="_blank">ALR Report</a></li>
                                                        <li><a href="reportlist.aspx?report=7" target="_blank">FQR Report</a></li>
                                                        <li><a href="reportlist.aspx?report=3" target="_blank">CSS Report</a></li>
                                                        <li><a href="reportlist.aspx?report=8" target="_blank">EMT Report</a></li>
                                                        <li><a href="reportlist.aspx?report=4" target="_blank">Project Report</a></li>
                                                        <li><a href="reportlist.aspx?report=11" target="_blank">Adhoc Report</a></li>
                                                        <li><a href="reportlist.aspx?report=13" target="_blank">Utilization and Productivity Report</a></li>
                                                        <li><a href="reportlist.aspx?report=14" target="_blank">Utilization and Productivity Report Per Employee</a></li>
                                                    </ul>
                                                </div>
                                                <h2><span></span></h2>
                                                <div id="Div2"></div>
                                            </div>
                                        </div>--%>

                                    </div>
                                </div>

                            </div>
                           <div id="divEnterPassorword" style="display:none">
                                <p>Please enter your network password.</p>
                                <p><input type="password" id="txtPassword" /></p>
                                <button type="button" id="btnEnterPassword">Submit</button>
                            </div>
                            <div id="divLoading" style="display:none;">Loading.....<%--<img alt="Loading..." src="../../images/loading.gif" />--%></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="AddNewRecord" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" id="myCancel">&times;</button>
          <h4 class="modal-title">Add New Record</h4>
        </div>
        <div class="modal-body">
            <div id="example">
    <div class="demo-section k-content">
        <ul class="fieldlist">
            <li hidden="hidden">
                <label for="txtProcessDate">Process Date</label>
                <input id="txtProcessDate" type="text" class="k-textbox" style="width: 100%;" disabled="disabled" />
            </li>
            <li>
                <label for="txtStoreName">Store Name</label>
                <input id="txtStoreName" type="text" class="k-textbox" style="width: 100%;" required/>
            </li>

            <li>
                <label for="selectTypeController">Request Type</label>
                <select id="selectTypeController" class="k-input" style="width: 100%;">
                    <option value="">Ahold Bakery Oven Override</option>
                    <option value="">H&amp;M Inbound</option>
                    <option value="">H&amp;M Triage</option>
                    <option value="">H&amp;M Work Request</option>
                    <option value="">Others</option>
                    <option value="">Quarterly Backup</option>
                </select>
            </li>
            <li>
                <div class="" style="position: relative;">
                    <label for="txtDateTimeRequested">Date Time Requested:</label>
                     <input class="full-width datePicker" id="txtDateTimeRequested" title="datetimepicker" style="width: 100%;" name="txtDateTimeRequested" />
                    
                </div>
            </li>
            <li>
                <label for="txtRequestor">Requestor:</label>
                <input class="full-width k-textbox" style="width: 100%;" type="text" id="txtRequestor" name="txtRequestor" value="" required/>
            </li>
            <li>
                <label for="txtWorkOrderNumber">Work Order Number:</label>
                <input class="full-width k-textbox" style="width: 100%;" type="number" id="txtWorkOrderNumber" name="txtWorkOrderNumber" value="" />
            </li>
            <li>
                <label for="txtRequestMade">Request Made:</label>
                <input class="full-width k-textbox" style="width: 100%;" type="text" id="txtRequestMade" name="txtRequestMade" value="" />
            </li>
            <li hidden="hidden">
                <label for="txtDateTimeCompleted">Date Time Completed:</label>
                <input class="full-width k-textbox" style="width: 100%;" type="text" id="txtDateTimeCompleted" name="txtDateTimeCompleted" value="" />
            </li>
            <li>
                <label for="txtRemarks">Remarks:</label>
                <input class="full-width k-textbox" style="width: 100%;" type="text" id="txtRemarks" name="txtRemarks" value="" />
            </li>
            <li>
                <label for="select1stTouchStatus">1st Touch Status</label>
                <select id="select1stTouchStatus" class="k-input" style="width: 100%;">
<%--                    <option value="19">Cancelled</option>
                    <option value="20">Communication Problem</option>
                    <option value="21">Completed</option>
                    <option value="22">Completed - Further Action</option>
                    <option value="23">Completed - Out of Scope</option>--%>
                </select>
            </li>
            <li id="DateTimeFollowUpList" hidden="hidden">
                <div class="" style="position: relative;">
                    <label for="txtDateTimeFollowUp">Date Time to Follow Up:</label>
                    <input class="full-width datePicker" id="txtDateTimeFollowUp" title="datetimepicker" style="width: 100%;" name="txtDateTimeFollowUp" />
                </div>
            </li>
            <li>
                <button id="addNewRecord" class="k-button k-primary">Save</button>
            </li>
        </ul>
        <style type="text/css">
            .fieldlist {
                margin: 0 0 -2em;
                padding: 0;
            }

            .fieldlist li {
                list-style: none;
                padding-bottom: 2em;
            }

            .fieldlist label {
                display: block;
                padding-bottom: 1em;
                font-weight: bold;
                text-transform: uppercase;
                font-size: 12px;
                color: #444;
            }

        </style>
    </div>
</div>
        </div>


        <div class="modal-footer" id="fileSaveBtn" hidden="hidden">
            <div class="cta-buttons">
                <a id="btnSubmitFiled" class="btn btn-primary btn-arrow" href="#">
                    <span>
                        <span class="text-wrapper">Submit</span>
                    </span>
                </a>
            </div>
        </div>
      </div>
      
    </div>
  </div>
    
    <div id="footer"></div>
    <div id="footerMobile"></div>
         <!-- Javascript Libraries -->
    <script type="text/javascript" language="javascript" src="../../js/json2.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/jquery-2.1.4.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/bootstrap 3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="../../js/jqueryui/jquery-ui.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/bootbox.min.js"></script>
    <%--<script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/standardstyler.js"></script>--%>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/default.js"></script>
	 <script type="text/javascript" language="javascript" src="../../js/moment.js"></script>
	  <script type="text/javascript" language="javascript" src="../../js/moment-timezone.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_ADMIN.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_ADMINProductive.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_ALARM.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_CS.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_PROJECT.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_ALR.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_SPM.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_FQR.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_EMT.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_EFS.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_ESS.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_AA.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_TS.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_RG.js"></script>
    <script type="text/javascript" language="javascript">
        var ActivityType_ADMIN = 1;
        var ActivityType_ALARM = 2;
        var ActivityType_CS = 3;
        var ActivityType_PROJECT = 4;
        var ActivityType_ALR = 5;
        var ActivityType_SPM = 6;
        var ActivityType_FQR = 7;
        var ActivityType_EMT = 8;
        var ActivityType_EFS = 9;
        var ActivityType_ESS = 10;
        var ActivityType_AA = 11;
        var ActivityType_TS = 12;
        var ActivityType_ADMINPRODUCTIVE = 13;
        var ActivityType_RG = 14;
        var Employee_ID = jQuery("#hfEmployeeID").val();
        var RolePosition = jQuery("#hfRoleName").val();
    </script>
      
    <script type="text/javascript" >
        //jQuery.noConflict();
        //jQuery(document).ready(function () {
        //    jQuery('#select1stTouchStatus').on('change', function () {
        //        if (jQuery(this).val() == 1) {
        //            jQuery('#DateTimeFollowUpList').show();
        //        } else {
        //            jQuery('#DateTimeFollowUpList').hide();
        //        }
        //    });
        //    var validator = 0;
        //    jQuery("#addNewRecord").on('click', function () {
        //        if (jQuery('#txtDateTimeRequested').val() == "") { jQuery('#txtDateTimeRequested').css("border-color", "red");} else {jQuery('#txtDateTimeRequested').css("border-color", "#f0f0f0");}
        //        if (jQuery('#txtRequestMade').val() == "") { jQuery('#txtRequestMade').css("border-color", "red"); } else { jQuery('#txtRequestMade').css("border-color", "#f0f0f0"); }
        //        if (jQuery('#txtStoreName').val() == "") { jQuery('#txtStoreName').css("border-color", "red"); } else { jQuery('#txtStoreName').css("border-color", "#f0f0f0"); }
        //        if (jQuery('#txtRemarks').val() == "") { jQuery('#txtRemarks').css("border-color", "red");} else { jQuery('#txtRemarks').css("border-color", "#f0f0f0"); }
        //        if (jQuery('#txtRequestor').val() == "") { jQuery('#txtRequestor').css("border-color", "red");} else { jQuery('#txtRequestor').css("border-color", "#f0f0f0"); }
        //        if ((jQuery('#txtStoreName').val() != "") && (jQuery('#txtDateTimeRequested').val() != "") && (jQuery('#txtRequestMade').val() != "") && (jQuery('#txtRemarks').val() != "") && (jQuery('#txtRequestor').val() != "")) { loadGridEFS.dataSourceEFS.create(); } else { alert('Please input required field'); }
        //    });
        //    jQuery(".datePicker").kendoDateTimePicker({
        //        value: new Date(),
        //        dateInput: true
        //    });

        //    callAjaxRequestJSON("CheckEFSTechPermission"
        //            , { EmployeeID: Employee_ID }
        //            , function (response) {
        //                var data = response['d']
        //                var ro = data.ResponseItem;
        //                if (data.ErrorMessage == null) {
        //                    if (!ro) {
        //                        bootbox.alert("You have no permission on this page.");
        //                        window.history.back();
        //                    }
        //                    else {
        //                        if (!sessionStorage.getItem("UserPassword")) {
        //                            jQuery("#divEnterPassorword").show().kendoWindow({
        //                                width: "420px",
        //                                height: "150px",
        //                                modal: true,
        //                                resizable: false,
        //                                draggable: false,
        //                                title: ""
        //                            }).data("kendoWindow").center();
        //                            jQuery("#divEnterPassorword").parent().find(".k-window-action").css("visibility", "hidden");
        //                            jQuery("#txtPassword").focus();
        //                        }
        //                    }
        //                } else {
        //                    bootbox.alert("Web service error when loading activitylog page. " + data.ErrorMessage);
        //                }
        //            }
        //            , function () {
        //                window.history.back();
        //            }
        //    );

        //});

        //function generateIndividualSummaryReport() {

        //    if (jQuery("#hfRoleName").val() == "Supervisor")
        //        window.open("http://phectsmpc-db01.emrsn.org/ReportServer/Pages/ReportViewer.aspx?%2fERS+Reports%2fOnline+Log+Sheet%2fActivityLogReport&rs:Command=Render", "_blank");
        //    else
        //        window.open("http://phectsmpc-db01.emrsn.org/ReportServer/Pages/ReportViewer.aspx?%2fERS+Reports%2fOnline+Log+Sheet%2fActivityLogReportIndividual&rs:Command=Render&EmployeeID=" + jQuery("#hfEmployeeID").val(), "_blank");
        //}

        //HeaderHolder();
        //LogSheetMenu();
        //FooterMobile();
        //Footer();        
    </script>

</body>
</html>

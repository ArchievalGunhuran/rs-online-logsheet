﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportmaintenance.aspx.cs" Inherits="webscreens_OnlineLogSheet_reportmaintenance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Online Log Sheet</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link rel="icon" type="image/ico" href="../../resources/images/favicon.ico" />
    <link rel="shortcut icon" href="../../resources/images/favicon.ico" />

    <!-- Core CSS -->
    <link href="../../js/external/bootstrap 3.5/css/bootstrap.css" rel="stylesheet" />
    <link href="../../js/external/Material-Font/css/materialdesignicons.min.css" rel="stylesheet" />
    <link href="../../js/external/animation/css/animations.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common-material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap 3.5/css/sticky-footer.css" rel="stylesheet" />

    <!-- Editable CSS -->
    <%--<link type="text/css" rel="stylesheet" href="css/bootstraphack.css" />--%>
    <link type="text/css" rel="stylesheet" href="../../css/StyleSheet.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    
        <input type="hidden" runat="server" id="hfAccountID" name="hfAccountID" />
        <input type="hidden" runat="server" id="hfUsername" name="hfUsername" />
        <input type="hidden" runat="server" id="hfEmailAddress" name="hfEmailAddress" />
        <input type="hidden" runat="server" id="hfEmployeeName" name="hfEmployeeName" />
        <input type="hidden" runat="server" id="hfEmployeeID" name="hfEmployeeID" />
        <input type="hidden" runat="server" id="hfRoleName" name="hfRoleName" />
        <input type="hidden" runat="server" id="hfAlarmCenter" name="hfAlarmCenter" />
        <input type="hidden" runat="server" id="hfPermissionName" name="hfPermissionName" />
        <input type="hidden" runat="server" id="hfUserGroup" name="hfUserGroup" />

    </form>

    <div id="wrap">
        <div id="wrapper">
            <div id="headerholder">
            </div>
            <div id="navigationmenu">
            </div>

            
            <div class="main-container">
                <div class="container">
                    <div class="row">
                        <form role="form">      
                            <div class="col-xs-12 col-sm-12 col-lg-12>
                            
                                <div class="form-holder">

                                        <div><h2><span>Online Log Sheet - Reports</span></h2></div>
                                        
                                        <div id="Div1">
                                            <ul> 
                                                <li><a href="javascript:generateIndividualSummaryReport();">Individual Summary Report</a></li>
                                                <li><a href="reportlist.aspx?report=9" target="_blank">EFS Report</a></li>
                                                <li><a href="reportlist.aspx?report=6" target="_blank">SPM Report</a></li>
                                                <%--<li><a href="reportlist.aspx?report=5" target="_blank">ALR Report</a></li>--%>
                                                <li><a href="reportlist.aspx?report=7" target="_blank">FQR Report</a></li>
                                                <li><a href="reportlist.aspx?report=3" target="_blank">CSS Report</a></li>
                                                <li><a href="reportlist.aspx?report=8" target="_blank">EMT Report</a></li>
                                                <%--<li><a href="reportlist.aspx?report=4" target="_blank">Project Report</a></li>--%>
                                                <li><a href="reportlist.aspx?report=11" target="_blank">Adhoc Report</a></li>
                                                <li><a href="reportlist.aspx?report=13" target="_blank">Utilization and Productivity Report</a></li>
                                                <li><a href="reportlist.aspx?report=14" target="_blank">Utilization and Productivity Report Per Employee</a></li>
                                            </ul>
                                        </div>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <div id="footer"></div>
    <div id="footerMobile"></div>
         <!-- Javascript Libraries -->
    <script type="text/javascript" language="javascript" src="../../js/json2.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/jquery-2.1.4.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/bootbox.min.js"></script>
    <%--<script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/standardstyler.js"></script>--%>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/default.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/kendo.all.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet.js"></script>


    
    <script type="text/javascript">
        var Employee_ID = jQuery("#hfEmployeeID").val();
        var RolePosition = jQuery("#hfRoleName").val();

        jQuery.noConflict();

        jQuery(document).ready(function () {
            callAjaxRequestJSON("CheckCSAssignmentPermission"
                    , { EmployeeID: Employee_ID }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            if (!ro) {
                                bootbox.alert("You have no permission on this page.");
                                window.history.back();
                            }
                        } else {
                            bootbox.alert("Web service error when loading activitylog page. " + data.ErrorMessage);
                        }
                    }
                    , function () {
                        window.history.back();
                    }
            );

        });

        function generateIndividualSummaryReport() {
			alert(jQuery("#hfUserGroup").val() + ">>" + jQuery("#hfRoleName").val());
		    if (jQuery("#hfUserGroup").val() == "AES") {
                if (jQuery("#hfRoleName").val() == "Supervisor")
                    window.open("http://phectsmpc-db01.emrsn.org/ReportServer/Pages/ReportViewer.aspx?%2fERS+Reports%2fOnline+Log+Sheet%2fActivityLogReport&rs:Command=Render", "_blank");
                else
                    window.open("http://phectsmpc-db01.emrsn.org/ReportServer/Pages/ReportViewer.aspx?%2fERS+Reports%2fOnline+Log+Sheet%2fActivityLogReportIndividual&rs:Command=Render&EmployeeID=" + jQuery("#hfEmployeeID").val(), "_blank");
            }
            else {
                if (jQuery("#hfRoleName").val() == "Supervisor") {
                    window.open("http://phectsmpc-db01.emrsn.org/ReportServer/Pages/ReportViewer.aspx?%2fERS+Reports%2fOnline+Log+Sheet%2fActivityLogReportQA&rs:Command=Render", "_blank");
                }
                else {
                    window.open("http://phectsmpc-db01.emrsn.org/ReportServer/Pages/ReportViewer.aspx?%2fERS+Reports%2fOnline+Log+Sheet%2fActivityLogReportIndividualQA&rs:Command=Render&EmployeeID=" + jQuery("#hfEmployeeID").val(), "_blank");
                }
            }
		}

        HeaderHolder();
        LogSheetMenu();
        FooterMobile();
        Footer();        
    </script>

</body>
</html>

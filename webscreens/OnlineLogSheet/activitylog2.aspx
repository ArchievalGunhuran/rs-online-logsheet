﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="activitylog2.aspx.cs" Inherits="webscreens_OnlineLogSheet_activitylog2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Online Log Sheet</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link rel="icon" type="image/ico" href="../../resources/images/favicon.ico" />
    <link rel="shortcut icon" href="../../resources/images/favicon.ico" />

    <!-- Core CSS -->
    <link href="../../js/external/bootstrap 3.5/css/bootstrap.css" rel="stylesheet" />
    <link href="../../js/external/bootstrap 3.5/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../js/external/Material-Font/css/materialdesignicons.min.css" rel="stylesheet" />
    <link href="../../js/external/animation/css/animations.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common-material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap 3.5/css/sticky-footer.css" rel="stylesheet" />

    <!-- Editable CSS -->
    <%--<link type="text/css" rel="stylesheet" href="../../css/bootstraphack.css" />--%>
    <link type="text/css" rel="stylesheet" href="../../css/StyleSheet.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    
        <input type="hidden" runat="server" id="hfAccountID" name="hfAccountID" />
        <input type="hidden" runat="server" id="hfUsername" name="hfUsername" />
        <input type="hidden" runat="server" id="hfEmailAddress" name="hfEmailAddress" />
        <input type="hidden" runat="server" id="hfEmployeeName" name="hfEmployeeName" />
        <input type="hidden" runat="server" id="hfEmployeeID" name="hfEmployeeID" />
        <input type="hidden" runat="server" id="hfRoleName" name="hfRoleName" />
        <input type="hidden" runat="server" id="hfAlarmCenter" name="hfAlarmCenter" />
        <input type="hidden" runat="server" id="hfPermissionName" name="hfPermissionName" />
        <input type="hidden" runat="server" id="hfPassword" name="hfPassword" />
        <input type="hidden" runat="server" id="hfUserGroup" name="hfUserGroup" />

    </form>

    <div id="wrap">
        <div id="wrapper">
            <div id="headerholder">
            </div>
            <div id="navigationmenu">
            </div>

            <div class="main-container">
                <div class="container-full-width">
                    <div class="row">
                        <form role="form">      
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                            
                                <div class="form-holder">
                                    <div id="tabstrip">
                                        <ul>
                                            <li title="Admin" class="k-state-active">
                                                Log Sheet Timer
                                            </li>
                                            <%--<li title="Admin">
                                                Log Sheet Manual
                                            </li>--%>
                                        </ul>
                                        <div>
                                            <div>
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Logsheet Timer</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtActivityDate"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridActivityLog"></div>
                                            </div>
                                        </div>
                                       <%-- <div>
                                            <div>
                                                <div>
                                                    <table width="100%">
                                                        <tr>
                                                            <td align="left"><h2><span>Logsheet Manual</span></h2></td>
                                                            <td align="right"><span><input type="text" data-type="date" data-role="datetimepicker" id="txtActivityDateManual"/></span></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="gridActivityLogManual"></div>
                                            </div>
                                        </div>--%>
                                        
                                    </div>
                                </div>

                            </div>
                            <div id="divEnterPassorword" style="display:none">
                                <p>Please enter your network password.</p>
                                <p><input type="password" id="txtPassword" /></p>
                                <button type="button" id="btnEnterPassword">Submit</button>
                            </div>
                            <div id="divLoading" style="display:none;">Loading.....<%--<img alt="Loading..." src="../../images/loading.gif" />--%></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
    <div id="footer"></div>
    <div id="footerMobile"></div>
         <!-- Javascript Libraries -->
    <script type="text/javascript" language="javascript" src="../../js/json2.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/jquery-2.1.4.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/bootstrap 3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/kendo.all.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/bootbox.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/default.js"></script>

    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet2_Enhanced.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet2_Manual.js"></script>
    <script type="text/javascript" language="javascript">
        var ActivityType_ADMIN = 1;
        var ActivityType_ALARM = 2;
        var ActivityType_CS = 3;
        var ActivityType_PROJECT = 4;
        var ActivityType_ALR = 5;
        var ActivityType_SPM = 6;
        var ActivityType_FQR = 7;
        var ActivityType_EMT = 8;
        var ActivityType_EFS = 9;
        var ActivityType_ESS = 10;
        var ActivityType_AA = 11;
        var ActivityType_TS = 12;
        var ActivityType_ADMINPRODUCTIVE = 13;
        var ActivityType_RG = 14;
        var USER_GROUP = jQuery("#hfUserGroup").val();
        var Employee_ID = jQuery("#hfEmployeeID").val();
        var RolePosition = jQuery("#hfRoleName").val();
        var EmployeeName = jQuery("#hfEmployeeName").val();
    </script>
      
    <script type="text/javascript">
        jQuery.noConflict();

        jQuery(document).ready(function () {

            callAjaxRequestJSON("CheckOnlineLogSheetPermission"
                    , { EmployeeID: Employee_ID }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        //alert(ro);
                        
                        if (data.ErrorMessage == null) {
                            if (!ro) {
                                bootbox.alert("You have no permission on this page.");
                                //window.history.back();
                            }
                            else {
                                //if (!sessionStorage.getItem("UserPassword")) {
                                //    jQuery("#divEnterPassorword").show().kendoWindow({
                                //        width: "420px",
                                //        height: "150px",
                                //        modal: true,
                                //        resizable: false,
                                //        draggable: false,
                                //        title: ""
                                //    }).data("kendoWindow").center();
                                //    jQuery("#divEnterPassorword").parent().find(".k-window-action").css("visibility", "hidden");
                                //    jQuery("#txtPassword").focus();
                                //}
                            }
                        } else {
                            bootbox.alert("Web service error when loading activitylog page. " + data.ErrorMessage);
                        }
                    }
                    , function () {
                        //window.history.back();
                    }
            );

        });

        HeaderHolder();
        LogSheetMenu();
        FooterMobile();
        Footer();

    </script>

</body>
</html>

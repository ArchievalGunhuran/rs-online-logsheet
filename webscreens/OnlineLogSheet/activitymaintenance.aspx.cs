﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webscreens_OnlineLogSheet_activitymaintenance : System.Web.UI.Page
{
    private string login = System.Configuration.ConfigurationManager.AppSettings["rootLoginPage"];

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            initializeForm();
        }
    }

    private void initializeForm()
    {
        General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
        HttpCookie cookie = Request.Cookies["CurrentUser"];
        if (cookie != null)
        {
            String decriptedcookie = sm.Decrypt(cookie.Value);
            string[] cookieInfo = decriptedcookie.Split("|".ToCharArray());
            if (cookieInfo.Length > 0)
            {
                this.hfAccountID.Value = cookieInfo[0].ToString();
                this.hfEmployeeID.Value = cookieInfo[1].ToString();
                this.hfUsername.Value = cookieInfo[2].ToString();
                this.hfEmailAddress.Value = cookieInfo[3].ToString();
                this.hfEmployeeName.Value = cookieInfo[4].ToString();
                this.hfRoleName.Value = cookieInfo[5].ToString();
                this.hfAlarmCenter.Value = cookieInfo[6].ToString();
                this.hfPermissionName.Value = cookieInfo[7].ToString();
                this.hfUserGroup.Value = cookieInfo[8].ToString();
            }
            else
                Response.Redirect(login);
        }
        else
            Response.Redirect(login);

    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="projectQAForm.aspx.cs" Inherits="webscreens_OnlineLogSheet_projectQAForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>QA for Project</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link rel="icon" type="image/ico" href="../../resources/images/favicon.ico" />
    <link rel="shortcut icon" href="../../resources/images/favicon.ico" />

    <!-- Core CSS -->
    <link href="../../js/external/bootstrap 3.5/css/bootstrap.css" rel="stylesheet" />
    <link href="../../js/external/Material-Font/css/materialdesignicons.min.css" rel="stylesheet" />
    <link href="../../js/external/animation/css/animations.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common-material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap 3.5/css/sticky-footer.css" rel="stylesheet" />

    <!-- Editable CSS -->
    <%--<link type="text/css" rel="stylesheet" href="css/bootstraphack.css" />--%>
    <link type="text/css" rel="stylesheet" href="../../css/StyleSheet.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    
        <input type="hidden" runat="server" id="hfAccountID" name="hfAccountID" />
        <input type="hidden" runat="server" id="hfUsername" name="hfUsername" />
        <input type="hidden" runat="server" id="hfEmailAddress" name="hfEmailAddress" />
        <input type="hidden" runat="server" id="hfEmployeeName" name="hfEmployeeName" />
        <input type="hidden" runat="server" id="hfEmployeeID" name="hfEmployeeID" />
        <input type="hidden" runat="server" id="hfRoleName" name="hfRoleName" />
        <input type="hidden" runat="server" id="hfAlarmCenter" name="hfAlarmCenter" />
        <input type="hidden" runat="server" id="hfPermissionName" name="hfPermissionName" />


    </form>


    <div id="wrap">
        <div id="wrapper">
            <div id="headerholder">
            </div>
            <div id="navigationmenu">
            </div>

            
            <div class="main-container">
                <div class="container">
                    <div class="row">
                        <form role="form">      
                            <div class="col-xs-12 col-sm-12 col-lg-12>
                            
                                <div class="form-holder">

                                        <div><h2><span>QA for Project (Staging)</span></h2></div>
                                        <div id="gridProjectAssignment"></div>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="wnd">
                <%--<div id="grid"></div>--%>
            </div>
            <div id="loading" style="display:none"></div>
        </div>
    </div>

    <script type="text/x-kendo-template" id="template">
        <div id="details-container">
            <h2>Ticket Number : <span> #= ProjectNumber# </span></h2>
            <br>
            <form>
              <div class="form-group">
                <label for="txtDaysDelayed">No. of Days Delayed</label>
                <input type="number" onchange="recomputeTimeliness()" class="form-control" value = "#= vDaysDelayed#" id="txtDaysDelayed">
                <input type="hidden" id="projectassignmentID" value = "#= LogDetailID#">
              </div>
              <div class="form-group">
                <label for="txtTimeliness">First Pass Timeliness - 40%</label>
                <input type="number" class="form-control" value = "#= vTimeliness#"  id="txtTimeliness" disabled>
              </div>
                <div class="form-group">
                <label for="txtActionItem">Action Item</label>
                <input type="number" onchange="computeAccuracy()" class="form-control" value = "0" id="txtActionItem" required>
              </div>
              <div class="form-group">
                <label for="txtWorking">Working</label>
                <input type="number" onchange="computeAccuracy()" class="form-control" value = "0" id="txtWorking" required>
              </div>
              <div class="form-group">
                <label for="txtAccuracy">First Pass Accuracy - 60%</label>
                <input type="number" class="form-control" id="txtAccuracy" value = "0" disabled>
              </div>
              <div class="form-group">
                <label for="txtFirstPass">First Pass 40%</label>
                <input type="number" class="form-control" id="txtFirstPass" value = "0" disabled>
              </div>
                <ul/>
              <div class="form-group">
                <label for="txtTimeSpent">Time Spent of QA</label>
                <input type="number" class="form-control" id="txtTimeSpent">
              </div>
             <div class="window-footer">
                    <button type="submit" class="k-primary k-button" id="btnComplete" required>Complete</button>
              </div>
            </form>
        </div>
    </script>


    <div id="footer"></div>
    <div id="footerMobile"></div>
         <!-- Javascript Libraries -->
    <script type="text/javascript" language="javascript" src="../../js/json2.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/jquery-2.1.4.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/bootbox.min.js"></script>
    <%--<script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/standardstyler.js"></script>--%>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/default.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/kendo.all.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_ProjectAssignmentQAForm.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

    <script type="text/javascript" language="javascript">
        var ActivityType_AA = 11;
        var Employee_ID = jQuery("#hfEmployeeID").val();
        var RolePosition = jQuery("#hfRoleName").val();
    </script>

    
    <script type="text/javascript">
        jQuery.noConflict();

        jQuery(document).ready(function () {
            callAjaxRequestJSON("CheckCSAssignmentPermission"
                    , { EmployeeID: Employee_ID }
                    , function (response) {
                        var data = response['d']
                        var ro = data.ResponseItem;
                        if (data.ErrorMessage == null) {
                            if (!ro) {
                                bootbox.alert("You have no permission on this page.");
                                //window.history.back();
                            }
                        } else {
                            bootbox.alert("Web service error when loading activitylog page. " + data.ErrorMessage);
                        }
                    }
                    , function () {
                        window.history.back();
                    }
            );

        });
        function computeAccuracy() {
            var timeliness = $('#txtTimeliness').val();

            var actionItem = $('#txtActionItem').val();
            var working = $('#txtWorking').val();
            var accuracy = (working / actionItem * 100) * 0.6;
            var totalFirstPass = Number(timeliness) + Number(accuracy);


            $('#txtFirstPass').val(totalFirstPass.toFixed(2));
            $('#txtAccuracy').val(accuracy.toFixed(2));
        }

        function recomputeTimeliness() {
            var daysDelayed = $('#txtDaysDelayed').val();
            var timeliness = (100 - (daysDelayed * 15)) * 0.4;
            $('#txtTimeliness').val(timeliness.toFixed(2));
            $('#txtActionItem').val(0);
            $('#txtWorking').val(0);
            $('#txtAccuracy').val(0);
            $('#txtFirstPass').val(0);
        }

        HeaderHolder();
        LogSheetMenu();
        FooterMobile();
        Footer();
    </script>

    <style type="text/css">
        #loading {
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 100;
            width: 100%;
            height: 100%;
            background-color: rgba(192, 192, 192, 0.5);
            background-image: url("../../images/loading.gif");
            background-repeat: no-repeat;
            background-position: center;
      }
        #details-container
        {
            padding: 10px;
        }

        #details-container h2
        {
            margin: 0;
            font-weight:bold;
        }

        #details-container h2 span
        {
            color:forestgreen;
        }

        #details-container em
        {
            color: #8c8c8c;
        }

        #details-container dt span
        {
            margin:0;
            display: inline;
            color:forestgreen;
        }
        #details-container dl
        {
           margin-top: 3%;
        }

        #txtDaysDelayed, #txtTimeliness, #txtActionItem, #txtWorking, #txtAccuracy, #txtFirstPass, #txtTimeSpent
        {
           width: 90%;
        }
    </style>

</body>
</html>
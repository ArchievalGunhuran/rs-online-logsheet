﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reportlist.aspx.cs" Inherits="webscreens_OnlineLogSheet_reportlist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
    <link rel="icon" type="image/ico" href="../../resources/images/favicon.ico" />
    <link rel="shortcut icon" href="../../resources/images/favicon.ico" />

    <!-- Core CSS -->
    <link href="../../js/external/bootstrap 3.5/css/bootstrap.css" rel="stylesheet" />
    <link href="../../js/external/Material-Font/css/materialdesignicons.min.css" rel="stylesheet" />
    <link href="../../js/external/animation/css/animations.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap-datepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.default.min.css" rel="stylesheet" type="text/css" />

    <link href="../../js/external/telerik-kendo/styles/kendo.common-material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/telerik-kendo/styles/kendo.material.min.css" rel="stylesheet" type="text/css" />
    <link href="../../js/external/bootstrap 3.5/css/sticky-footer.css" rel="stylesheet" />

    <!-- Editable CSS -->
    <%--<link type="text/css" rel="stylesheet" href="css/bootstraphack.css" />--%>
    <link type="text/css" rel="stylesheet" href="../../css/StyleSheet.css" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <form id="frmMain" runat="server">
        <asp:HiddenField ID="hfPageID" runat="server" />
                
        <input type="hidden" runat="server" id="hfAccountID" name="hfAccountID" />
        <input type="hidden" runat="server" id="hfUsername" name="hfUsername" />
        <input type="hidden" runat="server" id="hfEmailAddress" name="hfEmailAddress" />
        <input type="hidden" runat="server" id="hfEmployeeName" name="hfEmployeeName" />
        <input type="hidden" runat="server" id="hfEmployeeID" name="hfEmployeeID" />
        <input type="hidden" runat="server" id="hfRoleName" name="hfRoleName" />
        <input type="hidden" runat="server" id="hfAlarmCenter" name="hfAlarmCenter" />
        <input type="hidden" runat="server" id="hfPermissionName" name="hfPermissionName" />
        <input type="hidden" runat="server" id="hfActivityTypeID" name="hfActivityTypeID" />
        <input type="hidden" runat="server" id="hfUserGroup" name="hfUserGroup" />

        <asp:HiddenField ID="hfMobile" runat="server" />
        <asp:HiddenField ID="hfAddress" runat="server" />

        <asp:HiddenField ID="hfBaseURL" runat="server" />
        <asp:HiddenField ID="hfURLChunks" runat="server" />
    </form>
    <div id="wrap">
        <div id="wrapper">
            <div id="headerholder">
            </div>
            <div id="navigationmenu">
            </div>
            <div class="main-container">
                <div class="container">
                    <div class="row">
                        <form role="form">      
                            <div class="col-xs-12 col-sm-12 col-lg-12">
                               <%-- <div class="form-holder">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                              <p>
                                                <label for="categories">Fiscal Year:</label><input id="divFiscalYear" style="width: 270px" />
                                              </p>
                                            </div>
                                        </div>  
                                    </div>                                
                                </div>--%>
                                <div id="divFY" style="display:none">
                                    <p><label for="categories">Fiscal Year:</label><input id="divFiscalYear" style="width: 270px" /></p>
                                </div>
                                <div id="divCY" style="display:none">
                                    <p><label for="categories">Calendar Year:</label><input id="divCalendarYear" style="width: 270px" />
                                    <label for="categories">Calendar Month:</label><input id="divCalendarMonth" style="width: 270px" /></p>
                                </div>
                                <div class="form-holder">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div id="divChart1"></div>
                                            </div>
                                        </div>  
                                    </div>                                
                                </div>
                                <div class="form-holder">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div id="divChart2"></div>
                                            </div>
                                        </div>  
                                    </div>                                
                                </div>
                                <div class="form-holder">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div id="divChart3"></div>
                                            </div>
                                        </div>  
                                    </div>                                
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        

    <div id="footer"></div>
    <div id="footerMobile"></div>
         <!-- Javascript Libraries -->
    <script type="text/javascript" language="javascript" src="../../js/jquery-2.1.4.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/jquery.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/json2.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/bootstrap 3.5/js/bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/bootbox.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/bootstrap-datepicker/js/moment.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js"></script>
    <%--<script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/standardstyler.js"></script>--%>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/default.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/telerik-kendo/js/kendo.all.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/external/AbigImage/abigimage.min.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet.js"></script>
    <script type="text/javascript" language="javascript" src="../../js/ajaxfunctions/OnlineLogSheet/OnlineLogSheet_Report.js"></script>
    <script type="text/javascript" src="../../js/external/highcharts/highcharts.js"></script>
    <script type="text/javascript" src="../../js/external/highcharts/highcharts-3d.js"></script>
    
    <script type="text/javascript">
        jQuery.noConflict();

        var SelectedActivityTypeID = 0;
        var currentDate = new Date();
        var currentYear = currentDate.getFullYear();
        var fiscalYear = currentYear;
        var currentMonth = currentDate.getMonth() + 1
        var FYList = new Array();
        var CYList = new Array();
        var ActivityTypeList = ["ADMIN", "ALARM", "CS", "PROJECT", "ALR", "SPM", "FQR", "EM&T", "EFS", "ESS", "ADHOC ANALYTICS", "TOOL SUPPORT"];
        var ActivityMonthList = [{ ListID: 1, ListDescription: "January" }
                    , { ListID: 2, ListDescription: "February" }
                    , { ListID: 3, ListDescription: "March" }
                    , { ListID: 4, ListDescription: "April" }
                    , { ListID: 5, ListDescription: "May" }
                    , { ListID: 6, ListDescription: "June" }
                    , { ListID: 7, ListDescription: "July" }
                    , { ListID: 8, ListDescription: "August" }
                    , { ListID: 9, ListDescription: "September" }
                    , { ListID: 10, ListDescription: "October" }
                    , { ListID: 11, ListDescription: "November" }
                    , { ListID: 12, ListDescription: "December" }
                ];

        if (currentMonth > 9)
            fiscalYear = fiscalYear + 1;

        CYList.push(currentYear);
        FYList.push(fiscalYear);
        for (var i = 1; i < 3; i++) {
            fiscalYear = fiscalYear - 1;
            FYList.push(fiscalYear);

            currentYear = currentYear - 1;
            CYList.push(currentYear);
        }
        
        jQuery(window).load(function () {
            
            if (jQuery("#hfActivityTypeID").val() < 14) {
                jQuery("#divFY").show();
                jQuery("#divCY").hide();

                var fy = jQuery("#divFiscalYear").kendoDropDownList({
                    //optionLabel: "Select category...",
                    //dataTextField: "CategoryName",
                    //dataValueField: "CategoryID",
                    dataSource: FYList,
                    change: function () { generateReport(jQuery("#hfActivityTypeID").val(), fy.value()); }
                }).data("kendoDropDownList");

                generateReport(jQuery("#hfActivityTypeID").val(), fy.value());
            }
            else {
                jQuery("#divFY").hide();
                jQuery("#divCY").show();

                var cy = jQuery("#divCalendarYear").kendoDropDownList({
                    dataSource: CYList
                    , change: function () { generateReportPerEmployee(jQuery("#hfActivityTypeID").val(), cy.value(), cm.value()); }
                }).data("kendoDropDownList");


                var cm = jQuery("#divCalendarMonth").kendoDropDownList({
                    dataTextField: "ListDescription",
                    dataValueField: "ListID",
                    dataSource: ActivityMonthList
                    ,value: currentMonth
                    , change: function () { generateReportPerEmployee(jQuery("#hfActivityTypeID").val(), cy.value(), cm.value()); }
                }).data("kendoDropDownList");


                generateReportPerEmployee(jQuery("#hfActivityTypeID").val(), cy.value(), cm.value());
            }

        });


        HeaderHolder();
        LogSheetMenu();
        FooterMobile();
        Footer();
    </script>
</body>
</html>

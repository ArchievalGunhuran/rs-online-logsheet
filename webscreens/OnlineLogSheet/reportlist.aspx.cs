﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class webscreens_OnlineLogSheet_reportlist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
            HttpCookie cookie = Request.Cookies["CurrentUser"];
            if (cookie != null)
            {
                String decriptedcookie = sm.Decrypt(cookie.Value);
                string[] cookieInfo = decriptedcookie.Split("|".ToCharArray());
                if (cookieInfo.Length > 0)
                {
                    this.hfActivityTypeID.Value = cookieInfo[0];
                }
                else
                    Response.Redirect("Login.aspx");
            }
            else
                Response.Redirect("Login.aspx");

            if (HttpContext.Current.Request.QueryString.Count > 0)
                this.hfActivityTypeID.Value = HttpContext.Current.Request.QueryString[0].ToString();
            else
                this.hfActivityTypeID.Value = "0";
        }

    }
}
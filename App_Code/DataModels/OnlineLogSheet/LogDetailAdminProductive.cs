﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{

    public class LogDetailAdminProductive
    {
        private int logDetailID;
        private int activityLogID;
        private int activityID;
        private string activityDescription;
        private string remarks;
        private float timeSpent;
        private DateTime activityDate;
        private int employeeID;
        private int activityTypeID;
        private int numberoftransaction;

        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }
        public string Remarks { get { return this.remarks; } set { this.remarks = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int Numberoftransaction { get { return this.numberoftransaction; } set { this.numberoftransaction = value; } }

        public LogDetailAdminProductive() { }
        public LogDetailAdminProductive(
            int _logDetailID
            , int _activityLogID
            , int _activityID
            , string _activityDescription
            , string _remarks
            , float _timeSpent
            , DateTime _activityDate
            , int _employeeID
            , DataModels.Lists _adminTask
            , int _activityTypeID
            ,int _numberoftransaction)
        {
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.activityID = _activityID;
            this.activityDescription = _activityDescription;
            this.remarks = _remarks;
            this.timeSpent = _timeSpent;
            this.activityDate = _activityDate;
            this.employeeID = _employeeID;
            this.activityTypeID = _activityTypeID;
            this.numberoftransaction = _numberoftransaction;
        }
    }
}
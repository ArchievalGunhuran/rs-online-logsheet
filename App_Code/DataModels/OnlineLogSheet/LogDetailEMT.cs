﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailEMT
    {
        private int activityTypeID;
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private DateTime activityDate;

        private int activityID;
        private string activityDescription;

        private string storeName;
        private int storeNumber;
        private string dateTimeRequested;
        private string requestor;
        private DateTime dateTimeSaved;
        private string dateTimeCompleted;
        private string remarks;

        private int statusID;
        private string statusDescription;

        private float timeSpent;
        private int numberoftransaction;
        private string tat; 
        private string timeSpentStr;
        private float totalTimeSpent;

        private string sla;
        private DateTime dueDate;
        private string ticketNumber;
        private int emtAssignmentID;
        private int assignEmployeeID;
        private string assignEmployeeName;

        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public string StoreName { get { return storeName; } set { storeName = value; } }
        public int StoreNumber { get { return storeNumber; } set { storeNumber = value; } }
        public string DateTimeRequested { get { return dateTimeRequested; } set { dateTimeRequested = value; } }
        public string Requestor { get { return requestor; } set { requestor = value; } }
        public DateTime DateTimeSaved { get { return dateTimeSaved; } set { dateTimeSaved = value; } }
        public string DateTimeCompleted { get { return dateTimeCompleted; } set { dateTimeCompleted = value; } }
        public string Remarks { get { return remarks; } set { remarks = value; } }

        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }

        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public int Numberoftransaction { get { return this.numberoftransaction; } set { this.numberoftransaction = value; } }
        public string TAT { get { return this.tat; } set { this.tat = value; } }
        public string TimeSpentStr { get { return timeSpentStr; } set { timeSpentStr = value; } }
        public float TotalTimeSpent { get { return this.totalTimeSpent; } set { this.totalTimeSpent = value; } }

        public string SLA { get { return this.sla; } set { this.sla = value; } }
        public DateTime DueDate { get { return this.dueDate; } set { this.dueDate = value; } }
        public string TicketNumber { get { return this.ticketNumber; } set { this.ticketNumber = value; } }
        public int EMTAssignmentID { get { return this.emtAssignmentID; } set { this.emtAssignmentID = value; } }
        public int AssignEmployeeID { get { return this.assignEmployeeID; } set { this.assignEmployeeID = value; } }
        public string AssignEmployeeName { get { return this.assignEmployeeName; } set { this.assignEmployeeName = value; } }

        public LogDetailEMT() { }
        public LogDetailEMT(int _activityTypeID,
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            DateTime _activityDate,

            int _activityID,
            string _activityDescription,

            string _storeName,
            int _storeNumber,
            string _dateTimeRequested,
            string _requestor,
            DateTime _dateTimeSaved,
            string _dateTimeCompleted,
            string _remarks,

            int _statusID,
            string _statusDescription,

            float _timeSpent,
            int _numberoftransaction,
            string _tat,
            string _timeSpentStr,
            float _totalTimeSpent,

            string _sla,
            DateTime _dueDate,
            string _ticketNumber,
            int _emtAssignmentID,
            int _assignEmployeeID,
            string _assignEmployeeName
            )
        {
            this.activityTypeID = _activityTypeID;
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;

            this.activityID = _activityID;
            this.activityDescription = _activityDescription;

            this.storeName = _storeName;
            this.storeNumber = _storeNumber;
            this.dateTimeRequested = _dateTimeRequested;
            this.requestor = _requestor;
            this.dateTimeSaved = _dateTimeSaved;
            this.dateTimeCompleted = _dateTimeCompleted;
            this.remarks = _remarks;

            this.statusID = _statusID;
            this.statusDescription = _statusDescription;

            this.timeSpent = _timeSpent;
            this.numberoftransaction = _numberoftransaction;
            this.tat = _tat;
            this.timeSpentStr = _timeSpentStr;
            this.totalTimeSpent = _totalTimeSpent;

            this.sla = _sla;
            this.dueDate = _dueDate;
            this.ticketNumber = _ticketNumber;
            this.emtAssignmentID = _emtAssignmentID;
            this.assignEmployeeID = _assignEmployeeID;
            this.assignEmployeeName = _assignEmployeeName;
        }
    }
}
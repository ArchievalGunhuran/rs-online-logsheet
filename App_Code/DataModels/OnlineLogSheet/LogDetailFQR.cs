﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailFQR
    {
        private int activityTypeID;
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private DateTime activityDate;

        private int activityID;
        private string activityDescription;

        private string state;
        private string storeName;
        private int storeNumber;
        private DateTime dateTimeRequested;
        private string requestedBy;
        private string requestMade;
        private string notesComment;

        private int statusID;
        private string statusDescription;

        private DateTime dateTimeSaved;
        private string dateTimeCompleted;
        private float timeSpent;
        private int numberoftransaction;
        private string closedWithin24Hrs; //Yes or No
        private string timeSpentStr;
        private float totalTimeSpent;

        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public string State { get { return state; } set { state = value; } }
        public string StoreName { get { return storeName; } set { storeName = value; } }
        public int StoreNumber { get { return storeNumber; } set { storeNumber = value; } }
        public DateTime DateTimeRequested { get { return dateTimeRequested; } set { dateTimeRequested = value; } }
        public string RequestedBy { get { return requestedBy; } set { requestedBy = value; } }
        public string RequestMade { get { return requestMade; } set { requestMade = value; } }
        public string NotesComment { get { return notesComment; } set { notesComment = value; } }

        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }

        public DateTime DateTimeSaved { get { return dateTimeSaved; } set { dateTimeSaved = value; } }
        public string DateTimeCompleted { get { return this.dateTimeCompleted; } set { this.dateTimeCompleted = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public int Numberoftransaction { get { return this.numberoftransaction; } set { this.numberoftransaction = value; } }
        public string ClosedWithin24Hrs { get { return this.closedWithin24Hrs; } set { this.closedWithin24Hrs = value; } }
        public string TimeSpentStr { get { return timeSpentStr; } set { timeSpentStr = value; } }
        public float TotalTimeSpent { get { return this.totalTimeSpent; } set { this.totalTimeSpent = value; } }

        public LogDetailFQR() { }
        public LogDetailFQR(int _activityTypeID,
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            DateTime _activityDate,

            int _activityID,
            string _activityDescription,

            string _state,
            string _storeName,
            int _storeNumber,
            DateTime _dateTimeRequested,
            string _requestedBy,
            string _requestMade,
            string _notesComment,

            int _statusID,
            string _statusDescription,

            DateTime _dateTimeSaved,
            string _dateTimeCompleted,
            float _timeSpent,
            int _numberoftransaction,
            string _closedWithin24Hrs,
            string _timeSpentStr,
            float _totalTimeSpent)
        {
            this.activityTypeID = _activityTypeID;
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;

            this.activityID = _activityID;
            this.activityDescription = _activityDescription;

            this.state = _state;
            this.storeName = _storeName;
            this.storeNumber = _storeNumber;
            this.dateTimeRequested = _dateTimeRequested;
            this.requestedBy = _requestedBy;
            this.requestMade = _requestMade;
            this.notesComment = _notesComment;

            this.statusID = _statusID;
            this.statusDescription = _statusDescription;

            this.dateTimeSaved = _dateTimeSaved;
            this.dateTimeCompleted = _dateTimeCompleted;
            this.timeSpent = _timeSpent;
            this.numberoftransaction = _numberoftransaction;
            this.closedWithin24Hrs = _closedWithin24Hrs;
            this.timeSpentStr = _timeSpentStr;
            this.totalTimeSpent = _totalTimeSpent;
        }
    }
}
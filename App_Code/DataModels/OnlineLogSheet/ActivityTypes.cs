﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ActivityTypes
/// </summary>
namespace DataModels
{
    public class ActivityTypes
    {

        private int activityTypeID;
        private string activityTypeDescription;
        private string userGroup;


        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public string ActivityTypeDescription { get { return this.activityTypeDescription; } set { this.activityTypeDescription = value; } }
        public string UserGroup { get { return this.userGroup; } set { this.userGroup = value; } }


        public ActivityTypes() { }
        public ActivityTypes(
            int _activityTypeID,
            string _activityTypeDescription,
            string _userGroup
        )
        {
            this.activityTypeID = _activityTypeID;
            this.activityTypeDescription = _activityTypeDescription;
            this.userGroup = _userGroup;

        }
    }
}
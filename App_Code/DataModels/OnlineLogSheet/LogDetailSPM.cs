﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailSPM
    {
        private int activityTypeID;
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private DateTime activityDate;

        private int activityID;
        private string activityDescription;

        private string storeName;
        private int storeNumber;
        private DateTime dateTimeRequested;
        private string requestor;
        private DateTime dateTimeCompleted;
        private string remarks;

        private int statusID;
        private string statusDescription;

        private float timeSpent;
        private string timeSpentStr;
        private int numberoftransaction;

        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public string StoreName { get { return storeName; } set { storeName = value; } }
        public int StoreNumber { get { return storeNumber; } set { storeNumber = value; } }
        public DateTime DateTimeRequested { get { return dateTimeRequested; } set { dateTimeRequested = value; } }
        public string Requestor { get { return requestor; } set { requestor = value; } }
        public DateTime DateTimeCompleted { get { return dateTimeCompleted; } set { dateTimeCompleted = value; } }
        public string Remarks { get { return this.remarks; } set { this.remarks = value; } }

        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }

        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public int Numberoftransaction { get { return this.numberoftransaction; } set { this.numberoftransaction = value; } }
        public string TimeSpentStr { get { return this.timeSpentStr; } set { this.timeSpentStr = value; } }

        public LogDetailSPM() { }
        public LogDetailSPM(int _activityTypeID,
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            DateTime _activityDate,

            int _activityID,
            string _activityDescription,

            string _storeName,
            int _storeNumber,
            DateTime _dateTimeRequested,
            string _requestor,
            DateTime _dateTimeCompleted,
            string _remarks,
            
            int _statusID,
            string _statusDescription,

            float _timeSpent,
            string _timeSpentStr,
            int _numberoftransaction)
        {
            this.activityTypeID = _activityTypeID;
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;

            this.activityID = _activityID;
            this.activityDescription = _activityDescription;

            this.storeName = _storeName;
            this.storeNumber = _storeNumber;
            this.dateTimeRequested = _dateTimeRequested;
            this.requestor = _requestor;
            this.dateTimeCompleted = _dateTimeCompleted;
            this.remarks = _remarks;

            this.statusID = _statusID;
            this.statusDescription = _statusDescription;

            this.timeSpent = _timeSpent;
            this.timeSpentStr = _timeSpentStr;
            this.numberoftransaction = _numberoftransaction;
        }
    }
}
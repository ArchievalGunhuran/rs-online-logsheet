﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailAlarm
    {
        private int logDetailID;
        private int activityLogID;
        private int activityID;
        private string activityDescription;
        private float timeSpent;
        private DateTime activityDate;
        private DateTime startTime;
        private DateTime endTime;
        private int numberOfTransaction;
        private int employeeID;
        //private DataModels.Lists taskList;
        //private DataModels.Lists statusList;
        private int statusID;
        private string statusDescription;
        private int activityTypeID;
        private string timeSpentStr;
        private string remarks;

        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }
        public DateTime StartTime { get { return this.startTime; } set { this.startTime = value; } }
        public DateTime EndTime { get { return this.endTime; } set { this.endTime = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public int NumberOfTransaction { get { return this.numberOfTransaction; } set { this.numberOfTransaction = value; } }
        //public DataModels.Lists TaskList { get { return this.taskList; } set { this.taskList = value; } }
        //public DataModels.Lists StatusList { get { return this.statusList; } set { this.statusList = value; } }
        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }
        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public string TimeSpentStr { get { return this.timeSpentStr; } set { this.timeSpentStr = value; } }
        public string Remarks { get { return this.remarks; } set { this.remarks = value; } }

        public LogDetailAlarm() { }
        public LogDetailAlarm(
            int _logDetailID
            , int _activityLogID
            , int _activityID
            , string _activityDescription
            , float _timeSpent
            , DateTime _activityDate
            , DateTime _startTime
            , DateTime _endTime
            , int _numberoftransaction
            , int _employeeID
            , int _statusID
            , string _statusDescription
            , int _activityTypeID
            , string _timeSpentStr
            , string _remarks)
        {
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.activityID = _activityID;
            this.activityDescription = _activityDescription;
            this.timeSpent = _timeSpent;
            this.activityDate = _activityDate;
            this.startTime = _startTime;
            this.endTime = _endTime;
            this.numberOfTransaction = _numberoftransaction;
            this.employeeID = _employeeID;
            this.statusID = _statusID;
            this.statusDescription = _statusDescription;
            this.activityTypeID = _activityTypeID;
            this.timeSpentStr = _timeSpentStr;
            this.remarks = _remarks;
        }
    }
}
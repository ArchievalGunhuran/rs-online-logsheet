﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{

    public class ActivityLevelAssignment
    {
        private int alAssignID;
        private int activitySTID;
        private DateTime effectiveDate;
        private int employeeID;
        private string employeeName;
        private bool isActive;
        private int userEmployeeID;

        public int ALAssignID { get { return this.alAssignID; } set { this.alAssignID = value; } }
        public int ActivitySTID { get { return this.activitySTID; } set { this.activitySTID = value; } }
        public DateTime EffectiveDate { get { return this.effectiveDate; } set { this.effectiveDate = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public string EmployeeName { get { return this.employeeName; } set { this.employeeName = value; } }
        public bool IsActive { get { return this.isActive; } set { this.isActive = value; } }
        public int UserEmployeeID { get { return this.userEmployeeID; } set { this.userEmployeeID = value; } }

        public ActivityLevelAssignment() { }
        public ActivityLevelAssignment(
            int _alAssignID
            , int _activitySTID
            , DateTime _effectiveDate
            , int _employeeID
            , string _employeeName
            , bool _isActive
            , int _userEmployeeID)
        {
            this.alAssignID = _alAssignID;
            this.activitySTID = _activitySTID;
            this.effectiveDate = _effectiveDate;
            this.employeeID = _employeeID;
            this.employeeName = _employeeName;
            this.isActive = _isActive;
            this.userEmployeeID = _userEmployeeID;
        }
    }
}
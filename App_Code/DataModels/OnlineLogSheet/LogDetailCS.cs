﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailCS
    {
        private int logDetailID;
        private int activityLogID;
        private int activityID;
        private string activityDescription;
        private string storeName;
        private string storeNumber;
        private string ticketNumber;
        private float timeSpent;
        private string totalTimeSpent;
        private DateTime activityDate;
        private DateTime dueDate;
        private int employeeID;
        private int activityTypeID;
        //private DataModels.Lists programType;
        //private DataModels.Lists statusList;
        //private DataModels.Lists employeeList;
        private int csAssignmentID;
        private int statusID;
        private string statusDescription;
        private int assignEmployeeID;
        private string assignEmployeeName;

        private string dateCompleted;
        private string sla;

        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }
        public string StoreName { get { return this.storeName; } set { this.storeName = value; } }
        public string TicketNumber { get { return this.ticketNumber; } set { this.ticketNumber = value; } }
        public string StoreNumber { get { return this.storeNumber; } set { this.storeNumber = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public string TotalTimeSpent { get { return this.totalTimeSpent; } set { this.totalTimeSpent = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }
        public DateTime DueDate { get { return this.dueDate; } set { this.dueDate = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        //public DataModels.Lists ProgramType { get { return this.programType; } set { this.programType = value; } }
        //public DataModels.Lists StatusList { get { return this.statusList; } set { this.statusList = value; } }
        //public DataModels.Lists EmployeeList { get { return this.employeeList; } set { this.employeeList = value; } }
        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int CSAssignmentID { get { return this.csAssignmentID; } set { this.csAssignmentID = value; } }
        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }
        public int AssignEmployeeID { get { return this.assignEmployeeID; } set { this.assignEmployeeID = value; } }
        public string AssignEmployeeName { get { return this.assignEmployeeName; } set { this.assignEmployeeName = value; } }

        public string DateCompleted { get { return this.dateCompleted; } set { this.dateCompleted = value; } }
        public string SLA { get { return this.sla; } set { this.sla = value; } }

        public LogDetailCS() { }
        public LogDetailCS(
            int _logDetailID
            , int _activityLogID
            , int _activityID
            , string _activityDescription
            , string _storeName
            , string _storeNumber
            , string _ticketNumberks
            , float _timeSpent
            , string _totalTimeSpent
            , DateTime _activityDate
            , DateTime __dueDate
            , int _employeeID
            , int _activityTypeID
            , int _csAssignment
            , int _statusID
            , string _statusDescription
            , int _assignEmployeeID
            , string _assignEmployeeName
            , string _dateCompleted
            , string _sla
            )
        {
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.activityID = _activityID;
            this.activityDescription = _activityDescription;
            this.storeName = _storeName;
            this.storeNumber = _storeNumber;
            this.TicketNumber = _ticketNumberks;
            this.timeSpent = _timeSpent;
            this.totalTimeSpent = _totalTimeSpent;
            this.activityDate = _activityDate;
            this.dueDate = __dueDate;
            this.employeeID = _employeeID;
            //this.ProgramType = _programType;
            //this.statusList = _statusList;
            //this.EmployeeList = _employeeList;
            this.activityTypeID = _activityTypeID;
            this.csAssignmentID = _csAssignment;
            this.statusID = _statusID;
            this.statusDescription = _statusDescription;
            this.assignEmployeeID = _assignEmployeeID;
            this.assignEmployeeName = _assignEmployeeName;
            this.dateCompleted = _dateCompleted;
            this.sla = _sla;
        }
    }
}
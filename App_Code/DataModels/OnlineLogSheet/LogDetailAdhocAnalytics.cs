﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailAdhocAnalytics
    {
        private int activityTypeID;
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private DateTime activityDate;

        private int activityID;
        private string activityDescription;

        private int statusID;
        private string statusDescription;

        private string ticketNumber;
        private int priorityListID;
        private string priorityListDescription;
        private string taskDescription;
        private string requestedBy;
        private DateTime dateReceived;
        private DateTime dueDate;
        private string dateCompleted;
        private int numberOfTransaction;
        private float timeSpent;
        private string sla;

        private int aaAssignmentID;
        private int assignEmployeeID;
        private string assignEmployeeName;


        private string emailAddressofRequestor;
        private float timespentofProcessor;

        private int qabyEmployeeID;
        private string qaByEmployeeName;
        private int noOfDaysDelayed;
        private float timeliness;
        private int correctData;
        private int meetTheCriteria;
        private float accuracy;
        private float totalQAScore;
        private float timeSpentofQA;
        private string qaStatus;




        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }

        public string TicketNumber { get { return this.ticketNumber; } set { this.ticketNumber = value; } } 
        public int PriorityListID { get { return this.priorityListID; } set { this.priorityListID = value; } } 
        public string PriorityListDescription { get { return this.priorityListDescription; } set { this.priorityListDescription = value; } }
        public string TaskDescription { get { return this.taskDescription; } set { this.taskDescription = value; } }
        public string RequestedBy { get { return this.requestedBy; } set { this.requestedBy = value; } }
        public DateTime DateReceived { get { return this.dateReceived; } set { this.dateReceived = value; } }
        public DateTime DueDate { get { return this.dueDate; } set { this.dueDate = value; } }
        public string DateCompleted { get { return this.dateCompleted; } set { this.dateCompleted = value; } }
        public int NumberOfTransaction { get { return this.numberOfTransaction; } set { this.numberOfTransaction = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public string SLA { get { return this.sla; } set { this.sla = value; } }

        public int AAAssignmentID { get { return this.aaAssignmentID; } set { this.aaAssignmentID = value; } }
        public int AssignEmployeeID { get { return this.assignEmployeeID; } set { this.assignEmployeeID = value; } }
        public string AssignEmployeeName { get { return this.assignEmployeeName; } set { this.assignEmployeeName = value; } }

        public string EmailAddressofRequestor { get { return this.emailAddressofRequestor; } set { this.emailAddressofRequestor = value; } }
        public float TimeSpentofProcessor { get { return this.timespentofProcessor; } set { this.timespentofProcessor = value; } }


        public int QAByEmployeeID { get { return this.qabyEmployeeID; } set { this.qabyEmployeeID = value; } }
        public string QAByEmployeeName { get { return this.qaByEmployeeName; } set { this.qaByEmployeeName = value; } }
        public int NoOfDaysDelayed { get { return this.noOfDaysDelayed; } set { this.noOfDaysDelayed = value; } }
        public float Timeliness { get { return this.timeliness; } set { this.timeliness = value; } }
        public int CorrectData { get { return this.correctData; } set { this.correctData = value; } }
        public int MeetTheCriteria { get { return this.meetTheCriteria; } set { this.meetTheCriteria = value; } }
        public float Accuracy { get { return this.accuracy; } set { this.accuracy = value; } }
        public float TotalQAScore { get { return this.totalQAScore; } set { this.totalQAScore = value; } }
        public float TimeSpentOfQA { get { return this.timeSpentofQA; } set { this.timeSpentofQA = value; } }
        public string QAStatus { get { return this.qaStatus; } set { this.qaStatus = value; } }

        public LogDetailAdhocAnalytics() { }
        public LogDetailAdhocAnalytics(int _activityTypeID,
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            DateTime _activityDate,

            int _activityID,
            string _activityDescription,

            int _statusID,
            string _statusDescription,

            string _ticketNumber,
            int _priorityListID,
            string _priorityListDescription,
            string _taskDescription,
            string _requestedBy,
            DateTime _dateReceived,
            DateTime _dueDate,
            string _dateCompleted,
            int _numberOfTransaction,
            float _timeSpent,
            string _sla,
            int _aaAssignmentID
            , int _assignEmployeeID
            , string _assignEmployeeName

            , string _emailAddressofProcessor
            , float _timeSpentofProcessor

            , int _qaByEmployeeID
            , string _qaByEmployeeName
            , int _noOfDaysDelayed
            , float _timeliness
            , int _correctData
            , int _meetTheCriteria
            , float _accuracy
            , float _totalQAScore
            , float _timeSpentQA
            , string _qaStatus


            )
        {
            this.activityTypeID = _activityTypeID;
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;

            this.activityID = _activityID;
            this.activityDescription = _activityDescription;
            this.statusID = _statusID;
            this.statusDescription = _statusDescription;

            this.ticketNumber = _ticketNumber;
            this.priorityListID = _priorityListID;
            this.priorityListDescription = _priorityListDescription;
            this.taskDescription = _taskDescription;
            this.requestedBy = _requestedBy;
            this.dateReceived = _dateReceived;
            this.dueDate = _dueDate;
            this.dateCompleted = _dateCompleted;
            this.numberOfTransaction = _numberOfTransaction;
            this.timeSpent = _timeSpent;
            this.sla = _sla;
            this.aaAssignmentID = _aaAssignmentID;
            this.assignEmployeeID = _assignEmployeeID;
            this.assignEmployeeName = _assignEmployeeName;

            this.emailAddressofRequestor = _emailAddressofProcessor;
            this.timespentofProcessor = _timeSpentofProcessor;

            this.qabyEmployeeID = _qaByEmployeeID;
            this.qaByEmployeeName = _qaByEmployeeName;
            this.noOfDaysDelayed = _noOfDaysDelayed;
            this.timeliness = _timeliness;
            this.correctData = _correctData;
            this.meetTheCriteria = _meetTheCriteria;
            this.accuracy = _accuracy;
            this.totalQAScore = _totalQAScore;
            this.timeSpentofQA = _timeSpentQA;
            this.qaStatus = _qaStatus;
        }
    }

    public class LogDetailDailyUploads
    {
        private int activityTypeID;
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private DateTime activityDate;

        private int activityID;
        private string activityDescription;

        private int statusID;
        private string statusDescription;

        private string ticketNumber;
        private int priorityListID;
        private string priorityListDescription;
        private string taskDescription;
        private string requestedBy;
        private DateTime dateReceived;
        private DateTime dueDate;
        private string dateCompleted;
        private int numberOfTransaction;
        private float timeSpent;
        private string sla;

        private int duAssignmentID;
        private int assignEmployeeID;
        private string assignEmployeeName;


        private string emailAddressofRequestor;
        private float timespentofProcessor;

        private int qabyEmployeeID;
        private string qaByEmployeeName;
        private int noOfDaysDelayed;
        private float timeliness;
        private int correctData;
        private int meetTheCriteria;
        private float accuracy;
        private float totalQAScore;
        private float timeSpentofQA;
        private string qaStatus;




        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }

        public string TicketNumber { get { return this.ticketNumber; } set { this.ticketNumber = value; } }
        public int PriorityListID { get { return this.priorityListID; } set { this.priorityListID = value; } }
        public string PriorityListDescription { get { return this.priorityListDescription; } set { this.priorityListDescription = value; } }
        public string TaskDescription { get { return this.taskDescription; } set { this.taskDescription = value; } }
        public string RequestedBy { get { return this.requestedBy; } set { this.requestedBy = value; } }
        public DateTime DateReceived { get { return this.dateReceived; } set { this.dateReceived = value; } }
        public DateTime DueDate { get { return this.dueDate; } set { this.dueDate = value; } }
        public string DateCompleted { get { return this.dateCompleted; } set { this.dateCompleted = value; } }
        public int NumberOfTransaction { get { return this.numberOfTransaction; } set { this.numberOfTransaction = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public string SLA { get { return this.sla; } set { this.sla = value; } }

        public int DUAssignmentID { get { return this.duAssignmentID; } set { this.duAssignmentID = value; } }
        public int AssignEmployeeID { get { return this.assignEmployeeID; } set { this.assignEmployeeID = value; } }
        public string AssignEmployeeName { get { return this.assignEmployeeName; } set { this.assignEmployeeName = value; } }

        public string EmailAddressofRequestor { get { return this.emailAddressofRequestor; } set { this.emailAddressofRequestor = value; } }
        public float TimeSpentofProcessor { get { return this.timespentofProcessor; } set { this.timespentofProcessor = value; } }


        public int QAByEmployeeID { get { return this.qabyEmployeeID; } set { this.qabyEmployeeID = value; } }
        public string QAByEmployeeName { get { return this.qaByEmployeeName; } set { this.qaByEmployeeName = value; } }
        public int NoOfDaysDelayed { get { return this.noOfDaysDelayed; } set { this.noOfDaysDelayed = value; } }
        public float Timeliness { get { return this.timeliness; } set { this.timeliness = value; } }
        public int CorrectData { get { return this.correctData; } set { this.correctData = value; } }
        public int MeetTheCriteria { get { return this.meetTheCriteria; } set { this.meetTheCriteria = value; } }
        public float Accuracy { get { return this.accuracy; } set { this.accuracy = value; } }
        public float TotalQAScore { get { return this.totalQAScore; } set { this.totalQAScore = value; } }
        public float TimeSpentOfQA { get { return this.timeSpentofQA; } set { this.timeSpentofQA = value; } }
        public string QAStatus { get { return this.qaStatus; } set { this.qaStatus = value; } }

        public LogDetailDailyUploads() { }
        public LogDetailDailyUploads(int _activityTypeID,
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            DateTime _activityDate,

            int _activityID,
            string _activityDescription,

            int _statusID,
            string _statusDescription,

            string _ticketNumber,
            int _priorityListID,
            string _priorityListDescription,
            string _taskDescription,
            string _requestedBy,
            DateTime _dateReceived,
            DateTime _dueDate,
            string _dateCompleted,
            int _numberOfTransaction,
            float _timeSpent,
            string _sla,
            int _duAssignmentID
            , int _assignEmployeeID
            , string _assignEmployeeName

            , string _emailAddressofProcessor
            , float _timeSpentofProcessor

            , int _qaByEmployeeID
            , string _qaByEmployeeName
            , int _noOfDaysDelayed
            , float _timeliness
            , int _correctData
            , int _meetTheCriteria
            , float _accuracy
            , float _totalQAScore
            , float _timeSpentQA
            , string _qaStatus


            )
        {
            this.activityTypeID = _activityTypeID;
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;

            this.activityID = _activityID;
            this.activityDescription = _activityDescription;
            this.statusID = _statusID;
            this.statusDescription = _statusDescription;

            this.ticketNumber = _ticketNumber;
            this.priorityListID = _priorityListID;
            this.priorityListDescription = _priorityListDescription;
            this.taskDescription = _taskDescription;
            this.requestedBy = _requestedBy;
            this.dateReceived = _dateReceived;
            this.dueDate = _dueDate;
            this.dateCompleted = _dateCompleted;
            this.numberOfTransaction = _numberOfTransaction;
            this.timeSpent = _timeSpent;
            this.sla = _sla;
            this.duAssignmentID = _duAssignmentID;
            this.assignEmployeeID = _assignEmployeeID;
            this.assignEmployeeName = _assignEmployeeName;

            this.emailAddressofRequestor = _emailAddressofProcessor;
            this.timespentofProcessor = _timeSpentofProcessor;

            this.qabyEmployeeID = _qaByEmployeeID;
            this.qaByEmployeeName = _qaByEmployeeName;
            this.noOfDaysDelayed = _noOfDaysDelayed;
            this.timeliness = _timeliness;
            this.correctData = _correctData;
            this.meetTheCriteria = _meetTheCriteria;
            this.accuracy = _accuracy;
            this.totalQAScore = _totalQAScore;
            this.timeSpentofQA = _timeSpentQA;
            this.qaStatus = _qaStatus;
        }
    }
}
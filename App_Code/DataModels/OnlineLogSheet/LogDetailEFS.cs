﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailEFS
    {
        private int activityTypeID;
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private DateTime activityDate;

        private int activityID;
        private string activityDescription;

        private string storeName;
        private DateTime dateTimeRequested;
        private string requestor;
        private int workOrderNumber;
        private string requestMade;
        private DateTime dateTimeCompleted;
        private string remarks;

        private int statusID;
        private string statusDescription;

        private float timeSpent;
        private string timeSpentStr;
        private string followUpBy;
        private string followUpDateTime;
        private string followUpStatusList;
        private string datetimeToFollowUp;

		private string targetDateTime;
        private string sla;
		
        private string fullName;

        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public string StoreName { get { return storeName; } set { storeName = value; } }
        public DateTime DateTimeRequested { get { return dateTimeRequested; } set { dateTimeRequested = value; } }
        public string Requestor { get { return requestor; } set { requestor = value; } }
        public int WorkOrderNumber { get { return workOrderNumber; } set { workOrderNumber = value; } }
        public string RequestMade { get { return requestMade; } set { requestMade = value; } }
        public DateTime DateTimeCompleted { get { return dateTimeCompleted; } set { dateTimeCompleted = value; } }
        public string Remarks { get { return remarks; } set { remarks = value; } }

        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }

        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public string TimeSpentStr { get { return timeSpentStr; } set { timeSpentStr = value; } }
        public string FollowUpBy { get { return this.followUpBy; } set { this.followUpBy = value; } }
        public string FollowUpDateTime { get { return this.followUpDateTime; } set { this.followUpDateTime = value; } }
        public string FollowUpStatusList { get { return followUpStatusList; } set { followUpStatusList = value; } }
        public string DateTimeToFollowUp { get { return this.datetimeToFollowUp; } set { this.datetimeToFollowUp = value; } }
		
		public string TargetDateTime { get { return targetDateTime; } set { targetDateTime = value; } }
        public string SLA { get { return this.sla; } set { this.sla = value; } }

        public string FullName
        {
            get
            {
                return fullName;
            }

            set
            {
                fullName = value;
            }
        }

        public LogDetailEFS() { }
        public LogDetailEFS(int _activityTypeID,
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            DateTime _activityDate,

            int _activityID,
            string _activityDescription,

            string _storeName,
            DateTime _dateTimeRequested,
            string _requestor,
            int _workOrderNumber,
            string _requestMade,
            DateTime _dateTimeCompleted,
            string _remarks,

            int _statusID,
            string _statusDescription,

            float _timeSpent,
            string _timeSpentStr,
            string _followUpBy,
            string _followUpDateTime,
            string _followUpStatusList,
            string _datetimeToFollowUp,
			
			string _targetDateTime,
            string _sla
)
        {
            this.activityTypeID = _activityTypeID;
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;

            this.activityID = _activityID;
            this.activityDescription = _activityDescription;

            this.storeName = _storeName;
            this.dateTimeRequested = _dateTimeRequested;
            this.requestor = _requestor;
            this.workOrderNumber = _workOrderNumber;
            this.requestMade = _requestMade;
            this.dateTimeCompleted = _dateTimeCompleted;
            this.remarks = _remarks;

            this.statusID = _statusID;
            this.statusDescription = _statusDescription;

            this.timeSpent = _timeSpent;
            this.timeSpentStr = _timeSpentStr;
            this.followUpBy = _followUpBy;
            this.followUpDateTime = _followUpDateTime;
            this.followUpStatusList = _followUpStatusList;
            this.datetimeToFollowUp = _datetimeToFollowUp;
			
			this.targetDateTime = _targetDateTime;
            this.sla = _sla;
 
        }

        public LogDetailEFS(string fullName)
        {
            this.FullName = fullName;
        }
    }
}
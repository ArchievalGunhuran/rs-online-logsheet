﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ScheduleMaintenance
/// </summary>
namespace DataModels
{

    public class ScheduleMaintenance
    {

        private int scheduleID;
        private int employeeID;
        private string employeeName;
        private int scheduleMonthID;
        private string scheduleMonthName;
        private int scheduleYear;
        private float expectedWorkingDays;
        private float numberOfLeaves;
        private float unscheduledLeaves;
        private float tardiness;
        private string registered;
        private int registeredBy;
        private string updated;
        private int updatedBy;

        public int ScheduleID { get { return this.scheduleID; } set { this.scheduleID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public string EmployeeName { get { return this.employeeName; } set { this.employeeName = value; } }
        public int ScheduleMonthID { get { return this.scheduleMonthID; } set { this.scheduleMonthID = value; } }
        public string ScheduleMonthName { get { return this.scheduleMonthName; } set { this.scheduleMonthName = value; } }
        public int ScheduleYear { get { return this.scheduleYear; } set { this.scheduleYear = value; } }
        public float ExpectedWorkingDays { get { return this.expectedWorkingDays; } set { this.expectedWorkingDays = value; } }
        public float NumberOfLeaves { get { return this.numberOfLeaves; } set { this.numberOfLeaves = value; } }
        public float UnScheduledLeaves { get { return this.unscheduledLeaves; } set { this.unscheduledLeaves = value; } }
        public float Tardiness { get { return this.tardiness; } set { this.tardiness = value; } }
        public string Registered { get { return this.registered; } set { this.registered = value; } }
        public int RegisteredBy { get { return this.registeredBy; } set { this.registeredBy = value; } }
        public string Updated { get { return this.updated; } set { this.updated = value; } }
        public int UpdatedBy { get { return this.updatedBy; } set { this.updatedBy = value; } }

        public ScheduleMaintenance() { }
        public ScheduleMaintenance(
            int _scheduleID
            , int _employeeID
            , string _employeeName
            , int _scheduleMonthID
            , string _scheduleMonthName
            , int _scheduleYear
            , float _expectedWorkingDays
            , float _numberOfLeaves
            , float _unscheduledLeaves
            , float _tardiness
            , string _registered
            , int _registeredBy
            , string _updated
            , int _updatedBy)
        {
            this.scheduleID = _scheduleID;
            this.employeeID = _employeeID;
            this.employeeName = _employeeName;
            this.scheduleMonthID = _scheduleMonthID;
            this.scheduleMonthName = _scheduleMonthName;
            this.scheduleYear = _scheduleYear;
            this.expectedWorkingDays = _expectedWorkingDays;
            this.numberOfLeaves = _numberOfLeaves;
            this.unscheduledLeaves = _unscheduledLeaves;
            this.tardiness = _tardiness;
            this.registered = _registered;
            this.registeredBy = _registeredBy;
            this.updated = _updated;
            this.updatedBy = _updatedBy;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailProject
    {
        private int logDetailID;
        private int activityLogID;
        private int activityID;
        private string activityDescription;
        private string remarks;
        private float timeSpent;
        private DateTime activityDate;
        private int employeeID;
        //private DataModels.Lists projectList;
        //private DataModels.Lists statusList;
        private int statusID;
        private string statusDescription;
        private int activityTypeID;

        private float timeSpentofProcessor;
        private string projectNumber;
        private string sponsor;
        private string startDate;

        private string targetTestingDate;
        private string testingDate;
        private string jointTestingDate;
        private string targetDate;
        private string completionDate;
        private string acceptanceDate;

        private int noOfDaysDelayedST;
        private float firstPassTimelinessST;
        private int actionItemST;
        private int workingST;
        private float firstPassAccuracyST;

        private int noOfDaysDelayedPR;
        private float projectClosureTimelinessPR;
        private int actionItemPR;
        private int workingPR;
        private float projectClosureAccuracyPR;

        private float firstPass;
        private float projectClosure;
        private float totalQAScore;
        private float timeSpentOfQA;
        private float timeSpentOfQAProd;

        private string qaStatus;
        private string qaStatusProd;

        private int qabyEmployeeID;
        private string qaByEmployeeName;


        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }
        public string Remarks { get { return this.remarks; } set { this.remarks = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }
        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }

        
        public float TimeSpentofProcessor { get { return this.timeSpentofProcessor; } set { this.timeSpentofProcessor = value; } }
        public string ProjectNumber { get { return this.projectNumber; } set { this.projectNumber = value; } }
        public string Sponsor { get { return this.sponsor; } set { this.sponsor = value; } }
        public string StartDate { get { return this.startDate; } set { this.startDate = value; } }
        public string TargetTestingDate { get { return this.targetTestingDate; } set { this.targetTestingDate = value; } }
        public string TestingDate { get { return this.testingDate; } set { this.testingDate = value; } }
        public string JointTestingDate { get { return this.jointTestingDate; } set { this.jointTestingDate = value; } }
        public string TargetDate { get { return this.targetDate; } set { this.targetDate = value; } }
        public string CompletionDate { get { return this.completionDate; } set { this.completionDate = value; } }
        public string AcceptanceDate { get { return this.acceptanceDate; } set { this.acceptanceDate = value; } }



        public int NoOfDaysDelayedST { get { return this.noOfDaysDelayedST; } set { this.noOfDaysDelayedST = value; } }
        public float FirstPassTimelinessST { get { return this.firstPassTimelinessST; } set { this.firstPassTimelinessST = value; } }
        public int ActionItemST { get { return this.actionItemST; } set { this.actionItemST = value; } }
        public int WorkingST { get { return this.workingST; } set { this.workingST = value; } }
        public float FirstPassAccuracyST { get { return this.firstPassAccuracyST; } set { this.firstPassAccuracyST = value; } }


        public int NoOfDaysDelayedPR { get { return this.noOfDaysDelayedPR; } set { this.noOfDaysDelayedPR = value; } }
        public float ProjectClosureTimelinessPR { get { return this.projectClosureTimelinessPR; } set { this.projectClosureTimelinessPR = value; } }
        public int ActionItemPR { get { return this.actionItemPR; } set { this.actionItemPR = value; } }
        public int WorkingPR { get { return this.workingPR; } set { this.workingPR = value; } }
        public float ProjectClosureAccuracyPR { get { return this.projectClosureAccuracyPR; } set { this.projectClosureAccuracyPR = value; } }


        public float FirstPass { get { return this.firstPass; } set { this.firstPass = value; } }
        public float ProjectClosure { get { return this.projectClosure; } set { this.projectClosure = value; } }
        public float TotalQAScore { get { return this.totalQAScore; } set { this.totalQAScore = value; } }
        public float TimeSpentOfQA { get { return this.timeSpentOfQA; } set { this.timeSpentOfQA = value; } }
        public float TimeSpentOfQAProd { get { return this.timeSpentOfQAProd; } set { this.timeSpentOfQAProd = value; } }
        public string QAStatus { get { return this.qaStatus; } set { this.qaStatus = value; } }
        public string QAStatusProd { get { return this.qaStatusProd; } set { this.qaStatusProd = value; } }


        public int QAByEmployeeID { get { return this.qabyEmployeeID; } set { this.qabyEmployeeID = value; } }
        public string QAByEmployeeName { get { return this.qaByEmployeeName; } set { this.qaByEmployeeName = value; } }



        public LogDetailProject() { }
        public LogDetailProject(
            int _logDetailID
            , int _activityLogID
            , int _activityID
            , string _activityDescription
            , string _remarks
            , float _timeSpent
            , DateTime _activityDate
            , int _employeeID
            , int _statusID
            , string _statusDescription
            , int _activityTypeID

            , float _timeSpentofProcessor
            , string _projectNumber
            , string _sponsor
            , string _startDate

            , string _targetTestingDate
            , string _testingDate
            , string _jointTestingDate
            , string _targetDate
            , string _completionDate
            , string _acceptanceDate

            , int _noOfDaysDelayedST
            , float _firstPassTimelinessST
            , int _actionItemST
            , int _workingST
            , float _firstPassAccuracyST

            , int _noOfDaysDelayedPR
            , float _projectClosureTimelinessPR
            , int _actionItemPR
            , int _workingPR
            , float _projectClosureAccuracyPR


            , float _firstPass
            , float _projectClosure
            , float _totalQAScore
            , float _timeSpentOfQA
            , float _timeSpentOfQAProd
            , string _qaStatus
            , string _qaStatusProd

            , int _qabyEmployeeID
            , string _qaByEmployeeName



            )
        {
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.activityID = _activityID;
            this.activityDescription = _activityDescription;
            this.remarks = _remarks;
            this.timeSpent = _timeSpent;
            this.activityDate = _activityDate;
            this.employeeID = _employeeID;
            this.statusID = _statusID;
            this.statusDescription = _statusDescription;
            this.activityTypeID = _activityTypeID;


            this.timeSpentofProcessor = _timeSpentofProcessor;
            this.projectNumber = _projectNumber;
            this.sponsor = _sponsor;
            this.startDate = _startDate;

            this.targetTestingDate = _targetTestingDate;
            this.testingDate = _testingDate;
            this.jointTestingDate = _jointTestingDate;
            this.targetDate = _targetDate;
            this.completionDate = _completionDate;
            this.acceptanceDate = _acceptanceDate;

            this.noOfDaysDelayedST = _noOfDaysDelayedST;
            this.firstPassTimelinessST = _firstPassTimelinessST;
            this.actionItemST = _actionItemST;
            this.workingST = _workingST;
            this.firstPassAccuracyST = _firstPassAccuracyST;

            this.noOfDaysDelayedPR = _noOfDaysDelayedPR;
            this.projectClosureTimelinessPR = _projectClosureTimelinessPR;
            this.actionItemPR = _actionItemPR;
            this.workingPR = _workingPR;
            this.projectClosureAccuracyPR = _projectClosureAccuracyPR;


            this.firstPass = _firstPass;
            this.projectClosure = _projectClosure;
            this.totalQAScore = _totalQAScore;
            this.timeSpentOfQA = _timeSpentOfQA;
            this.timeSpentOfQAProd = _timeSpentOfQAProd;
            this.qaStatus = _qaStatus;
            this.qaStatusProd = _qaStatusProd;

            this.qabyEmployeeID = _qabyEmployeeID;
            this.qaByEmployeeName = _qaByEmployeeName;




        }
    }
}
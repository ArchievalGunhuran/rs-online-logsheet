﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class ActivityLogsParam
    {
        public int ActivityLogID { get; set; }
        public int LogDetailID { get; set; }
        public int ActivityTypeID { get; set; }
        public int EmployeeID { get; set; }
        public DateTime ActivityDate { get; set; }
        public int ActivityID { get; set; }
        public float TimeSpent { get; set; }
        public int NumberOfTransaction { get; set; }
        public string Remarks { get; set; }
        public string TimeStatus { get; set; }
        public string TimeSpentStr { get; set; }
    }

    public class LogDetailQADBCS
    {
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private string activityDate;

        private int activityTypeID;
        private string activityTypeDescription;
        private int activityID;
        private string activityDescription;

        private int numberOfTransaction;
        private string remarks;
        private string timeStatus;
        private string timeSpentStr;
        private float timeSpent;

        private string fullName;

        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public string ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public string ActivityTypeDescription { get { return this.activityTypeDescription; } set { this.activityTypeDescription = value; } }
        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public int NumberOfTransaction { get { return this.numberOfTransaction; } set { this.numberOfTransaction = value; } }
        public string Remarks { get { return this.remarks; } set { this.remarks = value; } }
        public string TimeStatus { get { return this.timeStatus; } set { this.timeStatus = value; } }
        public string TimeSpentStr { get { return this.timeSpentStr; } set { this.timeSpentStr = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }

        public string FullName { get { return this.fullName; } set { this.fullName = value; } }

        public LogDetailQADBCS() { }
        public LogDetailQADBCS(         
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            string _activityDate,
            int _activityTypeID,
            string _activityTypeDescription,
            int _activityID,
            string _activityDescription,
            int _numberOfTransaction,
            string _remarks,
            string _timeStatus,
            string _timeSpentStr,
            float _timeSpent,
            string _fullName)
        {
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;
            this.activityTypeID = _activityTypeID;
            this.activityTypeDescription = _activityTypeDescription;
            this.activityID = _activityID;
            this.activityDescription = _activityDescription;
            this.numberOfTransaction = _numberOfTransaction;
            this.remarks = _remarks;
            this.timeStatus = _timeStatus;
            this.timeSpentStr = _timeSpentStr;
            this.timeSpent = _timeSpent;
            this.fullName = _fullName;
        }
    }
}
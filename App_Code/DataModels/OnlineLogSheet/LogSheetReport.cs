﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    /// <summary>
    /// Summary description for LogSheetReport
    /// </summary>
    public class LogSheetReport
    {
        private int groupNumber;
        private int dateID;
        private double reportHours;
        private int reportVolume;
        private string labelDate;
        private string activityDescription;
        private double percentage;

        public int GroupNumber { get { return this.groupNumber; } set { this.groupNumber = value; } }
        public int DateID { get { return this.dateID; } set { this.dateID = value; } }
        public double ReportHours { get { return this.reportHours; } set { this.reportHours = value; } }
        public int ReportVolume { get { return this.reportVolume; } set { this.reportVolume = value; } }
        public string LabelDate { get { return this.labelDate; } set { this.labelDate = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }
        public double Percentage { get { return this.percentage; } set { this.percentage = value; } }

        public LogSheetReport() { }

        public LogSheetReport(
            int _groupNumber,
            int _dateID,
            double _reportHours,
            int _reportVolume,
            string _labelDate,
            string _activityDescription,
            double _percentage)
        {
            this.groupNumber = _groupNumber;
            this.dateID = _dateID;
            this.reportHours = _reportHours;
            this.reportVolume = _reportVolume;
            this.labelDate = _labelDate;
            this.activityDescription = _activityDescription;
            this.percentage = _percentage;
        }
    }
}
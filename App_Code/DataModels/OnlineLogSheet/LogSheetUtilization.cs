﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    /// <summary>
    /// Summary description for LogSheetReport
    /// </summary>
    public class LogSheetUtilization
    {
        private string employeeNames;
        private int calMonthNo;
        private string calMonthName;
        private float expectedWorkingDays;
        private float numberOfLeaves;
        private double efsActualTime;
        private double cssActualTime;
        private double alrActualTime;
        private double fqrActualTime;
        private double spmActualTime;
        private double emtActualTime;
        private double essActualTime;
        private double alarmActualTime;
        private double projectActualTime;
        private double adhocAnalyticsActualTime;
        private double toolSupportActualTime;
        private double trainingActualTime;
        private double nptActualTime;
        private double efsStandardTime;
        private double cssStandardTime;
        private double adminprodActualTime;

        public string EmployeeNames { get { return this.employeeNames; } set { this.employeeNames = value; } }
        public int CalMonthNo { get { return this.calMonthNo; } set { this.calMonthNo = value; } }
        public string CalMonthName { get { return this.calMonthName; } set { this.calMonthName = value; } }
        public float ExpectedWorkingDays { get { return this.expectedWorkingDays; } set { this.expectedWorkingDays = value; } }
        public float NumberOfLeaves { get { return this.numberOfLeaves; } set { this.numberOfLeaves = value; } }
        public double EFSActualTime { get { return this.efsActualTime; } set { this.efsActualTime = value; } }
        public double CSSActualTime { get { return this.cssActualTime; } set { this.cssActualTime = value; } }
        public double ALRActualTime { get { return this.alrActualTime; } set { this.alrActualTime = value; } }
        public double FQRActualTime { get { return this.fqrActualTime; } set { this.fqrActualTime = value; } }
        public double SPMActualTime { get { return this.spmActualTime; } set { this.spmActualTime = value; } }
        public double EMTActualTime { get { return this.emtActualTime; } set { this.emtActualTime = value; } }
        public double ESSActualTime { get { return this.essActualTime; } set { this.essActualTime = value; } }
        public double AlarmActualTime { get { return this.alarmActualTime; } set { this.alarmActualTime = value; } }
        public double ProjectActualTime { get { return this.projectActualTime; } set { this.projectActualTime = value; } }
        public double AdhocAnalyticsActualTime { get { return this.adhocAnalyticsActualTime; } set { this.adhocAnalyticsActualTime = value; } }
        public double ToolSupportActualTime { get { return this.toolSupportActualTime; } set { this.toolSupportActualTime = value; } }
        public double TrainingActualTime { get { return this.trainingActualTime; } set { this.trainingActualTime = value; } }
        public double NPTActualTime { get { return this.nptActualTime; } set { this.nptActualTime = value; } }
        public double EFSStandardTime { get { return this.efsStandardTime; } set { this.efsStandardTime = value; } }
        public double CSSStandardTime { get { return this.cssStandardTime; } set { this.cssStandardTime = value; } }
        public double AdminProdActualTime { get { return this.adminprodActualTime; } set { this.adminprodActualTime = value; } }

        public LogSheetUtilization() { }

        public LogSheetUtilization(
            string _employeeNames,
            int _calMonthNo,
            string _calMonthName,
            float _expectedWorkingDays,
            float _numberOfLeaves,
            double _efsActualTime,
            double _cssActualTime,
            double _alrActualTime,
            double _fqrActualTime,
            double _spmActualTime,
            double _emtActualTime,
            double _essActualTime,
            double _alarmActualTime,
            double _projectActualTime,
            double _adhocAnalyticsActualTime,
            double _toolSupportActualTime,
            double _trainingActualTime,
            double _nptActualTime,
            double _efsStandardTime,
            double _cssStandardTime,
            double _adminprodActualTime
            )
        {
            this.employeeNames = _employeeNames;
            this.calMonthNo = _calMonthNo;
            this.calMonthName = _calMonthName;
            this.expectedWorkingDays = _expectedWorkingDays;
            this.numberOfLeaves = _numberOfLeaves;
            this.efsActualTime = _efsActualTime;
            this.cssActualTime = _cssActualTime;
            this.alrActualTime = _alrActualTime;
            this.fqrActualTime = _fqrActualTime;
            this.spmActualTime = _spmActualTime;
            this.emtActualTime = _emtActualTime;
            this.essActualTime = _essActualTime;
            this.alarmActualTime = _alarmActualTime;
            this.projectActualTime = _projectActualTime;
            this.adhocAnalyticsActualTime = _adhocAnalyticsActualTime;
            this.toolSupportActualTime = _toolSupportActualTime;
            this.trainingActualTime = _trainingActualTime;
            this.nptActualTime = _nptActualTime;
            this.efsStandardTime = _efsStandardTime;
            this.cssStandardTime = _cssStandardTime;
            this.adminprodActualTime = _adminprodActualTime;
        }
    }
}
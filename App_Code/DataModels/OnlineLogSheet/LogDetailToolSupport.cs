﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailToolSupport
    {
        private int activityTypeID;
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private DateTime activityDate;

        private int activityID;
        private string activityDescription;

        private string ticketNumber;
        private string toolName;
        private string taskDescription;
        private string requestedBy;
        private DateTime dateReceived;
        private DateTime dueDate;
        private string dateCompleted;
        private int numberOfTransaction;

        private int statusID;
        private string statusDescription;

        private float timeSpent;
        private string sla;

        private int tsAssignmentID;
        private int assignEmployeeID;
        private string assignEmployeeName;

        private int assignedEmployeeID;
        private string assignedEmployeeName;
        private float timespentofProcessor;
        private int noOfDaysDelayed;
        private float timeliness;
        private int actioniItem;
        private int working;
        private float accuracy;
        private float totalQAScore;
        private float timeSpentofQA;
        private string qaStatus;

        private int qabyEmployeeID;
        private string qaByEmployeeName;

        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public string TicketNumber { get { return this.ticketNumber; } set { this.ticketNumber = value; } }
        public string ToolName { get { return this.toolName; } set { this.toolName = value; } }
        public string TaskDescription { get { return this.taskDescription; } set { this.taskDescription = value; } }
        public string RequestedBy { get { return this.requestedBy; } set { this.requestedBy = value; } }
        public DateTime DateReceived { get { return this.dateReceived; } set { this.dateReceived = value; } }
        public DateTime DueDate { get { return this.dueDate; } set { this.dueDate = value; } }
        public string DateCompleted { get { return this.dateCompleted; } set { this.dateCompleted = value; } }
        public int NumberOfTransaction { get { return this.numberOfTransaction; } set { this.numberOfTransaction = value; } }

        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }

        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public string SLA { get { return this.sla; } set { this.sla = value; } }

        public int TSAssignmentID { get { return this.tsAssignmentID; } set { this.tsAssignmentID = value; } }
        public int AssignEmployeeID { get { return this.assignEmployeeID; } set { this.assignEmployeeID = value; } }
        public string AssignEmployeeName { get { return this.assignEmployeeName; } set { this.assignEmployeeName = value; } }



        public int AssignedByEmployeeID { get { return this.assignedEmployeeID; } set { this.assignedEmployeeID = value; } }
        public string AssignedEmployeeName { get { return this.assignedEmployeeName; } set { this.assignedEmployeeName = value; } }
        public float TimeSpentofProcessor { get { return this.timespentofProcessor; } set { this.timespentofProcessor = value; } }
        public int NoOfDaysDelayed { get { return this.noOfDaysDelayed; } set { this.noOfDaysDelayed = value; } }
        public float Timeliness { get { return this.timeliness; } set { this.timeliness = value; } }
        public int ActionItem { get { return this.actioniItem; } set { this.actioniItem = value; } }
        public int Working { get { return this.working; } set { this.working = value; } }
        public float Accuracy { get { return this.accuracy; } set { this.accuracy = value; } }
        public float TotalQAScore { get { return this.totalQAScore; } set { this.totalQAScore = value; } }
        public float TimeSpentOfQA { get { return this.timeSpentofQA; } set { this.timeSpentofQA = value; } }
        public string QAStatus { get { return this.qaStatus; } set { this.qaStatus = value; } }

        public int QAByEmployeeID { get { return this.qabyEmployeeID; } set { this.qabyEmployeeID = value; } }
        public string QAByEmployeeName { get { return this.qaByEmployeeName; } set { this.qaByEmployeeName = value; } }

        public LogDetailToolSupport() { }
        public LogDetailToolSupport(int _activityTypeID,
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            DateTime _activityDate,

            DataModels.Lists _toolSupportType,
            int _activityID,
            string _activityDescription,

            string _ticketNumber,
            string _toolName,
            string _taskDescription,
            string _requestedBy,
            DateTime _dateReceived,
            DateTime _dueDate,
            string _dateCompleted,
            int _numberOfTransaction,

            int _statusID,
            string _statusDescription,

            float _timeSpent,
            string _sla,
            int _tsAssignmentID
            , int _assignEmployeeID
            , string _assignEmployeeName

            , int _qaByEmployeeID
            , string _qaByEmployeeName

            , int _assignedEmployeeID
            , string _assignedEmployeeName
            , float _timeSpentofProcessor
            , int _noOfDaysDelayed
            , float _timeliness
            , int _actionItem
            , int _working
            , float _accuracy
            , float _totalQAScore
            , float _timeSpentQA
            , string _qaStatus)
        {
            this.activityTypeID = _activityTypeID;
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;

            this.activityID = _activityID;
            this.activityDescription = _activityDescription;

            this.ticketNumber = _ticketNumber;
            this.toolName = _toolName;
            this.taskDescription = _taskDescription;
            this.requestedBy = _requestedBy;
            this.dateReceived = _dateReceived;
            this.dueDate = _dueDate;
            this.dateCompleted = _dateCompleted;
            this.numberOfTransaction = _numberOfTransaction;
            this.statusID = _statusID;
            this.statusDescription = _statusDescription;
            this.timeSpent = _timeSpent;
            this.sla = _sla;
            this.tsAssignmentID = _tsAssignmentID;
            this.assignEmployeeID = _assignEmployeeID;
            this.assignEmployeeName = _assignEmployeeName;

            this.assignedEmployeeID = _assignedEmployeeID;
            this.assignedEmployeeName = _assignedEmployeeName;
            this.timespentofProcessor = _timeSpentofProcessor;
            this.noOfDaysDelayed = _noOfDaysDelayed;
            this.timeliness = _timeliness;
            this.actioniItem = _actionItem;
            this.working = _working;
            this.accuracy = _accuracy;
            this.totalQAScore = _totalQAScore;
            this.timeSpentofQA = _timeSpentQA;
            this.qaStatus = _qaStatus;

            this.qabyEmployeeID = _qaByEmployeeID;
            this.qaByEmployeeName = _qaByEmployeeName;
        }
    }
}
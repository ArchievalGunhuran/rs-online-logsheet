﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class ActivityList
    {
        private int activityTypeID;
        private string activityTypeDescription;
        private int activityID;
        private string activityDescription;
        private int activitySTID;
        private float standardTime;
        private int employeeID;
        private bool isActive;
        private bool haveST;

        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public int ActivitySTID { get { return this.activitySTID; } set { this.activitySTID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }
        public string ActivityTypeDescription { get { return this.activityTypeDescription; } set { this.activityTypeDescription = value; } }
        public float StandardTime { get { return this.standardTime; } set { this.standardTime = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public bool IsActive { get { return this.isActive; } set { this.isActive = value; } }
        public bool HaveST { get { return this.haveST; } set { this.haveST = value; } }

        public ActivityList() { }
        public ActivityList(
            int _activityTypeID,
            int _activityID,
            int _activitySTID,
            string _activityDescription,
            string _activityTypeDescription,
            float _standardTime,
            int _employeeID,
            bool _isActive,
            bool _haveST)
        {
            this.activityTypeID = _activityTypeID;
            this.activityID = _activityID;
            this.activitySTID = _activitySTID;
            this.activityDescription = _activityDescription;
            this.activityTypeDescription = _activityTypeDescription;
            this.standardTime = _standardTime;
            this.employeeID = _employeeID;
            this.isActive = _isActive;
            this.haveST = _haveST;
        }
    }
}
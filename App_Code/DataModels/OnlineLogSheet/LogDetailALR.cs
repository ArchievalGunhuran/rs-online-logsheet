﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailALR
    {
        private int logDetailID;
        private int activityLogID;
        private int activityID;
        private string activityDescription;
        private float timeSpent;
        private DateTime activityDate;
        private int employeeID;
        private int statusID;
        private string statusDescription;
        private int activityTypeID;

        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }
        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }
        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }

        public LogDetailALR() { }
        public LogDetailALR(
            int _logDetailID
            , int _activityLogID
            , int _activityID
            , string _activityDescription
            , float _timeSpent
            , DateTime _activityDate
            , int _employeeID
            , int _statusID
            , string _statusDescription
            , int _activityTypeID)
        {
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.activityID = _activityID;
            this.activityDescription = _activityDescription;
            this.timeSpent = _timeSpent;
            this.activityDate = _activityDate;
            this.employeeID = _employeeID;
            this.statusID = _statusID;
            this.statusDescription = _statusDescription;
            this.activityTypeID = _activityTypeID;
        }
    }
}
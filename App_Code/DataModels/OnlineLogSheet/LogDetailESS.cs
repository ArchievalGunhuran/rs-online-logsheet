﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataModels
{
    public class LogDetailESS
    {
        private int activityTypeID;
        private int logDetailID;
        private int activityLogID;
        private int employeeID;
        private DateTime activityDate;

        private int activityID;
        private string activityDescription;

        private DateTime dateTimeProcessed;
        private DateTime dateTimeSaved;
        private DateTime dateTimeReceived;
        private string customerName;
        private string customerCompany;
        private string issue;
        private int ticketNumber;

        private int statusID;
        private string statusDescription;

        private float timeSpent;
        private string timeSpentStr;
        private string sla;

        public int ActivityTypeID { get { return this.activityTypeID; } set { this.activityTypeID = value; } }
        public int LogDetailID { get { return this.logDetailID; } set { this.logDetailID = value; } }
        public int ActivityLogID { get { return this.activityLogID; } set { this.activityLogID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public DateTime ActivityDate { get { return this.activityDate; } set { this.activityDate = value; } }

        public int ActivityID { get { return this.activityID; } set { this.activityID = value; } }
        public string ActivityDescription { get { return this.activityDescription; } set { this.activityDescription = value; } }

        public DateTime DateTimeProcessed { get { return this.dateTimeProcessed; } set { this.dateTimeProcessed = value; } }
        public DateTime DateTimeSaved { get { return this.dateTimeSaved; } set { this.dateTimeSaved = value; } }
        public DateTime DateTimeReceived { get { return this.dateTimeReceived; } set { this.dateTimeReceived = value; } }
        public string CustomerName { get { return this.customerName; } set { this.customerName = value; } }
        public string CustomerCompany { get { return this.customerCompany; } set { this.customerCompany = value; } }
        public string Issue { get { return this.issue; } set { this.issue = value; } }
        public int TicketNumber { get { return this.ticketNumber; } set { this.ticketNumber = value; } }

        public int StatusID { get { return this.statusID; } set { this.statusID = value; } }
        public string StatusDescription { get { return this.statusDescription; } set { this.statusDescription = value; } }

        public float TimeSpent { get { return this.timeSpent; } set { this.timeSpent = value; } }
        public string TimeSpentStr { get { return this.timeSpentStr; } set { this.timeSpentStr = value; } }
        public string SLA { get { return this.sla; } set { this.sla = value; } }

        public LogDetailESS() { }
        public LogDetailESS(int _activityTypeID,
            int _logDetailID,
            int _activityLogID,
            int _employeeID,
            DateTime _activityDate,

            int _activityID,
            string _activityDescription,
            
            DateTime _dateTimeProcessed,
            DateTime _dateTimeSaved,
            DateTime _dateTimeReceived,
            string _customerName,
            string _customerCompany,
            string _issue,
            int _ticketNumber,

            int _statusID,
            string _statusDescription,

            float _timeSpent,
            string _timeSpentStr,
            string _sla)
        {
            this.activityTypeID = _activityTypeID;
            this.logDetailID = _logDetailID;
            this.activityLogID = _activityLogID;
            this.employeeID = _employeeID;
            this.activityDate = _activityDate;

            this.activityID = _activityID;
            this.activityDescription = _activityDescription;

            this.dateTimeProcessed = _dateTimeProcessed;
            this.dateTimeSaved = _dateTimeSaved;
            this.dateTimeReceived = _dateTimeReceived;
            this.customerName = _customerName;
            this.customerCompany = _customerCompany;
            this.issue = _issue;
            this.ticketNumber = _ticketNumber;

            this.statusID = _statusID;
            this.statusDescription = _statusDescription;

            this.timeSpent = _timeSpent;
            this.timeSpentStr = _timeSpentStr;
            this.sla = _sla;

        }
    }
}
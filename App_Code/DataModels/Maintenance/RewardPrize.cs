﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RewardPrize
/// </summary>
/// 
namespace DataModels
{
    public class RewardPrize
    {

        private int prizeID;
        private string prizeName;
        private int starPrice;

        public int PrizeID { get { return this.prizeID; } set { this.prizeID = value; } }
        public string PrizeName { get { return this.prizeName; } set { this.prizeName = value; } }
        public int StarPrice { get { return this.starPrice; } set { this.starPrice = value; } }

        public RewardPrize() { }
        public RewardPrize(
            int _prizeID
            , string _prizeName
            , int _starPrice)
        {
            this.prizeID = _prizeID;
            this.prizeName = _prizeName;
            this.starPrice = _starPrice;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RewardList
/// </summary>
/// 
namespace DataModels
{
    public class RewardList
    {

        private int rewardTypeID;
        private string rewardName;
        private int equivalentStars;

        public int RewardTypeID { get { return this.rewardTypeID; } set { this.rewardTypeID = value; } }
        public string RewardName { get { return this.rewardName; } set { this.rewardName = value; } }
        public int EquivalentStars { get { return this.equivalentStars; } set { this.equivalentStars = value; } }

        public RewardList() { }
        public RewardList(
            int _rewardTypeID
            , string _rewardName
            , int _equivalentStars)
        {
            this.rewardTypeID = _rewardTypeID;
            this.rewardName = _rewardName;
            this.equivalentStars = _equivalentStars;
        }

    }
}
﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Employee
/// </summary>

namespace DataModels
{
    public class Employee
    {
        private int employeeID;
        private string firstname;
        private string lastname;
        private string dateHired;
        private string alarmCenter;
        private bool status;
        private string mobile;
        private string address;
        private string department;
        private int supervisorID;
        private string supervisorName;
        private string dateResigned;
        private string birthday;
        private string position;
        private string profilepicture;
        private int positionID;
        private int showBirthDateYear;

        private string course;
        private string college;

        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public string Firstname { get { return this.firstname; } set { this.firstname = value; } }
        public string DateHired { get { return this.dateHired; } set { this.dateHired = value; } }
        public string Lastname { get { return this.lastname; } set { this.lastname = value; } }
        public string AlarmCenter { get { return this.alarmCenter; } set { this.alarmCenter = value; } }
        public bool Status { get { return this.status; } set { this.status = value; } }
        public string Mobile { get { return this.mobile; } set { this.mobile = value; } }
        public string Address { get { return this.address; } set { this.address = value; } }
        public string Department { get { return this.department; } set { this.department = value; } }
        public int SupervisorID { get { return this.supervisorID; } set { this.supervisorID = value; } }
        public string SupervisorName { get { return this.supervisorName; } set { this.supervisorName = value; } }
        public string DateResigned { get { return this.dateResigned; } set { this.dateResigned = value; } }
        public string Birthday { get { return this.birthday; } set { this.birthday = value; } }
        public string Position { get { return this.position; } set { this.position = value; } }
        public string Profilepicture { get { return this.profilepicture; } set { this.profilepicture = value; } }
        public int PositionID { get { return this.positionID; } set { this.positionID = value; } }
        public int ShowBirthDateYear { get { return this.showBirthDateYear; } set { this.showBirthDateYear = value; } }

        public string Course { get { return this.course; } set { this.course = value; } }
        public string College { get { return this.college; } set { this.college = value; } }

        public Employee() { }
        public Employee
        (
            int _employeeID,
            string _firstname,
            string _lastname,
            string _dateHired,
            string _alarmCenter,
            bool _status
        )
        {
            this.EmployeeID = _employeeID;
            this.Firstname = _firstname;
            this.Lastname = _lastname;
            this.DateHired = _dateHired;
            this.AlarmCenter = _alarmCenter;
            this.Status = _status;
        }
    }
}
﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Accounts
/// </summary>
/// 
namespace DataModels
{
    public class Accounts
    {
        private int accountID;
        private string username;
        private int empID;
        private int roleID;
        private string emailAddress;
        private bool active;

        private string alarmCenter;

        private string employeeName;
        private string roleName;
        private string permissionName;
        private string password;
        private string logSheetUserGroup;

        public int AccountID { get { return this.accountID; } set { this.accountID = value; } }
        public string Username { get { return this.username; } set { this.username = value; } }
        public int EmpID { get { return this.empID; } set { this.empID = value; } }
        public int RoleID { get { return this.roleID; } set { this.roleID = value; } }
        public string EmailAddress { get { return this.emailAddress; } set { this.emailAddress = value; } }
        public bool Active { get { return this.active; } set { this.active = value; } }

        public string EmployeeName { get { return this.employeeName; } set { this.employeeName = value; } }
        public string RoleName { get { return this.roleName; } set { this.roleName = value; } }
        public string PermissionName { get { return this.permissionName; } set { this.permissionName = value; } }
        public string Password { get { return this.password; } set { this.password = value; } }

        public string AlarmCenter { get { return this.alarmCenter; } set { this.alarmCenter = value; } }
        public string LogSheetUserGroup { get { return this.logSheetUserGroup; } set { this.logSheetUserGroup = value; } }

        public Accounts() { }
        public Accounts
        (
            int _accountID,
            string _username,
            int _empID,
            int _roleID,
            string _emailAddress,
            bool _active,

            string _employeeName,
            string _roleName,
            string _permissionName,
            string _password,

            string _alarmCenter
            , string _logSheetUserGroup
        )
        {
            this.AccountID = _accountID;
            this.Username = _username;
            this.EmpID = _empID;
            this.RoleID = _roleID;
            this.EmailAddress = _emailAddress;
            this.Active = _active;

            this.EmployeeName = _employeeName;
            this.RoleName = _roleName;
            this.PermissionName = _permissionName;
            this.password = _password;

            this.AlarmCenter = _alarmCenter;
            this.logSheetUserGroup = _logSheetUserGroup;
        }
    }
}

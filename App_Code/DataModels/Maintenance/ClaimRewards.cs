﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClaimRewards
/// </summary>
/// 
namespace DataModels
{
    public class ClaimRewards
    {

        private int claimID;
        private int employeeID;
        private string employeeName;
        private int prizeID;
        private string prizeName;
        private int quantity;
        private int totalCost;
        private int starBalance;
        private string created;
        private int createdBy;

        public int ClaimID { get { return this.claimID; } set { this.claimID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public string EmployeeName { get { return this.employeeName; } set { this.employeeName = value; } }
        public int PrizeID { get { return this.prizeID; } set { this.prizeID = value; } }
        public string PrizeName { get { return this.prizeName; } set { this.prizeName = value; } }
        public int Quantity { get { return this.quantity; } set { this.quantity = value; } }
        public int TotalCost { get { return this.totalCost; } set { this.totalCost = value; } }
        public int StarBalance { get { return this.starBalance; } set { this.starBalance = value; } }
        public string Created { get { return this.created; } set { this.created = value; } }
        public int CreatedBy { get { return this.createdBy; } set { this.createdBy = value; } }

        public ClaimRewards() { }
        public ClaimRewards(
            int _claimID
            , int _employeeID
            , int _prizeID
            , int _quantity
            , int _totalCost
            , int _starBalance
            , string _created
            , int _createdBy)
        {
            this.claimID = _claimID;
            this.employeeID = _employeeID;
            this.prizeID = _prizeID;
            this.quantity = _quantity;
            this.totalCost = _totalCost;
            this.starBalance = _starBalance;
            this.created = _created;
            this.createdBy = _createdBy;
        }
    }
}
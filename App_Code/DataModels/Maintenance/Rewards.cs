﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for Rewards
/// </summary>
/// 
namespace DataModels
{
    public class Rewards
    {

        private int rewardID;
        private int employeeID;
        private string employeeName;
        private string rewardName;
        private int equivalentStars;
        private int rewardTypeID;
        private string remarks;
        private string dateAchieved;
        private string created;
        private int createdBy;
        private string updated;
        private int updatedBy;

        public int RewardID { get { return this.rewardID; } set { this.rewardID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public int EquivalentStars { get { return this.equivalentStars; } set { this.equivalentStars = value; } }
        public int RewardTypeID { get { return this.rewardTypeID; } set { this.rewardTypeID = value; } }
        public string Remarks { get { return this.remarks; } set { this.remarks = value; } }
        public string EmployeeName { get { return this.employeeName; } set { this.employeeName = value; } }
        public string RewardName { get { return this.rewardName; } set { this.rewardName = value; } }
        public string DateAchieved { get { return this.dateAchieved; } set { this.dateAchieved = value; } }
        public string Created { get { return this.created; } set { this.created = value; } }
        public int CreatedBy { get { return this.createdBy; } set { this.createdBy = value; } }
        public string Updated { get { return this.updated; } set { this.updated = value; } }
        public int UpdatedBy { get { return this.updatedBy; } set { this.updatedBy = value; } }

        public Rewards() { }
        public Rewards(
            int _rewardID
            , int _employeeID
            , int _rewardTypeID
            , string _remarks
            , string _dateAchieved
            , string _created
            , int _createdBy
            , string _updated
            , int _updatedBy)
        {
            this.rewardID = _rewardID;
            this.employeeID = _employeeID;
            this.rewardTypeID = _rewardTypeID;
            this.remarks = _remarks;
            this.dateAchieved = _dateAchieved;
            this.created = _created;
            this.createdBy = _createdBy;
            this.updated = _updated;
            this.updatedBy = _updatedBy;
        }
    }
}
﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Scorecards
/// </summary>
/// 
namespace DataModels
{
    public class Scorecards
    {
        private int scorecardID;
        private int employeeID;
        private int scoreYear;
        private int monthID;
        private float productivity;
        private float cfc;
        private float cfcsd;
        private float totalQA;
        private float qaWav;
        private float qaNotes;
        private float audit;
        private float ppl;
        private float pcc;
        private float sd;
        private float otCompliance;
        private float absenteeism;
        private float tardyOccurance;
        private float tardyHourIncurred;
        private float trainingHours;
        private float trainingPercentage;
        private int kaizen;
        private int voc;
        private string criticalIncident;
        private float wavAttribute1;
        private float tardyPercentage;

        private string employeeName;
        private string monthName;

        private string late;
        private string callOff;

        private int shiftID;
        private string shiftName;
        private float responseTime;
        private float teamResponseTime;

        //additional variables and properties for 2.0
        private float workedAlarms;
        private float answeredIn30;
        private float answeredIn40;
        private float attemptCount;
        private float answeredIn60;
        private float answeredMoreThan60;
        private float actualHC;
        private float forcastedHC;

        private float prodLess6;
        private float prodLessYear;
        private float prodMoreYear;

        private string queueGroup;

        public int ScorecardID { get { return this.scorecardID; } set { this.scorecardID = value; } }
        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public int ScoreYear { get { return this.scoreYear; } set { this.scoreYear = value; } }
        public int MonthID { get { return this.monthID; } set { this.monthID = value; } }
        public float Productivity { get { return this.productivity; } set { this.productivity = value; } }
        public float CFC { get { return this.cfc; } set { this.cfc = value; } }
        public float CFCSD { get { return this.cfcsd; } set { this.cfcsd = value; } }
        public float TotalQA { get { return this.totalQA; } set { this.totalQA = value; } }
        public float QAWav { get { return this.qaWav; } set { this.qaWav = value; } }
        public float QANotes { get { return this.qaNotes; } set { this.qaNotes = value; } }
        public float Audit { get { return this.audit; } set { this.audit = value; } }
        public float PPL { get { return this.ppl; } set { this.ppl = value; } }
        public float PCC { get { return this.pcc; } set { this.pcc = value; } }
        public float SD { get { return this.sd; } set { this.sd = value; } }
        public float OTCompliance { get { return this.otCompliance; } set { this.otCompliance = value; } }
        public float Absenteeism { get { return this.absenteeism; } set { this.absenteeism = value; } }
        public float TardyOccurance { get { return this.tardyOccurance; } set { this.tardyOccurance = value; } }
        public float TardyHourIncurred { get { return this.tardyHourIncurred; } set { this.tardyHourIncurred = value; } }
        public float TrainingHours { get { return this.trainingHours; } set { this.trainingHours = value; } }
        public float TrainingPercentage { get { return this.trainingPercentage; } set { this.trainingPercentage = value; } }
        public int Kaizen { get { return this.kaizen; } set { this.kaizen = value; } }
        public int VOC { get { return this.voc; } set { this.voc = value; } }
        public string CriticalIncident { get { return this.criticalIncident; } set { this.criticalIncident = value; } }
        public float WavAttribute1 { get { return this.wavAttribute1; } set { this.wavAttribute1 = value; } }
        public float TardyPercentage { get { return this.tardyPercentage; } set { this.tardyPercentage = value; } }

        public string EmployeeName { get { return this.employeeName; } set { this.employeeName = value; } }
        public string MonthName { get { return this.monthName; } set { this.monthName = value; } }

        public string Late { get { return this.late; } set { this.late = value; } }
        public string CallOff { get { return this.callOff; } set { this.callOff= value; } }

        public int ShiftID { get { return this.shiftID; } set { this.shiftID = value; } }
        public string ShiftName { get { return this.shiftName; } set { this.shiftName = value; } }
        public float ResponseTime { get { return this.responseTime; } set { this.responseTime = value; } }
        public float TeamResponseTime { get { return this.teamResponseTime; } set { this.teamResponseTime = value; } }

        public float WorkAlarms { get { return this.workedAlarms; } set { this.workedAlarms = value; } }
        public float AnsweredIn30 { get { return this.answeredIn30; } set { this.answeredIn30 = value; } }
        public float AnsweredIn40 { get { return this.answeredIn40; } set { this.answeredIn40 = value; } }
        public float AttemptCount { get { return this.attemptCount; } set { this.attemptCount = value; } }
        public float AnsweredIn60 { get { return this.answeredIn60; } set { this.answeredIn60 = value; } }
        public float AnsweredMoreThan60 { get { return this.answeredMoreThan60; } set { this.answeredMoreThan60 = value; } }
        public float ActualHC { get { return this.actualHC; } set { this.actualHC = value; } }
        public float ForcastedHC { get { return this.forcastedHC; } set { this.forcastedHC = value; } }

        public float ProdLess6 { get { return this.prodLess6; } set { this.prodLess6 = value; } }
        public float ProdLessYear { get { return this.prodLessYear; } set { this.prodLessYear = value; } }
        public float ProdMoreYear { get { return this.prodMoreYear; } set { this.prodMoreYear = value; } }

        public string QueueGroup { get { return this.queueGroup; } set { this.queueGroup = value; } }

        public Scorecards() { }
        public Scorecards(
            int _scorecardID
            , int _employeeID
            , int _scoreYear
            , int _monthID
            , float _productivity
            , float _cfc
            , float _cfcsd
            , float _totalQA
            , float _qaWav
            , float _qaNotes
            , float _audit
            , float _ppl
            , float _pcc
            , float _sd
            , float _otCompliance
            , float _absenteeism
            , float _tardyOccurance
            , float _tardyHourIncurred
            , float _trainingHours
            , float _trainingPercentage
            , int _kaizen
            , int _voc
            , string _criticalIncident
            , float _wavAttribute1
            , float _tardyPercentage

            , string _employeeName
            , string _monthName

            , string _late
            , string _callOff

            , int _shiftID
            , string _shiftName
            , float _responseTime
            , float _teamResponseTime
            )
        {

            this.scorecardID = _scorecardID;
            this.employeeID = _employeeID;
            this.scoreYear = _scoreYear;
            this.monthID = _monthID;
            this.productivity = _productivity;
            this.cfc = _cfc;
            this.cfcsd = _cfcsd;
            this.totalQA = _totalQA;
            this.qaWav = _qaWav;
            this.qaNotes = _qaNotes;
            this.audit = _audit;
            this.ppl = _ppl;
            this.pcc = _pcc;
            this.sd = _sd;
            this.otCompliance = _otCompliance;
            this.absenteeism = _absenteeism;
            this.tardyOccurance = _tardyOccurance;
            this.tardyHourIncurred = _tardyHourIncurred;
            this.trainingHours = _trainingHours;
            this.trainingPercentage = _trainingPercentage;
            this.kaizen = _kaizen;
            this.voc = _voc;
            this.criticalIncident = _criticalIncident;
            this.wavAttribute1 = _wavAttribute1;
            this.tardyPercentage = _tardyPercentage;

            this.EmployeeName = _employeeName;
            this.MonthName = _monthName;

            this.late = _late;
            this.callOff = _callOff;

            this.shiftID = _shiftID;
            this.shiftName = _shiftName;
            this.responseTime = _responseTime;
            this.teamResponseTime = _teamResponseTime;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Testimonials
/// </summary>
/// 
namespace DataModels
{
    public class Testimonials
    {

        private int employeeID;
        private int testimonialID;
        private int registeredBy;
        private int updatedBy;
        private string registeredByName;
        private string updatedByName;
        private string testimonial;
        private bool approved;

        private string registered;
        private string updated;
        private string registeredByPicture;


        public int EmployeeID { get { return this.employeeID; } set { this.employeeID = value; } }
        public int TestimonialID { get { return this.testimonialID; } set { this.testimonialID = value; } }
        public int RegisteredBy { get { return this.registeredBy; } set { this.registeredBy = value; } }
        public int UpdatedBy { get { return this.updatedBy; } set { this.updatedBy = value; } }
        public string RegisteredByName { get { return this.registeredByName; } set { this.registeredByName = value; } }
        public string UpdatedByName { get { return this.updatedByName; } set { this.updatedByName = value; } }
        public string Testimonial { get { return this.testimonial; } set { this.testimonial = value; } }
        public bool Approved { get { return this.approved; } set { this.approved = value; } }

        public string Registered { get { return this.registered; } set { this.registered = value; } }
        public string Updated { get { return this.updated; } set { this.updated = value; } }
        public string RegisteredByPicture { get { return this.registeredByPicture; } set { this.registeredByPicture = value; } }

        public Testimonials()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}
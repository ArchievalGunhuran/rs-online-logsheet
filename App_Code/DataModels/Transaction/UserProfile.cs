﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Lists
/// </summary>
/// 
namespace DataModels
{
    public class UserProfile
    {
        private int listID;
        private string strListID;
        private string listDescription;

        public int ListID { get { return this.listID; } set { this.listID = value; } }
        public string ListDescription { get { return this.listDescription; } set { this.listDescription = value; } }
        public string StrListID { get { return this.strListID; } set { this.strListID = value; } }

        public UserProfile() { }
        public UserProfile
        (
            int _listID,
            string _listDescription,
            string _strListID
        )
        {
            this.ListID = _listID;
            this.ListDescription = _listDescription;
            this.StrListID = strListID;
        }
    }
}
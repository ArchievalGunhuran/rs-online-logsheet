﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ScheduleMaintenance
/// </summary>
/// 
namespace Transactions
{
    public class ScheduleMaintenance
    {
        public ScheduleMaintenance()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        string[] MonthName = new string[] {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

        public ArrayList getScheduleMaintenance(DataModels.ScheduleMaintenance schedulemaintenance)
        {
            ArrayList schedulemaintenanceList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@ScheduleID", schedulemaintenance.ScheduleID));
            paramList.Add(new SqlParameter("@EmployeeID", schedulemaintenance.EmployeeID));
            paramList.Add(new SqlParameter("@ScheduleMonth", schedulemaintenance.ScheduleMonthID));
            paramList.Add(new SqlParameter("@ScheduleYear", schedulemaintenance.ScheduleYear));

            DataTable dt = cm.retrieveDataTableBySP("dbo.sp_LogSheet_Select_ScheduleMaintenanceQADB", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                schedulemaintenance = new DataModels.ScheduleMaintenance();

                schedulemaintenance.ScheduleID = int.Parse(dtu.extractStringValue(dr["ScheduleID"], DataTransformerUtility.DataType.PRIMARYKEY));
                schedulemaintenance.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.EmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                schedulemaintenance.ScheduleMonthID = int.Parse(dtu.extractStringValue(dr["ScheduleMonth"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.ScheduleMonthName = MonthName[schedulemaintenance.ScheduleMonthID - 1];
                schedulemaintenance.ScheduleYear = int.Parse(dtu.extractStringValue(dr["ScheduleYear"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.ExpectedWorkingDays = float.Parse(dtu.extractStringValue(dr["ExpectedWorkingDays"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.NumberOfLeaves = float.Parse(dtu.extractStringValue(dr["NumberOfLeaves"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.UnScheduledLeaves = float.Parse(dtu.extractStringValue(dr["UnScheduledLeaves"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.Tardiness = float.Parse(dtu.extractStringValue(dr["Tardiness"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenanceList.Add(schedulemaintenance);
            }

            return schedulemaintenanceList;
        }
        
        public ArrayList addScheduleMaintenance(DataModels.ScheduleMaintenance schedulemaintenance)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", schedulemaintenance.EmployeeID));
            paramList.Add(new SqlParameter("@ScheduleMonth", schedulemaintenance.ScheduleMonthID));
            paramList.Add(new SqlParameter("@ScheduleYear", schedulemaintenance.ScheduleYear));
            paramList.Add(new SqlParameter("@ExpectedWorkingDays", schedulemaintenance.ExpectedWorkingDays));
            paramList.Add(new SqlParameter("@NumberOfLeaves", schedulemaintenance.NumberOfLeaves));
            paramList.Add(new SqlParameter("@UnScheduledLeaves", schedulemaintenance.UnScheduledLeaves));
            paramList.Add(new SqlParameter("@Tardiness", schedulemaintenance.Tardiness));
            paramList.Add(new SqlParameter("@RegisteredBy", schedulemaintenance.RegisteredBy));

            list = getScheduleMaintenance(schedulemaintenance);

            if (list.Count > 0)
                throw new Exception("Record is alrady existing in the database.");

            DataTable dt = cm.retrieveDataTableBySP("dbo.sp_LogSheet_Insert_ScheduleMaintenance", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                schedulemaintenance = new DataModels.ScheduleMaintenance();

                schedulemaintenance.ScheduleID = int.Parse(dtu.extractStringValue(dr["ScheduleID"], DataTransformerUtility.DataType.PRIMARYKEY));
                schedulemaintenance.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.EmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                schedulemaintenance.ScheduleMonthID = int.Parse(dtu.extractStringValue(dr["ScheduleMonth"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.ScheduleMonthName = MonthName[schedulemaintenance.ScheduleMonthID - 1];
                schedulemaintenance.ScheduleYear = int.Parse(dtu.extractStringValue(dr["ScheduleYear"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.ExpectedWorkingDays = int.Parse(dtu.extractStringValue(dr["ExpectedWorkingDays"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.NumberOfLeaves = int.Parse(dtu.extractStringValue(dr["NumberOfLeaves"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.UnScheduledLeaves = float.Parse(dtu.extractStringValue(dr["UnScheduledLeaves"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.Tardiness = int.Parse(dtu.extractStringValue(dr["Tardiness"], DataTransformerUtility.DataType.NUMBER));
                al.Add(schedulemaintenance);
            }

            return al;
        }

        public ArrayList updateScheduleMaintenance(DataModels.ScheduleMaintenance schedulemaintenance)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ScheduleID", schedulemaintenance.ScheduleID));
            paramList.Add(new SqlParameter("@EmployeeID", schedulemaintenance.EmployeeID));
            paramList.Add(new SqlParameter("@ScheduleMonth", schedulemaintenance.ScheduleMonthID));
            paramList.Add(new SqlParameter("@ScheduleYear", schedulemaintenance.ScheduleYear));
            paramList.Add(new SqlParameter("@ExpectedWorkingDays", schedulemaintenance.ExpectedWorkingDays));
            paramList.Add(new SqlParameter("@NumberOfLeaves", schedulemaintenance.NumberOfLeaves));
            paramList.Add(new SqlParameter("@UnScheduledLeaves", schedulemaintenance.UnScheduledLeaves));
            paramList.Add(new SqlParameter("@Tardiness", schedulemaintenance.Tardiness));
            paramList.Add(new SqlParameter("@UpdatedBy", schedulemaintenance.UpdatedBy));

            list = getScheduleMaintenance(schedulemaintenance);

            if (list.Count > 0)
                throw new Exception("Record is alrady existing in the database.");

            DataTable dt = cm.retrieveDataTableBySP("dbo.sp_LogSheet_Update_ScheduleMaintenance", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                schedulemaintenance = new DataModels.ScheduleMaintenance();

                schedulemaintenance.ScheduleID = int.Parse(dtu.extractStringValue(dr["ScheduleID"], DataTransformerUtility.DataType.PRIMARYKEY));
                schedulemaintenance.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.EmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                schedulemaintenance.ScheduleMonthID = int.Parse(dtu.extractStringValue(dr["ScheduleMonth"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.ScheduleMonthName = MonthName[schedulemaintenance.ScheduleMonthID - 1];
                schedulemaintenance.ScheduleYear = int.Parse(dtu.extractStringValue(dr["ScheduleYear"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.ExpectedWorkingDays = int.Parse(dtu.extractStringValue(dr["ExpectedWorkingDays"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.NumberOfLeaves = int.Parse(dtu.extractStringValue(dr["NumberOfLeaves"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.UnScheduledLeaves = float.Parse(dtu.extractStringValue(dr["UnScheduledLeaves"], DataTransformerUtility.DataType.NUMBER));
                schedulemaintenance.Tardiness = int.Parse(dtu.extractStringValue(dr["Tardiness"], DataTransformerUtility.DataType.NUMBER));
                al.Add(schedulemaintenance);
            }

            return al;
        }

        public int deleteScheduleMaintenance(int scheduleid)
        {
            ArrayList schedulemaintenanceList = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ScheduleID", scheduleid));
            int result = (int)cm.executeCommandBySP("dbo.sp_LogSheet_Delete_ScheduleMaintenance", paramList, false);
            return result;
        }

    }
}
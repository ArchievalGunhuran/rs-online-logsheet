﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Exchange.WebServices.Data;
using System.DirectoryServices;
using System.Net;
using System.Net.Mail;

namespace Transactions
{
    public class ActivityLogs
    {
        public ActivityLogs()
        {
        }

        public ArrayList getActivityList(int activityTypeID, int employeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ActivityList", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }
        
        public ArrayList getItemList(int activityTypeID, int listTypeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activityTypeID));
            paramList.Add(new SqlParameter("@ListTypeID", listTypeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ItemList", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["ListItemID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["ListItemDescription"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }

        public ArrayList getEmployeeList()
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_TSEList", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }
        public ArrayList getEmployeeListAdhoc()
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_TSEListForAdhoc", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }
        public ArrayList getEmployeeListTS()
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_TSEListForTS", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }

        public long deleteActivityLog(int activityTypeID, int activitylogID)
        {
            long result=0;
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activityTypeID));
            paramList.Add(new SqlParameter("@ActivityLogID", activitylogID));
            cm.executeCommandBySP("sp_LogSheet_Delete_ActivityLog", paramList, false);
            //switch (activityTypeID)
            //{
            //    case 1: result = cm.executeCommandBySP("sp_LogSheet_Delete_AdminActivityLog", paramList, false); break;
            //    case 2: result = cm.executeCommandBySP("sp_LogSheet_Delete_AlarmActivityLog", paramList, false); break;
            //    case 3: result = cm.executeCommandBySP("sp_LogSheet_Delete_CSActivityLog", paramList, false); break;
            //    case 4: result = cm.executeCommandBySP("sp_LogSheet_Delete_ProjectActivityLog", paramList, false); break;
            //    case 5: result = cm.executeCommandBySP("sp_LogSheet_Delete_ALRActivityLog", paramList, false); break;
            //    case 6: result = cm.executeCommandBySP("sp_LogSheet_Delete_SPMActivityLog", paramList, false); break;
            //    case 7: result = cm.executeCommandBySP("sp_LogSheet_Delete_FQRActivityLog", paramList, false); break;
            //    case 8: result = cm.executeCommandBySP("sp_LogSheet_Delete_EMTActivityLog", paramList, false); break;
            //    case 9: result = cm.executeCommandBySP("sp_LogSheet_Delete_EFSActivityLog", paramList, false); break;
            //    case 10: result = cm.executeCommandBySP("sp_LogSheet_Delete_ESSActivityLog", paramList, false); break;
            //    case 11: result = cm.executeCommandBySP("sp_LogSheet_Delete_AAActivityLog", paramList, false); break;
            //    case 12: result = cm.executeCommandBySP("sp_LogSheet_Delete_TSActivityLog", paramList, false); break;
            //    default: throw new Exception(activitylogID + " is not a valid Activity Type ID");
            //}

            return result;

        }

        #region Admin
        public ArrayList getAdminActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_AdminActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdmin logdetail = new DataModels.LogDetailAdmin();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addAdminActivityLog(DataModels.LogDetailAdmin activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_AdminActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdmin logdetail = new DataModels.LogDetailAdmin();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateAdminActivityLog(DataModels.LogDetailAdmin activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_AdminActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdmin logdetail = new DataModels.LogDetailAdmin();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region AdminProductive
        public ArrayList getAdminProductiveActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_AdminProductiveActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdminProductive logdetail = new DataModels.LogDetailAdminProductive();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addAdminProductiveActivityLog(DataModels.LogDetailAdminProductive activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@NumberOfTransaction", activitylog.Numberoftransaction));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_AdminProductiveActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdminProductive logdetail = new DataModels.LogDetailAdminProductive();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateAdminProductiveActivityLog(DataModels.LogDetailAdminProductive activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@NumberOfTransaction", activitylog.Numberoftransaction));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_AdminProductiveActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdminProductive logdetail = new DataModels.LogDetailAdminProductive();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region Project
        public ArrayList getProjectActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ProjectActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQAProd = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQAProd"], DataTransformerUtility.DataType.NUMBER));

                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addProjectActivityLog(DataModels.LogDetailProject activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_ProjectActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateProjectActivityLog(DataModels.LogDetailProject activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_ProjectActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getProjectAssignment()
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ProjectAssignment", new ArrayList());
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectNumber = dtu.extractStringValue(dr["ProjectNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Sponsor = dtu.extractStringValue(dr["Sponsor"], DataTransformerUtility.DataType.STRING);
                logdetail.StartDate = dtu.extractStringValue(dr["StartDate"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TargetTestingDate = dtu.extractStringValue(dr["TargetTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TestingDate = dtu.extractStringValue(dr["TestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.JointTestingDate = dtu.extractStringValue(dr["JointTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TargetDate = dtu.extractStringValue(dr["TargetDate"], DataTransformerUtility.DataType.STRING);
                logdetail.CompletionDate = dtu.extractStringValue(dr["CompletionDate"], DataTransformerUtility.DataType.STRING);
                logdetail.AcceptanceDate = dtu.extractStringValue(dr["AcceptanceDate"], DataTransformerUtility.DataType.STRING);

                logdetail.NoOfDaysDelayedST = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassTimelinessST = float.Parse(dtu.extractStringValue(dr["FirstPassTimelinessST"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemST = int.Parse(dtu.extractStringValue(dr["ActionItemST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingST = int.Parse(dtu.extractStringValue(dr["WorkingST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassAccuracyST = float.Parse(dtu.extractStringValue(dr["FirstPassAccuracyST"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.NoOfDaysDelayedPR = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureTimelinessPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureTimelinessPR"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemPR = int.Parse(dtu.extractStringValue(dr["ActionItemPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingPR = int.Parse(dtu.extractStringValue(dr["WorkingPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureAccuracyPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureAccuracyPR"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.FirstPass = float.Parse(dtu.extractStringValue(dr["FirstPass"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ProjectClosure = float.Parse(dtu.extractStringValue(dr["ProjectClosure"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentOfQAProd = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQAProd"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addProjectAssignment(DataModels.LogDetailProject activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@Sponsor", activitylog.Sponsor));
            paramList.Add(new SqlParameter("@StartDate", activitylog.StartDate));
            paramList.Add(new SqlParameter("@TargetTestingDate", activitylog.TargetTestingDate));
            paramList.Add(new SqlParameter("@TestingDate", activitylog.TestingDate));
            paramList.Add(new SqlParameter("@JointTestingDate", activitylog.JointTestingDate));
            paramList.Add(new SqlParameter("@AcceptanceDate", activitylog.AcceptanceDate));
            paramList.Add(new SqlParameter("@TargetDate", activitylog.TargetDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_ProjectAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectNumber = dtu.extractStringValue(dr["ProjectNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Sponsor = dtu.extractStringValue(dr["Sponsor"], DataTransformerUtility.DataType.STRING);
                logdetail.StartDate = dtu.extractStringValue(dr["StartDate"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TargetTestingDate = dtu.extractStringValue(dr["TargetTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TestingDate = dtu.extractStringValue(dr["TestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.JointTestingDate = dtu.extractStringValue(dr["JointTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TargetDate = dtu.extractStringValue(dr["TargetDate"], DataTransformerUtility.DataType.STRING);
                logdetail.CompletionDate = dtu.extractStringValue(dr["CompletionDate"], DataTransformerUtility.DataType.STRING);
                logdetail.AcceptanceDate = dtu.extractStringValue(dr["AcceptanceDate"], DataTransformerUtility.DataType.STRING);

                logdetail.NoOfDaysDelayedST = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassTimelinessST = float.Parse(dtu.extractStringValue(dr["FirstPassTimelinessST"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemST = int.Parse(dtu.extractStringValue(dr["ActionItemST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingST = int.Parse(dtu.extractStringValue(dr["WorkingST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassAccuracyST = float.Parse(dtu.extractStringValue(dr["FirstPassAccuracyST"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.NoOfDaysDelayedPR = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureTimelinessPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureTimelinessPR"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemPR = int.Parse(dtu.extractStringValue(dr["ActionItemPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingPR = int.Parse(dtu.extractStringValue(dr["WorkingPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureAccuracyPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureAccuracyPR"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.FirstPass = float.Parse(dtu.extractStringValue(dr["FirstPass"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ProjectClosure = float.Parse(dtu.extractStringValue(dr["ProjectClosure"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateProjectAssignment(DataModels.LogDetailProject activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@Sponsor", activitylog.Sponsor));
            paramList.Add(new SqlParameter("@StartDate", activitylog.StartDate));
            paramList.Add(new SqlParameter("@TargetTestingDate", activitylog.TargetTestingDate));
            paramList.Add(new SqlParameter("@TestingDate", activitylog.TestingDate));
            paramList.Add(new SqlParameter("@JointTestingDate", activitylog.JointTestingDate));
            paramList.Add(new SqlParameter("@AcceptanceDate", activitylog.AcceptanceDate));
            paramList.Add(new SqlParameter("@TargetDate", activitylog.TargetDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_ProjectAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectNumber = dtu.extractStringValue(dr["ProjectNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Sponsor = dtu.extractStringValue(dr["Sponsor"], DataTransformerUtility.DataType.STRING);
                logdetail.StartDate = dtu.extractStringValue(dr["StartDate"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TargetTestingDate = dtu.extractStringValue(dr["TargetTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TestingDate = dtu.extractStringValue(dr["TestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.JointTestingDate = dtu.extractStringValue(dr["JointTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TargetDate = dtu.extractStringValue(dr["TargetDate"], DataTransformerUtility.DataType.STRING);
                logdetail.CompletionDate = dtu.extractStringValue(dr["CompletionDate"], DataTransformerUtility.DataType.STRING);
                logdetail.AcceptanceDate = dtu.extractStringValue(dr["AcceptanceDate"], DataTransformerUtility.DataType.STRING);

                logdetail.NoOfDaysDelayedST = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassTimelinessST = float.Parse(dtu.extractStringValue(dr["FirstPassTimelinessST"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemST = int.Parse(dtu.extractStringValue(dr["ActionItemST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingST = int.Parse(dtu.extractStringValue(dr["WorkingST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassAccuracyST = float.Parse(dtu.extractStringValue(dr["FirstPassAccuracyST"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.NoOfDaysDelayedPR = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureTimelinessPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureTimelinessPR"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemPR = int.Parse(dtu.extractStringValue(dr["ActionItemPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingPR = int.Parse(dtu.extractStringValue(dr["WorkingPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureAccuracyPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureAccuracyPR"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.FirstPass = float.Parse(dtu.extractStringValue(dr["FirstPass"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ProjectClosure = float.Parse(dtu.extractStringValue(dr["ProjectClosure"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }

        public ArrayList completeProjectAssignment(DataModels.LogDetailProject activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@CompletionDate", activitylog.CompletionDate));
            paramList.Add(new SqlParameter("@QAByEmployeeID", activitylog.QAByEmployeeID));
            paramList.Add(new SqlParameter("@StatusListID", activitylog.StatusID));


            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Complete_ProjectAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectNumber = dtu.extractStringValue(dr["ProjectNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Sponsor = dtu.extractStringValue(dr["Sponsor"], DataTransformerUtility.DataType.STRING);
                logdetail.StartDate = dtu.extractStringValue(dr["StartDate"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TargetTestingDate = dtu.extractStringValue(dr["TargetTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TestingDate = dtu.extractStringValue(dr["TestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.JointTestingDate = dtu.extractStringValue(dr["JointTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TargetDate = dtu.extractStringValue(dr["TargetDate"], DataTransformerUtility.DataType.STRING);
                logdetail.CompletionDate = dtu.extractStringValue(dr["CompletionDate"], DataTransformerUtility.DataType.STRING);
                logdetail.AcceptanceDate = dtu.extractStringValue(dr["AcceptanceDate"], DataTransformerUtility.DataType.STRING);

                logdetail.NoOfDaysDelayedST = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassTimelinessST = float.Parse(dtu.extractStringValue(dr["FirstPassTimelinessST"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemST = int.Parse(dtu.extractStringValue(dr["ActionItemST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingST = int.Parse(dtu.extractStringValue(dr["WorkingST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassAccuracyST = float.Parse(dtu.extractStringValue(dr["FirstPassAccuracyST"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.NoOfDaysDelayedPR = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureTimelinessPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureTimelinessPR"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemPR = int.Parse(dtu.extractStringValue(dr["ActionItemPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingPR = int.Parse(dtu.extractStringValue(dr["WorkingPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureAccuracyPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureAccuracyPR"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.FirstPass = float.Parse(dtu.extractStringValue(dr["FirstPass"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ProjectClosure = float.Parse(dtu.extractStringValue(dr["ProjectClosure"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }

        public long deleteProjectAssignment(int LogDetailID)
        {
            long result = 0;
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@LogDetailID", LogDetailID));

            cm.executeCommandBySP("sp_LogSheet_Delete_ProjectAssignment", paramList, false);

            return result;

        }
        public ArrayList getProjectAssignmentForQA(int qaByEmployeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@QAByEmployeeID", qaByEmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ProjectAssignmentForQA", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectNumber = dtu.extractStringValue(dr["ProjectNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Sponsor = dtu.extractStringValue(dr["Sponsor"], DataTransformerUtility.DataType.STRING);
                logdetail.StartDate = dtu.extractStringValue(dr["StartDate"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TargetTestingDate = dtu.extractStringValue(dr["TargetTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TestingDate = dtu.extractStringValue(dr["TestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.JointTestingDate = dtu.extractStringValue(dr["JointTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TargetDate = dtu.extractStringValue(dr["TargetDate"], DataTransformerUtility.DataType.STRING);
                logdetail.CompletionDate = dtu.extractStringValue(dr["CompletionDate"], DataTransformerUtility.DataType.STRING);
                logdetail.AcceptanceDate = dtu.extractStringValue(dr["AcceptanceDate"], DataTransformerUtility.DataType.STRING);

                logdetail.NoOfDaysDelayedST = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassTimelinessST = float.Parse(dtu.extractStringValue(dr["FirstPassTimelinessST"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemST = int.Parse(dtu.extractStringValue(dr["ActionItemST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingST = int.Parse(dtu.extractStringValue(dr["WorkingST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassAccuracyST = float.Parse(dtu.extractStringValue(dr["FirstPassAccuracyST"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.NoOfDaysDelayedPR = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureTimelinessPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureTimelinessPR"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemPR = int.Parse(dtu.extractStringValue(dr["ActionItemPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingPR = int.Parse(dtu.extractStringValue(dr["WorkingPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureAccuracyPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureAccuracyPR"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.FirstPass = float.Parse(dtu.extractStringValue(dr["FirstPass"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ProjectClosure = float.Parse(dtu.extractStringValue(dr["ProjectClosure"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentOfQAProd = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQAProd"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.QAStatus = dtu.extractStringValue(dr["QAStatus"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getProjectAssignmentForQAProd(int qaByEmployeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@QAByEmployeeID", qaByEmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ProjectAssignmentForQAProd", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectNumber = dtu.extractStringValue(dr["ProjectNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Sponsor = dtu.extractStringValue(dr["Sponsor"], DataTransformerUtility.DataType.STRING);
                logdetail.StartDate = dtu.extractStringValue(dr["StartDate"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TargetTestingDate = dtu.extractStringValue(dr["TargetTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TestingDate = dtu.extractStringValue(dr["TestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.JointTestingDate = dtu.extractStringValue(dr["JointTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TargetDate = dtu.extractStringValue(dr["TargetDate"], DataTransformerUtility.DataType.STRING);
                logdetail.CompletionDate = dtu.extractStringValue(dr["CompletionDate"], DataTransformerUtility.DataType.STRING);
                logdetail.AcceptanceDate = dtu.extractStringValue(dr["AcceptanceDate"], DataTransformerUtility.DataType.STRING);

                logdetail.NoOfDaysDelayedST = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassTimelinessST = float.Parse(dtu.extractStringValue(dr["FirstPassTimelinessST"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemST = int.Parse(dtu.extractStringValue(dr["ActionItemST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingST = int.Parse(dtu.extractStringValue(dr["WorkingST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassAccuracyST = float.Parse(dtu.extractStringValue(dr["FirstPassAccuracyST"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.NoOfDaysDelayedPR = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureTimelinessPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureTimelinessPR"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemPR = int.Parse(dtu.extractStringValue(dr["ActionItemPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingPR = int.Parse(dtu.extractStringValue(dr["WorkingPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureAccuracyPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureAccuracyPR"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.FirstPass = float.Parse(dtu.extractStringValue(dr["FirstPass"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ProjectClosure = float.Parse(dtu.extractStringValue(dr["ProjectClosure"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentOfQAProd = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQAProd"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.QAStatus = dtu.extractStringValue(dr["QAStatus"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region CS
        public ArrayList getCSActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_CSActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailCS logdetail = new DataModels.LogDetailCS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalTimeSpent = string.Format("{0:00}", Math.Floor(double.Parse(dtu.extractStringValue(dr["TotalTimeSpent"], DataTransformerUtility.DataType.DECIMAL)) / 60)) + ":" + string.Format("{0:00}", decimal.Parse(dtu.extractStringValue(dr["TotalTimeSpent"], DataTransformerUtility.DataType.DECIMAL)) % 60);

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getCSAssignedPerTech(int employeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_CSAssignedPerTech", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailCS logdetail = new DataModels.LogDetailCS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["ListItemID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["ListItemDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.CSAssignmentID = int.Parse(dtu.extractStringValue(dr["CSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.TimeSpent = 0;
                logdetail.TotalTimeSpent = string.Format("{0:00}", Math.Floor(double.Parse(dtu.extractStringValue(dr["TotalTimeSpent"], DataTransformerUtility.DataType.DECIMAL)) / 60)) + ":" + string.Format("{0:00}", (double.Parse(dtu.extractStringValue(dr["TotalTimeSpent"], DataTransformerUtility.DataType.DECIMAL)) % 60));
                    
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addCSActivityLog(DataModels.LogDetailCS activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@CSAssignmentID", activitylog.CSAssignmentID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_CSActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailCS logdetail = new DataModels.LogDetailCS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["ListItemID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["ListItemDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.CSAssignmentID = int.Parse(dtu.extractStringValue(dr["CSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.TimeSpent = 0;
                logdetail.TotalTimeSpent = string.Format("{0:00}", Math.Floor(double.Parse(dtu.extractStringValue(dr["TotalTimeSpent"], DataTransformerUtility.DataType.DECIMAL)) / 60)) + ":" + string.Format("{0:00}", (double.Parse(dtu.extractStringValue(dr["TotalTimeSpent"], DataTransformerUtility.DataType.DECIMAL)) % 60));

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateCSActivityLog(DataModels.LogDetailCS activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_CSActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailCS logdetail = new DataModels.LogDetailCS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalTimeSpent = string.Format("{0:00}", Math.Floor(double.Parse(dtu.extractStringValue(dr["TotalTimeSpent"], DataTransformerUtility.DataType.DECIMAL)) / 60)) + ":" + string.Format("{0:00}", decimal.Parse(dtu.extractStringValue(dr["TotalTimeSpent"], DataTransformerUtility.DataType.DECIMAL)) % 60);

                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region Alarm
        public ArrayList getAlarmActivityLog(int employeeID, string actviityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", actviityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_AlarmActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAlarm logdetail = new DataModels.LogDetailAlarm();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                //logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                //logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StartTime = DateTime.Parse(dtu.extractStringValue(dr["StartTime"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EndTime = DateTime.Parse(dtu.extractStringValue(dr["EndTime"], DataTransformerUtility.DataType.NUMBER));
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentStr"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addAlarmActivityLog(DataModels.LogDetailAlarm activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@StartTime", activitylog.StartTime));
            paramList.Add(new SqlParameter("@EndTime", activitylog.EndTime));
            paramList.Add(new SqlParameter("@NumberOfTransaction", activitylog.NumberOfTransaction));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            //paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@TimeSpentStr", activitylog.TimeSpentStr));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_AlarmActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAlarm logdetail = new DataModels.LogDetailAlarm();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                //logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                //logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StartTime = DateTime.Parse(dtu.extractStringValue(dr["StartTime"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EndTime = DateTime.Parse(dtu.extractStringValue(dr["EndTime"], DataTransformerUtility.DataType.NUMBER));
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentStr"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateAlarmActivityLog(DataModels.LogDetailAlarm activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@StartTime", activitylog.StartTime));
            paramList.Add(new SqlParameter("@EndTime", activitylog.EndTime));
            paramList.Add(new SqlParameter("@NumberOfTransaction", activitylog.NumberOfTransaction));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            //paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_AlarmActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAlarm logdetail = new DataModels.LogDetailAlarm();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                //logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                //logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StartTime = DateTime.Parse(dtu.extractStringValue(dr["StartTime"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EndTime = DateTime.Parse(dtu.extractStringValue(dr["EndTime"], DataTransformerUtility.DataType.NUMBER));
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentStr"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region ALR
        public ArrayList getALRActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ALRActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailALR logdetail = new DataModels.LogDetailALR();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addALRActivityLog(DataModels.LogDetailALR activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_ALRActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailALR logdetail = new DataModels.LogDetailALR();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateALRActivityLog(DataModels.LogDetailALR activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_ALRActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailALR logdetail = new DataModels.LogDetailALR();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region SPM
        public ArrayList getSPMActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_SPMActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailSPM logdetail = new DataModels.LogDetailSPM();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = DateTime.Parse(dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addSPMActivityLog(DataModels.LogDetailSPM activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@Requestor", activitylog.Requestor));
            paramList.Add(new SqlParameter("@DateTimeCompleted", activitylog.DateTimeCompleted));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            //paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@Numberoftransaction", activitylog.Numberoftransaction));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_SPMActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailSPM logdetail = new DataModels.LogDetailSPM();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = DateTime.Parse(dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateSPMActivityLog(DataModels.LogDetailSPM activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@Requestor", activitylog.Requestor));
            paramList.Add(new SqlParameter("@DateTimeCompleted", activitylog.DateTimeCompleted));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@Numberoftransaction", activitylog.Numberoftransaction));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_SPMActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailSPM logdetail = new DataModels.LogDetailSPM();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = DateTime.Parse(dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region FQR
        public ArrayList getFQRActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_FQRActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailFQR logdetail = new DataModels.LogDetailFQR();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.State = dtu.extractStringValue(dr["State"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.NUMBER));
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.NotesComment = dtu.extractStringValue(dr["NotesComments"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ClosedWithin24Hrs = dtu.extractStringValue(dr["ClosedWithin24Hrs"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getFQRAPending(int employeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_FQRPending", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailFQR logdetail = new DataModels.LogDetailFQR();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.State = dtu.extractStringValue(dr["State"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.NUMBER));
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.NotesComment = dtu.extractStringValue(dr["NotesComments"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ClosedWithin24Hrs = dtu.extractStringValue(dr["ClosedWithin24Hrs"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addFQRActivityLog(DataModels.LogDetailFQR activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));

            paramList.Add(new SqlParameter("@State", activitylog.@State));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.RequestedBy));
            paramList.Add(new SqlParameter("@RequestMade", activitylog.RequestMade));
            paramList.Add(new SqlParameter("@NotesComment", activitylog.NotesComment));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@DateTimeSaved", activitylog.DateTimeSaved));
            //paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@Numberoftransaction", activitylog.Numberoftransaction));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_FQRActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailFQR logdetail = new DataModels.LogDetailFQR();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.State = dtu.extractStringValue(dr["State"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.NUMBER));
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.NotesComment = dtu.extractStringValue(dr["NotesComments"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ClosedWithin24Hrs = dtu.extractStringValue(dr["ClosedWithin24Hrs"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateFQRActivityLog(DataModels.LogDetailFQR activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));

            paramList.Add(new SqlParameter("@State", activitylog.@State));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.RequestedBy));
            paramList.Add(new SqlParameter("@RequestMade", activitylog.RequestMade));
            paramList.Add(new SqlParameter("@NotesComment", activitylog.NotesComment));
            paramList.Add(new SqlParameter("@Numberoftransaction", activitylog.Numberoftransaction));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_FQRActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailFQR logdetail = new DataModels.LogDetailFQR();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.State = dtu.extractStringValue(dr["State"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.NUMBER));
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.NotesComment = dtu.extractStringValue(dr["NotesComments"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ClosedWithin24Hrs = dtu.extractStringValue(dr["ClosedWithin24Hrs"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateFQRPending(DataModels.LogDetailFQR activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ParentID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));

            paramList.Add(new SqlParameter("@State", activitylog.@State));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.RequestedBy));
            paramList.Add(new SqlParameter("@RequestMade", activitylog.RequestMade));
            paramList.Add(new SqlParameter("@NotesComment", activitylog.NotesComment));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@DateTimeSaved", activitylog.DateTimeSaved));
            //paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@Numberoftransaction", activitylog.Numberoftransaction));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_FQRPending", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailFQR logdetail = new DataModels.LogDetailFQR();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.State = dtu.extractStringValue(dr["State"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.NUMBER));
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.NotesComment = dtu.extractStringValue(dr["NotesComments"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ClosedWithin24Hrs = dtu.extractStringValue(dr["ClosedWithin24Hrs"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region EMT
        public ArrayList getEMTActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_EMTActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEMT logdetail = new DataModels.LogDetailEMT();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.STRING);
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TAT = string.Format("{0:00}", (int.Parse(dtu.extractStringValue(dr["TAT"], DataTransformerUtility.DataType.NUMBER)) / 60)) + ":" + string.Format("{0:00}", (int.Parse(dtu.extractStringValue(dr["TAT"], DataTransformerUtility.DataType.NUMBER)) % 60));

                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getEMTAssignedPerTech(int employeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_EMTAssignedPerTech", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEMT logdetail = new DataModels.LogDetailEMT();

                //logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                //logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.EMTAssignmentID = int.Parse(dtu.extractStringValue(dr["EMTAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addEMTActivityLog(DataModels.LogDetailEMT activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@DateTimeSaved", activitylog.DateTimeSaved));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@Numberoftransaction", activitylog.Numberoftransaction));
            paramList.Add(new SqlParameter("@EMTAssignmentID", activitylog.EMTAssignmentID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_EMTActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEMT logdetail = new DataModels.LogDetailEMT();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.EMTAssignmentID = int.Parse(dtu.extractStringValue(dr["EMTAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateEMTActivityLog(DataModels.LogDetailEMT activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@Numberoftransaction", activitylog.Numberoftransaction));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_EMTActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEMT logdetail = new DataModels.LogDetailEMT();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.STRING);
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TAT = string.Format("{0:00}", (int.Parse(dtu.extractStringValue(dr["TAT"], DataTransformerUtility.DataType.NUMBER)) / 60)) + ":" + string.Format("{0:00}", (int.Parse(dtu.extractStringValue(dr["TAT"], DataTransformerUtility.DataType.NUMBER)) % 60));

                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateEMTPending(DataModels.LogDetailEMT activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ParentID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));

            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@Requestor", activitylog.Requestor));
            paramList.Add(new SqlParameter("@DateTimeSaved", activitylog.DateTimeSaved));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@Numberoftransaction", activitylog.Numberoftransaction));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_EMTPending", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEMT logdetail = new DataModels.LogDetailEMT();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeRequested = dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.STRING);
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Numberoftransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TAT = string.Format("{0:00}", (int.Parse(dtu.extractStringValue(dr["TAT"], DataTransformerUtility.DataType.NUMBER)) / 60)) + ":" + string.Format("{0:00}", (int.Parse(dtu.extractStringValue(dr["TAT"], DataTransformerUtility.DataType.NUMBER)) % 60));

                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region EFS
        public ArrayList getEFSActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_EFSActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEFS logdetail = new DataModels.LogDetailEFS();
                //DataModels.Lists followupStatusList = new DataModels.Lists();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                //followupStatusList.ListID = int.Parse(dtu.extractStringValue(dr["FollowUpStatusID"], DataTransformerUtility.DataType.NUMBER));
                //followupStatusList.ListDescription = dtu.extractStringValue(dr["FollowUpStatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeReceived"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.WorkOrderNumber = int.Parse(dtu.extractStringValue(dr["WorkOrderNumber"], DataTransformerUtility.DataType.STRING));
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = DateTime.Parse(dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.DATE));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpBy = dtu.extractStringValue(dr["FollowUpBy"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpStatusList = dtu.extractStringValue(dr["FollowUpStatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpDateTime = dtu.extractStringValue(dr["FollowUpDateTime"], DataTransformerUtility.DataType.DATE);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                logdetail.DateTimeToFollowUp = dtu.extractStringValue(dr["DateTimeToFollowUp"], DataTransformerUtility.DataType.DATE);

                //logdetail.TargetDateTime = dtu.extractStringValue(dr["TargetDateTime"], DataTransformerUtility.DataType.DATE);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.DATE);
                
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getEFSFollowUp()
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_EFSFollowUp", new ArrayList());
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEFS logdetail = new DataModels.LogDetailEFS();
                //DataModels.Lists followupStatusList = new DataModels.Lists();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                //followupStatusList.ListID = int.Parse(dtu.extractStringValue(dr["FollowUpStatusID"], DataTransformerUtility.DataType.NUMBER));
                //followupStatusList.ListDescription = dtu.extractStringValue(dr["FollowUpStatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeReceived"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.WorkOrderNumber = int.Parse(dtu.extractStringValue(dr["WorkOrderNumber"], DataTransformerUtility.DataType.STRING));
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = DateTime.Parse(dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.DATE));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeToFollowUp = dtu.extractStringValue(dr["DateTimeToFollowUp"], DataTransformerUtility.DataType.DATE);
                logdetail.FollowUpBy = dtu.extractStringValue(dr["FollowUpBy"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addEFSActivityLog(DataModels.LogDetailEFS activitylog, DataModels.Accounts account)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@Requestor", activitylog.Requestor));
            paramList.Add(new SqlParameter("@WorkOrderNumber", activitylog.WorkOrderNumber));
            paramList.Add(new SqlParameter("@RequestMade", activitylog.RequestMade));
            paramList.Add(new SqlParameter("@DateTimeCompleted", activitylog.DateTimeCompleted));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@DateTimeToFollowUp", DateTime.Parse(activitylog.DateTimeToFollowUp)));

            bool sendMeetingReminder = (activitylog.StatusID == 22); //Completed - Further Action send meeting reminder

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_EFSActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEFS logdetail = new DataModels.LogDetailEFS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeReceived"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.WorkOrderNumber = int.Parse(dtu.extractStringValue(dr["WorkOrderNumber"], DataTransformerUtility.DataType.STRING));
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = DateTime.Parse(dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.DATE));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpBy = dtu.extractStringValue(dr["FollowUpBy"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpStatusList = dtu.extractStringValue(dr["FollowUpStatus"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpDateTime = dtu.extractStringValue(dr["FollowUpDateTime"], DataTransformerUtility.DataType.DATE);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                logdetail.DateTimeToFollowUp = dtu.extractStringValue(dr["DateTimeToFollowUp"], DataTransformerUtility.DataType.DATE);

                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.DATE);

                al.Add(logdetail);
            }
            // EMAILING PART
            sendLogsheetEFSEmail(activitylog.FullName, activitylog.ActivityDate, activitylog.StoreName, activitylog.DateTimeRequested, activitylog.Requestor, activitylog.WorkOrderNumber, activitylog.RequestMade, activitylog.DateTimeCompleted, activitylog.Remarks, activitylog.StatusDescription, activitylog.DateTimeToFollowUp);
            //
            //if (sendMeetingReminder)
            //    createMeeting(activitylog, account);

            return al;
        }
        public ArrayList updateEFSActivityLog(DataModels.LogDetailEFS activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));

            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@DateTimeReceived", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@Requestor", activitylog.Requestor));
            paramList.Add(new SqlParameter("@WorkOrderNumber", activitylog.WorkOrderNumber));
            paramList.Add(new SqlParameter("@RequestMade", activitylog.RequestMade));
            paramList.Add(new SqlParameter("@DateTimeCompleted", activitylog.DateTimeCompleted));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_EFSActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEFS logdetail = new DataModels.LogDetailEFS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeReceived"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.WorkOrderNumber = int.Parse(dtu.extractStringValue(dr["WorkOrderNumber"], DataTransformerUtility.DataType.STRING));
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = DateTime.Parse(dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.DATE));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpBy = dtu.extractStringValue(dr["FollowUpBy"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpStatusList = dtu.extractStringValue(dr["FollowUpStatus"], DataTransformerUtility.DataType.STRING);
                logdetail.FollowUpDateTime = dtu.extractStringValue(dr["FollowUpDateTime"], DataTransformerUtility.DataType.DATE);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                logdetail.DateTimeToFollowUp = dtu.extractStringValue(dr["DateTimeToFollowUp"], DataTransformerUtility.DataType.DATE);

                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.DATE);

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateEFSFollowUp(DataModels.LogDetailEFS activitylog, DataModels.Accounts account)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));

            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@Requestor", activitylog.Requestor));
            paramList.Add(new SqlParameter("@WorkOrderNumber", activitylog.WorkOrderNumber));
            paramList.Add(new SqlParameter("@RequestMade", activitylog.RequestMade));
            paramList.Add(new SqlParameter("@DateTimeCompleted", activitylog.DateTimeCompleted));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@DateTimeToFollowUp", DateTime.Parse(activitylog.DateTimeToFollowUp)));

            bool sendMeetingReminder = (activitylog.StatusID == 22); //Completed - Further Action send meeting reminder

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_EFSFollowUp", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEFS logdetail = new DataModels.LogDetailEFS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));

                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = DateTime.Parse(dtu.extractStringValue(dr["DateTimeReceived"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["Requestor"], DataTransformerUtility.DataType.STRING);
                logdetail.WorkOrderNumber = int.Parse(dtu.extractStringValue(dr["WorkOrderNumber"], DataTransformerUtility.DataType.STRING));
                logdetail.RequestMade = dtu.extractStringValue(dr["RequestMade"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeCompleted = DateTime.Parse(dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.DATE));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeToFollowUp = dtu.extractStringValue(dr["DateTimeToFollowUp"], DataTransformerUtility.DataType.DATE);
                logdetail.FollowUpBy = dtu.extractStringValue(dr["FollowUpBy"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                al.Add(logdetail);
            }

            //if (sendMeetingReminder)
            //    createMeeting(activitylog, account);

            return al;
        }
        #endregion

        #region ESS
        public ArrayList getESSActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ESSActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailESS logdetail = new DataModels.LogDetailESS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["DateTimeProcessed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                logdetail.CustomerCompany = dtu.extractStringValue(dr["CustomerCompany"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeProcessed = DateTime.Parse(dtu.extractStringValue(dr["DateTimeProcessed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Issue = dtu.extractStringValue(dr["Issue"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeSaved = DateTime.Parse(dtu.extractStringValue(dr["DateTimeSaved"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeReceived = DateTime.Parse(dtu.extractStringValue(dr["DateTimeReceived"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = int.Parse(dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addESSActivityLog(DataModels.LogDetailESS activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@DateTimeSaved", activitylog.DateTimeSaved));
            paramList.Add(new SqlParameter("@DateTimeReceived", activitylog.DateTimeReceived));
            paramList.Add(new SqlParameter("@CustomerName", activitylog.CustomerName));
            paramList.Add(new SqlParameter("@CustomerCompany", activitylog.CustomerCompany));
            paramList.Add(new SqlParameter("@Issue", activitylog.Issue));
            paramList.Add(new SqlParameter("@TicketNumber", activitylog.TicketNumber));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_ESSActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailESS logdetail = new DataModels.LogDetailESS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["DateTimeProcessed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                logdetail.CustomerCompany = dtu.extractStringValue(dr["CustomerCompany"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeProcessed = DateTime.Parse(dtu.extractStringValue(dr["DateTimeProcessed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Issue = dtu.extractStringValue(dr["Issue"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeSaved = DateTime.Parse(dtu.extractStringValue(dr["DateTimeSaved"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeReceived = DateTime.Parse(dtu.extractStringValue(dr["DateTimeReceived"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = int.Parse(dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateESSActivityLog(DataModels.LogDetailESS activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@DateTimeSaved", activitylog.DateTimeSaved));
            paramList.Add(new SqlParameter("@DateTimeReceived", activitylog.DateTimeReceived));
            paramList.Add(new SqlParameter("@CustomerName", activitylog.CustomerName));
            paramList.Add(new SqlParameter("@CustomerCompany", activitylog.CustomerCompany));
            paramList.Add(new SqlParameter("@Issue", activitylog.Issue));
            paramList.Add(new SqlParameter("@TicketNumber", activitylog.TicketNumber));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_ESSActivityLog_v2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailESS logdetail = new DataModels.LogDetailESS();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["DateTimeProcessed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CustomerName = dtu.extractStringValue(dr["CustomerName"], DataTransformerUtility.DataType.STRING);
                logdetail.CustomerCompany = dtu.extractStringValue(dr["CustomerCompany"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeProcessed = DateTime.Parse(dtu.extractStringValue(dr["DateTimeProcessed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Issue = dtu.extractStringValue(dr["Issue"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeSaved = DateTime.Parse(dtu.extractStringValue(dr["DateTimeSaved"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateTimeReceived = DateTime.Parse(dtu.extractStringValue(dr["DateTimeReceived"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = int.Parse(dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentStr = string.Format("{0:00}", Math.Floor(logdetail.TimeSpent / 60)) + ":" + string.Format("{0:00}", (logdetail.TimeSpent % 60));
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region AdhocAnalytics
        public ArrayList getAAAssignedPerTech(int employeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_AAAssignedPerTech", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER)); 
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getAAActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_AAActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransactions"], DataTransformerUtility.DataType.NUMBER));

                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addAAActivityLog(DataModels.LogDetailAdhocAnalytics activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@AAAssignmentID", activitylog.AAAssignmentID));
            paramList.Add(new SqlParameter("@NumberofTransaction", activitylog.NumberOfTransaction));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_AAActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList completeStatusAAActivityLog(DataModels.LogDetailAdhocAnalytics activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@AAAssignmentID", activitylog.AAAssignmentID));
            paramList.Add(new SqlParameter("@NumberofTransaction", activitylog.NumberOfTransaction));
            paramList.Add(new SqlParameter("@QAByEmployeeID", activitylog.QAByEmployeeID));

            paramList.Add(new SqlParameter("@DateCompleted", activitylog.DateCompleted));
            paramList.Add(new SqlParameter("@NoOfDaysDelayed", activitylog.NoOfDaysDelayed));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_AAActivityLogComplete", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            MailMaster mm = new MailMaster();

            //send email
            string bodyContent = "<body style='padding: 1em; font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 14;'>";
            bodyContent += "<p>Hi,</p>";
            bodyContent += "<p>The ticket number <strong>" + activitylog.TicketNumber + "</strong> is now assigned to you for quality check.</p>";
            bodyContent += "<p>Here’s the <a href='http://proact.emerson.com/onlinelogsheet/webscreens/OnlineLogSheet/aaassignmentQAForm.aspx'>link</a> to where you can input the QA Scores after checking.</p>";
            bodyContent += "<p>For your reference:</p>";
            bodyContent += "<span>Processor: <strong>" + activitylog.AssignEmployeeName + "</strong></span><br/>";
            bodyContent += "<span>Assigned QA: <strong>" + activitylog.QAByEmployeeName + "</strong></span><br/>";
            bodyContent += "<span>Ticket number - <strong>" + activitylog.TicketNumber + "</strong></span><br/>";
            bodyContent += "<span>Priority - <strong>" + activitylog.PriorityListDescription + "</strong></span><br/>";
            bodyContent += "<span>Description of task:  <strong>" + activitylog.TaskDescription + "</strong></span><br/>";
            bodyContent += "<p>If you have any further questions, please contact us referencing your ticket number.</p>";
            bodyContent += "<p><strong>Thank you.</strong></p>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>Need Support?</span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>770-425-7430 / US Toll Free 800-829-2724, 3</li></span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>ProActServiceOrders@Emerson.com</li></span>";
            bodyContent += "<p style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>The information contained in this message is confidential or protected by law. If you are not the intended recipient, please contact the sender and delete this message. Any unauthorized copying of this message or unauthorized distribution of the information contained herein is prohibited.</p>";
            bodyContent += "</body>";

            string subject = "Ad Hoc Request Ticket number - " + activitylog.TicketNumber + "";

            List<string> eal = new List<string>();
            
           
            if (activitylog.QAByEmployeeName.Contains("Jennylyn".ToUpper()))
            {
                eal.Add("Jennylyn.Sevilla@Emerson.com");
            }
            else if (activitylog.QAByEmployeeName.Contains("Lloyd Cyrus".ToUpper()))
            {
                eal.Add("LloydCyrus.Sison@Emerson.com");
            }
            else if (activitylog.QAByEmployeeName.Contains("John Nelson".ToUpper()))
            {
                eal.Add("JohnNelson.Salonga@Emerson.com");
            }
            else if (activitylog.QAByEmployeeName.Contains("Hazel Glo".ToUpper()))
            {
                eal.Add("HazelGlo.Bernal@Emerson.com");
            }

            List<string> ccal = new List<string>();
			// ccal.Add("Archieval.Gunhuran@Emerson.com");
            ccal.Add("Michelle.Pio@Emerson.com");
            ccal.Add("Jennylyn.Sevilla@Emerson.com");
            ccal.Add("LloydCyrus.Sison@Emerson.com");
            ccal.Add("JohnNelson.Salonga@Emerson.com");
            ccal.Add("HazelGlo.Bernal@Emerson.com");
            ccal.Remove(eal[0]);

            //mm.sendMailAdhoc(eal, ccal, bodyContent, subject);

            return al;
        }

        public ArrayList addQAForAdhocAnalyticsAssignments(DataModels.LogDetailAdhocAnalytics activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@AAAssignmentID", activitylog.AAAssignmentID));
            paramList.Add(new SqlParameter("@NoOfDaysDelayed", activitylog.NoOfDaysDelayed));
            paramList.Add(new SqlParameter("@Timeliness", activitylog.Timeliness));
            paramList.Add(new SqlParameter("@CorrectData", activitylog.CorrectData));
            paramList.Add(new SqlParameter("@MeetTheCriteria", activitylog.MeetTheCriteria));
            paramList.Add(new SqlParameter("@Accuracy", activitylog.Accuracy));
            paramList.Add(new SqlParameter("@TotalQAScore", activitylog.TotalQAScore));
            paramList.Add(new SqlParameter("@TimeSpentOfQA", activitylog.TimeSpentOfQA));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_AAAssignmentForQA", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            MailMaster mm = new MailMaster();

            //send email
            string bodyContent = "<body style='padding: 1em; font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 14;'>";
            bodyContent += "<p>Hi,</p>";
            bodyContent += "<p>This is to inform you that the quality check for ticket number "+activitylog.TicketNumber+" is now complete.</p>";
            bodyContent += "<p>You may view your QA Score in this <a href='http://proact.emerson.com/onlinelogsheet/webscreens/OnlineLogSheet/aaassignment.aspx'>link</a>.</p>";
            bodyContent += "<p>For your reference:</p>";
            bodyContent += "<span>Processor Time Spent: <strong>" + activitylog.TimeSpentofProcessor + "</strong></span><br/>";
            bodyContent += "<span>Assigned QA Time Spent: <strong>" + activitylog.TimeSpentOfQA + "</strong></span><br/>";
            bodyContent += "<p><strong>Thank you.</strong></p>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>Need Support?</span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>770-425-7430 / US Toll Free 800-829-2724, 3</li></span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>ProActServiceOrders@Emerson.com</li></span>";
            bodyContent += "<p style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>The information contained in this message is confidential or protected by law. If you are not the intended recipient, please contact the sender and delete this message. Any unauthorized copying of this message or unauthorized distribution of the information contained herein is prohibited.</p>";
            bodyContent += "</body>";

            string subject = "Ad Hoc Request Ticket number - " + activitylog.TicketNumber + "";

            List<string> eal = new List<string>();
            eal.Add("Jennylyn.Sevilla@Emerson.com");
            eal.Add("LloydCyrus.Sison@Emerson.com");
            eal.Add("JohnNelson.Salonga@Emerson.com");
            eal.Add("HazelGlo.Bernal@Emerson.com");
            // eal.Add("Archieval.Gunhuran@Emerson.com");

            List<string> ccal = new List<string>();
            ccal.Add("Michelle.Pio@Emerson.com");
            // ccal.Add("Archieval.Gunhuran@Emerson.com");

            //mm.sendMailAdhoc(eal, ccal, bodyContent, subject);

            return al;
        }
        public ArrayList addQAForTSAssignments(DataModels.LogDetailToolSupport activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@TSAssignmentID", activitylog.TSAssignmentID));
            paramList.Add(new SqlParameter("@NoOfDaysDelayed", activitylog.NoOfDaysDelayed));
            paramList.Add(new SqlParameter("@Timeliness", activitylog.Timeliness));
            paramList.Add(new SqlParameter("@ActionItem", activitylog.ActionItem));
            paramList.Add(new SqlParameter("@Working", activitylog.Working));
            paramList.Add(new SqlParameter("@Accuracy", activitylog.Accuracy));
            paramList.Add(new SqlParameter("@TotalQAScore", activitylog.TotalQAScore));
            paramList.Add(new SqlParameter("@TimeSpentOfQA", activitylog.TimeSpentOfQA));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_TSAssignmentForQA", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TSAssignmentID = int.Parse(dtu.extractStringValue(dr["TSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);


                logdetail.AssignedByEmployeeID = int.Parse(dtu.extractStringValue(dr["AssignedPersonID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignedEmployeeName = dtu.extractStringValue(dr["AssignedEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActionItem = int.Parse(dtu.extractStringValue(dr["ActionItem"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Working = int.Parse(dtu.extractStringValue(dr["Working"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addQAForProjectAssignments(DataModels.LogDetailProject activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@NoOfDaysDelayedST", activitylog.NoOfDaysDelayedST));
            paramList.Add(new SqlParameter("@FirstPassTimelinessST", activitylog.FirstPassTimelinessST));
            paramList.Add(new SqlParameter("@ActionItemST", activitylog.ActionItemST));
            paramList.Add(new SqlParameter("@WorkingST", activitylog.WorkingST));
            paramList.Add(new SqlParameter("@FirstPassAccuracyST", activitylog.FirstPassAccuracyST));
            paramList.Add(new SqlParameter("@FirstPass", activitylog.FirstPass));
            paramList.Add(new SqlParameter("@TimeSpentOfQA", activitylog.TimeSpentOfQA));
            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_ProjectAssignmentForQA", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectNumber = dtu.extractStringValue(dr["ProjectNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Sponsor = dtu.extractStringValue(dr["Sponsor"], DataTransformerUtility.DataType.STRING);
                logdetail.StartDate = dtu.extractStringValue(dr["StartDate"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TargetTestingDate = dtu.extractStringValue(dr["TargetTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TestingDate = dtu.extractStringValue(dr["TestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.JointTestingDate = dtu.extractStringValue(dr["JointTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TargetDate = dtu.extractStringValue(dr["TargetDate"], DataTransformerUtility.DataType.STRING);
                logdetail.CompletionDate = dtu.extractStringValue(dr["CompletionDate"], DataTransformerUtility.DataType.STRING);
                logdetail.AcceptanceDate = dtu.extractStringValue(dr["AcceptanceDate"], DataTransformerUtility.DataType.STRING);

                logdetail.NoOfDaysDelayedST = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassTimelinessST = float.Parse(dtu.extractStringValue(dr["FirstPassTimelinessST"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemST = int.Parse(dtu.extractStringValue(dr["ActionItemST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingST = int.Parse(dtu.extractStringValue(dr["WorkingST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassAccuracyST = float.Parse(dtu.extractStringValue(dr["FirstPassAccuracyST"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.NoOfDaysDelayedPR = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureTimelinessPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureTimelinessPR"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemPR = int.Parse(dtu.extractStringValue(dr["ActionItemPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingPR = int.Parse(dtu.extractStringValue(dr["WorkingPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureAccuracyPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureAccuracyPR"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.FirstPass = float.Parse(dtu.extractStringValue(dr["FirstPass"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ProjectClosure = float.Parse(dtu.extractStringValue(dr["ProjectClosure"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addQAForProjectAssignmentsProd(DataModels.LogDetailProject activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@NoOfDaysDelayedPR", activitylog.NoOfDaysDelayedPR));
            paramList.Add(new SqlParameter("@ProjectClosureTimelinessPR", activitylog.ProjectClosureTimelinessPR));
            paramList.Add(new SqlParameter("@ActionItemPR", activitylog.ActionItemPR));
            paramList.Add(new SqlParameter("@WorkingPR", activitylog.WorkingPR));
            paramList.Add(new SqlParameter("@ProjectClosureAccuracyPR", activitylog.ProjectClosureAccuracyPR));
            paramList.Add(new SqlParameter("@ProjectClosure", activitylog.ProjectClosure));
            paramList.Add(new SqlParameter("@TimeSpentOfQAProd", activitylog.TimeSpentOfQAProd));
            paramList.Add(new SqlParameter("@TotalQAScore", activitylog.TotalQAScore));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_ProjectAssignmentForQAProd", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailProject logdetail = new DataModels.LogDetailProject();

                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectNumber = dtu.extractStringValue(dr["ProjectNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.Sponsor = dtu.extractStringValue(dr["Sponsor"], DataTransformerUtility.DataType.STRING);
                logdetail.StartDate = dtu.extractStringValue(dr["StartDate"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TargetTestingDate = dtu.extractStringValue(dr["TargetTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TestingDate = dtu.extractStringValue(dr["TestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.JointTestingDate = dtu.extractStringValue(dr["JointTestingDate"], DataTransformerUtility.DataType.STRING);
                logdetail.TargetDate = dtu.extractStringValue(dr["TargetDate"], DataTransformerUtility.DataType.STRING);
                logdetail.CompletionDate = dtu.extractStringValue(dr["CompletionDate"], DataTransformerUtility.DataType.STRING);
                logdetail.AcceptanceDate = dtu.extractStringValue(dr["AcceptanceDate"], DataTransformerUtility.DataType.STRING);

                logdetail.NoOfDaysDelayedST = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassTimelinessST = float.Parse(dtu.extractStringValue(dr["FirstPassTimelinessST"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemST = int.Parse(dtu.extractStringValue(dr["ActionItemST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingST = int.Parse(dtu.extractStringValue(dr["WorkingST"], DataTransformerUtility.DataType.NUMBER));
                logdetail.FirstPassAccuracyST = float.Parse(dtu.extractStringValue(dr["FirstPassAccuracyST"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.NoOfDaysDelayedPR = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayedPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureTimelinessPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureTimelinessPR"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ActionItemPR = int.Parse(dtu.extractStringValue(dr["ActionItemPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.WorkingPR = int.Parse(dtu.extractStringValue(dr["WorkingPR"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ProjectClosureAccuracyPR = float.Parse(dtu.extractStringValue(dr["ProjectClosureAccuracyPR"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.FirstPass = float.Parse(dtu.extractStringValue(dr["FirstPass"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.ProjectClosure = float.Parse(dtu.extractStringValue(dr["ProjectClosure"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }

        public ArrayList updateAAActivityLog(DataModels.LogDetailAdhocAnalytics activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@NumberofTransaction", activitylog.NumberOfTransaction));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_AAActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransactions"], DataTransformerUtility.DataType.NUMBER));

                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region ToolSupport
        public ArrayList getTSAssignedPerTech(int employeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_TSAssignedPerTech", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.TSAssignmentID = int.Parse(dtu.extractStringValue(dr["TSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getTSActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_TSActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);

                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransactions"], DataTransformerUtility.DataType.NUMBER));

                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addTSActivityLog(DataModels.LogDetailToolSupport activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@NumberOfTransaction", activitylog.NumberOfTransaction));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@TSAssignmentID", activitylog.TSAssignmentID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_TSActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.TSAssignmentID = int.Parse(dtu.extractStringValue(dr["TSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList completeStatusTSActivityLog(DataModels.LogDetailToolSupport activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@NumberOfTransaction", activitylog.NumberOfTransaction));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@TSAssignmentID", activitylog.TSAssignmentID));
            paramList.Add(new SqlParameter("@QAByEmployeeID", activitylog.QAByEmployeeID));
            paramList.Add(new SqlParameter("@AssignedPersonID", activitylog.AssignedByEmployeeID));
            paramList.Add(new SqlParameter("@DateCompleted", activitylog.DateCompleted));
            paramList.Add(new SqlParameter("@NoOfDaysDelayed", activitylog.NoOfDaysDelayed));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_TSActivityLogComplete", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.TSAssignmentID = int.Parse(dtu.extractStringValue(dr["TSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateTSActivityLog(DataModels.LogDetailToolSupport activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@NumberOfTransaction", activitylog.NumberOfTransaction));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_TSActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));

                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransactions"], DataTransformerUtility.DataType.NUMBER));

                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.NUMBER));
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        #endregion
        
        #region ReportGeneration
        public ArrayList getRGActivityLog(int employeeID, string activityDate)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_RGActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdmin logdetail = new DataModels.LogDetailAdmin();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addRGActivityLog(DataModels.LogDetailRG activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_RGActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdmin logdetail = new DataModels.LogDetailAdmin();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateRGActivityLog(DataModels.LogDetailRG activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylog.ActivityLogID));
            paramList.Add(new SqlParameter("@LogDetailID", activitylog.LogDetailID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylog.ActivityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@Remarks", activitylog.Remarks));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_RGActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdmin logdetail = new DataModels.LogDetailAdmin();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = DateTime.Parse(dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.NUMBER);
                logdetail.TimeSpent = float.Parse(dtu.extractStringValue(dr["TimeSpent"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        #endregion

        #region CSAssignment
        public ArrayList getCSAssignment()
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_CSAssignment_v2", new ArrayList());
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailCS logdetail = new DataModels.LogDetailCS();
                //DataModels.Lists programType = new DataModels.Lists();
                //DataModels.Lists employeeList = new DataModels.Lists();
                //DataModels.Lists statusList = new DataModels.Lists();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["ListItemID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["ListItemDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.CSAssignmentID = int.Parse(dtu.extractStringValue(dr["CSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.NUMBER));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addCSAssignment(DataModels.LogDetailCS activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@ProgramTypeID", activitylog.ActivityID));                                                                                                                                                                                                                                                                                                                                      
            paramList.Add(new SqlParameter("@TicketNumber", activitylog.TicketNumber));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));
            paramList.Add(new SqlParameter("@RegisteredBy", activitylog.EmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_CSAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailCS logdetail = new DataModels.LogDetailCS();
                //DataModels.Lists programType = new DataModels.Lists();
                //DataModels.Lists employeeList = new DataModels.Lists();
                //DataModels.Lists statusList = new DataModels.Lists();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["ListItemID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["ListItemDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.CSAssignmentID = int.Parse(dtu.extractStringValue(dr["CSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.NUMBER));

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateCSAssignment(DataModels.LogDetailCS activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@CSAssignmentID", activitylog.CSAssignmentID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@ProgramTypeID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@TicketNumber", activitylog.TicketNumber));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@UpdatedBy", activitylog.EmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_CSAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailCS logdetail = new DataModels.LogDetailCS();
                //DataModels.Lists programType = new DataModels.Lists();
                //DataModels.Lists employeeList = new DataModels.Lists();
                //DataModels.Lists statusList = new DataModels.Lists();

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["ListItemID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["ListItemDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.CSAssignmentID = int.Parse(dtu.extractStringValue(dr["CSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.NUMBER));

                al.Add(logdetail);
            }

            return al;
        }
        public long deleteCSAssignment(int csAssignmentID)
        {
            long result = 0;
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@CSAssignmentID", csAssignmentID));

            cm.executeCommandBySP("sp_LogSheet_Delete_CSAssignment", paramList, false);

            return result;

        }
        #endregion

        #region AAAssignment
        public ArrayList getAAAssignment()
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_AAAssignment", new ArrayList());
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.EmailAddressofRequestor = dtu.extractStringValue(dr["EmailAddressofRequestor"], DataTransformerUtility.DataType.STRING);

                logdetail.QAByEmployeeID = int.Parse(dtu.extractStringValue(dr["QAByEmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CorrectData = int.Parse(dtu.extractStringValue(dr["CorrectData"], DataTransformerUtility.DataType.NUMBER));
                logdetail.MeetTheCriteria = int.Parse(dtu.extractStringValue(dr["MeetTheCriteria"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getAAAssignmentForQA(int qaByEmployeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@QAByEmployeeID", qaByEmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_AAAssignmentForQA", paramList);


            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAStatus = dtu.extractStringValue(dr["QAStatus"], DataTransformerUtility.DataType.STRING);

                //logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                //logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                //logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                //logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                //logdetail.EmailAddressofRequestor = dtu.extractStringValue(dr["EmailAddressofRequestor"], DataTransformerUtility.DataType.STRING);
                //logdetail.QAByEmployeeID = int.Parse(dtu.extractStringValue(dr["QAByEmployeeID"], DataTransformerUtility.DataType.NUMBER));

                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.CorrectData = int.Parse(dtu.extractStringValue(dr["CorrectData"], DataTransformerUtility.DataType.NUMBER));
                logdetail.MeetTheCriteria = int.Parse(dtu.extractStringValue(dr["MeetTheCriteria"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addAAAssignment(DataModels.LogDetailAdhocAnalytics activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@PriorityID", activitylog.PriorityListID));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@TaskDescription", activitylog.TaskDescription));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.RequestedBy));
            paramList.Add(new SqlParameter("@DateReceived", activitylog.DateReceived));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));
            paramList.Add(new SqlParameter("@RegisteredBy", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@EmailAddressofRequestor", activitylog.EmailAddressofRequestor));
            paramList.Add(new SqlParameter("@TimeSpentofProcessor", activitylog.TimeSpentofProcessor));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_AAAssignment", paramList);
            string ticketNumber = dt.Rows[0]["TicketNumber"].ToString();
            string priorityDescription = dt.Rows[0]["PriorityDescription"].ToString();
            string descriptionOfTask = dt.Rows[0]["TaskDescription"].ToString();
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            MailMaster mm = new MailMaster();

            //send email
            string bodyContent = "<body style='padding: 1em; font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 14;'>";
            bodyContent += "<p>Hi,</p>";
            bodyContent += "<p>Thank you for contacting Emerson Cold Chain Analytics.</p>";
            bodyContent += "<p>This is to inform you that a ticket was created for your request and is now being worked on.</p>";
            bodyContent += "<p>For your reference:</p>";
            bodyContent += "<span>Ticket number - <strong>"+ticketNumber+ "</strong></span><br/>";
            bodyContent += "<span>Priority – <strong>" + priorityDescription + "</strong></span><br/>";
            bodyContent += "<span>Description of task: <strong>" + descriptionOfTask + "</strong></span><br/>";
            bodyContent += "<p>If you have any further questions, please contact us referencing your ticket number.</p>";
            bodyContent += "<p><strong>Thank you.</strong></p>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>Need Support?</span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>770-425-7430 / US Toll Free 800-829-2724, 3</li></span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>ProActServiceOrders@Emerson.com</li></span>";
            bodyContent += "<p style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>The information contained in this message is confidential or protected by law. If you are not the intended recipient, please contact the sender and delete this message. Any unauthorized copying of this message or unauthorized distribution of the information contained herein is prohibited.</p>";
            bodyContent += "</body>";

            string subject = "Ad Hoc Request Ticket number - "+ticketNumber+"";

            List<string> eal = new List<string>();
            eal.Add(activitylog.EmailAddressofRequestor.ToString());

            List<string> ccal = new List<string>();
            ccal.Add("Jennylyn.Sevilla@Emerson.com");
            ccal.Add("LloydCyrus.Sison@Emerson.com");
            ccal.Add("JohnNelson.Salonga@Emerson.com");
            ccal.Add("HazelGlo.Bernal@Emerson.com");
            ccal.Add("Michelle.Pio@Emerson.com");
            // ccal.Add("Archieval.Gunhuran@Emerson.com");

            //mm.sendMailAdhoc(eal, ccal, bodyContent, subject);

            return al;
        }
        public ArrayList updateAAAssignment(DataModels.LogDetailAdhocAnalytics activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@AAAssignmentID", activitylog.AAAssignmentID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@PriorityID", activitylog.PriorityListID));
            paramList.Add(new SqlParameter("@TaskDescription", activitylog.TaskDescription));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.RequestedBy));
            paramList.Add(new SqlParameter("@DateReceived", activitylog.DateReceived));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));
            paramList.Add(new SqlParameter("@UpdatedBy", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@EmailAddressofRequestor", activitylog.EmailAddressofRequestor));
            paramList.Add(new SqlParameter("@TimeSpentofProcessor", activitylog.TimeSpentofProcessor));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_AAAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailAdhocAnalytics logdetail = new DataModels.LogDetailAdhocAnalytics();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.PriorityListID = int.Parse(dtu.extractStringValue(dr["PriorityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.PriorityListDescription = dtu.extractStringValue(dr["PriorityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.AAAssignmentID = int.Parse(dtu.extractStringValue(dr["AAAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }

        public long deleteAAAssignment(int aaAssignmentID)
        {
            long result = 0;
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@AAAssignmentID", aaAssignmentID));

            cm.executeCommandBySP("sp_LogSheet_Delete_AAAssignment", paramList, false);

            return result;

        }
        #endregion

        #region TSAssignment
        public ArrayList getTSAssignment()
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_TSAssignment", new ArrayList());
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TSAssignmentID = int.Parse(dtu.extractStringValue(dr["TSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);


                logdetail.AssignedByEmployeeID = int.Parse(dtu.extractStringValue(dr["AssignedPersonID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignedEmployeeName = dtu.extractStringValue(dr["AssignedEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActionItem = int.Parse(dtu.extractStringValue(dr["ActionItem"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Working = int.Parse(dtu.extractStringValue(dr["Working"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getTSAssignmentForQA(int qaByEmployeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@QAByEmployeeID", qaByEmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_TSAssignmentForQA", paramList);

            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TSAssignmentID = int.Parse(dtu.extractStringValue(dr["TSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);


                logdetail.AssignedByEmployeeID = int.Parse(dtu.extractStringValue(dr["AssignedPersonID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignedEmployeeName = dtu.extractStringValue(dr["AssignedEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActionItem = int.Parse(dtu.extractStringValue(dr["ActionItem"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Working = int.Parse(dtu.extractStringValue(dr["Working"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.QAStatus = dtu.extractStringValue(dr["QAStatus"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addTSAssignment(DataModels.LogDetailToolSupport activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@ToolName", activitylog.ToolName));
            paramList.Add(new SqlParameter("@TaskDescription", activitylog.TaskDescription));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.RequestedBy));
            paramList.Add(new SqlParameter("@DateReceived", activitylog.DateReceived));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));
            paramList.Add(new SqlParameter("@RegisteredBy", activitylog.EmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_TSAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TSAssignmentID = int.Parse(dtu.extractStringValue(dr["TSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateTSAssignment(DataModels.LogDetailToolSupport activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@TSAssignmentID", activitylog.TSAssignmentID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@ToolName", activitylog.ToolName));
            paramList.Add(new SqlParameter("@TaskDescription", activitylog.TaskDescription));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.RequestedBy));
            paramList.Add(new SqlParameter("@DateReceived", activitylog.DateReceived));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));
            paramList.Add(new SqlParameter("@UpdatedBy", activitylog.EmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_TSAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailToolSupport logdetail = new DataModels.LogDetailToolSupport();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.ToolName = dtu.extractStringValue(dr["ToolName"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.TSAssignmentID = int.Parse(dtu.extractStringValue(dr["TSAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.RequestedBy = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public long deleteTSAssignment(int tsAssignmentID)
        {
            long result = 0;
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@TSAssignmentID", tsAssignmentID));

            cm.executeCommandBySP("sp_LogSheet_Delete_TSAssignment", paramList, false);

            return result;

        }
        #endregion

        #region EMTAssignment
        public ArrayList getEMTAssignment()
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_EMTAssignment", new ArrayList());
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEMT logdetail = new DataModels.LogDetailEMT();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.EMTAssignmentID = int.Parse(dtu.extractStringValue(dr["EMTAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addEMTAssignment(DataModels.LogDetailEMT activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.Requestor));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));
            paramList.Add(new SqlParameter("@RegisteredBy", activitylog.EmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_EMTAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEMT logdetail = new DataModels.LogDetailEMT();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.EMTAssignmentID = int.Parse(dtu.extractStringValue(dr["EMTAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList updateEMTAssignment(DataModels.LogDetailEMT activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EMTAssignmentID", activitylog.EMTAssignmentID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@ActivityID", activitylog.ActivityID));
            paramList.Add(new SqlParameter("@StoreName", activitylog.StoreName));
            paramList.Add(new SqlParameter("@StoreNumber", activitylog.StoreNumber));
            paramList.Add(new SqlParameter("@RequestedBy", activitylog.Requestor));
            paramList.Add(new SqlParameter("@DateTimeRequested", activitylog.DateTimeRequested));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));
            paramList.Add(new SqlParameter("@UpdatedBy", activitylog.EmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_EMTAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailEMT logdetail = new DataModels.LogDetailEMT();

                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);

                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = dtu.extractStringValue(dr["StatusDescription"], DataTransformerUtility.DataType.STRING);

                logdetail.EMTAssignmentID = int.Parse(dtu.extractStringValue(dr["EMTAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreName = dtu.extractStringValue(dr["StoreName"], DataTransformerUtility.DataType.STRING);
                logdetail.StoreNumber = int.Parse(dtu.extractStringValue(dr["StoreNumber"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Requestor = dtu.extractStringValue(dr["RequestedBy"], DataTransformerUtility.DataType.STRING);
                logdetail.DateTimeRequested = dtu.extractStringValue(dr["DateTimeRequested"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateTimeCompleted = dtu.extractStringValue(dr["DateTimeCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            return al;
        }
        public long deleteEMTAssignment(int emtAssignmentID)
        {
            long result = 0;
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EMTAssignmentID", emtAssignmentID));

            cm.executeCommandBySP("sp_LogSheet_Delete_EMTAssignment", paramList, false);

            return result;

        }
        #endregion

        private void createMeeting(DataModels.LogDetailEFS activitylog, DataModels.Accounts account)
        {
            General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();

            string strUsername = account.Username.ToLower();
            string strPassword = sm.Decrypt(account.Password);
            string strEmailAddress = account.EmailAddress.ToLower();

            ExchangeService xservice = new ExchangeService(ExchangeVersion.Exchange2010, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));

            //xservice.Credentials = new WebCredentials(strUsername, strPassword, "emrsn");
            xservice.Credentials = new WebCredentials(strEmailAddress, strPassword);
            xservice.Url = new Uri("https://outlook.office365.com/EWS/Exchange.asmx");
            //xservice.AutodiscoverUrl(strEmailAddress);
            Appointment newappointment = new Appointment(xservice);
            newappointment.Subject = "EFS Follow Up - " + activitylog.StoreName;
            newappointment.Body = activitylog.Remarks;
            newappointment.Start = DateTime.Parse(activitylog.DateTimeToFollowUp);
            newappointment.End = DateTime.Parse(activitylog.DateTimeToFollowUp).AddHours(1);
            newappointment.Location = "N/A";
            //newappointment.RequiredAttendees.Add("ectgscersefstech@emerson.com");
            newappointment.RequiredAttendees.Add("kent.ferriol@emerson.com");

            newappointment.ReminderMinutesBeforeStart = 15;

            newappointment.Save(SendInvitationsMode.SendToAllAndSaveCopy);
        }

                #region EmailingPart
        // EMAILING PART //
        public void sendLogsheetEFSEmail(string FullName, DateTime ActivityDate, string StoreName, DateTime DateTimeRequested, string Requestor, int WorkOrderNumber, string RequestMade, DateTime DateTimeCompleted, string Remarks, string StatusDescription, string DateTimeToFollowUp)
        {

            string EmailAddress = "DLECTGSCERSAdvancedEngr@Emerson.com";
            string sender = "DLECTGSCERSAdvancedEngr@Emerson.com";

            MailMaster oMail = new MailMaster();
            string eBody = "";
            eBody += "Hi Team, <br>";
            eBody += "<br>";
            eBody += "Please see EFS request log below.";
            eBody += "<br><br>";
            eBody += "<strong>Process Date:</strong> "+ ActivityDate +"";
            eBody += "<br>";
            eBody += "<strong>Store Name:</strong> "+ StoreName +"";
            eBody += "<br>";
            eBody += "<strong>Date Time Requested:</strong> "+ DateTimeRequested +"";
            eBody += "<br>";
            eBody += "<strong>Requestor:</strong> "+ Requestor +"";
            eBody += "<br>";
            eBody += "<strong>Work order number:</strong> "+ WorkOrderNumber +"";
            eBody += "<br>";
            eBody += "<strong>Request made:</strong> "+ RequestMade +"";
            eBody += "<br>";
            eBody += "<strong>Date time completed:</strong> "+ DateTimeCompleted +"";
            eBody += "<br>";
            eBody += "<strong>Remarks:</strong> "+ Remarks +"";
            eBody += "<br>";
            eBody += "<strong>1st touch status:</strong> "+ StatusDescription +"";

            if (StatusDescription == "Completed - Further Action") {
                eBody += "<br>";
                eBody += "<strong>Date time follow-up:</strong> " + DateTimeToFollowUp + "";
            }

            eBody += "<br><br>";
            eBody += ""+ FullName +"";
            eBody += "<br><br>";
            List<string> emailAddress = new List<string>();
            //EmailAddress
            emailAddress.Add(EmailAddress);
            oMail.sendEFSLogMail(sender, eBody, "EFS Log "+StoreName+"");
        }
        #endregion

        #region DailyUploads
        public ArrayList getDUAssignment()
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_DUAssignment", new ArrayList());
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailDailyUploads logdetail = new DataModels.LogDetailDailyUploads();
                logdetail.DUAssignmentID = int.Parse(dtu.extractStringValue(dr["DUAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));

                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = logdetail.StatusID == 0 ? logdetail.StatusDescription = "Pending" : logdetail.StatusDescription = "Completed";
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeID = int.Parse(dtu.extractStringValue(dr["QAByEmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CorrectData = int.Parse(dtu.extractStringValue(dr["CorrectData"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addDUAssignment(DataModels.LogDetailDailyUploads activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@TaskDescription", activitylog.TaskDescription));
            paramList.Add(new SqlParameter("@DateReceived", activitylog.DateReceived));
            paramList.Add(new SqlParameter("@StatusID", activitylog.StatusID));
            paramList.Add(new SqlParameter("@RegisteredBy", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_DUAssignment", paramList);
            //string ticketNumber = dt.Rows[0]["TicketNumber"].ToString();
            //string priorityDescription = dt.Rows[0]["PriorityDescription"].ToString();
            //string descriptionOfTask = dt.Rows[0]["TaskDescription"].ToString();
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailDailyUploads logdetail = new DataModels.LogDetailDailyUploads();

                logdetail.DUAssignmentID = int.Parse(dtu.extractStringValue(dr["DUAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = logdetail.StatusID == 0 ? logdetail.StatusDescription = "Pending" : logdetail.StatusDescription = "Completed";
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeID = int.Parse(dtu.extractStringValue(dr["QAByEmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CorrectData = int.Parse(dtu.extractStringValue(dr["CorrectData"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            //MailMaster mm = new MailMaster();

            ////send email
            //string bodyContent = "<body style='padding: 1em; font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 14;'>";
            //bodyContent += "<p>Hi,</p>";
            //bodyContent += "<p>Thank you for contacting Emerson Cold Chain Analytics.</p>";
            //bodyContent += "<p>This is to inform you that a ticket was created for your request and is now being worked on.</p>";
            //bodyContent += "<p>For your reference:</p>";
            //bodyContent += "<span>Ticket number - <strong>" + ticketNumber + "</strong></span><br/>";
            //bodyContent += "<span>Priority – <strong>" + priorityDescription + "</strong></span><br/>";
            //bodyContent += "<span>Description of task: <strong>" + descriptionOfTask + "</strong></span><br/>";
            //bodyContent += "<p>If you have any further questions, please contact us referencing your ticket number.</p>";
            //bodyContent += "<p><strong>Thank you.</strong></p>";
            //bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>Need Support?</span>";
            //bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>770-425-7430 / US Toll Free 800-829-2724, 3</li></span>";
            //bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>ProActServiceOrders@Emerson.com</li></span>";
            //bodyContent += "<p style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>The information contained in this message is confidential or protected by law. If you are not the intended recipient, please contact the sender and delete this message. Any unauthorized copying of this message or unauthorized distribution of the information contained herein is prohibited.</p>";
            //bodyContent += "</body>";

            //string subject = "Ad Hoc Request Ticket number - " + ticketNumber + "";

            //List<string> eal = new List<string>();
            ////eal.Add(activitylog.EmailAddressofRequestor.ToString());

            //eal.Add("Archieval.Gunhuran@Emerson.com");

            //List<string> ccal = new List<string>();
            ////ccal.Add("Jennylyn.Sevilla@Emerson.com");
            ////ccal.Add("LloydCyrus.Sison@Emerson.com");
            ////ccal.Add("JohnNelson.Salonga@Emerson.com");
            ////ccal.Add("HazelGlo.Bernal@Emerson.com");
            ////ccal.Add("Michelle.Pio@Emerson.com");
            //ccal.Add("Archieval.Gunhuran@Emerson.com");

            //mm.sendMailAdhoc(eal, ccal, bodyContent, subject);

            return al;
        }
        public ArrayList completeStatusDUActivityLog(DataModels.LogDetailDailyUploads activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@DUAssignmentID", activitylog.DUAssignmentID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@TimeSpent", activitylog.TimeSpent));
            paramList.Add(new SqlParameter("@QAByEmployeeID", activitylog.QAByEmployeeID));
            paramList.Add(new SqlParameter("@DateCompleted", activitylog.DateCompleted));
            paramList.Add(new SqlParameter("@Remarks", activitylog.TaskDescription));
            paramList.Add(new SqlParameter("@ActivityDate", activitylog.ActivityDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_DUActivityLogComplete", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailDailyUploads logdetail = new DataModels.LogDetailDailyUploads();

                logdetail.DUAssignmentID = int.Parse(dtu.extractStringValue(dr["DUAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = logdetail.StatusID == 0 ? logdetail.StatusDescription = "Pending" : logdetail.StatusDescription = "Completed";
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeID = int.Parse(dtu.extractStringValue(dr["QAByEmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CorrectData = int.Parse(dtu.extractStringValue(dr["CorrectData"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            MailMaster mm = new MailMaster();

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@ID", activitylog.DUAssignmentID));
            DataTable dtforMail = cm.retrieveDataTableBySP("[sp_LogSheet_Select_DUAssignmentPerID]", parameters);

            string TicketNumber = dtforMail.Rows[0]["TicketNumber"].ToString();
            string AssignEmployeeName = dtforMail.Rows[0]["EmployeeName"].ToString();
            string QAByEmployeeName = dtforMail.Rows[0]["QAByEmployeeName"].ToString();
            string TaskDescription = dtforMail.Rows[0]["TaskDescription"].ToString();
            string EmailAddress = dtforMail.Rows[0]["EmailAddress"].ToString();

            //send email
            string bodyContent = "<body style='padding: 1em; font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 14;'>";
            bodyContent += "<p>Hi,</p>";
            bodyContent += "<p>The ticket number <strong>" + TicketNumber + "</strong> is now assigned to you for quality check.</p>";
            bodyContent += "<p>Here’s the <a href='https://staging.apps.emerson.com/onlinelogsheet/webscreens/OnlineLogSheet/dailyuploadQAForm.aspx'>link</a> to where you can input the QA Scores after checking.</p>";
            bodyContent += "<p>For your reference:</p>";
            bodyContent += "<span>Processor: <strong>" + AssignEmployeeName + "</strong></span><br/>";
            bodyContent += "<span>Assigned QA: <strong>" + QAByEmployeeName + "</strong></span><br/>";
            bodyContent += "<span>Ticket number - <strong>" + TicketNumber + "</strong></span><br/>";
            bodyContent += "<span>Priority - <strong>Medium</strong></span><br/>";
            bodyContent += "<span>Description of task:  <strong>" + TaskDescription + "</strong></span><br/>";
            //bodyContent += "<p>If you have any further questions, please contact us referencing your ticket number.</p>";
            bodyContent += "<p><strong>Thank you.</strong></p>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>Need Support?</span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>770-425-7430 / US Toll Free 800-829-2724, 3</li></span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>ProActServiceOrders@Emerson.com</li></span>";
            bodyContent += "<p style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>The information contained in this message is confidential or protected by law. If you are not the intended recipient, please contact the sender and delete this message. Any unauthorized copying of this message or unauthorized distribution of the information contained herein is prohibited.</p>";
            bodyContent += "</body>";

            string subject = "Daily Uploads: Ticket number - " + TicketNumber.Trim() + " - Quality Check ";

            List<string> eal = new List<string>();
            //eal.Add(EmailAddress);
            //eal.Add("Michelle.Pio@Emerson.com");
            //eal.Add("JohnNelson.Salonga@Emerson.com");
            //if (EmailAddress.ToUpper() == "JOHNNELSON.SALONGA@Emerson.com")
            //{
            //    eal.Remove(eal[2]);
            //}
            eal.Add("Archieval.Gunhuran@Emerson.com");



            List<string> ccal = new List<string>();
            //ccal.Add("Archieval.Gunhuran@Emerson.com");

            //ccal.Add("Michelle.Pio@Emerson.com");
            //ccal.Add("Jennylyn.Sevilla@Emerson.com");
            //ccal.Add("LloydCyrus.Sison@Emerson.com");
            //ccal.Add("JohnNelson.Salonga@Emerson.com");
            //ccal.Add("HazelGlo.Bernal@Emerson.com");
            //ccal.Remove(eal[0]);

            mm.sendMailAdhoc(eal, ccal, bodyContent, subject);

            return al;
        }
        public ArrayList updateDUAssignment(DataModels.LogDetailDailyUploads activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@DUAssignmentID", activitylog.DUAssignmentID));
            paramList.Add(new SqlParameter("@AssignEmployeeID", activitylog.AssignEmployeeID));
            paramList.Add(new SqlParameter("@EmployeeID", activitylog.EmployeeID));
            paramList.Add(new SqlParameter("@TaskDescription", activitylog.TaskDescription));
            paramList.Add(new SqlParameter("@DateReceived", activitylog.DateReceived));
            paramList.Add(new SqlParameter("@DueDate", activitylog.DueDate));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_DUAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailDailyUploads logdetail = new DataModels.LogDetailDailyUploads();

                logdetail.DUAssignmentID = int.Parse(dtu.extractStringValue(dr["DUAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = logdetail.StatusID == 0 ? logdetail.StatusDescription = "Pending" : logdetail.StatusDescription = "Completed";
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeID = int.Parse(dtu.extractStringValue(dr["QAByEmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CorrectData = int.Parse(dtu.extractStringValue(dr["CorrectData"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));
                al.Add(logdetail);
            }

            return al;
        }
        public long deleteDUAssignment(int duAssignmentID)
        {
            long result = 0;
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@DUAssignmentID", duAssignmentID));

            cm.executeCommandBySP("sp_LogSheet_Delete_DUAssignment", paramList, false);

            return result;

        }
        public ArrayList getDUAssignmentForQA(int qaByEmployeeID)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@QAByEmployeeID", qaByEmployeeID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_DUAssignmentForQA", paramList);


            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailDailyUploads logdetail = new DataModels.LogDetailDailyUploads();

                logdetail.DUAssignmentID = int.Parse(dtu.extractStringValue(dr["DUAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = logdetail.StatusID == 0 ? logdetail.StatusDescription = "Pending" : logdetail.StatusDescription = "Completed";
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeID = int.Parse(dtu.extractStringValue(dr["QAByEmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CorrectData = int.Parse(dtu.extractStringValue(dr["CorrectData"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));

                logdetail.QAStatus = dtu.extractStringValue(dr["QAStatus"], DataTransformerUtility.DataType.STRING);

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList addQAForDUAssignments(DataModels.LogDetailDailyUploads activitylog)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@DUAssignmentID", activitylog.DUAssignmentID));
            paramList.Add(new SqlParameter("@NoOfDaysDelayed", activitylog.NoOfDaysDelayed));
            paramList.Add(new SqlParameter("@Timeliness", activitylog.Timeliness));
            paramList.Add(new SqlParameter("@CorrectData", activitylog.CorrectData));
            paramList.Add(new SqlParameter("@Accuracy", activitylog.Accuracy));
            paramList.Add(new SqlParameter("@TotalQAScore", activitylog.TotalQAScore));
            paramList.Add(new SqlParameter("@TimeSpentOfQA", activitylog.TimeSpentOfQA));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_DUAssignmentForQA", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailDailyUploads logdetail = new DataModels.LogDetailDailyUploads();

                logdetail.DUAssignmentID = int.Parse(dtu.extractStringValue(dr["DUAssignmentID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.AssignEmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.TicketNumber = dtu.extractStringValue(dr["TicketNumber"], DataTransformerUtility.DataType.STRING);
                logdetail.TaskDescription = dtu.extractStringValue(dr["TaskDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.DateReceived = DateTime.Parse(dtu.extractStringValue(dr["DateReceived"], DataTransformerUtility.DataType.DATE));
                logdetail.DueDate = DateTime.Parse(dtu.extractStringValue(dr["DueDate"], DataTransformerUtility.DataType.DATE));
                logdetail.DateCompleted = dtu.extractStringValue(dr["DateCompleted"], DataTransformerUtility.DataType.STRING);
                logdetail.SLA = dtu.extractStringValue(dr["SLA"], DataTransformerUtility.DataType.STRING);
                logdetail.StatusID = int.Parse(dtu.extractStringValue(dr["StatusID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.StatusDescription = logdetail.StatusID == 0 ? logdetail.StatusDescription = "Pending" : logdetail.StatusDescription = "Completed";
                logdetail.TimeSpentofProcessor = float.Parse(dtu.extractStringValue(dr["TimeSpentofProcessor"], DataTransformerUtility.DataType.DECIMAL));
                logdetail.QAByEmployeeID = int.Parse(dtu.extractStringValue(dr["QAByEmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.QAByEmployeeName = dtu.extractStringValue(dr["QAByEmployeeName"], DataTransformerUtility.DataType.STRING);
                logdetail.NoOfDaysDelayed = int.Parse(dtu.extractStringValue(dr["NoOfDaysDelayed"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Timeliness = float.Parse(dtu.extractStringValue(dr["Timeliness"], DataTransformerUtility.DataType.NUMBER));
                logdetail.CorrectData = int.Parse(dtu.extractStringValue(dr["CorrectData"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Accuracy = float.Parse(dtu.extractStringValue(dr["Accuracy"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TotalQAScore = float.Parse(dtu.extractStringValue(dr["TotalQAScore"], DataTransformerUtility.DataType.NUMBER));
                logdetail.TimeSpentOfQA = float.Parse(dtu.extractStringValue(dr["TimeSpentOfQA"], DataTransformerUtility.DataType.DECIMAL));

                //logdetail.QAStatus = dtu.extractStringValue(dr["QAStatus"], DataTransformerUtility.DataType.STRING);
                al.Add(logdetail);
            }

            MailMaster mm = new MailMaster();

            //send email
            string bodyContent = "<body style='padding: 1em; font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 14;'>";
            bodyContent += "<p>Hi,</p>";
            bodyContent += "<p>This is to inform you that the quality check for ticket number <strong>" + activitylog.TicketNumber + "</strong>is now complete.</p>";
            //bodyContent += "<p>You may view your QA Score in this <a href='http://proact.emerson.com/onlinelogsheet/webscreens/OnlineLogSheet/dailyupload.aspx'>link</a>.</p>";
            bodyContent += "<p>You may view your QA Score in this <a href='https://staging.apps.emerson.com/onlinelogsheet/webscreens/OnlineLogSheet/dailyupload.aspx'>link</a>.</p>";
            bodyContent += "<p>For your reference:</p>";
            bodyContent += "<span>Processor Time Spent: <strong>" + activitylog.TimeSpentofProcessor + "</strong></span><br/>";
            bodyContent += "<span>Assigned QA Time Spent: <strong>" + activitylog.TimeSpentOfQA + "</strong></span><br/>";
            bodyContent += "<p><strong>Thank you.</strong></p>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>Need Support?</span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>770-425-7430 / US Toll Free 800-829-2724, 3</li></span>";
            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>ProActServiceOrders@Emerson.com</li></span>";
            bodyContent += "<p style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>The information contained in this message is confidential or protected by law. If you are not the intended recipient, please contact the sender and delete this message. Any unauthorized copying of this message or unauthorized distribution of the information contained herein is prohibited.</p>";
            bodyContent += "</body>";

            string subject = "Daily Uploads Ticket number - " + activitylog.TicketNumber.Trim() + " - Quality Check Completed";

            List<string> eal = new List<string>();
            //eal.Add("Jennylyn.Sevilla@Emerson.com");
            //eal.Add("LloydCyrus.Sison@Emerson.com");
            //eal.Add("JohnNelson.Salonga@Emerson.com");
            //eal.Add("HazelGlo.Bernal@Emerson.com");
            eal.Add("Archieval.Gunhuran@Emerson.com");

            List<string> ccal = new List<string>();
            //ccal.Add("Michelle.Pio@Emerson.com");
            // ccal.Add("Archieval.Gunhuran@Emerson.com");

            mm.sendMailAdhoc(eal, ccal, bodyContent, subject);

            return al;
        }
        #endregion

    }
}
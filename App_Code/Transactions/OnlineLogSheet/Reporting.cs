﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for Reporting
/// </summary>
namespace Transactions{
public class Reporting
{
	public Reporting() { }

    public ArrayList reportPerStandardTime(int activityTypeID, int selectedFY)
    {
        ArrayList al = new ArrayList();
        DataTransformerUtility dtu = new DataTransformerUtility();
        ArrayList paramlist = new ArrayList();
        ArrayList paramList = new ArrayList();
        ConnectionMaster cm = new ConnectionMaster();

        paramlist.Add(new SqlParameter("@ActivityTypeID", activityTypeID));
        paramlist.Add(new SqlParameter("@SelectedFY", selectedFY));

        DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Report_PerTimeSpent", paramlist);
        foreach (DataRow dr in dt.Rows)
        {
            DataModels.LogSheetReport report = new DataModels.LogSheetReport();
            report.GroupNumber = int.Parse(dtu.extractStringValue(dr["GROUPNUMBER"], DataTransformerUtility.DataType.NUMBER));
            report.DateID = int.Parse(dtu.extractStringValue(dr["ID"], DataTransformerUtility.DataType.NUMBER));
            report.LabelDate = dtu.extractStringValue(dr["LABELDATE"], DataTransformerUtility.DataType.NUMBER);
            report.ReportHours = double.Parse(dtu.extractStringValue(dr["HOURS"], DataTransformerUtility.DataType.STRING));
            al.Add(report);
        }
        return al;
    }

    public ArrayList reportPerVolume(int activityTypeID, int selectedFY)
    {
        ArrayList al = new ArrayList();
        DataTransformerUtility dtu = new DataTransformerUtility();
        ArrayList paramlist = new ArrayList();
        ArrayList paramList = new ArrayList();
        ConnectionMaster cm = new ConnectionMaster();

        paramlist.Add(new SqlParameter("@ActivityTypeID", activityTypeID));
        paramlist.Add(new SqlParameter("@SelectedFY", selectedFY));

        DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Report_PerVolume", paramlist);
        foreach (DataRow dr in dt.Rows)
        {
            DataModels.LogSheetReport report = new DataModels.LogSheetReport();
            report.GroupNumber = int.Parse(dtu.extractStringValue(dr["GROUPNUMBER"], DataTransformerUtility.DataType.NUMBER));
            report.DateID = int.Parse(dtu.extractStringValue(dr["ID"], DataTransformerUtility.DataType.NUMBER));
            report.LabelDate = dtu.extractStringValue(dr["LABELDATE"], DataTransformerUtility.DataType.NUMBER);
            report.ReportVolume = int.Parse(dtu.extractStringValue(dr["VOLUME"], DataTransformerUtility.DataType.STRING));
            al.Add(report);
        }
        return al;
    }

    public ArrayList reportPieDonutChart(int activityTypeID, int selectedFY)
    {
        ArrayList al = new ArrayList();
        DataTransformerUtility dtu = new DataTransformerUtility();
        ArrayList paramlist = new ArrayList();
        ArrayList paramList = new ArrayList();
        ConnectionMaster cm = new ConnectionMaster();

        paramlist.Add(new SqlParameter("@ActivityTypeID", activityTypeID));
        paramlist.Add(new SqlParameter("@SelectedFY", selectedFY));

        DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Report_PieDonutChart", paramlist);
        foreach (DataRow dr in dt.Rows)
        {
            DataModels.LogSheetReport report = new DataModels.LogSheetReport();
            report.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
            report.Percentage = double.Parse(dtu.extractStringValue(dr["Percentage"], DataTransformerUtility.DataType.NUMBER));
            al.Add(report);
        }
        return al;
    }

    public ArrayList reportUtilization(int selectedFY)
    {
        ArrayList al = new ArrayList();
        DataTransformerUtility dtu = new DataTransformerUtility();
        ArrayList paramlist = new ArrayList();
        ConnectionMaster cm = new ConnectionMaster();

        paramlist.Add(new SqlParameter("@SelectedFY", selectedFY));

        DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Report_Utilization", paramlist);
        foreach (DataRow dr in dt.Rows)
        {
            DataModels.LogSheetUtilization utilization = new DataModels.LogSheetUtilization();
            utilization.CalMonthNo = int.Parse(dtu.extractStringValue(dr["CalMonthNo"], DataTransformerUtility.DataType.NUMBER));
            utilization.CalMonthName = dtu.extractStringValue(dr["MonthName"], DataTransformerUtility.DataType.STRING);
            utilization.ExpectedWorkingDays = float.Parse(dtu.extractStringValue(dr["ExpectedWorkingDays"], DataTransformerUtility.DataType.NUMBER));
            utilization.NumberOfLeaves = float.Parse(dtu.extractStringValue(dr["NumberOfLeaves"], DataTransformerUtility.DataType.NUMBER));
            utilization.EFSActualTime = double.Parse(dtu.extractStringValue(dr["EFSActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.CSSActualTime = double.Parse(dtu.extractStringValue(dr["CSSActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.ALRActualTime = double.Parse(dtu.extractStringValue(dr["ALRActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.FQRActualTime = double.Parse(dtu.extractStringValue(dr["FQRActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.SPMActualTime = double.Parse(dtu.extractStringValue(dr["SPMActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.EMTActualTime = double.Parse(dtu.extractStringValue(dr["EMTActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.ESSActualTime = double.Parse(dtu.extractStringValue(dr["ESSActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.AlarmActualTime = double.Parse(dtu.extractStringValue(dr["AlarmActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.ProjectActualTime = double.Parse(dtu.extractStringValue(dr["ProjectActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.AdhocAnalyticsActualTime = double.Parse(dtu.extractStringValue(dr["AdhocAnalyticsActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.ToolSupportActualTime = double.Parse(dtu.extractStringValue(dr["ToolSupportActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.TrainingActualTime = double.Parse(dtu.extractStringValue(dr["TrainingActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.NPTActualTime = double.Parse(dtu.extractStringValue(dr["NPTActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.EFSStandardTime = double.Parse(dtu.extractStringValue(dr["EFSStandardTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.CSSStandardTime = double.Parse(dtu.extractStringValue(dr["CSSStandardTime"], DataTransformerUtility.DataType.NUMBER));

            al.Add(utilization);
        }
        return al;
    }

    public ArrayList reportUtilizationPerEmployee(int selectedCY, int selectedCMonth)
    {
        ArrayList al = new ArrayList();
        DataTransformerUtility dtu = new DataTransformerUtility();
        ArrayList paramlist = new ArrayList();
        ConnectionMaster cm = new ConnectionMaster();

        paramlist.Add(new SqlParameter("@CY", selectedCY));
        paramlist.Add(new SqlParameter("@Month", selectedCMonth));

        DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Report_UtilizationPerEmployee", paramlist);
        foreach (DataRow dr in dt.Rows)
        {
            DataModels.LogSheetUtilization utilization = new DataModels.LogSheetUtilization();
            utilization.EmployeeNames = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
            utilization.ExpectedWorkingDays = float.Parse(dtu.extractStringValue(dr["ExpectedWorkingDays"], DataTransformerUtility.DataType.NUMBER));
            utilization.NumberOfLeaves = float.Parse(dtu.extractStringValue(dr["NumberOfLeaves"], DataTransformerUtility.DataType.NUMBER));
            utilization.EFSActualTime = double.Parse(dtu.extractStringValue(dr["EFSActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.CSSActualTime = double.Parse(dtu.extractStringValue(dr["CSSActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.ALRActualTime = double.Parse(dtu.extractStringValue(dr["ALRActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.FQRActualTime = double.Parse(dtu.extractStringValue(dr["FQRActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.SPMActualTime = double.Parse(dtu.extractStringValue(dr["SPMActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.EMTActualTime = double.Parse(dtu.extractStringValue(dr["EMTActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.ESSActualTime = double.Parse(dtu.extractStringValue(dr["ESSActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.AlarmActualTime = double.Parse(dtu.extractStringValue(dr["AlarmActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.ProjectActualTime = double.Parse(dtu.extractStringValue(dr["ProjectActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.AdhocAnalyticsActualTime = double.Parse(dtu.extractStringValue(dr["AdhocAnalyticsActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.ToolSupportActualTime = double.Parse(dtu.extractStringValue(dr["ToolSupportActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.TrainingActualTime = double.Parse(dtu.extractStringValue(dr["TrainingActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.NPTActualTime = double.Parse(dtu.extractStringValue(dr["NPTActualTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.EFSStandardTime = double.Parse(dtu.extractStringValue(dr["EFSStandardTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.CSSStandardTime = double.Parse(dtu.extractStringValue(dr["CSSStandardTime"], DataTransformerUtility.DataType.NUMBER));
            utilization.AdminProdActualTime = double.Parse(dtu.extractStringValue(dr["AdminProductiveActualTime"], DataTransformerUtility.DataType.NUMBER));

            al.Add(utilization);
        }
        return al;
    }
}
}
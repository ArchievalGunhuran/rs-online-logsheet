﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
//using Microsoft.Exchange.WebServices.Data;
using System.DirectoryServices;
using System.Net;
using System.Net.Mail;

namespace Transactions
{
    public class ActivityLogs2
    {
        public ActivityLogs2()
        {
        }

        public ArrayList getActivityTypeList(string userGroup, int employeeID, bool isManual)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@UserGroup", userGroup));
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@IsManual", isManual));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_AssignedActivityTypeList", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }

        public ArrayList getActivityList(int activityTypeID, int employeeID, bool isManual)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activityTypeID));
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@IsManual", isManual));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ActivityList_CSQADB", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }

        public string getEmployeeUserGroup(int EmployeeID)
        {
            string usergroup= "";
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            paramList.Add(new SqlParameter("@EmployeeID", EmployeeID));
            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_GroupAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                usergroup = dtu.extractStringValue(dr["UserGroup"], DataTransformerUtility.DataType.STRING);
            }
            return usergroup;
        }

        public ArrayList getEmployeeList()
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_OnlinelogSheet_Employee", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }

        public ArrayList getActivityLog(int employeeID, string activityDate, bool ismanual)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));
            paramList.Add(new SqlParameter("@IsManual", ismanual));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_QADBCSActivityLog", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailQADBCS logdetail = new DataModels.LogDetailQADBCS();

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeStatus = dtu.extractStringValue(dr["TimeStatus"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentString"], DataTransformerUtility.DataType.STRING);

                al.Add(logdetail);
            }

            return al;
        }
        public ArrayList getActivityLogCompleted(int employeeID, string activityDate, bool ismanual)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", employeeID));
            paramList.Add(new SqlParameter("@ActivityDate", activityDate));
            paramList.Add(new SqlParameter("@IsManual", ismanual));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_QADBCSActivityLogCompleted", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailQADBCS logdetail = new DataModels.LogDetailQADBCS();

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeStatus = dtu.extractStringValue(dr["TimeStatus"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentString"], DataTransformerUtility.DataType.STRING);

                al.Add(logdetail);
            }

            return al;
        }

        public List<DataModels.LogDetailQADBCS> checkActivityLogForTheDay(int EmployeeID, string ActivityDate)
        {
            List<DataModels.LogDetailQADBCS> list = new List<DataModels.LogDetailQADBCS>();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@EmployeeID", EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", ActivityDate));


            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_QADBCSActivityLog", paramList);
            //[sp_LogSheet_ActivityChecker]
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailQADBCS logdetail = new DataModels.LogDetailQADBCS();

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeStatus = dtu.extractStringValue(dr["TimeStatus"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentString"], DataTransformerUtility.DataType.STRING);

                list.Add(logdetail);
            }

            return list;
        }

        public List<DataModels.LogDetailQADBCS> updateActivityLog(int EmployeeID, string ActivityDate, DataTable ActivityLog, bool IsManual)
        {
            ArrayList al = new ArrayList();
            List<DataModels.LogDetailQADBCS> qaDbCsList = new List<DataModels.LogDetailQADBCS>();

            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", ActivityDate));
            paramList.Add(new SqlParameter("@QADBCSLogSheetData", ActivityLog));
            paramList.Add(new SqlParameter("@IsManual", IsManual));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_QADBCSActivityLog", paramList);

            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailQADBCS logdetail = new DataModels.LogDetailQADBCS();

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeStatus = dtu.extractStringValue(dr["TimeStatus"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentString"], DataTransformerUtility.DataType.STRING);

                qaDbCsList.Add(logdetail);
            }

   
            return qaDbCsList;
        }

        public bool autoOnPauseActivity(int EmployeeID, string ActivityDate, DataTable ActivityLog, DataTable AutoPause)
        {
            bool isSuccess = false;
            var getCurrentOngoingID = GetListByDataTable(ActivityLog).FirstOrDefault().ActivityLogID; //Expects only 1 Data
            var listAutoPause = GetListByDataTable(AutoPause); //Expects more than 1 data


            try
            {
                List<DataModels.ActivityLogsParam> getListToAutoPause = listAutoPause.Where(i => i.ActivityLogID != getCurrentOngoingID).ToList();

                foreach (var item in getListToAutoPause)
                {
                    DataTable dt = ToDataTable(getListToAutoPause.Where(i => i.ActivityLogID == item.ActivityLogID).ToList());

                    updateActivityLog(EmployeeID, ActivityDate, dt, false);
                }

                isSuccess = true;
            }
            catch(Exception ex)
            {
                isSuccess = false;
            }

            return isSuccess;
        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            System.Reflection.PropertyInfo[] Props = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            foreach (System.Reflection.PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        private List<DataModels.ActivityLogsParam> GetListByDataTable(DataTable dt)
        {
            //objAuto.ActivityLogID = datasourceAll[i].ActivityLogID == "" ? 0 : datasourceAll[i].ActivityLogID;
            //objAuto.LogDetailID = datasourceAll[i].LogDetailID == "" ? 0 : datasourceAll[i].LogDetailID;
            //objAuto.ActivityTypeID = datasourceAll[i].ActivityTypeID;
            //objAuto.EmployeeID = Employee_ID;
            //objAuto.ActivityDate = jQuery("#txtActivityDate").val();//datasourceAll[i].ActivityDate;
            //objAuto.ActivityID = datasourceAll[i].ActivityID;
            //objAuto.TimeSpent = actualTimeSpent; //0; //datasourceAll[i].TimeSpent;
            //objAuto.NumberOfTransaction = datasourceAll[i].NumberOfTransaction;
            //objAuto.Remarks = datasourceAll[i].Remarks;
            //objAuto.TimeStatus = "On-pause";
            //objAuto.TimeSpentStr = strtime;

            List<DataModels.ActivityLogsParam> reult = (from rw in dt.AsEnumerable()
                         select new DataModels.ActivityLogsParam
                         {
                             ActivityLogID = Convert.ToInt32(rw["ActivityLogID"]),
                             LogDetailID = Convert.ToInt32(rw["LogDetailID"]),
                             ActivityTypeID = Convert.ToInt32(rw["ActivityTypeID"]),
                             EmployeeID = Convert.ToInt32(rw["EmployeeID"]),
                             ActivityDate = Convert.ToDateTime(rw["ActivityDate"].ToString()),
                             ActivityID = Convert.ToInt32(rw["ActivityID"]),
                             TimeSpent = rw["TimeSpent"] != DBNull.Value ? (float)Convert.ToDouble(rw["TimeSpent"]) : 0,
                             NumberOfTransaction = Convert.ToInt32(rw["NumberOfTransaction"]),
                             Remarks = rw["Remarks"].ToString(),
                             TimeStatus = rw["TimeStatus"].ToString(),
                             TimeSpentStr = rw["TimeSpentStr"].ToString()
                         }).ToList();

            return reult;
        }

        public ArrayList updateActivityLogManual(int EmployeeID, string ActivityDate, DataTable ActivityLog, bool IsManual, string UserGroup, string EmployeeName)
        {
            ArrayList logsCompleted = new ArrayList();

            if (ActivityLog.Rows.Count > 0)
            {
                foreach (DataRow drw in ActivityLog.Rows)
                {
                    string timeStatus = drw["TimeStatus"].ToString();
                    if (timeStatus == "Completed")
                    {
                        logsCompleted = getActivityLogCompleted(EmployeeID, ActivityDate, IsManual);
                        if(logsCompleted.Count > 0)
                        {
                         // 
                        }
                        else
                        {
                            // Send Email Notification
                            MailMaster mm = new MailMaster();

                            //send email
                            string bodyContent = "<body style='padding: 1em; font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 14;'>";
                            bodyContent += "<p>Hi,</p>";
                            bodyContent += "<p>"+ EmployeeName + " submitted Manual Logs Dated " + ActivityDate + ".</p>";
                            bodyContent += "<p>Please check and approve as needed.</p>";
                            bodyContent += "<p><strong>Thank you,</strong></p>";
                            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>Need Support?</span>";
                            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>770-425-7430 / US Toll Free 800-829-2724, 3</li></span>";
                            bodyContent += "<span style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'><li>ProActServiceOrders@Emerson.com</li></span>";
                            bodyContent += "<p style='padding: 1em; font-family: Arial, Calibri, Helvetica, sans-serif; font-size: 11; color:#808080;'>The information contained in this message is confidential or protected by law. If you are not the intended recipient, please contact the sender and delete this message. Any unauthorized copying of this message or unauthorized distribution of the information contained herein is prohibited.</p>";
                            bodyContent += "</body>";

                            string subject = "Kindly check Online Activity Logger";

                            List<string> eal = new List<string>();

                            //QA - Mami Che, Jen
                            //Leads - TL's (Jec, Dee, Mahdi, Tsong Vic)
                            //DB - Mami Che, Kuya Peter
                            //AA - Mami Che, Jen

                            switch (UserGroup)
                            {
                                case "QA":
                                    //eal.Add("Archieval.Gunhuran@Emerson.com");
                                    eal.Add("Michelle.Pio@Emerson.com");
                                    eal.Add("JennyLyn.Hikong@Emerson.com");
                                    break;
                                case "Leads":
                                    //eal.Add("Archieval.Gunhuran@Emerson.com");
                                    eal.Add("Teodoro.SamonteJr@Emerson.com");
                                    eal.Add("Adora.Francisco@Emerson.com");
                                    eal.Add("Mahdi.Chichane@Emerson.com");
                                    eal.Add("VictorMartin.Advento@Emerson.com");
                                    break;
                                case "DB":
                                    //eal.Add("Archieval.Gunhuran@Emerson.com");
                                    eal.Add("Michelle.Pio@Emerson.com");
                                    eal.Add("PeterMarkus.Sioson@Emerson.com");
                                    break;
                                case "AES":
                                    //eal.Add("Archieval.Gunhuran@Emerson.com");
                                    eal.Add("Michelle.Pio@Emerson.com");
                                    eal.Add("JennyLyn.Hikong@Emerson.com");
                                    break;
                                default:
                                    eal.Add("Archieval.Gunhuran@Emerson.com");
                                    //eal.Add("Michelle.Pio@Emerson.com");
                                    //eal.Add("Jennylyn.Sevilla@Emerson.com");
                                    break;
                            }
                            //eal.Add("Jennylyn.Sevilla@Emerson.com");
                            //eal.Add("LloydCyrus.Sison@Emerson.com");
                            //eal.Add("JohnNelson.Salonga@Emerson.com");
                            //eal.Add("HazelGlo.Bernal@Emerson.com");
                            

                            //List<string> ccal = new List<string>();
                            //ccal.Add("Michelle.Pio@Emerson.com");
                            // ccal.Add("Archieval.Gunhuran@Emerson.com");

                            mm.sendMailLogsheetManual(eal, bodyContent, subject);
                        }
                    }
                }
            }

            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@EmployeeID", EmployeeID));
            paramList.Add(new SqlParameter("@ActivityDate", ActivityDate));
            paramList.Add(new SqlParameter("@QADBCSLogSheetData", ActivityLog));
            paramList.Add(new SqlParameter("@IsManual", IsManual));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_QADBCSActivityLog", paramList);

            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailQADBCS logdetail = new DataModels.LogDetailQADBCS();

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeStatus = dtu.extractStringValue(dr["TimeStatus"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentString"], DataTransformerUtility.DataType.STRING);

                al.Add(logdetail);
            }

            return al;
        }

        public long deleteActivityLog(DataTable ActivityLog, int activitylogID)
        {
            long result = 0;
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityLogID", activitylogID));
            if (ActivityLog.Rows.Count > 0)
            {
                paramList.Add(new SqlParameter("@QADBCSLogSheetData", ActivityLog));
                cm.executeCommandBySP("sp_LogSheet_Delete_QADBCSActivityLog", paramList, false);
            }
            else
            {
                cm.executeCommandBySP("sp_LogSheet_Delete_OneQADBCSActivityLog", paramList, false);
            }

            return result;
        }



        #region ActivityLogsView
        public ArrayList getActivityLogView(string activityDateFrom, string activityDateTo)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityDateFrom", activityDateFrom));
            paramList.Add(new SqlParameter("@ActivityDateTo", activityDateTo));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_QADBCSActivityLogsView", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.LogDetailQADBCS logdetail = new DataModels.LogDetailQADBCS();

                logdetail.ActivityLogID = int.Parse(dtu.extractStringValue(dr["ActivityLogID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.LogDetailID = int.Parse(dtu.extractStringValue(dr["LogDetailID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDate = dtu.extractStringValue(dr["ActivityDate"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                logdetail.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                logdetail.NumberOfTransaction = int.Parse(dtu.extractStringValue(dr["NumberOfTransaction"], DataTransformerUtility.DataType.NUMBER));
                logdetail.Remarks = dtu.extractStringValue(dr["Remarks"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeStatus = dtu.extractStringValue(dr["TimeStatus"], DataTransformerUtility.DataType.STRING);
                logdetail.TimeSpentStr = dtu.extractStringValue(dr["TimeSpentString"], DataTransformerUtility.DataType.STRING);

                logdetail.FullName = dtu.extractStringValue(dr["FullName"], DataTransformerUtility.DataType.STRING);

                al.Add(logdetail);
            }

            return al;
        }
        #endregion
    }
}
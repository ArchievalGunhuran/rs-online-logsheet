﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
//using Microsoft.Exchange.WebServices.Data;
using System.DirectoryServices;

namespace Transactions
{
    public class ActivityList
    {
        public ActivityList()
        {
        }

        public ArrayList getActivityTypeList(string usergroup)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@UserGroup", usergroup));
            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ActivityTypeList2", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }

        public ArrayList getActivityMaintenance(DataModels.ActivityList activitylist)
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylist.ActivityTypeID));
            paramList.Add(new SqlParameter("@ActivityDescription", activitylist.ActivityDescription));
            paramList.Add(new SqlParameter("@StandardTime", activitylist.StandardTime));
            paramList.Add(new SqlParameter("@ActivityID", activitylist.ActivityID));
            paramList.Add(new SqlParameter("@HaveST", activitylist.HaveST));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ActivityMaintenance", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                activitylist = new DataModels.ActivityList();

                activitylist.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.IsActive = bool.Parse(dtu.extractStringValue(dr["IsActive"], DataTransformerUtility.DataType.BOOLEAN));
                activitylist.HaveST = bool.Parse(dtu.extractStringValue(dr["HaveST"], DataTransformerUtility.DataType.BOOLEAN));
                activitylist.StandardTime = float.Parse(dtu.extractStringValue(dr["StandardTime"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivitySTID = int.Parse(dtu.extractStringValue(dr["ActivitySTID"], DataTransformerUtility.DataType.NUMBER));

                al.Add(activitylist);
            }

            return al;
        }
        public ArrayList getActivityMaintenanceTypes(DataModels.ActivityTypes activitylist)
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylist.ActivityTypeID));
            paramList.Add(new SqlParameter("@ActivityDescription", activitylist.ActivityTypeDescription));
            paramList.Add(new SqlParameter("@UserGroup", activitylist.UserGroup));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_ActivityMaintenanceTypes", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                activitylist = new DataModels.ActivityTypes();            
                activitylist.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.UserGroup = dtu.extractStringValue(dr["UserGroup"], DataTransformerUtility.DataType.STRING);
                al.Add(activitylist);
            }

            return al;
        }
        public ArrayList addActivityMaintenance(DataModels.ActivityList activitylist)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylist.ActivityTypeID));
            paramList.Add(new SqlParameter("@ActivityDescription", activitylist.ActivityDescription));
            paramList.Add(new SqlParameter("@IsActive", activitylist.IsActive));
            paramList.Add(new SqlParameter("@HaveST", activitylist.HaveST));
            paramList.Add(new SqlParameter("@StandardTime", activitylist.StandardTime));
            paramList.Add(new SqlParameter("@EmployeeID", activitylist.EmployeeID));

            list = getActivityMaintenance(activitylist);

            if (list.Count > 0)
                throw new Exception("Record is alrady existing in the database.");

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_ActivityMaintenance", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                activitylist = new DataModels.ActivityList();

                activitylist.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.IsActive = bool.Parse(dtu.extractStringValue(dr["IsActive"], DataTransformerUtility.DataType.BOOLEAN));
                activitylist.HaveST = bool.Parse(dtu.extractStringValue(dr["HaveST"], DataTransformerUtility.DataType.BOOLEAN));
                activitylist.StandardTime = float.Parse(dtu.extractStringValue(dr["StandardTime"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivitySTID = int.Parse(dtu.extractStringValue(dr["ActivitySTID"], DataTransformerUtility.DataType.NUMBER));

                al.Add(activitylist);
            }

            return al;
        }
        public ArrayList addActivityMaintenanceTypes(DataModels.ActivityTypes activitylist)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylist.ActivityTypeID));
            paramList.Add(new SqlParameter("@ActivityDescription", activitylist.ActivityTypeDescription));
            paramList.Add(new SqlParameter("@UserGroup", activitylist.UserGroup));

            list = getActivityMaintenanceTypes(activitylist);

            if (list.Count > 0)
                throw new Exception("Record is alrady existing in the database.");

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_ActivityMaintenanceTypes", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                activitylist = new DataModels.ActivityTypes();

                activitylist.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.UserGroup = dtu.extractStringValue(dr["UserGroup"], DataTransformerUtility.DataType.STRING);

                al.Add(activitylist);
            }

            return al;
        }

        public ArrayList updateActivityMaintenance(DataModels.ActivityList activitylist)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityID", activitylist.ActivityID));
            paramList.Add(new SqlParameter("@ActivitySTID", activitylist.ActivitySTID));
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylist.ActivityTypeID));
            paramList.Add(new SqlParameter("@ActivityDescription", activitylist.ActivityDescription));
            paramList.Add(new SqlParameter("@IsActive", activitylist.IsActive));
            paramList.Add(new SqlParameter("@HaveST", activitylist.HaveST));
            paramList.Add(new SqlParameter("@StandardTime", activitylist.StandardTime));
            paramList.Add(new SqlParameter("@EmployeeID", activitylist.EmployeeID));


            list = getActivityMaintenance(activitylist);

            if (list.Count > 0)
                throw new Exception("Record is alrady existing in the database.");

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_ActivityMaintenance", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                activitylist = new DataModels.ActivityList();

                activitylist.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityTypeDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivityDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.ActivityID = int.Parse(dtu.extractStringValue(dr["ActivityID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.IsActive = bool.Parse(dtu.extractStringValue(dr["IsActive"], DataTransformerUtility.DataType.BOOLEAN));
                activitylist.HaveST = bool.Parse(dtu.extractStringValue(dr["HaveST"], DataTransformerUtility.DataType.BOOLEAN));
                activitylist.StandardTime = float.Parse(dtu.extractStringValue(dr["StandardTime"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivitySTID = int.Parse(dtu.extractStringValue(dr["ActivitySTID"], DataTransformerUtility.DataType.NUMBER));

                al.Add(activitylist);
            }

            return al;
        }

        public ArrayList updateActivityMaintenanceTypes(DataModels.ActivityTypes activitylist)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivityTypeID", activitylist.ActivityTypeID));
            paramList.Add(new SqlParameter("@ActivityDescription", activitylist.ActivityTypeDescription));
            paramList.Add(new SqlParameter("@UserGroup", activitylist.UserGroup));


            list = getActivityMaintenanceTypes(activitylist);

            if (list.Count > 0)
                throw new Exception("Record is alrady existing in the database.");

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_ActivityMaintenanceTypes", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                activitylist = new DataModels.ActivityTypes();

                activitylist.ActivityTypeID = int.Parse(dtu.extractStringValue(dr["ActivityTypeID"], DataTransformerUtility.DataType.NUMBER));
                activitylist.ActivityTypeDescription = dtu.extractStringValue(dr["ActivityDescription"], DataTransformerUtility.DataType.STRING);
                activitylist.UserGroup = dtu.extractStringValue(dr["UserGroup"], DataTransformerUtility.DataType.STRING);

                al.Add(activitylist);
            }

            return al;
        }

        public ArrayList getEmployeeList()
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_TSEList", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists list = new DataModels.Lists();
                list.ListID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                list.ListDescription = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                al.Add(list);
            }

            return al;
        }

        public ArrayList getEmployeeAssignment(DataModels.ActivityLevelAssignment alas)
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@ActivitySTID", alas.ActivitySTID));
            paramList.Add(new SqlParameter("@EmployeeID", alas.EmployeeID));
            paramList.Add(new SqlParameter("@ALAssignmentID", alas.ALAssignID));

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Select_EmployeeAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.ActivityLevelAssignment ala = new DataModels.ActivityLevelAssignment();

                ala.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                ala.EmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                ala.EffectiveDate = DateTime.Parse(dtu.extractStringValue(dr["EffectiveDate"], DataTransformerUtility.DataType.NUMBER));
                ala.IsActive = bool.Parse(dtu.extractStringValue(dr["IsActive"], DataTransformerUtility.DataType.BOOLEAN));
                ala.ALAssignID = int.Parse(dtu.extractStringValue(dr["ALAssignmentID"], DataTransformerUtility.DataType.NUMBER));

                al.Add(ala);
            }

            return al;
        }

        public ArrayList addEmployeeAssignment(DataModels.ActivityLevelAssignment ala)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivitySTID", ala.ActivitySTID));
            paramList.Add(new SqlParameter("@EmployeeID", ala.EmployeeID));
            paramList.Add(new SqlParameter("@EffectiveDate", ala.EffectiveDate));
            paramList.Add(new SqlParameter("@UserEmployeeID", ala.UserEmployeeID));
            paramList.Add(new SqlParameter("@IsActive", ala.IsActive));

            list = getEmployeeAssignment(ala);

            if (list.Count > 0)
                throw new Exception("Record is alrady existing in the database.");
            
            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Insert_EmployeeAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists employeeList = new DataModels.Lists();

                ala.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                ala.EmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                ala.EffectiveDate = DateTime.Parse(dtu.extractStringValue(dr["EffectiveDate"], DataTransformerUtility.DataType.NUMBER));
                ala.IsActive = bool.Parse(dtu.extractStringValue(dr["IsActive"], DataTransformerUtility.DataType.BOOLEAN));
                ala.ALAssignID = int.Parse(dtu.extractStringValue(dr["ALAssignmentID"], DataTransformerUtility.DataType.NUMBER));

                al.Add(ala);
            }

            return al;
        }


        public ArrayList updateEmployeeAssignment(DataModels.ActivityLevelAssignment ala)
        {
            ArrayList al = new ArrayList();
            ArrayList paramList = new ArrayList();
            ArrayList list = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();
            paramList.Add(new SqlParameter("@ActivitySTID", ala.ActivitySTID));
            paramList.Add(new SqlParameter("@EmployeeID", ala.EmployeeID));
            paramList.Add(new SqlParameter("@EffectiveDate", ala.EffectiveDate));
            paramList.Add(new SqlParameter("@UserEmployeeID", ala.UserEmployeeID));
            paramList.Add(new SqlParameter("@IsActive", ala.IsActive));
            paramList.Add(new SqlParameter("@ALAssignmentID", ala.ALAssignID));

            list = getEmployeeAssignment(ala);

            if (list.Count > 0)
                throw new Exception("Record is alrady existing in the database.");

            DataTable dt = cm.retrieveDataTableBySP("sp_LogSheet_Update_EmployeeAssignment", paramList);
            foreach (DataRow dr in dt.Rows)
            {
                DataModels.Lists employeeList = new DataModels.Lists();

                ala.EmployeeID = int.Parse(dtu.extractStringValue(dr["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                ala.EmployeeName = dtu.extractStringValue(dr["EmployeeName"], DataTransformerUtility.DataType.STRING);
                ala.EffectiveDate = DateTime.Parse(dtu.extractStringValue(dr["EffectiveDate"], DataTransformerUtility.DataType.NUMBER));
                ala.IsActive = bool.Parse(dtu.extractStringValue(dr["IsActive"], DataTransformerUtility.DataType.BOOLEAN));
                ala.ALAssignID = int.Parse(dtu.extractStringValue(dr["ALAssignmentID"], DataTransformerUtility.DataType.NUMBER));

                al.Add(ala);
            }

            return al;
        }
    }
}
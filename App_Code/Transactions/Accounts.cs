﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;

namespace Transactions
{
    public class Accounts
    {
        public Accounts()
        {
        }

        public DataModels.Accounts getUserDetailsForSession(int _employeeID)
        {
            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();
            DataTransformerUtility dtu = new DataTransformerUtility();

            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@EmployeeID", _employeeID));
            DataTable dt = cm.retrieveDataTableBySP("dbo.getUserDetails", paramList);
            DataModels.Accounts a = new DataModels.Accounts();

            if (dt.Rows.Count > 0)
            {
                a.AccountID = int.Parse(dtu.extractStringValue(dt.Rows[0]["AccountID"], DataTransformerUtility.DataType.PRIMARYKEY));
                a.EmpID = int.Parse(dtu.extractStringValue(dt.Rows[0]["EmployeeID"], DataTransformerUtility.DataType.NUMBER));
                a.Username = dtu.extractStringValue(dt.Rows[0]["Username"], DataTransformerUtility.DataType.STRING);
                a.EmailAddress = dtu.extractStringValue(dt.Rows[0]["EmailAddress"], DataTransformerUtility.DataType.STRING);
                a.EmployeeName = dtu.extractStringValue(dt.Rows[0]["EmployeeName"], DataTransformerUtility.DataType.STRING);
                a.RoleName = dtu.extractStringValue(dt.Rows[0]["RoleName"], DataTransformerUtility.DataType.STRING);
                a.AlarmCenter = dtu.extractStringValue(dt.Rows[0]["AlarmCenter"], DataTransformerUtility.DataType.STRING);
                a.PermissionName = dtu.extractStringValue(dt.Rows[0]["PermissionDescription"], DataTransformerUtility.DataType.STRING);
                a.LogSheetUserGroup = dtu.extractStringValue(dt.Rows[0]["UserGroup"], DataTransformerUtility.DataType.STRING);
            }

            return a;
        }

        public DataModels.Accounts isUserValid(string _username, string _password)
        {
            General.AvianCrypto.SecurityMaster sm = new General.AvianCrypto.SecurityMaster();
            DataModels.Accounts accounts = new DataModels.Accounts();
            accounts.Username = _username.ToLower();
            accounts.Password = sm.Encrypt(_password); //encrypted
            accounts.Active = false;

            DirectoryEntry de = new DirectoryEntry("LDAP://DC=emrsn,DC=org", @"emrsn\" + _username, _password);
            DirectorySearcher ds = new DirectorySearcher(de);
            ds.Filter = "(SAMAccountName=" + _username + ")";
            ds.PropertiesToLoad.Add("cn");
            SearchResult sr = ds.FindOne();
            
            if (sr != null)
                accounts.Active = true;

            return accounts;
        }

        public bool hasEFSTechPermission(int _employeeID)
        {

            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();

            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@EmployeeID", _employeeID));
            DataTable dt = cm.retrieveDataTableBySP("dbo.checkEmployeeEFSTechPremission", paramList);
            DataModels.Accounts a = new DataModels.Accounts();

            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

        public bool hasCSAssignmentPermission(int _employeeID)
        {

            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();

            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@EmployeeID", _employeeID));
            DataTable dt = cm.retrieveDataTableBySP("dbo.checkEmployeeCSAssignmentPremission", paramList);
            DataModels.Accounts a = new DataModels.Accounts();

            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }

        public bool hasOnlineLogSheetPermission(int _employeeID)
        {

            ArrayList al = new ArrayList();
            ConnectionMaster cm = new ConnectionMaster();

            ArrayList paramList = new ArrayList();
            paramList.Add(new SqlParameter("@EmployeeID", _employeeID));
            DataTable dt = cm.retrieveDataTableBySP("dbo.checkEmployeeOnlineLogSheetPremission", paramList);
            DataModels.Accounts a = new DataModels.Accounts();

            if (dt.Rows.Count > 0)
                return true;
            else
                return false;
        }
    }
}
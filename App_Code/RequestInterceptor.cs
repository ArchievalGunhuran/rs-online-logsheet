﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Security.Principal;
using System.Diagnostics;
using System.Configuration;

/// <summary>
/// Summary description for RequestInterceptor
/// </summary>
public class RequestInterceptor : IHttpModule
{
    private string baseurl = "";
    private HttpApplication httpApp;
    public void Init(HttpApplication httpApp)
    {
        this.httpApp = httpApp;
        this.httpApp.BeginRequest += new EventHandler(this.OnBeginRequest);
        this.httpApp.EndRequest += new EventHandler(this.OnEndRequest);
        this.httpApp.PreRequestHandlerExecute += new EventHandler(this.PreRequestHandlerExecute);

    }
    public void OnBeginRequest(object o, EventArgs args)
    {
        this.baseurl = this.GETBASEURL();
        string fromurl = this.httpApp.Request.Url.AbsoluteUri;
        HttpCookieCollection hcc = this.httpApp.Request.Cookies;
    }
    public void OnEndRequest(object o, EventArgs args) { }
    public void PreRequestHandlerExecute(object sender, EventArgs e)
    {
        if (isPageRequest() == true)
        {
            if (getModuleName().Equals("LOGIN") == false
                && getModuleName().Equals("DEFAULT") == false
                && getModuleName().Equals("ITEMSPECIFICATION") == false
                && getModuleName().Equals("SEARCH") == false
                && getModuleName().Equals("REGISTRATION") == false
                && getModuleName().Equals("CAPTCHAGENERATOR") == false
                && getModuleName().Equals("FORGOTPASSWORD") == false)
            {
                bool redirecttologin = false;
                if (this.httpApp.Context.Session != null)
                {
                    if (this.httpApp.Context.Session.IsNewSession)
                        redirecttologin = true;

                    string temp = (string)this.httpApp.Context.Session["LoginInformation"];
                    if (temp == null)
                        redirecttologin = true;

                    if (temp != null)
                    {
                        if (temp.Length <= 0)
                            redirecttologin = true;
                    }
                }
                else
                    redirecttologin = true;

                 //if (redirecttologin == true)
                   //this.httpApp.Response.Redirect(this.baseurl + "login.aspx");
                //this.httpApp.Response.Redirect(this.baseurl + "login.aspx?returnURL=" + getModuleName().ToLower() + ".aspx");
            }
        }
    }
    public void Dispose() { }
    private string getModuleName()
    {
        string moduleName = "";
        string[] segments = this.httpApp.Request.Url.Segments;

        string accessedFile = segments[segments.Length - 1];
        string[] filenameSegments = accessedFile.Split(".".ToCharArray());
        string extension = filenameSegments[filenameSegments.Length - 1].ToUpper();

        if (extension.Equals("ASPX") == true)
        {
            string pagename = filenameSegments[0].ToUpper();
            moduleName = pagename;
        }
        return moduleName.ToUpper();
    }
    private bool isPageRequest()
    {
        string[] segments = this.httpApp.Request.Url.Segments;

        string accessedFile = segments[segments.Length - 1];
        string[] filenameSegments = accessedFile.Split(".".ToCharArray());
        string extension = filenameSegments[filenameSegments.Length - 1].ToUpper();

        if ((extension.Equals("ASPX") == true) || (extension.Equals("ASHX") == true))
            return true;
        else
            return false;
    }
    private string GETBASEURL()
    {
        string burl = httpApp.Request.Url.GetLeftPart(UriPartial.Authority) + httpApp.Request.ApplicationPath;
        string origurl = httpApp.Request.Url.AbsoluteUri;
        if (burl.EndsWith("/", StringComparison.CurrentCultureIgnoreCase) == false)
            burl += "/";
        return burl;
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Web;
using System.Net.Mail;
using System.Configuration;


public class MailMaster
{
    private MailAddress from = new MailAddress("RetailSolutions_Automailer@emerson.com", "Retail Solutions Automailer");
    //private MailAddress to = new MailAddress("Jean.Ang@emerson.com", "Jean Ang");
    //private MailAddress to1 = new MailAddress("Patrick.SanLorenzo@emerson.com", "Patrick San Lorenzo");
    //private MailAddress to2 = new MailAddress("Arnel.Aves@emerson.com", "Arnel Aves");
    //private string host = "inetmail.emrsn.net";
    //private string host = "mail.asiapartnerweb.emersonclimate.com";


    //private string host = "localhost";
    private string host = ConfigurationManager.AppSettings["SMTPServerIP"].ToString();
    public MailMaster() { }
    public void sendMail(string _emailaddress, string _message, string _subject)
    {
        string body = _message;
        SmtpClient client = new SmtpClient();
        client.Host = host;
        client.Port = 25;
        MailMessage message = new MailMessage();
        //message.Bcc.Add(new "Joemar.Ecaro@Emerson.com; Kent.Ferriol@Emerson.com;");


        //  message.Bcc.Add(new MailAddress("Joemar.Ecaro@emerson.com"));
        //   message.Bcc.Add(new MailAddress("LloydCyrus.Sison@emerson.com"));


        //message.Bcc.Add(new MailAddress("Kent.Ferriol@emerson.com"));

        //Attachment item = new Attachment(HttpContext.Current.Server.MapPath("..\\.\\images\\favicon.png"));
        //message.Attachments.Add(item)

        message.From = from;
        if (_emailaddress != "")
            message.To.Add(new MailAddress(_emailaddress));
        //message.To.Add(_emailaddress);
        message.Subject = _subject;
        message.IsBodyHtml = true;
        message.Body = body;

        client.Send(message);
    }

    public void sendMail(List<string> _emailaddress, string _message, string _subject)
    {
        string body = _message;
        SmtpClient client = new SmtpClient();
        client.Host = host;
        client.Port = 25;
        MailMessage message = new MailMessage();
        //message.Bcc.Add(new "Joemar.Ecaro@Emerson.com; Kent.Ferriol@Emerson.com;");

        //    message.Bcc.Add(new MailAddress("Kristine.Calsado@emerson.com"));
        //   message.Bcc.Add(new MailAddress("LloydCyrus.Sison@emerson.com"));


        //message.Bcc.Add(new MailAddress("Kent.Ferriol@emerson.com"));

        //Attachment item = new Attachment(HttpContext.Current.Server.MapPath("..\\.\\images\\favicon.png"));
        //message.Attachments.Add(item)

        message.From = from;
        for (int i = 0; i < _emailaddress.Count; i++)
            message.To.Add(new MailAddress(_emailaddress[i]));
        message.CC.Add(new MailAddress("Kristine.Calsado@emerson.com"));
        message.CC.Add(new MailAddress("LloydCyrus.Sison@emerson.com"));
        //message.CC.Add(new MailAddress("HallenCedrick.Singca@Emerson.com"));
        message.Subject = _subject;
        message.IsBodyHtml = true;
        message.Body = body;

        client.Send(message);
    }
    public void sendMailAdhoc(List<string> _emailaddress, List<string> _ccaddress, string _message, string _subject)
    {
        string body = _message;
        SmtpClient client = new SmtpClient();
        client.Host = host;
        client.Port = 25;
        MailMessage message = new MailMessage();

        message.From = from;
        for (int i = 0; i < _emailaddress.Count; i++)
            message.To.Add(new MailAddress(_emailaddress[i]));

        for (int i = 0; i < _ccaddress.Count; i++)
            message.CC.Add(new MailAddress(_ccaddress[i]));

        //message.Bcc.Add(new MailAddress("DLMProactServiceCenter@Emerson.com"));
        message.Subject = _subject;
        message.IsBodyHtml = true;
        message.Body = body;

        client.Send(message);
    }

    public void sendMailLogsheetManual(List<string> _emailaddress, string _message, string _subject)
    {
        string body = _message;
        SmtpClient client = new SmtpClient();
        client.Host = host;
        client.Port = 25;
        MailMessage message = new MailMessage();

        message.From = from;
        for (int i = 0; i < _emailaddress.Count; i++)
            message.To.Add(new MailAddress(_emailaddress[i]));

        //message.Bcc.Add(new MailAddress("DLMProactServiceCenter@Emerson.com"));
        message.Subject = _subject;
        message.IsBodyHtml = true;
        message.Body = body;

        client.Send(message);
    }

    public void sendMailLeaveModule(string _emailaddress, string _message, string _subject, string _leadmessage, int status)
    {
        string body = _message;
        SmtpClient client = new SmtpClient();
        client.Host = host;
        client.Port = 25;
        MailMessage message = new MailMessage();

        //message.CC.Add(new MailAddress("HallenCedrick.Singca@Emerson.com"));
        message.From = from;
        if (_emailaddress != "")
            message.To.Add(new MailAddress(_emailaddress));
        message.Subject = _subject;
        message.IsBodyHtml = true;
        message.Body = body;
        client.Send(message);

        if (status == 1 || status == 2 || status == 3)
        {
            string lbody = _leadmessage;
            SmtpClient lclient = new SmtpClient();
            lclient.Host = host;
            lclient.Port = 25;
            MailMessage lmessage = new MailMessage();
            lmessage.From = from;
            // Leads Email Add
            lmessage.To.Add(new MailAddress("DLECTGSCERSShiftLead@Emerson.com"));
            //lmessage.To.Add(new MailAddress("HallenCedrick.Singca@Emerson.com")); 
            //lmessage.To.Add(new MailAddress("Jennylyn.Sevilla@Emerson.com")); 
            //
            lmessage.Subject = _subject;
            lmessage.IsBodyHtml = true;
            lmessage.Body = lbody;
            lclient.Send(lmessage);
        }
    }


    public void sendEFSLogMail(string _emailaddress, string _message, string _subject)
    {
        string body = _message;
        SmtpClient client = new SmtpClient();
        client.Host = host;
        client.Port = 25;
        MailMessage message = new MailMessage();
        //message.To.Add(new MailAddress("LloydCyrus.Sison@emerson.com"));
        //message.To.Add(new MailAddress("HallenCedrick.Singca@Emerson.com"));
        //message.To.Add(new MailAddress("Jennylyn.Sevilla@Emerson.com"));
        message.From = from;
        if (_emailaddress != "")
            message.To.Add(new MailAddress(_emailaddress));
        //message.To.Add(_emailaddress);
        message.Subject = _subject;
        message.IsBodyHtml = true;
        message.Body = body;

        client.Send(message);
    }

}